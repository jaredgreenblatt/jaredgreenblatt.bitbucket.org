
import static java.lang.Math.random;
import java.util.Random;
import java.util.Scanner;

/*
 *
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class RockPaperScissor {

    public static void main(String[] args) {
        // Variable for number of game rounds
        int gameRounds;
        //setting game rounds = to the user input. Input being handled by method.
        gameRounds = validateGameRoundInput("How Many Rounds Would You Like to Play? ");
        //using boolean to check if the input is valid between 1/10 
        boolean validationChecker = validateInputs(gameRounds);
        // if the input is correct it will run the play game method.
        if (validationChecker == true) {

            playGame(gameRounds);

        } //if the input is invalid it will tell use they inputed an incorrect
        // value and quit the game.
        else if (validationChecker == false) {
            System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
            System.out.println("ERROR You inputed a number outside of 1-10."
                    + " please input a number from the correct range when "
                    + "you play again. ");
            System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
           return;

        }

    }
   

    public static int validateGameRoundInput(String prompt) {

        // Reads in the values for the gameroundint.
        //Declere a a  new Scanner
        int intVal=0;
        Scanner sc = new Scanner(System.in);
        // Print Prompt
        System.out.println(prompt);
        /* Read Value Into String  but because we are returning an int i need to
        make sure my value is in int. So if it is not an int it will print an 
        error message and exit the game. 8675309 is the error code it will return
        if it is not an int. The will allow the main method to execute the return 
        to exit the program on non integers being used for the game round inputs.
        -1 return means error. So when it goes back to the main it will fail because
        it is  not in the range from 1-10.*/
        
        if (!(sc.hasNextInt())) {
            /* System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
            System.out.println("ERROR You inputed a number outside of 1-10."
                    + " please input a number from the correct range when "
                    + "you play again. ");
            System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
           return;*/
            
            return intVal = -1;
            

        }
        String input = sc.nextLine();
        // Converts String to Int
        intVal = Integer.parseInt(input);

        
        //returns the int
        return intVal;

    }

    public static String screenInputReader(String prompt) {
        //reads in the rock papper scissor value.
        //Declere a a  new Scanner
        Scanner sc = new Scanner(System.in);
        // Print Prompt
        System.out.println(prompt);
        // Read Value Into String

        String input = sc.nextLine();
        //making input all caps.
        input = input.toUpperCase();

        //returns the input
        return input;
    }

    public static void playGame(int turns) {

        //Play game Method. This is used throughout the game.
        String playersChoice = "";
        int playersChoiceValue = 0;
        Random computerValue = new Random();
        int pcTurn;
        int ties = 0;
        int pcWins = 0;
        int humanWins = 0;
        boolean userInputCheck;
        int rounds = 0;

        //Game While loop that will continue to play until turns remaining = 0
        while (turns > 0) {
            System.out.println("Rock Paper Scissor....");

            //Getting the 
            playersChoice = screenInputReader("Shoot");

            //validating user inputs with a method. 
            //This is checking if the input is either ROCK PAPER Or SCISSORS.
            userInputCheck = validateInputs(playersChoice);

            /* This boolean will return false if
            the user put in an incorrect input  if  they do have an incorrect
            input the user will be asked to resubmit there input this will 
            be done until the user enters in rock paper scissor shoot for that
            round. */
            while (userInputCheck == false) {

                System.out.println("Please Enter either ROCK, PAPER or SCISSORS!");
                System.out.println("_____________________________________________");
                System.out.println("_____________________________________________");

                System.out.println("Rock Paper Scissor....");
                // Calling Method that is used to get the players input.
                playersChoice = screenInputReader("Shoot");
                
                //Using Method to validate Input.
                
                if (validateInputs(playersChoice) == true) {
                    userInputCheck = true;
                }

            }
            /* If the user inputs evaluate as true  then the game logic will begin.*/
            if (userInputCheck == true) {
                if (playersChoice.equals("ROCK")) {
                    //rock =1  
                    playersChoiceValue = 1;

                } else if (playersChoice.equals("PAPER")) {
                    //paper =2
                    playersChoiceValue = 2;

                } else if (playersChoice.equals("SCISSOR")) {
                    //Scissor =3
                    playersChoiceValue = 3;

                }

            }
            /*Math Random that will return 1 2 or 3. This lines up with  the 
            convereted values of the user inputs*/

            pcTurn = computerValue.nextInt(3) + 1;
            if (pcTurn == 1) {
                System.out.println("The computer chose Rock.");

            } else if (pcTurn == 2) {
                System.out.println("The computer chose Paper.");
            } else if (pcTurn == 3) {
                System.out.println("The Computer chose Scissors.");
            }

            /* Logic below handles the evaluation between the user input and
            the random selections made by the computer it will evaluate if the 
            human wins loses or ties based off the rules of the rock paper
            scissor game.*/
            if (playersChoiceValue == pcTurn) {
                //Ties so we add 
                System.out.println("__________________________________________");
                System.out.println("You Tied this round.");
                ties++;
            } else if (playersChoiceValue == 1 && pcTurn == 2) {
                //Rock loses to paper. human loses pc+1
                System.out.println("__________________________________________");
                System.out.println("You Lost this Round. Paper Covers Rock!");
                pcWins++;

            } else if (playersChoiceValue == 1 && pcTurn == 3) {
                //Rock beats Scissors. human wins  human+1
                System.out.println("__________________________________________");
                System.out.println("You Won this round. Rock Crushes Scissors! ");
                humanWins++;
            } else if (playersChoiceValue == 2 && pcTurn == 1) {
                //Paper Beats Rock. Human wins +1.
                System.out.println("__________________________________________");
                System.out.println("You Won this Round. Paper Covers Rock!");
                humanWins++;
            } else if (playersChoiceValue == 2 && pcTurn == 3) {
                //Paper loses to scissors Pc wins +1
                System.out.println("__________________________________________");
                System.out.println("You Lost this Round. Scissors Cut Paper!");
                pcWins++;

            } else if (playersChoiceValue == 3 && pcTurn == 1) {
                //Scissor loses to rock PC wins +1
                System.out.println("__________________________________________");
                System.out.println("You Lost this round. Rock Crushes Scissors!");
                pcWins++;

            } else if (playersChoiceValue == 3 && pcTurn == 2) {
                //Scissor Beat Paper human wins +1
                System.out.println("__________________________________________");
                System.out.println("You Won this Round with Scissors Cuts Paper!");
                humanWins++;

            }
            // subtract 1 from turns so that the loop will end once the turns are over.
            turns--;
            // counts the rounds played up this is just used for the round by round output.
            rounds++;

            // round output to console for use to track round.
            System.out.println("_____________________________________________");
            System.out.println("After round " + rounds + " the Score is.");
            System.out.println("User Wins: " + humanWins);
            System.out.println("Computer Wins: " + pcWins);
            System.out.println("Ties: " + ties);
            System.out.println("You have " + turns + " turns left");
            System.out.println("_____________________________________________");

        }
        //Calling method to print end game results.
        printResults(pcWins, humanWins, ties, rounds);
        //calling method to play again. 
        playAgain(turns);

    }

    
    public static boolean validateInputs(int turns) {
        //Method used to validate that the rounds input is 1-10.
        boolean x;
        if (turns > 0 && turns <= 10) {
            x = true;

        } else {
            x = false;
        }

        return x;
    }

    public static boolean validateInputs(String input) {
        //Method used to check in the in game inputs are Rock Paper Scissor or Shoot!
        boolean x;
        if (input.equals("ROCK") || input.equals("PAPER") || input.equals("SCISSOR")) {
            x = true;

        } else {
            x = false;
        }

        return x;
    }

    public static void printResults(int computerWins, int userWins, int draws, int rounds) {

        // Reads in results and then prints them to the console.
        System.out.println("At the end of   " + rounds + " games the final score is ");
        System.out.println("You won " + userWins + " rounds!");
        System.out.println("The PC won " + computerWins + " rounds.");
        System.out.println("You tied " + draws + " rounds.");

        //this if elseif statement is checking to see if the PC or User won or tied
        if (userWins > computerWins) {
            System.out.println("");
            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            System.out.println("");
            System.out.println("You beat the Computer!");
            System.out.println("");
            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

        } else if (computerWins > userWins) {
            System.out.println("");
            System.out.println("----------------------------------------------");
            System.out.println("");
            System.out.println("You lost to the computer");
            System.out.println("");
            System.out.println("----------------------------------------------");
            System.out.println("");
        } else {
            System.out.println("");
            System.out.println("----------------------------------------------");
            System.out.println("");
            System.out.println("You tied with the computer.");
            System.out.println("");
            System.out.println("----------------------------------------------");
            System.out.println("");
        }

    }

    public static void playAgain(int turns) {

        String yesOrNo;
        int playAgain;
     

        yesOrNo = screenInputReader("Would You Like to Play Again? YES or NO?");

        if (yesOrNo.equals("YES")) {
            // Variable for number of game rounds
            int gameRounds;
            //prompts for number of rounds to be played
            gameRounds = validateGameRoundInput("How Many Rounds Would You Like to Play? ");
            //validates inputs are from 1-10
            boolean validationChecker = validateInputs(gameRounds);
            // if they are the game will be played.
            if (validationChecker == true) {

                playGame(gameRounds);

            }//if false the game will not be played.
            else if (validationChecker == false) {
                System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
                System.out.println("ERROR You inputed a number outside of 1-10."
                        + " please input a number from the correct range when "
                        + "you play again. ");
                System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

            }

        } else if (yesOrNo.equals("NO")) {
            System.out.println("Thanks for Playing!");
            return;

        }

    }
}
 