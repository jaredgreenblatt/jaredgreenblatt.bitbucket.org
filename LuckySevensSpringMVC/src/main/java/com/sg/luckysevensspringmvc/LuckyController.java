/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.luckysevensspringmvc;

import java.util.Map;
import java.util.Random;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class LuckyController {

    @RequestMapping(value = "playGame", method = RequestMethod.POST)
    public String playGame(HttpServletRequest request, Map<String, Object> model) {

        String input = request.getParameter("initialBetAmount");
        int originalBet = Integer.parseInt(input);
        int bigWin = originalBet;

        int highRoll = 1;
        int dice1;
        int dice2;
        int diceSum;
        int rollCount = 0;

        Random dice = new Random();

        int currentBet = originalBet;
        while (currentBet >= 1) {
            dice1 = dice.nextInt(6) + 1;
            dice2 = dice.nextInt(6) + 1;
            diceSum = dice1 + dice2;

            if (diceSum == 7) {
                currentBet += 4;
                rollCount++;
                if (currentBet > bigWin) {
                    bigWin = currentBet;
                    highRoll = rollCount;

                }

            } else {
                currentBet -= 1;
                rollCount++;

            }

        }
        model.put("originalBet", originalBet);
        model.put("rollCount", rollCount);
        model.put("highRoll", highRoll);
        model.put("bigWin", bigWin);

        return "result";
    }
}
