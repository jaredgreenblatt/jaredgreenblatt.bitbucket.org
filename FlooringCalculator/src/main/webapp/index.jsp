<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Flooring Calculator</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
    </head>
    <body>
        <div class="container">
            <h1>Flooring Calculator</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                	<li role="presentation" class="active"><a href="${pageContext.request.contextPath}/index.jsp">Home</a></li>
                	<li role="presentation"><a href="${pageContext.request.contextPath}/hello/sayhi">Hello Controller</a></li>
                </ul>    
            </div>
            <h2>Enter Information to Do Flooring Calculations.</h2>
                <p>
            Flooring Calculator used to calculate the cost of flooring for specified Area.
        </p>
        <p>
            The team can install flooring at a rate of 20 sq feet per hour. 

        </p>
        <p>
            The company bills every 15 minutes at a rate of 86 dollars an hour.
        </p>

        <form method="post" action="FlooringCalculater">
            <p> Width Of Area to Floor</p>
            <input type="text" name ="widthFlooringArea"/><br/>
             <p> Length Of Area to Floor</p>
            <input type="text" name ="lengthFlooringArea"/><br/>
             <p> Cost Per 1 Sq Foot Of Material.</p>
            <input type="text" name ="costPer1SquareFootOfMaterial"/><br/>

            <br/>
            <input type ="submit" value ="Calculate Flooring Cost"/>
        </form> 
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

