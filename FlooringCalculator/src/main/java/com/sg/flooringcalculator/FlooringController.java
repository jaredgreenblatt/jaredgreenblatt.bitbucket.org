/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringcalculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class FlooringController {

    @RequestMapping(value = "FlooringCalculater", method = RequestMethod.POST)
    public String FlooringCalculater(HttpServletRequest request, Map<String, Object> model) {
        BigDecimal laborCost = new BigDecimal(86.00);
        BigDecimal installPerHour = new BigDecimal(20.00);
        String inputWidth = request.getParameter("widthFlooringArea");
        String inputLength = request.getParameter("lengthFlooringArea");
        String inputMaterialCostPerSquareFoot = request.getParameter("costPer1SquareFootOfMaterial");
        BigDecimal width = new BigDecimal(inputWidth);
        BigDecimal length = new BigDecimal(inputLength);
        BigDecimal materialCostPerSquareFoot = new BigDecimal(inputMaterialCostPerSquareFoot);
        BigDecimal materialCost, area, timeToInstall, totalLaborCost, totalCost;
        area = length.multiply(width);
        materialCost = materialCostPerSquareFoot.multiply(area);
        materialCost = materialCost.setScale(2, RoundingMode.HALF_DOWN);

        // Area to cover divided 20 to calculate how long it takes to install
        timeToInstall = area.divide(installPerHour, 2, RoundingMode.HALF_DOWN);
        timeToInstall= timeToInstall.multiply(new BigDecimal(4));
        timeToInstall = timeToInstall.setScale(2, RoundingMode.CEILING);
        timeToInstall= timeToInstall.divide(new BigDecimal(4), 1, RoundingMode.CEILING);
        totalLaborCost = timeToInstall.multiply(laborCost);
        totalLaborCost = totalLaborCost.setScale(2, RoundingMode.HALF_DOWN);

        totalCost = materialCost.add(totalLaborCost);
        totalCost = totalCost.setScale(2, RoundingMode.HALF_DOWN);

        model.put("materialCost", materialCost);
        model.put("laborCost", totalLaborCost);
        model.put("timeToInstall", timeToInstall);
        model.put("totalCost", totalCost);

        return "result";
    }
}
