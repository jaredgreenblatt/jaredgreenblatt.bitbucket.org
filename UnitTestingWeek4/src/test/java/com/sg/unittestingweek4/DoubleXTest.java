/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.unittestingweek4;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class DoubleXTest {

    public DoubleXTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of doubleX method, of class DoubleX.
     */
    DoubleX xx = new DoubleX();

    @Test
    public void testDoubleaxxbbX() {
        assertTrue(xx.doubleX("axxbb"));

    }

    @Test
    public void testDoubleaxaxxax() {
        assertFalse(xx.doubleX("axaxxax"));

    }
     @Test
    public void testDoublexxxxx() {
        assertTrue(xx.doubleX("xxxxx"));

    }
    
      @Test
    public void testDoubleallregthanXX() {
        assertTrue(xx.doubleX("fojkwjohfjjfvjfdjlvljfdkvljfdlj"
                + "vhlfvhlfdlvlhdfvlhfdhjvljefljvfljvjlfejljddjdjdjdjdjjdxx"));

    }
       @Test
    public void testDoubleallregthanX() {
        assertFalse(xx.doubleX("fojkwjohfjjfvjfdkvljfdlj"
                + "vhlfvhlfdlvlhdfvlhfdhjjvfljvjlfexjljddjdjdjdjdjjd"));

    }

}

