/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.unittestingweek4;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class SpeedingTest {
    
    public SpeedingTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of caughtSpeeding method, of class Speeding.
     */
    Speeding speed = new Speeding();
    
    @Test
    public void testCaughtSpeeding60andNotBday() {
        
        int result  = speed.caughtSpeeding(60, false);
        assertEquals(result, 0);
    }
    
     @Test
    public void testCaughtSpeeding65andBday() {
        
        int result  = speed.caughtSpeeding(65, true);
        assertEquals(result, 0);
    }
        @Test
    public void testCaughtSpeeding65andNotBday() {
        
        int result  = speed.caughtSpeeding(65, false);
        assertEquals(result, 1);
    }
    
     @Test
    public void testCaughtSpeeding70andBday() {
        
        int result  = speed.caughtSpeeding(70, true);
        assertEquals(result, 1);
    }
    
           @Test
    public void testCaughtSpeeding81andNotBday() {
        
        int result  = speed.caughtSpeeding(81, false);
        assertEquals(result, 2);
    }
    
     @Test
    public void testCaughtSpeeding86andBday() {
        
        int result  = speed.caughtSpeeding(86, true);
        assertEquals(result, 2);
    }
    
    @Test
    public void testCaughtSpeeding77andBday() {
        
        int result  = speed.caughtSpeeding(77, true);
        assertEquals(result, 1);
    }
    
    
    
}
