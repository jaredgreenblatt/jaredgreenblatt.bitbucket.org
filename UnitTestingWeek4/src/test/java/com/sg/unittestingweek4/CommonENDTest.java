/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.unittestingweek4;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class CommonENDTest {

    public CommonENDTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of commonEnd method, of class CommonEND.
     */
    // commonEnd({1, 2, 3}, {7, 3}) -> true
    @Test
    public void testCommonEnd1() {
        CommonEND ce = new CommonEND();

        int[] a = {1, 2, 3};
        int[] b = {7, 3};

        assertTrue(ce.commonEnd(a, b));
    }

    // commonEnd({1, 2, 3}, {7, 3, 2}) -> false
    @Test
    public void testCommonEnd2() {
        CommonEND ce = new CommonEND();

        int[] a = {1, 2, 3};
        int[] b = {7, 3, 2};

        assertFalse(ce.commonEnd(a, b));
    }

    // commonEnd({1, 2, 3}, {7, 3, 2}) -> false
    @Test
    public void testCommonEnd3() {
        CommonEND ce = new CommonEND();

        int[] a = {1, 2, 3};
        int[] b = {1, 3};

        assertTrue(ce.commonEnd(a, b));
    }
    
    @Test
    public void testCommonEnd4() {
        CommonEND ce = new CommonEND();

        int[] a = {1, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3};
        int[] b = {1, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2};

        assertTrue(ce.commonEnd(a, b));
    }
    @Test
     public void testCommonEnd5() {
        CommonEND ce = new CommonEND();

        int[] a = { 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3};
        int[] b = {1, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2, 3, 2};

        assertFalse(ce.commonEnd(a, b));
    }

}
