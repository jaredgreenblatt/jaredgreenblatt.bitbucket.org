/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.unittestingweek4;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class SubstringTest {
    
    public SubstringTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of insertWord method, of class Substring.
     */
    
    Substring sub = new Substring();
    
    @Test
    public void testInsertWord1() {
        
        assertEquals(sub.insertWord("<<>>", "Yay"), "<<Yay>>");
    }
    
     @Test
    public void testInsertWord2() {
        
        assertEquals(sub.insertWord("<<>>", "WooHoo"), "<<WooHoo>>");
    }
    
    
       @Test
    public void testInsertWord3() {
        
        assertEquals(sub.insertWord("[[]]", "Word"), "[[Word]]");
    }
    
    @Test
     public void testInsertJared() {
        
        assertEquals(sub.insertWord("jaed", "r"), "jared");
    }
    
    
}
