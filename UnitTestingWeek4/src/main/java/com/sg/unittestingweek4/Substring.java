/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.unittestingweek4;

/**
 *
 * @author apprentice
 */
public class Substring {
        // Given an "out" String length 4, such as "<<>>", and a 
    // word, return a new String where the word is in the middle 
    // of the out String, e.g. "<<word>>".
    //
    // Hint: SubStrings are your friend here 
    //
    // insertWord("<<>>", "Yay") -> "<<Yay>>"
    // insertWord("<<>>", "WooHoo") -> "<<WooHoo>>"
    // insertWord("[[]]", "word") -> "[[word]]"
    public String insertWord(String container, String word) {
        String cont = container;
        String wrd = word;
        String result = "";
        
         result = cont.substring(0, 2);
         result = result+wrd;
         result = result+cont.substring(2, 4);
        
        
        
        return result;
        
    }
    
}
