/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.service;

import com.sg.vendingmachine.dao.VendingMachinePersistenceException;
import com.sg.vendingmachine.dto.Change;
import com.sg.vendingmachine.dto.VendingItem;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author Jared Greenblatt
 */
public interface VendingMachineServiceLayer {

    BigDecimal runningTotal = new BigDecimal("0.00");

    void createVendingItem(VendingItem vendingItem) throws
            VendingMachineDataValidationException,
            VendingMachinePersistenceException;
    
    void editVendingItem(VendingItem vendingItem) throws
            VendingMachineDataValidationException,
            VendingMachinePersistenceException;
    

    List<VendingItem> getAllVendingItem() throws
            VendingMachinePersistenceException;

    VendingItem getVendingItem(String itemId) throws
            VendingMachinePersistenceException,
            ItemDoesNotExistException;

    VendingItem removeVendingItem(String itemId) throws
            VendingMachinePersistenceException;

    VendingItem PurchaseItem(VendingItem vendingItem) throws
            NoItemInventoryException,
            InsufficientFundsException,
            VendingMachinePersistenceException;
    

    Change makeChange();

    void Open() throws VendingMachinePersistenceException;

    void Close() throws VendingMachinePersistenceException;

    public BigDecimal getrunningTotal();

    public void setrunningTotal(BigDecimal runningTotal);

}
