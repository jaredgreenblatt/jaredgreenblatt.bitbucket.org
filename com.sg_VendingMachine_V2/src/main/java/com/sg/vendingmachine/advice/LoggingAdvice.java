/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.advice;

import com.sg.vendingmachine.dao.VendingMachineAuditDao;
import com.sg.vendingmachine.dao.VendingMachinePersistenceException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.aspectj.lang.JoinPoint;

/**
 *
 * @author Jared Greenblatt
 */
public class LoggingAdvice {

    VendingMachineAuditDao auditDao;

    public LoggingAdvice(VendingMachineAuditDao auditDao) {
        this.auditDao = auditDao;
    }

    public void createAuditEntry(JoinPoint jp) {
        Object[] args = jp.getArgs();
        String auditEntry = jp.getSignature().getName() + ": ";
        for (Object currentArg : args) {
            auditEntry += currentArg;
        }
        try {
            auditDao.writeAuditEntry(auditEntry);
        } catch (VendingMachinePersistenceException e) {
            System.err.println(
                    "ERROR: Could not create audit entry in LoggingAdvice.");
        }
    }

    public void exceptionLogger(JoinPoint jp, Exception e) {

        String methodName = jp.getSignature().getName();
        String exceptionMessage = e.getMessage();
        try {
            auditDao.writeAuditEntry(methodName + " ERROR!");
            auditDao.writeAuditEntry(exceptionMessage);
        } catch (VendingMachinePersistenceException ex) {
            System.err.println(
                    "ERROR: Could not create audit entry in LoggingAdvice.");
        }

    }

}
