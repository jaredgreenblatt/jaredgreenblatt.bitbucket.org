/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.dao;

import com.sg.vendingmachine.dto.VendingItem;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jared Greenblatt
 */
public class VendingMachineDaoStubEmptyImpl implements VendingMachineDao {

    @Override
    public VendingItem addVendingItem(String itemId, VendingItem vendingItem)  {
        return vendingItem;
    }

    @Override
    public List<VendingItem> getAllVendingItems()  {
        return new ArrayList<VendingItem>();

    }

    @Override
    public VendingItem getVendingItem(String itemId) {
         return null;
    }

    @Override
    public VendingItem removeItem(String itemId)  {
        return null;
    }

    @Override
    public void editVendingItem(VendingItem vendingItem) {
        return;
    }

    @Override
    public void Open() throws VendingMachinePersistenceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void Close() throws VendingMachinePersistenceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
