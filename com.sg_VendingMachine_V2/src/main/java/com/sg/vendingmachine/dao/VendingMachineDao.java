/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.dao;

import com.sg.vendingmachine.dto.VendingItem;
import java.util.List;


/**
 *
 * @author Jared Greenblatt
 */
public interface VendingMachineDao {

    /**
     * Adds the given Vending Item to the Inventory and associates it with the
     * given Vending Item ID. If there is already a vending item associated with
     * the given Vending Item ID it will return that VendingItem object,
     * otherwise it will return null.
     *
     *
     * @param itemId
     * @param vendingItem
     * @return
     * @throws com.sg.vendingmachine.dao.VendingItemPersistenceException
     */
    VendingItem addVendingItem(String itemId, VendingItem vendingItem) throws VendingMachinePersistenceException;

    /**
     * Returns a String array containing the Item ids of all Items in the
     * Inventory.
     *
     *
     * @return
     * @throws com.sg.vendingmachine.dao.VendingItemPersistenceException
     */
    List<VendingItem> getAllVendingItems() throws VendingMachinePersistenceException;

    /**
     * Returns the VendingItem object associated with the given item id. Returns
     * null if no such item exists
     *
     * @param itemId
     * @return
     * @throws com.sg.vendingmachine.dao.VendingItemPersistenceException
     */

    VendingItem getVendingItem(String itemId) throws VendingMachinePersistenceException;

    /**
     * Removes from the inventory the VendingItem associated with the given id.
     * Returns the vending item object that is being removed or null if there is
     * no vendingItem associated with the given id
     *
     *
     * @param itemId
     * @return
     * @throws com.sg.vendingmachine.dao.VendingItemPersistenceException
     */
    VendingItem removeItem(String itemId) throws VendingMachinePersistenceException;
    
    void editVendingItem(VendingItem vendingItem) throws VendingMachinePersistenceException;

    void Open() throws VendingMachinePersistenceException;

    void Close() throws VendingMachinePersistenceException;

}
