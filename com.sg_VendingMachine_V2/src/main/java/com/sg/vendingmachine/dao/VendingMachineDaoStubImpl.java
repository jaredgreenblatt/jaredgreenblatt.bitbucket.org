/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.dao;

import com.sg.vendingmachine.dto.VendingItem;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jared Greenblatt
 */
public class VendingMachineDaoStubImpl implements VendingMachineDao {

    VendingItem vendingItem;
    List<VendingItem> vendingItemList = new ArrayList<>();
    VendingItem itemNoIventory;
    List<VendingItem> vendingItemListNoIventory = new ArrayList<>();

    public VendingMachineDaoStubImpl() {
        vendingItem = new VendingItem("0001");
        vendingItem.setItemName("SNACK SNACK");
        vendingItem.setItemCost(new BigDecimal("1.00").setScale(2, RoundingMode.HALF_EVEN));
        vendingItem.setNumberOfItemsInInventory(1);

        vendingItemList.add(vendingItem);

        itemNoIventory = new VendingItem("0002");
        itemNoIventory.setItemName("Food");
        itemNoIventory.setItemCost(new BigDecimal("3.00"));
        itemNoIventory.setNumberOfItemsInInventory(0);
        vendingItemListNoIventory.add(itemNoIventory);

    }

    @Override
    public VendingItem addVendingItem(String itemId, VendingItem vendingItem) throws VendingMachinePersistenceException {
        if (itemId.equals(this.vendingItem.getItemId())) {
            return this.vendingItem;
        } else {
            return null;
        }
    }
    
    
    public int editVendingMachineCount = 0;
    public VendingItem lastEdittedVendingItem= null;
    public String lastEditOldId = null;

    @Override
    public void editVendingItem(VendingItem vendingItem) throws VendingMachinePersistenceException {
       editVendingMachineCount++; // If edit VendingItem is called, incremenet its call count!
        this.lastEditOldId = vendingItem.getItemId(); // store id in case the test harness wants to check it
        this.lastEdittedVendingItem = vendingItem; // store Vending item in case the test harness wants to check it
        return;
    }

    @Override
    public List<VendingItem> getAllVendingItems() throws VendingMachinePersistenceException {
        return vendingItemList;
    }

    @Override
    public VendingItem getVendingItem(String itemId) throws VendingMachinePersistenceException {
        if (itemId.equals(vendingItem.getItemId())) {
            return vendingItem;
        } else if (itemId.equals("0002")) {
            vendingItem = new VendingItem("0002");
            vendingItem.setItemName("Food");
            vendingItem.setItemCost(new BigDecimal("3.00"));
            vendingItem.setNumberOfItemsInInventory(0);
            vendingItemList.add(vendingItem);
            return vendingItem;
        } else {
            return null;
        }
    }

    @Override
    public VendingItem removeItem(String itemId) throws VendingMachinePersistenceException {
        if (itemId.equals(vendingItem.getItemId())) {
            return vendingItem;
        } else {
            return null;
        }
    }

    public VendingItem VendingItemWithNoInventory(String itemId, VendingItem vendingItem) throws VendingMachinePersistenceException {

        if (itemId.equals(this.itemNoIventory.getItemId())) {
            return this.vendingItem;
        } else {
            return null;
        }
    }

    @Override
    public void Open() throws VendingMachinePersistenceException {
    }

    @Override
    public void Close() throws VendingMachinePersistenceException {
    }

}
