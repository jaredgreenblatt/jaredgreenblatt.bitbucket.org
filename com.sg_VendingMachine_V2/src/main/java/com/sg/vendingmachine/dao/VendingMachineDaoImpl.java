/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.dao;

import com.sg.vendingmachine.dto.VendingItem;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author Jared Greenblatt
 */
public class VendingMachineDaoImpl implements VendingMachineDao {

    private Map<String, VendingItem> vendingItems = new HashMap<>();
    public static final String INVENTORY_FILE = "inventory.txt";
    public static final String DELIMITER = "::";

    void loadInventory() throws VendingMachinePersistenceException {
        Scanner scanner;
        try {
            scanner = new Scanner(
                    new BufferedReader(
                            new FileReader(INVENTORY_FILE)));
        } catch (FileNotFoundException e) {
            throw new VendingMachinePersistenceException(
                    "-_- Could not load  vending item data into memory.", e);
        }
        String currentLine;

        String[] currentTokens;
        while (scanner.hasNextLine()) {

            currentLine = scanner.nextLine();
            // break up the line into tokens
            currentTokens = currentLine.split(DELIMITER);
            // Create a new Student object and put it into the map of students
            // NOTE FOR APPRENTICES: We are going to use the student id
            // which is currentTokens[0] as the map key for our student object.
            // We also have to pass the student id into the Student constructor
            VendingItem currentItem = new VendingItem(currentTokens[0]);
            // Set the remaining vlaues on currentStudent manually
            currentItem.setItemName(currentTokens[1]);
            currentItem.setItemCost(new BigDecimal(currentTokens[2]));
            currentItem.setNumberOfItemsInInventory(Integer.parseInt(currentTokens[3]));
            vendingItems.put(currentItem.getItemId(), currentItem);
        }
        scanner.close();
    }

    private void writeInventory() throws VendingMachinePersistenceException {
        PrintWriter out;

        try {
            out = new PrintWriter(new FileWriter(INVENTORY_FILE));
        } catch (IOException e) {
            throw new VendingMachinePersistenceException(
                    "Could not save Inventory Data.", e);
        }

        List<VendingItem> vendingItemList = this.getAllVendingItems();
        for (VendingItem currentVendingItem : vendingItemList) {
            out.println(currentVendingItem.getItemId() + DELIMITER
                    + currentVendingItem.getItemName() + DELIMITER
                    + currentVendingItem.getItemCost() + DELIMITER
                    + currentVendingItem.getNumberOfItemsInInventory());
            out.flush();
        }
        out.close();
    }

    @Override
    public VendingItem addVendingItem(String itemId, VendingItem vendingItem)
            throws VendingMachinePersistenceException {
        VendingItem newVendingItem = vendingItems.put(itemId, vendingItem);

        return newVendingItem;
    }

    @Override
    public List<VendingItem> getAllVendingItems()
            throws VendingMachinePersistenceException {

        return new ArrayList<VendingItem>(vendingItems.values());
    }

    @Override
    public VendingItem getVendingItem(String itemId)
            throws VendingMachinePersistenceException {

        return vendingItems.get(itemId);

    }

    @Override
    public VendingItem removeItem(String itemId) throws VendingMachinePersistenceException {
        VendingItem removedVendingItem = vendingItems.remove(itemId);
        return removedVendingItem;
    }

    @Override
    public void Open() throws VendingMachinePersistenceException {
        loadInventory();

    }

    @Override
    public void Close() throws VendingMachinePersistenceException {
        writeInventory();
    }

    @Override
    public void editVendingItem(VendingItem vendingItem) throws VendingMachinePersistenceException {

        removeItem(vendingItem.getItemId());
        vendingItems.put(vendingItem.getItemId(), vendingItem);

    }

}
