/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine;

import com.sg.vendingmachine.controller.VendingMachineController;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

 
/**
 * The App Class is used to launch the application. It fires off the components
 * needed for the vending machine to run.
 *
 * @author Jared Greenblatt
 */
public class App {

    public static void main(String[] args) {
//        UserIO MyIo = new UserIOConsoleImpl();
//        VendingMachineView myView = new VendingMachineView(MyIo);
//        VendingMachineDao myDao = new VendingMachineDaoImpl();
//        VendingMachineAuditDao myAuditDao  = new VendingMachineAuditDaoImpl();
//        VendingMachineServiceLayer myService = 
//                new VendingMachineServiceLayerImpl(myDao,myAuditDao);
//        VendingMachineController controller
//                = new VendingMachineController(myService, myView);
//        
//        controller.run();
//        

        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        VendingMachineController controller = ctx.getBean("controller", VendingMachineController.class);
        controller.run();

    }
}
