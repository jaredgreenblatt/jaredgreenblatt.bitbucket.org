/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.ui;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 *
 * @author Jared Greenblatt
 */
public class UserIOConsoleImpl implements UserIO {

    Scanner sc = new Scanner(System.in);

    @Override
    public void print(String message) {
        System.out.println(message);
    }

    @Override
    public double readDouble(String prompt) {
        double x;
        try {
            System.out.println(prompt);
            x = Double.parseDouble(sc.nextLine());

        } catch (NumberFormatException e) {
            System.out.println("You have entered a non double number");
            x = -1;

        }
        return x;

    }

    @Override
    public double readDouble(String prompt, double min, double max
    ) {
        double x, y;
        try {
            while (true) {
                System.out.println(prompt);
                x = Double.parseDouble(sc.nextLine());
                if (x >= min && x <= max) {
                    y = x;

                    break;

                }
            }
        } catch (NumberFormatException e) {
            System.out.println("You have entered a non double number");
            y = -1;

        }
        return y;
    }

    @Override
    public float readFloat(String prompt
    ) {
        float x;
        try {
            System.out.println(prompt);
            x = Float.parseFloat(sc.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("You have entered a non float number");
            x = -1;

        }

        return x;

    }

    @Override
    public float readFloat(String prompt, float min, float max
    ) {
        float x, y;

//        
        try {
            while (true) {
                System.out.println(prompt);
                x = Float.parseFloat(sc.nextLine());
                if (x >= min && x <= max) {
                    y = x;
                    break;

                }

            }
        } catch (NumberFormatException e) {
            System.out.println("You have entered a non float number");
            y = -1;

        }

        return y;

    }

    @Override
    public int readInt(String prompt
    ) {
        int x;

        try {
            System.out.println(prompt);
            x = Integer.parseInt(sc.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("You have entered a non interger number");
            x = -1;
        }

        return x;

    }

    @Override
    public int readInt(String prompt, int min, int max
    ) {
        int x, y;
        try {
            while (true) {
                System.out.println(prompt);

                x = Integer.parseInt(sc.nextLine());
                if (x >= min && x <= max) {
                    y = x;
                    break;
                } else {
                    System.out.println("_______________________________________________");
                    System.out.println("Please Select a number from 1-7 to correspond ");
                    System.out.println("with the activity you would like to perform on");
                    System.out.println("on the DVD collection");
                    System.out.println("_______________________________________________");
                    y = -1;

                }

            }

        } catch (NumberFormatException e) {
            System.out.println("_______________________________________________");
            System.out.println("Please Select a number from 1-7 to correspond ");
            System.out.println("with the activity you would like to perform on");
            System.out.println("on the DVD collection");
            System.out.println("_______________________________________________");
            y = -1;

        }
        return y;

    }

    @Override
    public long readLong(String prompt) {
        long x;
        try {
            System.out.println(prompt);
            x = Long.parseLong(sc.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("You've entered non-Long number");
            x = -1;

        }

        return x;

    }

    @Override
    public long readLong(String prompt, long min, long max
    ) {
        long x, y;
        try {

            while (true) {
                System.out.println(prompt);
                x = Long.parseLong(sc.nextLine());
                if (x >= min && x <= max) {
                    y = x;
                    break;

                }

            }
        } catch (NumberFormatException e) {
            System.out.println("You've entered non-Long number");
            y = -1;
        }
        return y;

    }

    @Override
    public String readString(String prompt
    ) {
        String x;
        System.out.println(prompt);
        x = sc.nextLine();
        return x;
    }

    @Override
    public BigDecimal readBigDecimal(String prompt) {
        BigDecimal x = new BigDecimal("0");
        try {
            System.out.println(prompt);
            x = new BigDecimal(sc.nextLine());
            x = x.setScale(2, RoundingMode.HALF_EVEN);
        } catch (NumberFormatException | InputMismatchException  e) {
            System.out.println("You've entered non-BigDecimal Number");

        }

        return x;

    }
     public BigDecimal readBigDecimal(String prompt, BigDecimal min, BigDecimal max) {
         
        BigDecimal x, w,v;
        double y;
        String z;
        try {
            while (true) {
                System.out.println(prompt);
                y = sc.nextDouble();
                z = Double.toString(y);
                x = new BigDecimal(z);
                if (x.compareTo(min) > -1 && x.compareTo(max) < 1) {
                    w = x;
                    break;
                }

            }
            return w;
        } catch (NumberFormatException | InputMismatchException e) {
            System.out.println("You've entered non-BigDecimal Number");
             v =min.subtract(BigDecimal.ONE);

        }
        
    return  v;
    }
}
