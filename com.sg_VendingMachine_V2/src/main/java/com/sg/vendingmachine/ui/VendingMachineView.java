/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *
 */
package com.sg.vendingmachine.ui;

import com.sg.vendingmachine.dto.Change;
import com.sg.vendingmachine.dto.VendingItem;
import java.math.BigDecimal;
import java.util.List;

/**
 * All User Interactions happen in the view of the application.
 *
 */
public class VendingMachineView {

    private UserIO io;

    public VendingMachineView(UserIO io) {
        this.io = io;

    }
//Admin Menu for the user to be able to work with the products in the Machine.

    public int printMenuAndGetSelection() {
        io.print("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
        io.print("Admin Menu");
        io.print("1. List Vending Items");
        io.print("2. Create New Vending Item");
        io.print("3. View a Vending Item");
        io.print("4. Remove a Vending Item");
        io.print("5. Exit");
        io.print("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
        return io.readInt("Please select from the above choices.", 1, 5);

    }

    /**
     * Menu for the Vending Machine application. It gives the users options to
     * Add Money, ,exit, launch admin console, get your change or select an Item
     * to vend.
     *
     * @param vendingItemList
     * @param money
     * @return
     */
    public String vendingMachineItem(List<VendingItem> vendingItemList, BigDecimal money) {
        io.print("Main Menu");
        io.print("Current Money $" + money);
        io.print("To Add Money please type a $");
        io.print("Type Change or C to get your Change.");

        // Access Admin Menu by typing @
        //
        io.print("============== Type In Item Id To Vend ====================");
        for (VendingItem currentVendingItem : vendingItemList) {
            io.print("Item ID: " + currentVendingItem.getItemId()
                    + " Name " + currentVendingItem.getItemName()
                    + " Cost $" + currentVendingItem.getItemCost()
                    + " Items In Stock " + currentVendingItem.getNumberOfItemsInInventory());
        }
        return io.readString("Please Select an Item by ID, Add Money or Exit!");

    }
//Getting a vending Items info to creat new item. Admin Function.

    public VendingItem getNewVendingItemInfo() {
        String itemId = io.readString("Please Enter an Item ID");
        String itemName = io.readString("Please Enter an Item Name");
        BigDecimal itemCost = io.readBigDecimal("Plese enter an Item Cost");
        int numberofItemsInInventory = io.readInt("Please Enter How Many we have in inventory");
        VendingItem currentVendingItem = new VendingItem(itemId);
        currentVendingItem.setItemName(itemName);
        currentVendingItem.setItemCost(itemCost);
        currentVendingItem.setNumberOfItemsInInventory(numberofItemsInInventory);
        return currentVendingItem;

    }
    //Getting a vending Items list. This is used  to list the items in the machine.

    public void displayVendingItemList(List<VendingItem> vendingItemList) {
        for (VendingItem currentVendingItem : vendingItemList) {
            io.print(currentVendingItem.getItemId() + ": "
                    + currentVendingItem.getItemName() + ": "
                    + currentVendingItem.getItemCost() + ": "
                    + currentVendingItem.getNumberOfItemsInInventory());

        }
        io.readString("Please hit enter to continue.");

    }

    //Display One vending ITEM in the admin console.
    public void displayVendingItem(VendingItem vendingItem) {
        if (vendingItem != null) {
            io.print("Vending Item ID: " + vendingItem.getItemId());
            io.print("Vending Item Name: " + vendingItem.getItemName());
            io.print("Vending Item Cost: $" + vendingItem.getItemCost());
            io.print("Vending Item in Inventrory: " + vendingItem.getNumberOfItemsInInventory());

        } else {
            io.print("No such Item.");
        }
        io.readString("Please hit enter to continue.");
    }
// String that is returned to the controller with the user choice.

    public String getVendingItemIdChoice() {
        return io.readString("Please enter the Vending Item ID.");
    }
//Method used to get money from the user. It is then sent to the controller so 
// it can be used throughout the application.

    public BigDecimal getMoney() {

        return io.readBigDecimal("Please Put Money Into Money Into the Vending"
                + " Machine");
    }

    public BigDecimal MoneyAmount(BigDecimal Money) {
        return Money;
    }

    //Used to Display Money during the get change, Exit or Vend function.
    public void PrintChangeTotalAmount(BigDecimal Money) {
        System.out.println("Your Remaining Money is $" + Money);
    }

    /**
     * With this we evaluate the change object that is passed in from the
     * controller for a certain purchase. It has already run through the make
     * change process. So we use the passed in change object. We then evaluate
     * whether the counter for the change amount it greater than ZERO. If thats
     * true it will print out that your change includes x of changeCount. If all
     * of these evaluate false. We print out no change is due.
     *
     * @param change
     */
    public void PrintChange(Change change) {
        System.out.println("_________________________________________________");
        if (change.getOneHundredDollarBillCount() >= 1) {
            System.out.println(change.getOneHundredDollarBillCount() + " One Hundred Dollar Bill");
        }
        if (change.getTwentyDollarBillCount() >= 1) {
            System.out.println(change.getTwentyDollarBillCount() + " Twenty Dollar Bill");
        }
        if (change.getTenDollarBillCount() >= 1) {
            System.out.println(change.getTenDollarBillCount() + " Ten Dollar Bill");
        }
        if (change.getFiveDollarBillCount() >= 1) {
            System.out.println(change.getFiveDollarBillCount() + " Five Dollar Bill");
        }
        if (change.getOneDollarBillCount() >= 1) {
            System.out.println(change.getOneDollarBillCount() + " One Dollar Bill");
        }
        if (change.getQuarterCount() >= 1) {
            System.out.println(change.getQuarterCount() + " Quarter");
        }
        if (change.getDimeCount() >= 1) {
            System.out.println(change.getDimeCount() + " Dime");
        }
        if (change.getNickleCount() >= 1) {
            System.out.println(change.getNickleCount() + " Nickle");
        }
        if (change.getPennyCount() >= 1) {
            System.out.println(change.getPennyCount() + " Penny");
        }
        if (change.getPennyCount() == 0 && change.getNickleCount() == 0
                && change.getDimeCount() == 0 && change.getQuarterCount() == 0
                && change.getOneDollarBillCount() == 0 && change.getFiveDollarBillCount() == 0
                && change.getTenDollarBillCount() == 0 && change.getTwentyDollarBillCount() == 0
                && change.getOneHundredDollarBillCount() == 0) {
            System.out.println("------------------");
            System.out.println("No Change is due.");
            System.out.println("------------------");
        }
        System.out.println("_________________________________________________");

    }

    //Method to display the item that has been purchased. We  do this by recieving
    //vending item from the controller/service layer.
    public void displayPurchasedItemName(VendingItem vending) {
        System.out.println("You selected " + vending.getItemName() + "!");
    }

    /*
    Methods that display header banners for the application.
     */
    public void displayVendingMachine() {
        io.print("=== Vending Machine Please Make your Selection ===");
    }

    public void displayCreateVendingItemBanner() {
        io.print("=== Create Vending Item ===");
    }

    public void displayDisplayVendingBanner() {
        io.print("=== Display Vending Item ===");

    }

    public void displayCreateSuccessBanner() {
        io.readString(
                "Vending Item successfully created.  Please hit enter to continue");
    }

    public void displayDisplayAllBanner() {
        io.print("=== Display All Students ===");
    }

    public void displayRemoveVendingItemBanner() {
        io.print("=== Remove Vending Item ===");
    }

    public void displayRemoveSuccessBanner() {
        io.readString("Vending Item successfully removed. Please hit enter to continue.");
    }

    public void displayAddMoneyBanner() {
        io.print("=== Please Add Money to the Vending Machine ===");
    }

    public void displayAddMoneySuccessBanner() {
        io.readString("Money Added Successfully. Please hit enter to continue.");
    }
     public void displayAddMoneyFailureBanner() {
        io.readString("Money Add Failure Please input value greater than one."
                + " Please hit enter to continue.");
    }

   

    public void displayMakeChangeBanner() {
        io.print("=== Your Change ===");
    }

    public void displayMakeChangeSuccessBanner() {
        io.readString("Please Collect your change and Press Enter.");
    }

    public void displayVendingExit() {
        io.print("Thank you for visiting the Vending Machine.");
    }

    public void displayExitBanner() {
        io.print("Good Bye!!!");
    }

    public void displayUnknownCommandBanner() {
        io.print("Unknown Command!!!");
    }

    public void displayErrorMessage(String errorMsg) {
        io.print("=== ERROR ===");
        io.print(errorMsg);
    }

    public void displayVending() {
        io.print("=== VENDING ===");
    }

    public void displayVendingComplete() {
        io.readString("Item vended press enter to get your CHANGE.");
    }

}
