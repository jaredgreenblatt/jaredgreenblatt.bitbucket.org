/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.dto;

import java.math.BigDecimal;
import java.math.RoundingMode;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jared Greenblatt
 */
public class ChangeTest {

    Change changeTest = new Change(new BigDecimal(0));

    public ChangeTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getONE_HUNDRED_DOLLAR_BILL method, of class Change.
     */
    Change changeTestMoneyAmount;

    @Test
    public void testChangefor0$() {
        changeTestMoneyAmount = new Change(new BigDecimal(0.00));
        assertEquals(changeTestMoneyAmount.getOneHundredDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getTwentyDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getTenDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getFiveDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getOneDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getQuarterCount(), 0);
        assertEquals(changeTestMoneyAmount.getDimeCount(), 0);
        assertEquals(changeTestMoneyAmount.getNickleCount(), 0);
        assertEquals(changeTestMoneyAmount.getPennyCount(), 0);

    }
     @Test
    public void testChangeforPenny() {
        changeTestMoneyAmount = new Change(new BigDecimal(0.01));
        assertEquals(changeTestMoneyAmount.getOneHundredDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getTwentyDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getTenDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getFiveDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getOneDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getQuarterCount(), 0);
        assertEquals(changeTestMoneyAmount.getDimeCount(), 0);
        assertEquals(changeTestMoneyAmount.getNickleCount(), 0);
        assertEquals(changeTestMoneyAmount.getPennyCount(), 1);

    }
       @Test
    public void testChangeforNickle() {
        changeTestMoneyAmount = new Change(new BigDecimal(0.05).setScale(2, RoundingMode.HALF_EVEN));
        assertEquals(changeTestMoneyAmount.getOneHundredDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getTwentyDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getTenDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getFiveDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getOneDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getQuarterCount(), 0);
        assertEquals(changeTestMoneyAmount.getDimeCount(), 0);
        assertEquals(changeTestMoneyAmount.getNickleCount(), 1);
        assertEquals(changeTestMoneyAmount.getPennyCount(), 0);

    }
         @Test
    public void testChangeforDime() {
        changeTestMoneyAmount = new Change(new BigDecimal(0.10).setScale(2, RoundingMode.HALF_EVEN));
        assertEquals(changeTestMoneyAmount.getOneHundredDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getTwentyDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getTenDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getFiveDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getOneDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getQuarterCount(), 0);
        assertEquals(changeTestMoneyAmount.getDimeCount(), 1);
        assertEquals(changeTestMoneyAmount.getNickleCount(), 0);
        assertEquals(changeTestMoneyAmount.getPennyCount(), 0);

    }
          @Test
    public void testChangeforQuarter() {
        changeTestMoneyAmount = new Change(new BigDecimal(0.25).setScale(2, RoundingMode.HALF_EVEN));
        assertEquals(changeTestMoneyAmount.getOneHundredDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getTwentyDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getTenDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getFiveDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getOneDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getQuarterCount(), 1);
        assertEquals(changeTestMoneyAmount.getDimeCount(), 0);
        assertEquals(changeTestMoneyAmount.getNickleCount(), 0);
        assertEquals(changeTestMoneyAmount.getPennyCount(), 0);

    }
              @Test
    public void testChangeforOneDollar() {
        changeTestMoneyAmount = new Change(new BigDecimal(1.00).setScale(2, RoundingMode.HALF_EVEN));
        assertEquals(changeTestMoneyAmount.getOneHundredDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getTwentyDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getTenDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getFiveDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getOneDollarBillCount(), 1);
        assertEquals(changeTestMoneyAmount.getQuarterCount(), 0);
        assertEquals(changeTestMoneyAmount.getDimeCount(), 0);
        assertEquals(changeTestMoneyAmount.getNickleCount(), 0);
        assertEquals(changeTestMoneyAmount.getPennyCount(), 0);

    }
             @Test
    public void testChangeforFiveDollar() {
        changeTestMoneyAmount = new Change(new BigDecimal(5.00).setScale(2, RoundingMode.HALF_EVEN));
        assertEquals(changeTestMoneyAmount.getOneHundredDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getTwentyDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getTenDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getFiveDollarBillCount(), 1);
        assertEquals(changeTestMoneyAmount.getOneDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getQuarterCount(), 0);
        assertEquals(changeTestMoneyAmount.getDimeCount(), 0);
        assertEquals(changeTestMoneyAmount.getNickleCount(), 0);
        assertEquals(changeTestMoneyAmount.getPennyCount(), 0);

    }
    
          @Test
    public void testChangeforTenDollar() {
        changeTestMoneyAmount = new Change(new BigDecimal(10.00).setScale(2, RoundingMode.HALF_EVEN));
        assertEquals(changeTestMoneyAmount.getOneHundredDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getTwentyDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getTenDollarBillCount(), 1);
        assertEquals(changeTestMoneyAmount.getFiveDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getOneDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getQuarterCount(), 0);
        assertEquals(changeTestMoneyAmount.getDimeCount(), 0);
        assertEquals(changeTestMoneyAmount.getNickleCount(), 0);
        assertEquals(changeTestMoneyAmount.getPennyCount(), 0);

    }
       @Test
    public void testChangeforTwentyDollar() {
        changeTestMoneyAmount = new Change(new BigDecimal(20.00).setScale(2, RoundingMode.HALF_EVEN));
        assertEquals(changeTestMoneyAmount.getOneHundredDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getTwentyDollarBillCount(), 1);
        assertEquals(changeTestMoneyAmount.getTenDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getFiveDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getOneDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getQuarterCount(), 0);
        assertEquals(changeTestMoneyAmount.getDimeCount(), 0);
        assertEquals(changeTestMoneyAmount.getNickleCount(), 0);
        assertEquals(changeTestMoneyAmount.getPennyCount(), 0);

    }
    
           @Test
    public void testChangeforOneHundredDollar() {
        changeTestMoneyAmount = new Change(new BigDecimal(100.00).setScale(2, RoundingMode.HALF_EVEN));
        assertEquals(changeTestMoneyAmount.getOneHundredDollarBillCount(), 1);
        assertEquals(changeTestMoneyAmount.getTwentyDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getTenDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getFiveDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getOneDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getQuarterCount(), 0);
        assertEquals(changeTestMoneyAmount.getDimeCount(), 0);
        assertEquals(changeTestMoneyAmount.getNickleCount(), 0);
        assertEquals(changeTestMoneyAmount.getPennyCount(), 0);

    }
      @Test
    public void testChangefor66Cents() {
        changeTestMoneyAmount = new Change(new BigDecimal(0.66).setScale(2, RoundingMode.HALF_EVEN));
        assertEquals(changeTestMoneyAmount.getOneHundredDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getTwentyDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getTenDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getFiveDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getOneDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getQuarterCount(), 2);
        assertEquals(changeTestMoneyAmount.getDimeCount(), 1);
        assertEquals(changeTestMoneyAmount.getNickleCount(), 1);
        assertEquals(changeTestMoneyAmount.getPennyCount(), 1);

    }
      @Test
    public void testChange36Dollars66Cents() {
        changeTestMoneyAmount = new Change(new BigDecimal(36.66).setScale(2, RoundingMode.HALF_EVEN));
        assertEquals(changeTestMoneyAmount.getOneHundredDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getTwentyDollarBillCount(), 1);
        assertEquals(changeTestMoneyAmount.getTenDollarBillCount(), 1);
        assertEquals(changeTestMoneyAmount.getFiveDollarBillCount(), 1);
        assertEquals(changeTestMoneyAmount.getOneDollarBillCount(), 1);
        assertEquals(changeTestMoneyAmount.getQuarterCount(), 2);
        assertEquals(changeTestMoneyAmount.getDimeCount(), 1);
        assertEquals(changeTestMoneyAmount.getNickleCount(), 1);
        assertEquals(changeTestMoneyAmount.getPennyCount(), 1);

    }
        @Test
    public void testChange136Dollars66Cents() {
        changeTestMoneyAmount = new Change(new BigDecimal(136.66).setScale(2, RoundingMode.HALF_EVEN));
        assertEquals(changeTestMoneyAmount.getOneHundredDollarBillCount(), 1);
        assertEquals(changeTestMoneyAmount.getTwentyDollarBillCount(), 1);
        assertEquals(changeTestMoneyAmount.getTenDollarBillCount(), 1);
        assertEquals(changeTestMoneyAmount.getFiveDollarBillCount(), 1);
        assertEquals(changeTestMoneyAmount.getOneDollarBillCount(), 1);
        assertEquals(changeTestMoneyAmount.getQuarterCount(), 2);
        assertEquals(changeTestMoneyAmount.getDimeCount(), 1);
        assertEquals(changeTestMoneyAmount.getNickleCount(), 1);
        assertEquals(changeTestMoneyAmount.getPennyCount(), 1);

    }
    
        @Test
    public void TestChangefor7Dollars83Cents() {
        changeTestMoneyAmount = new Change(new BigDecimal(7.83));
        assertEquals(changeTestMoneyAmount.getOneHundredDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getTwentyDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getTenDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getFiveDollarBillCount(), 1);
        assertEquals(changeTestMoneyAmount.getOneDollarBillCount(), 2);
        assertEquals(changeTestMoneyAmount.getQuarterCount(), 3);
        assertEquals(changeTestMoneyAmount.getDimeCount(), 0);
        assertEquals(changeTestMoneyAmount.getNickleCount(), 1);
        assertEquals(changeTestMoneyAmount.getPennyCount(), 3);

    }
    
    
     
        @Test
    public void TestChangefor33Dollars33Cents() {
        changeTestMoneyAmount = new Change(new BigDecimal(33.33).setScale(2, RoundingMode.HALF_EVEN));
        assertEquals(changeTestMoneyAmount.getOneHundredDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getTwentyDollarBillCount(), 1);
        assertEquals(changeTestMoneyAmount.getTenDollarBillCount(), 1);
        assertEquals(changeTestMoneyAmount.getFiveDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getOneDollarBillCount(), 3);
        assertEquals(changeTestMoneyAmount.getQuarterCount(), 1);
        assertEquals(changeTestMoneyAmount.getDimeCount(), 0);
        assertEquals(changeTestMoneyAmount.getNickleCount(), 1);
        assertEquals(changeTestMoneyAmount.getPennyCount(), 3);

    }
    


   
    @Test
    public void testChangefor1Doller77cents() {
        changeTestMoneyAmount = new Change(new BigDecimal(1.77).setScale(2, RoundingMode.HALF_EVEN));
        assertEquals(changeTestMoneyAmount.getOneHundredDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getTwentyDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getTenDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getFiveDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getOneDollarBillCount(), 1);
        assertEquals(changeTestMoneyAmount.getQuarterCount(), 3);
        assertEquals(changeTestMoneyAmount.getDimeCount(), 0);
        assertEquals(changeTestMoneyAmount.getNickleCount(), 0);
        assertEquals(changeTestMoneyAmount.getPennyCount(), 2);

    }

    @Test
    public void testChangefor88Cents() {
        changeTestMoneyAmount = new Change(new BigDecimal(0.88).setScale(2, RoundingMode.HALF_EVEN));
        assertEquals(changeTestMoneyAmount.getOneHundredDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getTwentyDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getTenDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getFiveDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getOneDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getQuarterCount(), 3);
        assertEquals(changeTestMoneyAmount.getDimeCount(), 1);
        assertEquals(changeTestMoneyAmount.getNickleCount(), 0);
        assertEquals(changeTestMoneyAmount.getPennyCount(), 3);

    }

    @Test
    public void testChangefor36Dollars() {
        changeTestMoneyAmount = new Change(new BigDecimal(36).setScale(2, RoundingMode.HALF_EVEN));
        assertEquals(changeTestMoneyAmount.getOneHundredDollarBillCount(), 0);
        assertEquals(changeTestMoneyAmount.getTwentyDollarBillCount(), 1);
        assertEquals(changeTestMoneyAmount.getTenDollarBillCount(), 1);
        assertEquals(changeTestMoneyAmount.getFiveDollarBillCount(), 1);
        assertEquals(changeTestMoneyAmount.getOneDollarBillCount(), 1);
        assertEquals(changeTestMoneyAmount.getQuarterCount(), 0);
        assertEquals(changeTestMoneyAmount.getDimeCount(), 0);
        assertEquals(changeTestMoneyAmount.getNickleCount(), 0);
        assertEquals(changeTestMoneyAmount.getPennyCount(), 0);

    }

  

    @Test
    public void testChangefor136Dollars() {
        changeTestMoneyAmount = new Change(new BigDecimal(136.00).setScale(2, RoundingMode.HALF_EVEN));
        assertEquals(changeTestMoneyAmount.getOneHundredDollarBillCount(), 1);
        assertEquals(changeTestMoneyAmount.getTwentyDollarBillCount(), 1);
        assertEquals(changeTestMoneyAmount.getTenDollarBillCount(), 1);
        assertEquals(changeTestMoneyAmount.getFiveDollarBillCount(), 1);
        assertEquals(changeTestMoneyAmount.getOneDollarBillCount(), 1);
        assertEquals(changeTestMoneyAmount.getQuarterCount(), 0);
        assertEquals(changeTestMoneyAmount.getDimeCount(), 0);
        assertEquals(changeTestMoneyAmount.getNickleCount(), 0);
        assertEquals(changeTestMoneyAmount.getPennyCount(), 0);

    }

 
  

}
