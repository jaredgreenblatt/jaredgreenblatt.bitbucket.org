/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.service;

import com.sg.vendingmachine.dto.Change;
import com.sg.vendingmachine.dto.VendingItem;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import sun.print.resources.serviceui;

/**
 *
 * @author Jared Greenblatt
 */
public class VendingMachineServiceLayerTest {

    private VendingMachineServiceLayer service;
    private VendingMachineServiceLayer serviceNewEmptyDao;

    public VendingMachineServiceLayerTest() {
//        VendingMachineDao dao = new VendingMachineDaoStubImpl();
//        VendingMachineAuditDao auditDao = new VendingMachineAuditDaoStubImpl();
//
//        service = new VendingMachineServiceLayerImpl(dao, auditDao);

        // wire the Service Layer with stub implementations of the Dao and
        ApplicationContext ctx
                = new ClassPathXmlApplicationContext("applicationContext.xml");
        service
                = ctx.getBean("serviceLayer", VendingMachineServiceLayer.class);

        ApplicationContext ctxEmptyDaoStub
                = new ClassPathXmlApplicationContext("applicationContext.xml");
        serviceNewEmptyDao
                = ctxEmptyDaoStub.getBean("serviceLayerEmptyDao", VendingMachineServiceLayer.class);

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getAllVendingItem method, of class VendingMachineServiceLayer.
     * @throws java.lang.Exception
     */
    @Test
    public void testGetAllVendingItem() throws Exception {
        List<VendingItem> VendingItemList = service.getAllVendingItem();
        // check it is empty list

        Assert.assertNotNull("Empty List Not Null", VendingItemList);
        Assert.assertTrue("Checking If List is not Empty.", VendingItemList.size() == 1);
        Assert.assertEquals("SNACK SNACK", VendingItemList.get(0).getItemName());

    }
    
   
    @Test
    public void testGetAllVendingItemEmptyDao() throws Exception {
        List<VendingItem> emptyVendingItemList = serviceNewEmptyDao.getAllVendingItem();
        // check it is empty list

        Assert.assertNotNull("Empty List Not Null", emptyVendingItemList);
        Assert.assertTrue("Checking If List is empty.", emptyVendingItemList.isEmpty());

    }

    /**
     * Test of getVendingItem method, of class VendingMachineServiceLayer.
     */
    @Test
    public void testGetVendingItem() throws Exception {
        VendingItem item = service.getVendingItem("0001");
        assertNotNull(item);

    }

    @Test
    public void testGetVendingItemEmptyDao() throws Exception {
        boolean correctExceptionThrown = false;

        try {
            VendingItem item = serviceNewEmptyDao.getVendingItem("V0002000");
            String id = "0002000";
            Assert.fail("Hey, expected this call w/ no items with the id 0002000");
            assertEquals(item, null);

        } catch (ItemDoesNotExistException e) {
            correctExceptionThrown = true;

            assertEquals("ERROR: Item you chose to vend does not exist. V0002000.", e.getMessage());
        } catch (Exception e) {
            Assert.fail("Call expected for no items in iventory.");

        }

        Assert.assertTrue(correctExceptionThrown);

    }


    /**
     * Test of removeVendingItem method, of class VendingMachineServiceLayer.
     */
    @Test
    public void testRemoveVendingItem() throws Exception {
        VendingItem item = service.removeVendingItem("0001");
        assertNotNull(item);
        assertEquals("0001", item.getItemId());

    }

    /**
     * Test of PurchaseItem method, of class VendingMachineServiceLayer. Manual test
     * I am calling item 0001 and then manually stepping through the purchase process.
     * Then set the edited item information and then assert that the information
     * is what we expect.
     */
    BigDecimal runningTotal;
    int remainingIventory;

    @Test
    public void testPurchaseItem() throws Exception {
        BigDecimal runningTotal = new BigDecimal("1.00");

        VendingItem item = service.getVendingItem("0001");

        BigDecimal tempCost = item.getItemCost();
        String tempName = item.getItemName();
        int tempInventory = item.getNumberOfItemsInInventory();
        tempInventory = tempInventory - 1;
        runningTotal = runningTotal.subtract(tempCost).setScale(2, RoundingMode.HALF_EVEN);
        VendingItem edititedClass = new VendingItem(item.getItemId());
        edititedClass.setItemName(tempName);
        edititedClass.setItemCost(tempCost);
        edititedClass.setNumberOfItemsInInventory(tempInventory);

        assertEquals(edititedClass.getNumberOfItemsInInventory(), remainingIventory);
        assertEquals(runningTotal, new BigDecimal(0.00).setScale(2, RoundingMode.HALF_EVEN));

    }
/**
 * Same test as above except this time we test purchasing an item using the purchase
 * item method. Set the  $ to 1 Dollar and then purchase item 001 which cost $1.
 * Assert that the running total is equal to zero and that the items in inventory are now 
 * equal to zero.
 * @throws Exception 
 */
    @Test
    public void testPurchaseItemUsingMethod() throws Exception {
        BigDecimal runningTotal = new BigDecimal("1.00");
        VendingItem vendingItem = service.getVendingItem("0001");
        service.setrunningTotal(runningTotal);
        vendingItem= service.PurchaseItem(vendingItem);
        assertEquals(service.getrunningTotal(), 
                new BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN));
        assertEquals(vendingItem.getNumberOfItemsInInventory(), 0);
    }
/**
 * Testing purchasing an item with not enough money. We are causing the insufficient
 * fund error to be thrown.  We then check that the correct expression was thrown.
 * Second we check that the inventory level remains at one. Also we confirm that the 
 * running total remains 0.
 * @throws Exception 
 */
    @Test
    public void testPurchaseItemWhenInsuficientFunds() throws Exception {
        boolean correctExceptionThrown = false;
        BigDecimal runningTotal = new BigDecimal("0.00");

        VendingItem item = service.getVendingItem("0001");
        try {
            service.PurchaseItem(item);
            Assert.fail("Hey, expected this call for insufficient funds.");

        } catch (InsufficientFundsException e) {
            correctExceptionThrown = true;

            assertEquals("ERROR: you have insufficient funds to purchase "
                    + item.getItemName() + ".", e.getMessage());
        } catch (Exception e) {
            Assert.fail("Call expected for insufficient Funds.");

        }

        Assert.assertTrue(correctExceptionThrown);
        assertEquals(item.getNumberOfItemsInInventory(), 1);
        assertEquals(runningTotal, new BigDecimal(0.00).setScale(2, RoundingMode.HALF_EVEN));

    }
/**
 * Testing purchasing an item with $5 inserted into the machine. We then attempt to purchase 
 * item 0002 which has no item in inventory. We then try to purchase using the purchaseItem
 * Method.  This will cause the no itemInInventory Exception. We then check if 
 * the correct exception is thrown and that the item in inventory count and the runningTotal 
 * remain the same.
 * @throws Exception 
 */
    @Test
    public void testPurchaseItemWhenNoItemInInventory() throws Exception {
        boolean correctExceptionThrown = false;
        BigDecimal runningTotal = new BigDecimal("5.00");

        VendingItem item = service.getVendingItem("0002");
        try {
            service.PurchaseItem(item);
            Assert.fail("Hey, expected this call w/ no items remaining in iventory");

        } catch (NoItemInventoryException e) {
            correctExceptionThrown = true;

            assertEquals("ERROR: No Item remains in Inventory "
                    + "we will need to reorder "
                    + item.getItemName() + ".", e.getMessage());
        } catch (Exception e) {
            Assert.fail("Call expected for no items in iventory.");

        }

        Assert.assertTrue(correctExceptionThrown);
        assertEquals(item.getNumberOfItemsInInventory(), 0);
        assertEquals(runningTotal, new BigDecimal(5.00).setScale(2, RoundingMode.HALF_EVEN));

    }
/**
 * In this test we try to purchase an item that doesn't exist. We try to get Vending 
 * item v00002000 this will return a null causing the no item in inventory exception to be thrown. 
 * We then check that the correct exception is thrown.
 * @throws Exception 
 */
    @Test
    public void testPurchaseItemDoesntExist() throws Exception {
        boolean correctExceptionThrown = false;

        try {
           service.getVendingItem("V0002000");
            
            Assert.fail("Hey, expected this call w/ no items with the id 0002000");

        } catch (ItemDoesNotExistException e) {
            correctExceptionThrown = true;

            assertEquals("ERROR: Item you chose to vend does not exist. V0002000.", e.getMessage());
        } catch (Exception e) {
            Assert.fail("Call expected for no items in iventory.");

        }

        Assert.assertTrue(correctExceptionThrown);

    }

    /**
     * Test of makeChange method, of class VendingMachineServiceLayer.
     */
    @Test
    public void makechangeTest() {
        runningTotal = new BigDecimal(3.66);

        Change change = new Change(runningTotal);
        assertEquals(change.getOneDollarBillCount(), 3);
        assertEquals(change.getQuarterCount(), 2);
        assertEquals(change.getDimeCount(), 1);
        assertEquals(change.getNickleCount(), 1);
        assertEquals(change.getPennyCount(), 1);

    }

    /**
     * Test of edit method, of class VendingMachineServiceLayer.
     */
    @Test
    public void EditVendingItem() throws Exception {
        VendingItem item = service.getVendingItem("0001");
        assertEquals(item.getItemName(), "SNACK SNACK");
        item.setItemName("SNACK SNACK SNACK");
        service.editVendingItem(item);
        assertEquals(item.getItemName(), "SNACK SNACK SNACK");

    }
}
