<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Super Hero Tracking System</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">  
        <link href="${pageContext.request.contextPath}/css/superHero.css" rel="stylesheet">   
        <link href="https://fonts.googleapis.com/css?family=Bungee+Shade" rel="stylesheet">

    </head>
    <body>
        <div class="container">
            <div class="row VendingMachineTitleRow header">
                <div class="col-xs-offset-2 col-xs-8">
                    <h1 class ="text-center">Super Hero Tracking System</h1> 

                </div>
            </div>
            <hr />
            <hr />
            <div class="col-xs-12">
                <center>
                    <div class="navbar text-center">
                        <ul class="nav nav-pills">
                            <li role="presentation"><a href="${pageContext.request.contextPath}/">Home</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/heroOrVillain">Heros</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/superPower">SuperPowers</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/locations">Locations</a></li>
                            <li role="presentation"class ="active"><a href="${pageContext.request.contextPath}/organizations">Organizations</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/sightings">Sightings</a></li>

                        </ul>    
                    </div>
                </center>
            </div>


            <h2 class ="col-xs-12 text-center">Organizations </h2> 

            <div class="col-xs-12">
                <div class ="col-xs-6">
                    <h3 class = "text-center">Organization List</h3>
                    <table id="contactTable" class="table table-hover">
                        <tr>
                            <th width="40%">Organization Name</th>
                            <th width="30%">Organization Description</th>
                            <th width="15%"></th>
                            <th width="15%"></th>

                        </tr>
                        <c:forEach var="currentOrg"  items="${organizationList}">
                            <tr>
                                <td>
                                    <a href="displayOrganization?organizationId=${currentOrg.organizationId}">

                                        <c:out value="${currentOrg.name}"/>  
                                    </a>

                                </td>
                                <td>
                                    <c:out value="${currentOrg.description}"/>
                                </td>
                                <td>
                                    <a href="displayUpdateOrganization?organizationId=${currentOrg.organizationId}">
                                        update
                                    </a>
                                </td>
                                <td>
                                    <a href="deleteOrganization?organizationId=${currentOrg.organizationId}">
                                        delete
                                    </a>
                                </td>

                            </tr>
                        </c:forEach>
                    </table>

                </div>
                <div class ="col-xs-6">
                    <h3 class = "text-center">Add Organization</h3>
                    <form class ="form-horizontal"
                          role ="form" method ="POST"
                          action="createOrganization">
                        <div class="form-group">
                            <label for="add-name" class="col-md-4 control-label">
                                Name:
                            </label>
                            <div class ="col-md-8">
                                <input type="text" class ="form-control" name ="name" placeholder="Organization Name" required/>
                            </div>
                        </div>
                        <div class ="form-group">
                            <label for="add-description" class ="col-md-4 control-label">
                                Description:
                            </label>
                            <div class ="col-md-8">
                                <input type="text" class ="form-control" name ="description" placeholder="Description" required/>
                            </div>
                        </div>
                        <div class ="form-group">
                            <label for="add-contact-information" class ="col-md-4 control-label">
                                Contact Information:
                            </label>
                            <div class ="col-md-8">
                                <input type="text" class ="form-control" name ="contact" placeholder="Contact Information" required/>
                            </div>
                        </div>

                        <div class ="form-group">
                            <label for="add-email" class ="col-md-4 control-label">
                                Heros Or Villains:
                            </label>
                            <div class ="col-md-8">
                                <select multiple class="form-control" id="powerSelectList" name ="heroList" required>

                                    <c:forEach var="orgHero"  items="${heroList}">

                                        <option value="${orgHero.heroOrVillainId}"> <c:out value="${orgHero.name}"/>   </option>

                                    </c:forEach>

                                </select>
                            </div>
                        </div>
                        <div class ="form-group">
                            <label for="add-email" class ="col-md-4 control-label">
                                Locations:
                            </label>
                            <div class ="col-md-8">
                                <select multiple class="form-control" id="powerSelectList" name ="locationList" required>

                                    <c:forEach var="orgLocations"  items="${locationList}">

                                        <option value="${orgLocations.locationId}"> <c:out value="${orgLocations.description}"/>   </option>

                                    </c:forEach>

                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-8">
                                <input type="submit" class="btn btn-default" value="Create Organization"/>
                            </div>
                        </div>


                    </form>
                </div>
            </div>



        </div> 






    </div>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

</body>
</html>

