<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Super Hero Tracking System</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">  
        <link href="${pageContext.request.contextPath}/css/superHero.css" rel="stylesheet">   
        <link href="https://fonts.googleapis.com/css?family=Bungee+Shade" rel="stylesheet">
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC39u3lFEsi0rTzB-Txk_zboCF2vltO4xk&callback=initMap">
        </script>
        <script>
            function initMap() {
                var uluru = {lat: ${location.latitude}, lng: ${location.longitude}};
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 13,
                    center: uluru
                });
                var marker = new google.maps.Marker({
                    position: uluru,
                    map: map
                });
            }
        </script>

    </head>
    <body>
        <div class="container">
            <div class="row VendingMachineTitleRow header">
                <div class="col-xs-offset-2 col-xs-8">
                    <h1 class ="text-center">Super Hero Tracking System</h1> 

                </div>
            </div>
            <hr />
            <hr />
            <div class="col-xs-12">
                <center>
                    <div class="navbar text-center">
                        <ul class="nav nav-pills">
                            <li role="presentation"><a href="${pageContext.request.contextPath}/">Home</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/heroOrVillain">Heros</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/superPower">SuperPowers</a></li>
                            <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/locations">Locations</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/organizations">Organizations</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/sightings">Sightings</a></li>

                        </ul>    
                    </div>
                </center>
            </div>


            <h2 class ="col-xs-12 text-center">  <c:out value="${location.description}"/> </h2> 
            <div class="col-xs-12">

                <div id="locInfo" class="col-xs-offset-2 col-xs-4">
                    <hr/>
                    <p><b>Location Description:</b> <c:out value="${location.description}"/> </p>
                    <p><b>Location City: </b><c:out value="${location.city}"/> </p>
                    <p><b>Location Country </b><c:out value="${location.country}"/> </p>
                    <p><b>Location Latitude</b> <c:out value="${location.latitude}"/> </p>
                    <p><b>Location Longitude</b> <c:out value="${location.longitude}"/> </p>


                    <hr/>
                </div>
                <div class ="col-xs-6"id="map"></div>
            </div>
           



        </div> 






    </div>

    <!-- Placed at the end of the document so the pages load faster -->
    <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>




</body>
</html>

