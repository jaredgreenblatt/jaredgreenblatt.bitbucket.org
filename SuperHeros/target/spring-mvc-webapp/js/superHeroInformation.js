$(document).ready(function(){
    searchGiphy();

});

function searchGiphy(){
    var searchTerms = $("#heroName").val();
   
    searchTerms = searchTerms.replace(" ", "+");
    $.ajax({
        url: 'http://api.giphy.com/v1/gifs/search?q='+searchTerms+'&api_key=dc6zaTOxFJmzC',
        type:'GET',
        headers : {
            'Accept' : 'application/json', 

        },
        'dataType' : 'json', 
        success : function(searchData){
         
            if(searchData.data.length <= 0){
                $("#gif-img-container").html($("<h1>").text("No images found. :("));
                return;
            }

      var searchIndex = Math.floor(Math.random() * 4);    
            var imgSrc = searchData.data[searchIndex].images.downsized.url;
            var newImgThing = $("<img>");
            newImgThing.attr({
                src : imgSrc
            });

            $("#gif-img-container").html(
                "<img id='heroGif' src='" + imgSrc + "' />"
            );
        }
    });
}
