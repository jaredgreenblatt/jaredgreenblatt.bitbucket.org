Hero_Villain_Super_Powersdrop  database  if exists  SuperHero;
--  Creates Database if SuperHero exists.
Create database if not exists  SuperHero;

--  Create Tables
use SuperHero;
CREATE TABLE IF NOT EXISTS `Hero_Villain` (
 `hero_villain_id` int(11) NOT NULL AUTO_INCREMENT,
 `hero_villain_name` varchar(50) NOT NULL,
 `description` varchar(250) NOT NULL,
 `isVillain` boolean NOT NULL,
 PRIMARY KEY (`hero_villain_id`)
 );
 
 
 CREATE TABLE IF NOT EXISTS `Hero_Villain_Super_Powers` (
 `hero_villain_id` int(11) NOT NULL ,
 `super_power_id` int(11) NOT NULL ,
 
 PRIMARY KEY (`hero_villain_id`,`super_power_id`)
 );
 
  CREATE TABLE IF NOT EXISTS `Super_Powers` (
 `super_power_id` int(11) NOT NULL AUTO_INCREMENT,
 `super_power_name` varchar(250) NOT NULL,
 `super_power_description` varchar(250) NOT NULL,
 
 
 PRIMARY KEY (`super_power_id`)
 );
 CREATE TABLE IF NOT EXISTS `Hero_Villain_Organizations` (
 `hero_villain_id` int(11) NOT NULL ,
 `organization_id` int(11) NOT NULL ,
 
 
 
 PRIMARY KEY (`hero_villain_id`,`organization_id`)
 );
 
 
 
   CREATE TABLE IF NOT EXISTS `Organizations` (
 `organization_id` int(11) NOT NULL AUTO_INCREMENT,
 `organization_name` varchar(250) NOT NULL,
 `organization_description` varchar(250) NOT NULL,
	`contact_information` varchar(250),
 
 
 PRIMARY KEY (`organization_id`)
 );
 
   CREATE TABLE IF NOT EXISTS `Organizations_Locations` (
`organization_id` int(11) NOT NULL,
`location_id` int(11) NOT NULL,
 
 
 

 PRIMARY KEY (`location_id`, `organization_id`)
 );
 
 
    CREATE TABLE IF NOT EXISTS `Locations` (
 `location_id` int(11) NOT NULL AUTO_INCREMENT,
 `description` varchar(250) NOT NULL,
 `city` varchar(250),
 `country` varchar(250) ,
 `latitude` decimal(10,5) ,
 `longitude` decimal(10,5) ,

 

 

 PRIMARY KEY (`location_id`)
 );
 
     CREATE TABLE IF NOT EXISTS `Sighting_Location` (
 `sighting_id` int(11) NOT NULL AUTO_INCREMENT,
 `location_id` int(11) NOT NULL,
 `date_of_sighting` date NOT NULL,
 `sighting_description` varchar(250) NOT NULL,

 PRIMARY KEY (`sighting_id`)
 );
 
  CREATE TABLE IF NOT EXISTS `Sighting_Hero_Villain` (
 `sighting_id` int(11) NOT NULL,
 `hero_villain_id` int(11) NOT NULL ,

 PRIMARY KEY (`sighting_id`,`hero_villain_id`)
 );
 

 -- Add Foreign Keys constraints

Alter Table Hero_Villain_Super_Powers
add constraint fk_hvsp_hero_villain_id
foreign key (hero_villain_id)
references Hero_Villain(hero_villain_id);
 
 
 Alter Table Hero_Villain_Super_Powers
add constraint fk_hvsp_super_power_id
foreign key (super_power_id)
references Super_Powers(super_power_id);
 
 Alter Table Hero_Villain_Organizations
add constraint fk_hvo_hero_villain_id
foreign key (hero_villain_id)
references Hero_Villain(hero_villain_id);
 


  Alter Table Hero_Villain_Organizations
add constraint fk_hvo_organization_id
foreign key (organization_id)
references Organizations(organization_id);

Alter Table Organizations_Locations
add constraint fk_orglocation_organization_id
foreign key (organization_id)
references Organizations(organization_id);

Alter Table Organizations_Locations
add constraint fk_orglocation_location_id
foreign key (location_id)
references Locations(location_id);


Alter Table Sighting_Location
add constraint fk_sighting_location_location_id
foreign key (location_id)
references Locations(location_id);

Alter Table Sighting_Hero_Villain
add constraint fk_sighting_hero_vilain_hero_villain_id
foreign key (hero_villain_id)
references Hero_Villain(hero_villain_id);
 
 Alter Table Sighting_Hero_Villain
add constraint fk_sighting_hero_vilain_sighting_id
foreign key (sighting_id)

references Sighting_Location(sighting_id);
 
 -- add data
 insert into Hero_Villain(hero_villain_name, description, isVillain)
 values ('IronMan','Super Hero Red and Gold Suit Likes to use fancy tech',false),
		('Captain America','Super Hero Red White and Blue Suit Uses Sheild' ,false),
        ('The Hulk','Super Hero Large Green Giant Likes to Smash Things' ,false),
        ('Thor','Super Hero / Norse God  Has Cape and Large Hammer' ,false),
        ('Black Widow','Super Hero Wears all Black ' ,false),
        ('HawkEye','Super Hero Wears all Black ' ,false),
		('Loki','Super Villain / Norse God  Thors Evil Brother Wears green and black' ,true);
 
  
 insert into Super_Powers(super_power_name,super_power_description)
 values ('Iron Man Suit','Iron Man Suit'),
		('Genius','Genius'),
        ('Super Strengh','Super Strengh'),
        ('Anger','Anger'),
        ('Super Speed','Super Speed'),
        ('Super Jumps','Super Jumps'),
        ('Captain America Shield','Captain America Shield'),
        ('Thors Hammer','Thors Hammer'),
        ('Flying','Flying'),
        ('Archery','Archery'),
        ('Hand To Hand Combat','Hand To Hand Combat'),
        ('Assasian','Assasian'),
		('Guns','Guns'),
        ('OdinForce','OdinForce'),
        ('Septor','Septor');
        
 insert into Hero_Villain_Super_Powers(hero_villain_id, super_power_id)
 values
 (1,1),
 (1,2),
 (1,9),
 (1,13),
 (2,6),
 (2,7),
 (2,3),
 (3,2),
 (3,3),
 (3,4),
 (3,5),
 (3,6),
 (3,11),
 (4,8),
 (4,3),
 (4,11),
 (4,9),
 (5,12),
 (5,13),
 (6,10),
 (6,11),
 (7,14),
 (7,15);
 
 
  insert into Organizations(organization_name,organization_description, contact_information)
 values
 ('Avengers','Avengers','Call or if the world is under attack!'),
 ('S.H.E.L.D','SHEILD','Call or if the world is under attack!');
 

 
 insert into Hero_Villain_Organizations(hero_villain_id,organization_id)
 values
 (1,1),
 (2,1),
 (3,1),
 (4,1),
 (5,1),
 (6,1),
 (6,2),
 (7,1);
 
 
 
 insert into Locations(description , city, country, latitude, longitude)
 values
 ('Midtown Manhattan', 'New York City', 'USA', 40.7549,73.9840),
 ('Shawarma House', 'New York City', 'USA', 40.7531,73.9846),
 ('Stuttgart Square', 'Stuttgart', 'Germany', 48.7758,9.1829),
 ('Stark Tower', 'New York City','USA',40.7549,73.9840),
 ('Forest in WhitePlanes', 'White Planes','USA',40.7549,73.9840),
 ('Floating S.H.E.I.L.D Base', null,null,null,null);
 
insert into Organizations_Locations(organization_id,location_id)
 values
 (1,4),
 (1,6),
 (2,6);
 
 
 insert into Sighting_Location(location_id , date_of_sighting, sighting_description)
 values
 (3,'2012/06/29', 'Kneel Down In Stuggart Square'),
 (1,'2012/07/01', 'Battle Of New York'),
 (2,'2012/07/02', 'Eating Shwarma'),
 (5,'2015/07/01', 'Outside of a Run Down Farm In White Planes New York.');
 
 
 
 
 
  insert into Sighting_Hero_Villain(sighting_id , hero_villain_id)
 values
  (3,1),
  (3,2),
  (3,3),
  (3,4),
  (3,5),
  (3,6),
  (2,1),
  (2,2),
  (2,3),
  (2,4),
  (2,5),
  (2,6),
  (2,7),
  (1,1),
  (1,2),
  (1,3),
  (1,4),
  (1,5),
  (1,6),
  (1,7),
  (4,3);
 

 
 
 


