use SuperHero;
-- The system must be able to report all of the superheros sighted at a particular location.
select hv.hero_villain_name from Hero_Villain hv
inner join Sighting_Hero_Villain shv
on hv.hero_villain_id = shv.hero_villain_id
inner join Sighting_Location sl
on shv.sighting_id = sl.sighting_id
inner join Locations l
on sl.location_id = l.location_id
where  l.location_id=5 ;

-- The system must be able to report all of the locations where a particular superhero has been seen.

select l.description from Locations l
inner join Sighting_Location sl
on l.location_id = sl.location_id
inner join Sighting_Hero_Villain shv
on shv.sighting_id = sl.sighting_id
inner join Hero_Villain hv
on shv.hero_villain_id = hv.hero_villain_id
where hv.hero_villain_id =3;

-- The system must be able to report all sightings (hero and location) for a particular date.
select l.description, hv.hero_villain_name from Locations l
inner join Sighting_Location sl
on l.location_id = sl.location_id
inner join Sighting_Hero_Villain shv
on shv.sighting_id = sl.sighting_id
inner join Hero_Villain hv
on shv.hero_villain_id = hv.hero_villain_id
where sl.date_of_sighting = '2015/07/01';


-- The system must be able to report all of the members of a particular organization.

select hv.hero_villain_name from Hero_Villain hv
inner join Hero_Villain_Organizations hvo
on hvo.hero_villain_id = hv.hero_villain_id
inner join Organizations o
on hvo.organization_id = o.organization_id 
where o.organization_id = 1;

-- The system must be able to report all of the organizations a particular superhero/villain belongs to.
select o.organization_description from Organizations o 
inner join Hero_Villain_Organizations hvo
on o.organization_id = hvo.organization_id
inner join Hero_Villain hv 
on hvo.hero_villain_id  = hv.hero_villain_id
where hv.hero_villain_id = 6;



-- Hero_Villain
 -- Insert
 insert into Hero_Villain(hero_villain_name, description, isVillain)
 values 
		('Captain UnderPants','Former HighSchool Principal Fights Crime in his underpants' ,false);

 -- Delete
 delete from Hero_Villain where hero_villain_id = 8;
 -- Update
   update Hero_Villain set hero_villain_name = 'Evil Captain Underpants', description ='Former HighSchool Principal who Broke Bad!' , isVillain = true 
   where hero_villain_id = 8 ;
 
 -- Select One
 select * from Hero_Villain
 where hero_villain_id = 8;
 -- Select All
 select * from Hero_Villain;
 
 -- Super_Powers
  -- Insert
  insert into Super_Powers(super_power_description)
 values
 ('Super Sonic BellyBump');

 -- Delete
 delete from  Super_Powers   where super_power_id = 16;
 -- Update
   update  Super_Powers  set super_power_description = 'SUPER DUPER PUNCH'
   where super_power_id = 16;
  
 
 -- Select One
 select * from Super_Powers
 where super_power_id = 16;
 -- Select All
 select * from Super_Powers;
 
 
 -- Hero_Villain_Super_Powers
 -- Insert
  insert into Hero_Villain_Super_Powers(hero_villain_id, super_power_id)
 values
 (8,1);

 -- Delete
 delete from  Hero_Villain_Super_Powers where hero_villain_id = 8 and super_power_id = 1;
 -- Update
   update  Hero_Villain_Super_Powers  set hero_villain_id = 8, super_power_id = 1
   where hero_villain_id = 8 
   and super_power_id = 1;
 
 -- Select One
 select * from Hero_Villain_Super_Powers
 where hero_villain_id = 8;
 -- Select All
 select * from Hero_Villain_Super_Powers;
 
 
  -- Organizations
 -- Insert
  insert into Organizations(organization_description, contact_information)
 values
 ('Justice League','Bat Signal');

 -- Delete
 delete from  Organizations where organization_id = 3;
 -- Update
   update  Organizations  set organization_description ='Legion Of Doom', contact_information = 'Screams'
  where organization_id = 3;
 
 -- Select One
 select * from Organizations
 where organization_id = 3;
 -- Select All
 select * from Organizations;
 
 
 
  -- Hero_Villain_Organizations
 -- Insert
  insert into Hero_Villain_Organizations(hero_villain_id, organization_id)
 values
 (8,3);

 -- Delete
 delete from  Hero_Villain_Organizations where organization_id = 3 and hero_villain_id = 8;
 -- Update
   update  Hero_Villain_Organizations  set organization_id = 3, hero_villain_id = 8
  where organization_id = 3 and hero_villain_id = 8;
 
 -- Select One
 select * from Hero_Villain_Organizations
  where organization_id = 3 and hero_villain_id = 8;
 -- Select All
 select * from Hero_Villain_Organizations;
 
 
 
 
   -- Locations
 -- Insert
  insert into Locations(description, city, country, latitude, longitude)
 values
 ('HighSchool','Boston', 'USA','123.00','321.00');

 -- Delete
 delete from  Locations where location_id=7;
 -- Update
   update  Locations  set description ='High School', city = 'Louisville', country ='Amurica', latitude ='0.00', longitude ='0.00'
   where location_id=7;
 
 -- Select One
 select * from Locations
  where location_id=7;
 -- Select All
 select * from Locations;
 
 
 -- Organizations_Locations
 -- Insert
  insert into Organizations_Locations(organization_id, location_id)
 values
 (3,7);

 -- Delete
 delete from  Organizations_Locations where organization_id = 3 and location_id = 7;
 -- Update
   update  Organizations_Locations  set organization_id = 3, location_id = 7
   where organization_id = 3 and location_id = 7;
 
 -- Select One
 select * from Organizations_Locations
	where organization_id = 3 and location_id = 7;
 -- Select All
 select * from Organizations_Locations;




 
 -- Sighting_Locations
 -- Insert
insert into Sighting_Location(location_id, date_of_sighting, sighting_description)
 values
 ('1','2017/04/01', 'LLAMA ATTACK!!!');

 -- Delete
 delete from  Sighting_Location where sighting_id=5;
 -- Update
   update  Sighting_Location  set location_id =1, date_of_sighting = '2017/04/01', sighting_description ='LLAMA TAKE OVER!'
   where sighting_id=5;
 
 -- Select One
 select * from Sighting_Location
  where sighting_id=5;
 -- Select All
 select * from Sighting_Location;





 -- Sighting_Hero_Villain
 -- Insert
  insert into Sighting_Hero_Villain(sighting_id, hero_villain_id)
 values
 (5,7);

 -- Delete
 delete from  Sighting_Hero_Villain where sighting_id = 5 and hero_villain_id = 7;
 -- Update
   update  Sighting_Hero_Villain  set sighting_id = 5 , hero_villain_id = 7
	where sighting_id = 5 and hero_villain_id = 7;
 
 -- Select One
 select * from Sighting_Hero_Villain
where sighting_id = 5 and hero_villain_id = 7;
 -- Select All
 select * from Sighting_Hero_Villain;
