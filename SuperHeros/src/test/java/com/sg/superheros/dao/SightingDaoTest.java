/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.superheros.dao;

import com.sg.superheros.model.HeroOrVillain;
import com.sg.superheros.model.Location;
import com.sg.superheros.model.Sighting;
import com.sg.superheros.model.SuperPower;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class SightingDaoTest {

    SightingDao dao;
    LocationDao locDao;
    HeroOrVillianSuperPowerDao hvspDao;

    public SightingDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = ctx.getBean("SightingDao", SightingDao.class);

        ApplicationContext ctxSuper
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        hvspDao = ctxSuper.getBean("HeroOrVillianSuperPowerDao", HeroOrVillianSuperPowerDao.class);

        ApplicationContext ctxLocation
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        locDao = ctx.getBean("LocationDao", LocationDao.class);

        JdbcTemplate cleaner = ctx.getBean(JdbcTemplate.class);
        cleaner.execute("delete from Hero_Villain_Organizations where 1 = 1 ");
        cleaner.execute("delete from  Hero_Villain_Super_Powers where  1= 1");
        cleaner.execute("delete from  Organizations_Locations where 1 = 1");
        cleaner.execute("delete from  Sighting_Hero_Villain where 1 = 1");
        cleaner.execute("delete from  Sighting_Location where 1=1");
        cleaner.execute("delete from Hero_Villain where 1 = 1");
        cleaner.execute("delete from  Super_Powers   where 1 = 1");
        cleaner.execute("delete from  Organizations where 1=1");
        cleaner.execute("delete from  Locations where 1=1");

    }

    @After
    public void tearDown() {
    }

    @Test
    public void testConnection() {
        dao.getAllSightings();
    }

    @Test
    public void addSuperHero() {
        SuperPower sp = new SuperPower();
        sp.setName("Gas");
        sp.setDescription("Bad Gas");

        hvspDao.addSuperPower(sp);

        HeroOrVillain hv = new HeroOrVillain();
        hv.setName("Gas Man");
        hv.setDescription("Villain Who Poisons The World With Bad Gas!");
        hv.setIsVillian(true);
        List<SuperPower> superPowers = new ArrayList<>();
        superPowers.add(sp);
        hv.setSuperPowers(superPowers);

        hvspDao.addHeroOrVillian(hv);

        HeroOrVillain fromDb = hvspDao.getHeroOrVillainById(hv.getHeroOrVillainId());
        assertEquals(hv, fromDb);

    }

    @Test
    public void testAddLocation() {
        Location l = new Location();
        l.setDescription("OHIO RIVER");
        l.setCity("Louisville");
        l.setCountry("USA");
        l.setLatitude(new BigDecimal(44.00));
        l.setLongitude(new BigDecimal(44.00));

        locDao.addLocation(l);

        Location fromDb = locDao.getLocationById(l.getLocationId());

        assertEquals(fromDb, l);

    }

    @Test
    public void testAddGetSighting() {
        Location l = new Location();
        l.setDescription("OHIO RIVER");
        l.setCity("Louisville");
        l.setCountry("USA");
        l.setLatitude(new BigDecimal(44.00));
        l.setLongitude(new BigDecimal(44.00));

        locDao.addLocation(l);
        Location locFromDB = locDao.getLocationById(l.getLocationId());
        assertEquals(locFromDB, l);
        SuperPower sp = new SuperPower();
        sp.setName("Gas");
        sp.setDescription("Bad Gas");
        hvspDao.addSuperPower(sp);

        HeroOrVillain hv = new HeroOrVillain();
        hv.setName("Gas Man");
        hv.setDescription("Villain Who Poisons The World With Bad Gas!");
        hv.setIsVillian(true);
        List<SuperPower> superPowers = new ArrayList<>();
        superPowers.add(sp);
        hv.setSuperPowers(superPowers);

        hvspDao.addHeroOrVillian(hv);
        HeroOrVillain hvspFromDB = hvspDao.getHeroOrVillainById(hv.getHeroOrVillainId());
        assertEquals(hvspFromDB, hv);

        Sighting s = new Sighting();
        s.setLocation(l);
        s.setDateOfSighting(LocalDate.parse("2010-01-01",
                DateTimeFormatter.ISO_DATE));
        s.setSightingDescription("River Battle");
        List<HeroOrVillain> hvList = new ArrayList<>();
        hvList.add(hv);
        s.setHeroOrVillains(hvList);
        dao.addSighting(s);

        Sighting fromDb = dao.getSightingById(s.getSighting_id());

        assertEquals(s, fromDb);

    }

    @Test
    public void testAddGetDeleteSighting() {
        Location l = new Location();
        l.setDescription("OHIO RIVER");
        l.setCity("Louisville");
        l.setCountry("USA");
        l.setLatitude(new BigDecimal(44.00));
        l.setLongitude(new BigDecimal(44.00));

        locDao.addLocation(l);
        Location locFromDB = locDao.getLocationById(l.getLocationId());
        assertEquals(locFromDB, l);
        SuperPower sp = new SuperPower();
        sp.setName("Gas");
        sp.setDescription("Bad Gas");
        hvspDao.addSuperPower(sp);

        HeroOrVillain hv = new HeroOrVillain();
        hv.setName("Gas Man");
        hv.setDescription("Villain Who Poisons The World With Bad Gas!");
        hv.setIsVillian(true);
        List<SuperPower> superPowers = new ArrayList<>();
        superPowers.add(sp);
        hv.setSuperPowers(superPowers);

        hvspDao.addHeroOrVillian(hv);
        HeroOrVillain hvspFromDB = hvspDao.getHeroOrVillainById(hv.getHeroOrVillainId());
        assertEquals(hvspFromDB, hv);

        Sighting s = new Sighting();
        s.setLocation(l);
        s.setDateOfSighting(LocalDate.parse("2010-01-01",
                DateTimeFormatter.ISO_DATE));
        s.setSightingDescription("River Battle");
        List<HeroOrVillain> hvList = new ArrayList<>();
        hvList.add(hv);
        s.setHeroOrVillains(hvList);
        dao.addSighting(s);

        Sighting fromDb = dao.getSightingById(s.getSighting_id());

        assertEquals(s, fromDb);

        dao.deleteSighting(s.getSighting_id());

        assertNull(dao.getSightingById(s.getSighting_id()));

    }

    @Test
    public void testGetAllSightingsByLocationIdAndDate() {
        Location l = new Location();
        l.setDescription("OHIO RIVER");
        l.setCity("Louisville");
        l.setCountry("USA");
        l.setLatitude(new BigDecimal(44.00));
        l.setLongitude(new BigDecimal(44.00));

        locDao.addLocation(l);
        Location locFromDB = locDao.getLocationById(l.getLocationId());
        assertEquals(locFromDB, l);
        SuperPower sp = new SuperPower();
        sp.setName("Gas");
        sp.setDescription("Bad Gas");
        hvspDao.addSuperPower(sp);

        HeroOrVillain hv = new HeroOrVillain();
        hv.setName("Gas Man");
        hv.setDescription("Villain Who Poisons The World With Bad Gas!");
        hv.setIsVillian(true);
        List<SuperPower> superPowers = new ArrayList<>();
        superPowers.add(sp);
        hv.setSuperPowers(superPowers);

        hvspDao.addHeroOrVillian(hv);
        HeroOrVillain hvspFromDB = hvspDao.getHeroOrVillainById(hv.getHeroOrVillainId());
        assertEquals(hvspFromDB, hv);

        Sighting s = new Sighting();
        s.setLocation(l);
        s.setDateOfSighting(LocalDate.parse("2010-01-01",
                DateTimeFormatter.ISO_DATE));
        s.setSightingDescription("River Battle");
        List<HeroOrVillain> hvList = new ArrayList<>();
        hvList.add(hv);
        s.setHeroOrVillains(hvList);
        dao.addSighting(s);

        Sighting fromDb = dao.getSightingById(s.getSighting_id());

        assertEquals(s, fromDb);

        assertEquals(dao.getAllSightingsByLocationAndDate(s.getDateOfSighting().toString(),
                s.getLocation().getLocationId()).size(), 1);
    }

    public void testGetAllSightingsByLocationId() {
        Location l = new Location();
        l.setDescription("OHIO RIVER");
        l.setCity("Louisville");
        l.setCountry("USA");
        l.setLatitude(new BigDecimal(44.00));
        l.setLongitude(new BigDecimal(44.00));

        locDao.addLocation(l);
        Location locFromDB = locDao.getLocationById(l.getLocationId());
        assertEquals(locFromDB, l);
        SuperPower sp = new SuperPower();
        sp.setName("Gas");
        sp.setDescription("Bad Gas");
        hvspDao.addSuperPower(sp);

        HeroOrVillain hv = new HeroOrVillain();
        hv.setName("Gas Man");
        hv.setDescription("Villain Who Poisons The World With Bad Gas!");
        hv.setIsVillian(true);
        List<SuperPower> superPowers = new ArrayList<>();
        superPowers.add(sp);
        hv.setSuperPowers(superPowers);

        hvspDao.addHeroOrVillian(hv);
        HeroOrVillain hvspFromDB = hvspDao.getHeroOrVillainById(hv.getHeroOrVillainId());
        assertEquals(hvspFromDB, hv);

        Sighting s = new Sighting();
        s.setLocation(l);
        s.setDateOfSighting(LocalDate.parse("2010-01-01",
                DateTimeFormatter.ISO_DATE));
        s.setSightingDescription("River Battle");
        List<HeroOrVillain> hvList = new ArrayList<>();
        hvList.add(hv);
        s.setHeroOrVillains(hvList);
        dao.addSighting(s);

        Sighting fromDb = dao.getSightingById(s.getSighting_id());

        assertEquals(s, fromDb);

        assertEquals(dao.getAllSightingsByLocations(s.getLocation().getLocationId()), 1);

    }

    public void testGetAllHerosByLocationId() {
        Location l = new Location();
        l.setDescription("OHIO RIVER");
        l.setCity("Louisville");
        l.setCountry("USA");
        l.setLatitude(new BigDecimal(44.00));
        l.setLongitude(new BigDecimal(44.00));

        locDao.addLocation(l);
        Location locFromDB = locDao.getLocationById(l.getLocationId());
        assertEquals(locFromDB, l);
        SuperPower sp = new SuperPower();
        sp.setName("Gas");
        sp.setDescription("Bad Gas");
        hvspDao.addSuperPower(sp);

        HeroOrVillain hv = new HeroOrVillain();
        hv.setName("Gas Man");
        hv.setDescription("Villain Who Poisons The World With Bad Gas!");
        hv.setIsVillian(true);
        List<SuperPower> superPowers = new ArrayList<>();
        superPowers.add(sp);
        hv.setSuperPowers(superPowers);

        hvspDao.addHeroOrVillian(hv);
        HeroOrVillain hvspFromDB = hvspDao.getHeroOrVillainById(hv.getHeroOrVillainId());
        assertEquals(hvspFromDB, hv);

        Sighting s = new Sighting();
        s.setLocation(l);
        s.setDateOfSighting(LocalDate.parse("2010-01-01",
                DateTimeFormatter.ISO_DATE));
        s.setSightingDescription("River Battle");
        List<HeroOrVillain> hvList = new ArrayList<>();
        hvList.add(hv);
        s.setHeroOrVillains(hvList);
        dao.addSighting(s);

        Sighting fromDb = dao.getSightingById(s.getSighting_id());

        assertEquals(s, fromDb);

        assertEquals(dao.getAllHerosBySightingLocation(s.getLocation().getLocationId()).size(), 1);

    }

    @Test
    public void testGetAllSightings() {
        Location l = new Location();
        l.setDescription("OHIO RIVER");
        l.setCity("Louisville");
        l.setCountry("USA");
        l.setLatitude(new BigDecimal(44.00));
        l.setLongitude(new BigDecimal(44.00));

        locDao.addLocation(l);
        Location locFromDB = locDao.getLocationById(l.getLocationId());
        assertEquals(locFromDB, l);
        SuperPower sp = new SuperPower();
        sp.setName("Gas");
        sp.setDescription("Bad Gas");
        hvspDao.addSuperPower(sp);

        HeroOrVillain hv = new HeroOrVillain();
        hv.setName("Gas Man");
        hv.setDescription("Villain Who Poisons The World With Bad Gas!");
        hv.setIsVillian(true);
        List<SuperPower> superPowers = new ArrayList<>();
        superPowers.add(sp);
        hv.setSuperPowers(superPowers);

        hvspDao.addHeroOrVillian(hv);
        HeroOrVillain hvspFromDB = hvspDao.getHeroOrVillainById(hv.getHeroOrVillainId());
        assertEquals(hvspFromDB, hv);

        Sighting s = new Sighting();
        s.setLocation(l);
        s.setDateOfSighting(LocalDate.parse("2010-01-01",
                DateTimeFormatter.ISO_DATE));
        s.setSightingDescription("River Battle");
        List<HeroOrVillain> hvList = new ArrayList<>();
        hvList.add(hv);
        s.setHeroOrVillains(hvList);
        dao.addSighting(s);

        Sighting fromDb = dao.getSightingById(s.getSighting_id());

        assertEquals(s, fromDb);

        assertEquals(dao.getAllSightings().size(), 1);
    }

    @Test
    public void testGetAllSightingsByDate() {
        Location l = new Location();
        l.setDescription("OHIO RIVER");
        l.setCity("Louisville");
        l.setCountry("USA");
        l.setLatitude(new BigDecimal(44.00));
        l.setLongitude(new BigDecimal(44.00));

        locDao.addLocation(l);
        Location locFromDB = locDao.getLocationById(l.getLocationId());
        assertEquals(locFromDB, l);
        SuperPower sp = new SuperPower();
        sp.setName("Gas");
        sp.setDescription("Bad Gas");
        hvspDao.addSuperPower(sp);

        HeroOrVillain hv = new HeroOrVillain();
        hv.setName("Gas Man");
        hv.setDescription("Villain Who Poisons The World With Bad Gas!");
        hv.setIsVillian(true);
        List<SuperPower> superPowers = new ArrayList<>();
        superPowers.add(sp);
        hv.setSuperPowers(superPowers);

        hvspDao.addHeroOrVillian(hv);
        HeroOrVillain hvspFromDB = hvspDao.getHeroOrVillainById(hv.getHeroOrVillainId());
        assertEquals(hvspFromDB, hv);

        Sighting s = new Sighting();
        s.setLocation(l);
        s.setDateOfSighting(LocalDate.parse("2010-01-01",
                DateTimeFormatter.ISO_DATE));
        s.setSightingDescription("River Battle");
        List<HeroOrVillain> hvList = new ArrayList<>();
        hvList.add(hv);
        s.setHeroOrVillains(hvList);
        dao.addSighting(s);

        Sighting fromDb = dao.getSightingById(s.getSighting_id());

        assertEquals(s, fromDb);

        assertEquals(1, dao.getAllSightingsByDate(s.getDateOfSighting().toString()).size());
    }

    @Test
    public void testSightingUpdate() {

        Location l = new Location();
        l.setDescription("OHIO RIVER");
        l.setCity("Louisville");
        l.setCountry("USA");
        l.setLatitude(new BigDecimal(44.00));
        l.setLongitude(new BigDecimal(44.00));

        locDao.addLocation(l);
        Location locFromDB = locDao.getLocationById(l.getLocationId());
        assertEquals(locFromDB, l);
        SuperPower sp = new SuperPower();
        sp.setName("Gas");
        sp.setDescription("Bad Gas");
        hvspDao.addSuperPower(sp);

        HeroOrVillain hv = new HeroOrVillain();
        hv.setName("Gas Man");
        hv.setDescription("Villain Who Poisons The World With Bad Gas!");
        hv.setIsVillian(true);
        List<SuperPower> superPowers = new ArrayList<>();
        superPowers.add(sp);
        hv.setSuperPowers(superPowers);

        hvspDao.addHeroOrVillian(hv);
        HeroOrVillain hvspFromDB = hvspDao.getHeroOrVillainById(hv.getHeroOrVillainId());
        assertEquals(hvspFromDB, hv);

        Sighting s = new Sighting();
        s.setLocation(l);
        s.setDateOfSighting(LocalDate.parse("2010-01-01",
                DateTimeFormatter.ISO_DATE));
        s.setSightingDescription("River Battle");
        List<HeroOrVillain> hvList = new ArrayList<>();
        hvList.add(hv);
        s.setHeroOrVillains(hvList);
        dao.addSighting(s);

        Sighting fromDbBefore = dao.getSightingById(s.getSighting_id());

        assertEquals(s, fromDbBefore);

        s.setSightingDescription("RIVER BATTLE FROM MARS");
        Sighting fromDbAfter = dao.getSightingById(s.getSighting_id());
        assertFalse(fromDbBefore == fromDbAfter);
        assertEquals(fromDbAfter.getSighting_id(), fromDbBefore.getSighting_id());
    }

    @Test
    public void testGetSightingByHero() {

        Location l = new Location();
        l.setDescription("OHIO RIVER");
        l.setCity("Louisville");
        l.setCountry("USA");
        l.setLatitude(new BigDecimal(44.00));
        l.setLongitude(new BigDecimal(44.00));

        locDao.addLocation(l);
        Location locFromDB = locDao.getLocationById(l.getLocationId());
        assertEquals(locFromDB, l);
        SuperPower sp = new SuperPower();
        sp.setName("Gas");
        sp.setDescription("Bad Gas");
        hvspDao.addSuperPower(sp);

        HeroOrVillain hv = new HeroOrVillain();
        hv.setName("Gas Man");
        hv.setDescription("Villain Who Poisons The World With Bad Gas!");
        hv.setIsVillian(true);
        List<SuperPower> superPowers = new ArrayList<>();
        superPowers.add(sp);
        hv.setSuperPowers(superPowers);

        hvspDao.addHeroOrVillian(hv);
        HeroOrVillain hvspFromDB = hvspDao.getHeroOrVillainById(hv.getHeroOrVillainId());
        assertEquals(hvspFromDB, hv);

        Sighting s = new Sighting();
        s.setLocation(l);
        s.setDateOfSighting(LocalDate.parse("2010-01-01",
                DateTimeFormatter.ISO_DATE));
        s.setSightingDescription("River Battle");
        List<HeroOrVillain> hvList = new ArrayList<>();
        hvList.add(hv);
        s.setHeroOrVillains(hvList);
        dao.addSighting(s);

        Sighting fromDbBefore = dao.getSightingById(s.getSighting_id());

        assertEquals(s, fromDbBefore);

        assertEquals(dao.getAllSightingsByHero(hv.getHeroOrVillainId()).size(), 1);
    }

}
