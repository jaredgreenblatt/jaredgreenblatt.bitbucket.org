/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.superheros.dao;

import com.sg.superheros.service.SightingLocationException;
import com.sg.superheros.model.Location;
import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class LocationDaoTest {

    LocationDao dao;

    public LocationDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = ctx.getBean("LocationDao", LocationDao.class);

        JdbcTemplate cleaner = ctx.getBean(JdbcTemplate.class);
        cleaner.execute("delete from Hero_Villain_Organizations where 1 = 1 ");
        cleaner.execute("delete from  Hero_Villain_Super_Powers where  1= 1");
        cleaner.execute("delete from  Organizations_Locations where 1 = 1");
        cleaner.execute("delete from  Sighting_Hero_Villain where 1 = 1");
        cleaner.execute("delete from  Sighting_Location where 1=1");
        cleaner.execute("delete from Hero_Villain where 1 = 1");
        cleaner.execute("delete from  Super_Powers   where 1 = 1");
        cleaner.execute("delete from  Organizations where 1=1");
        cleaner.execute("delete from  Locations where 1=1");

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addLocation method, of class LocationDao.
     */
    @Test
    public void testAddAndGetLocation() {
        Location l = new Location();
        l.setDescription("OHIO RIVER");
        l.setCity("Louisville");
        l.setCountry("USA");
        l.setLatitude(new BigDecimal(44.00));
        l.setLongitude(new BigDecimal(44.00));

        dao.addLocation(l);

        Location fromDb = dao.getLocationById(l.getLocationId());

        assertEquals(fromDb, l);

    }

    @Test
    public void testAddRemoveLocation() {
        Location l = new Location();
        l.setDescription("OHIO RIVER");
        l.setCity("Louisville");
        l.setCountry("USA");
        l.setLatitude(new BigDecimal(44.00));
        l.setLongitude(new BigDecimal(44.00));

        dao.addLocation(l);

        Location fromDb = dao.getLocationById(l.getLocationId());

        assertEquals(fromDb, l);

        try {
            dao.deleteLocation(l.getLocationId());
        } catch (SightingLocationException ex) {
            Logger.getLogger(LocationDaoTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertNull(dao.getLocationById(l.getLocationId()));

    }
    
    @Test 
    public void testGetAllLocations(){
        
         Location l = new Location();
        l.setDescription("OHIO RIVER");
        l.setCity("Louisville");
        l.setCountry("USA");
        l.setLatitude(new BigDecimal(44.00));
        l.setLongitude(new BigDecimal(44.00));

        dao.addLocation(l);
        
          Location l1 = new Location();
        l1.setDescription("Kentucky RIVER");
        l1.setCity("Lexington");
        l1.setCountry("USA");
        l1.setLatitude(new BigDecimal(44.00));
        l1.setLongitude(new BigDecimal(44.00));

        dao.addLocation(l1);
        
        
        assertEquals(2, dao.getAllLocations().size());
        
    }
  
       @Test
       public void getAllLocationsEmptyDb(){
           assertEquals(dao.getAllLocations().size(), 0); 
       }
       
        @Test
       public void getLocationEmptyDb(){
           assertEquals(dao.getLocationById(0), null); 
       }
       
       @Test 
       
       public void testUpdateLocation(){
            Location l = new Location();
        l.setDescription("OHIO RIVER");
        l.setCity("Louisville");
        l.setCountry("USA");
        l.setLatitude(new BigDecimal(44.00));
        l.setLongitude(new BigDecimal(44.00));

        dao.addLocation(l);

        Location fromDbBefore = dao.getLocationById(l.getLocationId());

        assertEquals(fromDbBefore, l);
        
        l.setDescription("Brown River");
        dao.updateLocation(l);
        
        Location fromDbAfter = dao.getLocationById(l.getLocationId());
           assertFalse(fromDbAfter==fromDbBefore);
           assertEquals(fromDbAfter.getLocationId(), fromDbBefore.getLocationId());
        
        
       }

    

}
