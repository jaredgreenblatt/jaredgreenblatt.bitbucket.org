/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.superheros.dao;

import com.sg.superheros.model.HeroOrVillain;
import com.sg.superheros.model.Location;
import com.sg.superheros.model.Organization;
import com.sg.superheros.model.SuperPower;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class OrganizationsDaoTest {

    OrganizationDao dao;
    LocationDao locDao;
    HeroOrVillianSuperPowerDao hvspDao;

    public OrganizationsDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = ctx.getBean("OrganizationDao", OrganizationDao.class);

        ApplicationContext ctxSuper
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        hvspDao = ctxSuper.getBean("HeroOrVillianSuperPowerDao", HeroOrVillianSuperPowerDao.class);

        ApplicationContext ctxLocation
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        locDao = ctx.getBean("LocationDao", LocationDao.class);

        JdbcTemplate cleaner = ctx.getBean(JdbcTemplate.class);
        cleaner.execute("delete from Hero_Villain_Organizations where 1 = 1 ");
        cleaner.execute("delete from  Hero_Villain_Super_Powers where  1= 1");
        cleaner.execute("delete from  Organizations_Locations where 1 = 1");
        cleaner.execute("delete from  Sighting_Hero_Villain where 1 = 1");
        cleaner.execute("delete from  Sighting_Location where 1=1");
        cleaner.execute("delete from Hero_Villain where 1 = 1");
        cleaner.execute("delete from  Super_Powers   where 1 = 1");
        cleaner.execute("delete from  Organizations where 1=1");
        cleaner.execute("delete from  Locations where 1=1");

    }

    @After
    public void tearDown() {
    }

    @Test
    public void testConnection() {
        dao.getAllOrganizations();
    }

    @Test
    public void addSuperHero() {
        SuperPower sp = new SuperPower();
        sp.setName("gas");
        sp.setDescription("Bad Gas");

        hvspDao.addSuperPower(sp);

        HeroOrVillain hv = new HeroOrVillain();
        hv.setName("Gas Man");
        hv.setDescription("Villain Who Poisons The World With Bad Gas!");
        hv.setIsVillian(true);
        List<SuperPower> superPowers = new ArrayList<>();
        superPowers.add(sp);
        hv.setSuperPowers(superPowers);

        hvspDao.addHeroOrVillian(hv);

        HeroOrVillain fromDb = hvspDao.getHeroOrVillainById(hv.getHeroOrVillainId());
        assertEquals(hv, fromDb);

    }

    @Test
    public void testAddLocation() {
        Location l = new Location();
        l.setDescription("OHIO RIVER");
        l.setCity("Louisville");
        l.setCountry("USA");
        l.setLatitude(new BigDecimal(44.00));
        l.setLongitude(new BigDecimal(44.00));

        locDao.addLocation(l);

        Location fromDb = locDao.getLocationById(l.getLocationId());

        assertEquals(fromDb, l);

    }

    @Test
    public void testAddOrganization() {

        Location l = new Location();
        l.setDescription("OHIO RIVER");
        l.setCity("Louisville");
        l.setCountry("USA");
        l.setLatitude(new BigDecimal(44.00));
        l.setLongitude(new BigDecimal(44.00));

        locDao.addLocation(l);
        Location locFromDB = locDao.getLocationById(l.getLocationId());
        assertEquals(locFromDB, l);
        SuperPower sp = new SuperPower();
        sp.setName("Gas");
        sp.setDescription("Bad Gas");
        hvspDao.addSuperPower(sp);

        HeroOrVillain hv = new HeroOrVillain();
        hv.setName("Gas Man");
        hv.setDescription("Villain Who Poisons The World With Bad Gas!");
        hv.setIsVillian(true);
        List<SuperPower> superPowers = new ArrayList<>();
        superPowers.add(sp);
        hv.setSuperPowers(superPowers);

        hvspDao.addHeroOrVillian(hv);
        HeroOrVillain hvspFromDB = hvspDao.getHeroOrVillainById(hv.getHeroOrVillainId());
        assertEquals(hvspFromDB, hv);
        Organization o = new Organization();
        o.setName("MR LOU");
        o.setDescription("Club LOOEY");
        o.setContactInformation("Drink Bourban");
        List<HeroOrVillain> hvList = new ArrayList<>();
        hvList.add(hv);
        o.setHeroOrVillains(hvList);
        List<Location> lList = new ArrayList<>();
        lList.add(l);
        o.setOrganizationLocations(lList);

        dao.addOrganization(o);

        Organization fromDb = dao.getOrganizationrById(o.getOrganizationId());
        assertEquals(o, fromDb);

    }

    @Test
    public void testAddDeleteOrganization() {

        Location l = new Location();
        l.setDescription("OHIO RIVER");
        l.setCity("Louisville");
        l.setCountry("USA");
        l.setLatitude(new BigDecimal(44.00));
        l.setLongitude(new BigDecimal(44.00));

        locDao.addLocation(l);
        Location locFromDB = locDao.getLocationById(l.getLocationId());
        assertEquals(locFromDB, l);
        SuperPower sp = new SuperPower();
        sp.setName("gas");
        sp.setDescription("Bad Gas");
        hvspDao.addSuperPower(sp);

        HeroOrVillain hv = new HeroOrVillain();
        hv.setName("Gas Man");
        hv.setDescription("Villain Who Poisons The World With Bad Gas!");
        hv.setIsVillian(true);
        List<SuperPower> superPowers = new ArrayList<>();
        superPowers.add(sp);
        hv.setSuperPowers(superPowers);

        hvspDao.addHeroOrVillian(hv);
        HeroOrVillain hvspFromDB = hvspDao.getHeroOrVillainById(hv.getHeroOrVillainId());
        assertEquals(hvspFromDB, hv);
        Organization o = new Organization();
        o.setName("MR LOU");
        o.setDescription("Club LOOEY");
        o.setContactInformation("Drink Bourban");
        List<HeroOrVillain> hvList = new ArrayList<>();
        hvList.add(hv);
        o.setHeroOrVillains(hvList);
        List<Location> lList = new ArrayList<>();
        lList.add(l);
        o.setOrganizationLocations(lList);

        dao.addOrganization(o);

        Organization fromDb = dao.getOrganizationrById(o.getOrganizationId());
        assertEquals(o, fromDb);

        dao.deleteOrganization(o.getOrganizationId());

        assertNull(dao.getOrganizationrById(o.getOrganizationId()));

    }

    @Test
    public void testGetAllOrganizations() {

        Location l = new Location();
        l.setDescription("OHIO RIVER");
        l.setCity("Louisville");
        l.setCountry("USA");
        l.setLatitude(new BigDecimal(44.00));
        l.setLongitude(new BigDecimal(44.00));

        locDao.addLocation(l);
        Location locFromDB = locDao.getLocationById(l.getLocationId());
        assertEquals(locFromDB, l);
        SuperPower sp = new SuperPower();
        sp.setName("Gas");
        sp.setDescription("Bad Gas");
        hvspDao.addSuperPower(sp);

        HeroOrVillain hv = new HeroOrVillain();
        hv.setName("Gas Man");
        hv.setDescription("Villain Who Poisons The World With Bad Gas!");
        hv.setIsVillian(true);
        List<SuperPower> superPowers = new ArrayList<>();
        superPowers.add(sp);
        hv.setSuperPowers(superPowers);

        hvspDao.addHeroOrVillian(hv);
        HeroOrVillain hvspFromDB = hvspDao.getHeroOrVillainById(hv.getHeroOrVillainId());
        assertEquals(hvspFromDB, hv);
        Organization o = new Organization();
        o.setName("MR LOU");
        o.setDescription("Club LOOEY");
        o.setContactInformation("Drink Bourban");
        List<HeroOrVillain> hvList = new ArrayList<>();
        hvList.add(hv);
        o.setHeroOrVillains(hvList);
        List<Location> lList = new ArrayList<>();
        lList.add(l);
        o.setOrganizationLocations(lList);

        dao.addOrganization(o);

        Organization fromDb = dao.getOrganizationrById(o.getOrganizationId());
        assertEquals(o, fromDb);
        assertEquals(dao.getAllOrganizations().size(), 1);
        assertEquals(dao.getAllOrganizations().contains(o), true);

    }

    @Test
    public void getAllOrganizationsByHero() {

        Location l = new Location();
        l.setDescription("OHIO RIVER");
        l.setCity("Louisville");
        l.setCountry("USA");
        l.setLatitude(new BigDecimal(44.00));
        l.setLongitude(new BigDecimal(44.00));

        locDao.addLocation(l);
        Location locFromDB = locDao.getLocationById(l.getLocationId());
        assertEquals(locFromDB, l);
        SuperPower sp = new SuperPower();
        sp.setName("gas");
        sp.setDescription("Bad Gas");
        hvspDao.addSuperPower(sp);

        HeroOrVillain hv = new HeroOrVillain();
        hv.setName("Gas Man");
        hv.setDescription("Villain Who Poisons The World With Bad Gas!");
        hv.setIsVillian(true);
        List<SuperPower> superPowers = new ArrayList<>();
        superPowers.add(sp);
        hv.setSuperPowers(superPowers);

        hvspDao.addHeroOrVillian(hv);
        HeroOrVillain hvspFromDB = hvspDao.getHeroOrVillainById(hv.getHeroOrVillainId());
        assertEquals(hvspFromDB, hv);
        Organization o = new Organization();
        o.setName("MR LOU");
        o.setDescription("Club LOOEY");
        o.setContactInformation("Drink Bourban");
        List<HeroOrVillain> hvList = new ArrayList<>();
        hvList.add(hv);
        o.setHeroOrVillains(hvList);
        List<Location> lList = new ArrayList<>();
        lList.add(l);
        o.setOrganizationLocations(lList);

        dao.addOrganization(o);

        assertEquals(dao.getAllOrganizationsByHero(hv.getHeroOrVillainId()).size(), 1);

    }

    @Test
    public void getAllOrganizationsEmptyDb() {
        assertEquals(dao.getAllOrganizations().size(), 0);
    }

    @Test
    public void getOrganizationsEmptyDb() {
        assertEquals(dao.getOrganizationrById(0), null);
    }

    @Test
    public void testUpdateOrganization() {

        Location l = new Location();
        l.setDescription("OHIO RIVER");
        l.setCity("Louisville");
        l.setCountry("USA");
        l.setLatitude(new BigDecimal(44.00));
        l.setLongitude(new BigDecimal(44.00));

        locDao.addLocation(l);
        Location locFromDB = locDao.getLocationById(l.getLocationId());
        assertEquals(locFromDB, l);
        SuperPower sp = new SuperPower();
        sp.setName("Gas");
        sp.setDescription("Bad Gas");
        hvspDao.addSuperPower(sp);

        HeroOrVillain hv = new HeroOrVillain();
        hv.setName("Gas Man");
        hv.setDescription("Villain Who Poisons The World With Bad Gas!");
        hv.setIsVillian(true);
        List<SuperPower> superPowers = new ArrayList<>();
        superPowers.add(sp);
        hv.setSuperPowers(superPowers);

        hvspDao.addHeroOrVillian(hv);
        HeroOrVillain hvspFromDB = hvspDao.getHeroOrVillainById(hv.getHeroOrVillainId());
        assertEquals(hvspFromDB, hv);
        Organization o = new Organization();
        o.setName("MR LOU");
        o.setDescription("Club LOOEY");
        o.setContactInformation("Drink Bourban");
        List<HeroOrVillain> hvList = new ArrayList<>();
        hvList.add(hv);
        o.setHeroOrVillains(hvList);
        List<Location> lList = new ArrayList<>();
        lList.add(l);
        o.setOrganizationLocations(lList);

        dao.addOrganization(o);

        Organization fromDbBefore = dao.getOrganizationrById(o.getOrganizationId());
        assertEquals(o, fromDbBefore);

        o.setContactInformation("Drink a ship ton of Bourbon");
        dao.updateOrganization(o);

        Organization fromDbAfter = dao.getOrganizationrById(o.getOrganizationId());

        assertFalse(fromDbAfter == fromDbBefore);
        assertEquals(fromDbAfter.getOrganizationId(), fromDbBefore.getOrganizationId());

    }

    @Test
    public void tesNoHeroOrganization() {

        Location l = new Location();
        l.setDescription("OHIO RIVER");
        l.setCity("Louisville");
        l.setCountry("USA");
        l.setLatitude(new BigDecimal(44.00));
        l.setLongitude(new BigDecimal(44.00));

        locDao.addLocation(l);
        Location locFromDB = locDao.getLocationById(l.getLocationId());
        assertEquals(locFromDB, l);
        SuperPower sp = new SuperPower();
        sp.setName("Gas");
        sp.setDescription("Bad Gas");
        hvspDao.addSuperPower(sp);

        

        Organization o = new Organization();
        o.setName("MR LOU");
        o.setDescription("Club LOOEY");
        o.setContactInformation("Drink Bourban");

        List<Location> lList = new ArrayList<>();
        lList.add(l);
        o.setOrganizationLocations(lList);
        List<HeroOrVillain> hvList = new ArrayList<>();
       
        o.setHeroOrVillains(hvList);

        dao.addOrganization(o);

        Organization fromDbBefore = dao.getOrganizationrById(o.getOrganizationId());
        assertEquals(o, fromDbBefore);

    }
    
        @Test
    public void testNoLocationNoHeroOrganization() {

        

        

        Organization o = new Organization();
        o.setName("MR LOU");
        o.setDescription("Club LOOEY");
        o.setContactInformation("Drink Bourban");

        List<Location> lList = new ArrayList<>();
     
        o.setOrganizationLocations(lList);
        List<HeroOrVillain> hvList = new ArrayList<>();
       
        o.setHeroOrVillains(hvList);

        dao.addOrganization(o);

        Organization fromDbBefore = dao.getOrganizationrById(o.getOrganizationId());
        assertEquals(o, fromDbBefore);

    }

}
