/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.superheros.dao;

import com.sg.superheros.model.HeroOrVillain;
import com.sg.superheros.model.SuperPower;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 *
 * @author apprentice
 */
public class HeroOrVillianSuperPowerDaoTest {

    HeroOrVillianSuperPowerDao dao;

    public HeroOrVillianSuperPowerDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = ctx.getBean("HeroOrVillianSuperPowerDao", HeroOrVillianSuperPowerDao.class);

        JdbcTemplate cleaner = ctx.getBean(JdbcTemplate.class);
        cleaner.execute("delete from Hero_Villain_Organizations where 1 = 1 ");
        cleaner.execute("delete from  Hero_Villain_Super_Powers where  1= 1");
        cleaner.execute("delete from  Organizations_Locations where 1 = 1");
        cleaner.execute("delete from  Sighting_Hero_Villain where 1 = 1");
        cleaner.execute("delete from  Sighting_Location where 1=1");
        cleaner.execute("delete from Hero_Villain where 1 = 1");
        cleaner.execute("delete from  Super_Powers   where 1 = 1");
        cleaner.execute("delete from  Organizations where 1=1");
        cleaner.execute("delete from  Locations where 1=1");

    }

    @After
    public void tearDown() {
    }

    @Test
    public void AddSuperPower() {
        SuperPower sp = new SuperPower();
        sp.setName("Gas");
        sp.setDescription("Bad Gas");

        dao.addSuperPower(sp);

        SuperPower fromDb = dao.getSuperPowerById(sp.getSuperPowerId());

        assertEquals(sp, fromDb);
    }

    @Test
    public void AddDeleteSuperPower() {
        SuperPower sp = new SuperPower();
        sp.setName("Gas");
        sp.setDescription("Bad Gas");

        dao.addSuperPower(sp);

        SuperPower fromDb = dao.getSuperPowerById(sp.getSuperPowerId());

        assertEquals(sp, fromDb);
        dao.deleteSuperPower(sp.getSuperPowerId());
        assertNull(dao.getSuperPowerById(sp.getSuperPowerId()));
    }

    @Test
    public void getAllSuperPowers() {
        dao.getAllHeroOrVillains();

    }

    @Test
    public void addSuperHero() {
        SuperPower sp = new SuperPower();
        sp.setName("Gas");
        sp.setDescription("Bad Gas");

        dao.addSuperPower(sp);

        HeroOrVillain hv = new HeroOrVillain();
        hv.setName("Gas Man");
        hv.setDescription("Villain Who Poisons The World With Bad Gas!");
        hv.setIsVillian(true);
        List<SuperPower> superPowers = new ArrayList<>();
        superPowers.add(sp);
        hv.setSuperPowers(superPowers);

        dao.addHeroOrVillian(hv);

        HeroOrVillain fromDb = dao.getHeroOrVillainById(hv.getHeroOrVillainId());
        assertEquals(hv, fromDb);

    }

    @Test
    public void addSuperHero2Powers() {
        SuperPower sp1 = new SuperPower();
        sp1.setName("Gas");

        sp1.setDescription("Bad Gas");

        dao.addSuperPower(sp1);
        SuperPower sp2 = new SuperPower();
        sp2.setName("Wedgies");
        sp2.setDescription("Atomic Wedgies");
        dao.addSuperPower(sp2);

        HeroOrVillain hv = new HeroOrVillain();
        hv.setName("Gas Man");
        hv.setDescription("Villain Who Poisons The World With Bad Gas!");
        hv.setIsVillian(true);
        List<SuperPower> superPowers = new ArrayList<>();
        superPowers.add(sp1);
        superPowers.add(sp2);
        hv.setSuperPowers(superPowers);

        dao.addHeroOrVillian(hv);

        HeroOrVillain fromDb = dao.getHeroOrVillainById(hv.getHeroOrVillainId());
        assertEquals(hv, fromDb);

    }

    @Test
    public void AddDeleteHeroVillain() {
        SuperPower sp1 = new SuperPower();
        sp1.setName("gas");
        sp1.setDescription("Bad Gas");

        dao.addSuperPower(sp1);
        SuperPower sp2 = new SuperPower();
        sp2.setName("Wedgies");
        sp2.setDescription("Atomic Wedgies");
        dao.addSuperPower(sp2);

        HeroOrVillain hv = new HeroOrVillain();
        hv.setName("Gas Man");
        hv.setDescription("Villain Who Poisons The World With Bad Gas!");
        hv.setIsVillian(true);
        List<SuperPower> superPowers = new ArrayList<>();
        superPowers.add(sp1);
        superPowers.add(sp2);
        hv.setSuperPowers(superPowers);

        dao.addHeroOrVillian(hv);

        HeroOrVillain fromDb = dao.getHeroOrVillainById(hv.getHeroOrVillainId());
        assertEquals(hv, fromDb);

        dao.deleteHeroOrVillian(hv.getHeroOrVillainId());
        assertNull(dao.getHeroOrVillainById(hv.getHeroOrVillainId()));

    }

    @Test

    public void getAllHeroVillain() {

        SuperPower sp1 = new SuperPower();
        sp1.setName("Gas");
        sp1.setDescription("Bad Gas");

        dao.addSuperPower(sp1);
        SuperPower sp2 = new SuperPower();
        sp2.setName("Wedgies");
        sp2.setDescription("Atomic Wedgies");
        dao.addSuperPower(sp2);

        HeroOrVillain hv = new HeroOrVillain();
        hv.setName("Gas Man");
        hv.setDescription("Villain Who Poisons The World With Bad Gas!");
        hv.setIsVillian(true);
        List<SuperPower> superPowers = new ArrayList<>();
        superPowers.add(sp1);
        superPowers.add(sp2);
        hv.setSuperPowers(superPowers);

        dao.addHeroOrVillian(hv);

        assertEquals(dao.getAllHeroOrVillains().size(), 1);

    }

    @Test

    public void getSuperPowers() {

        SuperPower sp1 = new SuperPower();
        sp1.setName("Gas");
        sp1.setDescription("Bad Gas");

        dao.addSuperPower(sp1);
        SuperPower sp2 = new SuperPower();
        sp2.setName("Wedgie");
        sp2.setDescription("Atomic Wedgies");
        dao.addSuperPower(sp2);

        assertEquals(dao.getAllSuperPowers().size(), 2);

    }

    @Test
    public void getAllSuperPowersEmptyDb() {
        assertEquals(dao.getAllSuperPowers().size(), 0);
    }

    @Test
    public void getAllHerosVillainsEmptyDb() {
        assertEquals(dao.getAllHeroOrVillains().size(), 0);
    }

    @Test
    public void getSuperPowersEmptyDb() {
        assertEquals(dao.getSuperPowerById(0), null);
    }

    @Test
    public void getHeroVillainEmptyDb() {
        assertEquals(dao.getHeroOrVillainById(0), null);
    }

    @Test

    public void testUpdateSuperPower() {
        SuperPower sp = new SuperPower();
        sp.setName("Gas");
        sp.setDescription("Bad Gas");

        dao.addSuperPower(sp);

        SuperPower fromDbBeforeUpdate = dao.getSuperPowerById(sp.getSuperPowerId());

        assertEquals(sp, fromDbBeforeUpdate);
        sp.setDescription("Really Bad Gas");
        dao.updateSuperPower(sp);

        SuperPower fromDbAfterUpdate = dao.getSuperPowerById(sp.getSuperPowerId());
        assertEquals(sp, fromDbAfterUpdate);

        assertFalse(fromDbBeforeUpdate == fromDbAfterUpdate);
        assertTrue(fromDbAfterUpdate.getSuperPowerId() == fromDbBeforeUpdate.getSuperPowerId());

    }

    @Test
    public void testUpdateSuperHero() {
        SuperPower sp = new SuperPower();
        sp.setName("Gas");
        sp.setDescription("Bad Gas");

        dao.addSuperPower(sp);

        HeroOrVillain hv = new HeroOrVillain();
        hv.setName("Gas Man");
        hv.setDescription("Villain Who Poisons The World With Bad Gas!");
        hv.setIsVillian(true);
        List<SuperPower> superPowers = new ArrayList<>();
        superPowers.add(sp);
        hv.setSuperPowers(superPowers);

        dao.addHeroOrVillian(hv);

        HeroOrVillain fromDbBeforeSuperHeroUpdate = dao.getHeroOrVillainById(hv.getHeroOrVillainId());
        assertEquals(hv, fromDbBeforeSuperHeroUpdate);

        SuperPower fromDbBeforeUpdate = dao.getSuperPowerById(sp.getSuperPowerId());

        assertEquals(sp, fromDbBeforeUpdate);
        sp.setDescription("Really Bad Gas");
        dao.updateSuperPower(sp);

        SuperPower fromDbAfterUpdate = dao.getSuperPowerById(sp.getSuperPowerId());
        assertEquals(sp, fromDbAfterUpdate);

        assertFalse(fromDbBeforeUpdate == fromDbAfterUpdate);
        assertTrue(fromDbAfterUpdate.getSuperPowerId() == fromDbBeforeUpdate.getSuperPowerId());

        hv.setName("Batman");
        dao.updateHeroOrVillian(hv);

        HeroOrVillain fromDbSuperHeroAfterUpdate = dao.getHeroOrVillainById(hv.getHeroOrVillainId());
        assertFalse(fromDbSuperHeroAfterUpdate == fromDbBeforeSuperHeroUpdate);
    }

}
