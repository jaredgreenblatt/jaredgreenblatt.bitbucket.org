/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.superheros.dao;

import com.sg.superheros.model.HeroOrVillain;
import com.sg.superheros.model.Organization;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface OrganizationDao {

    public void addOrganization(Organization organization);

    public void deleteOrganization(int organizationId);

    public void updateOrganization(Organization organization);

    public Organization getOrganizationrById(int organizationId);

    public List<Organization> getAllOrganizations();

  
    public List<Organization> getAllOrganizationsByHero(int heroOrVillainId);

}
