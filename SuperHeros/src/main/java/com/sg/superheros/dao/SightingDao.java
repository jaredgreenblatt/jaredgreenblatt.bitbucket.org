/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.superheros.dao;

import com.sg.superheros.model.HeroOrVillain;
import com.sg.superheros.model.Sighting;
import java.time.LocalDate;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface SightingDao {

    public void addSighting(Sighting sighting);

    public void deleteSighting(int sightingId);

    public void updateSighting(Sighting sighting);

    public Sighting getSightingById(int sightingId);

    public List<Sighting> getAllSightings();

    public List<Sighting> getAllSightingsByLocationAndDate(String sightingDate, int locationId);

    public List<Sighting> getAllSightingsByLocations(int locationId);

    public List<Sighting> getAllSightingsByDate(String sightingDate);

    public List<HeroOrVillain> getAllHerosBySightingLocation(int locationId);

    public List<Sighting> getAllSightingsByHero(int heroId);

    public List<Sighting> getLast10Sightings();

}
