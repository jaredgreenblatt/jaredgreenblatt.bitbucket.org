/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.superheros.dao;

import com.sg.superheros.mapper.HeroOrVillainMapper;
import com.sg.superheros.mapper.SuperPowerMapper;
import com.sg.superheros.model.HeroOrVillain;
import com.sg.superheros.model.SuperPower;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class HeroOrVillianSuperPowerDaoJdbcTemplateImpl implements HeroOrVillianSuperPowerDao {

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    // *******************************************************************************
    // Prepared Statement for Inserting values into the super hero powers bridge table
    // *******************************************************************************
    private static final String SQL_INSERT_HEROS_VILLAINS_SUPER_POWERS
            = "insert into "
            + "Hero_Villain_Super_Powers(hero_villain_id, super_power_id) "
            + "values (?,?);";

    /**
     * Method Used to run the prepared statement to insert the values into the
     * HEROS_VILLAINS_SUPER_POWERS bridge table. It uses an enhanced for loop
     * and final variables to properly set the relationships between the
     * superhero and there power.
     *
     * @param heroOrVillain
     */
    private void insertSuperPowers(HeroOrVillain heroOrVillain) {
        final int heroOrVillainId = heroOrVillain.getHeroOrVillainId();
        final List<SuperPower> superPowers = heroOrVillain.getSuperPowers();

        //update the HEROS_VILLAINS_SUPER_POWERS with an entry for each of the heroes superPowers
        for (SuperPower currentSuperPower : superPowers) {
            jdbcTemplate.update(SQL_INSERT_HEROS_VILLAINS_SUPER_POWERS,
                    heroOrVillainId,
                    currentSuperPower.getSuperPowerId());
        }
    }

    // *******************************************************************************
    // Prepared Statement for Getting all of the Powers Associated with Each Hero/Villain.
    // *******************************************************************************
    private static final String SQL_GET_ALL_POWERS_BY_HERO
            = "Select sp.super_power_id ,sp.super_power_name, sp.super_power_description"
            + " from Super_Powers sp left join Hero_Villain_Super_Powers hvsp"
            + " on sp.super_power_id = hvsp.super_power_id left join Hero_Villain hv"
            + " on hvsp.hero_villain_id = hv.hero_villain_id where hv.hero_villain_id  =?; ";

    /**
     * findSuperPowersByHeroOrVillain is the method used to find the superPowers
     * that associate to a specific hero/vilain based on whats passed in as a
     * parameter. It then maps that superpower data using the datamapper.
     *
     * @param heroOrVillain
     * @return
     */
    private List<SuperPower> findSuperPowersByHeroOrVillain(HeroOrVillain heroOrVillain) {
        return jdbcTemplate.query(SQL_GET_ALL_POWERS_BY_HERO,
                new SuperPowerMapper(),
                heroOrVillain.getHeroOrVillainId());
    }

    /**
     * associateSuperPowersWithHeroes is used to associate super powers with the
     * the Hero object. it goes through each hero and the runs the
     * findSuperPowersByHeroOrVillain helper method and associates the power
     * with the hero/villain
     *
     * @param heroOrVillainList
     * @return
     */
    private List<HeroOrVillain>
            associateSuperPowersWithHeroes(List<HeroOrVillain> heroOrVillainList) {
        // set the complete list of superPowers ids for each hero.
        for (HeroOrVillain currentHeroOrVillain : heroOrVillainList) {
            // add authors to current book
            currentHeroOrVillain.setSuperPowers(findSuperPowersByHeroOrVillain(currentHeroOrVillain));

        }
        return heroOrVillainList;
    }

    // *******************************************************************************
    // Prepared Statement to insert a  SuperPower
    // *******************************************************************************
    private static final String SQL_INSERT_SUPER_POWER
           ="insert into Super_Powers(super_power_name,super_power_description) values (?, ?);";

    /**
     * addSuperPower is a method used to insert a new super power. It inserts a
     * description of a power and then gets back the new id from the database.
     * This is wrapped in a Transactional tag so that if either of the database
     * interactions fail the change will be rolled back.
     *
     * @param superPower
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void addSuperPower(SuperPower superPower) {
        jdbcTemplate.update(SQL_INSERT_SUPER_POWER,
                superPower.getName(),
                superPower.getDescription());

        int superPowerId
                = jdbcTemplate.queryForObject("select LAST_INSERT_ID()",
                        Integer.class);
        superPower.setSuperPowerId(superPowerId);
    }

    // *******************************************************************************
    // Prepared Statement to remove a  SuperPower
    // *******************************************************************************
    private static final String SQL_REMOVE_SUPER_POWER
            = "delete from  Super_Powers   where super_power_id = ?;";

    private static final String SQL_REMOVE_SUPER_POWER_FROM_BRIDGE_TABLE
            = "delete from  Hero_Villain_Super_Powers where super_power_id = ?;";

    /**
     * deleteSuperPower is the method used to delete a superpower. This method
     * is Transactional because to delete a super power we need to remove it
     * from two tables. The Bridge and the superpowers table. We cannot simply
     * delete the power as there is a chance a hero is linked to it. So we first
     * need to delete any connections to that power in the bridge table.
     *
     * @param superPowerId
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void deleteSuperPower(int superPowerId) {
        jdbcTemplate.update(SQL_REMOVE_SUPER_POWER_FROM_BRIDGE_TABLE, superPowerId);
        jdbcTemplate.update(SQL_REMOVE_SUPER_POWER, superPowerId);

    }

    // *******************************************************************************
    // Prepared Statement to update a  SuperPower
    // *******************************************************************************
    private static final String SQL_UPDATE_SUPER_POWER
            = "update  Super_Powers  set super_power_name = ?, super_power_description = ? "
            + "where super_power_id = ?;";

    /**
     * updateSuperPower is used to update a super power.
     *
     * @param superPower
     */
    @Override
    public void updateSuperPower(SuperPower superPower) {
        jdbcTemplate.update(SQL_UPDATE_SUPER_POWER,
                superPower.getName(),
                superPower.getDescription(),
                superPower.getSuperPowerId());
    }
    // *******************************************************************************
    // Prepared Statement to get a SuperPower by ID.
    // *******************************************************************************
    private static final String SQL_GET_SUPER_POWER_BY_ID
            = "select * from Super_Powers where super_power_id = ?;";

    /**
     * getSuperPowerById is used to find a specific SuperPower when given an ID.
     * It then maps the super power into the rowmapper. This is wrapped in a try
     * catch as we used queryForObject which only returns one item. If 0 items
     * are returned it will throw an EmptyResultDataAccessException.
     *
     * @param superPowerId
     * @return
     */
    @Override
    public SuperPower getSuperPowerById(int superPowerId) {
        try {
            return jdbcTemplate.queryForObject(SQL_GET_SUPER_POWER_BY_ID,
                    new SuperPowerMapper(),
                    superPowerId);

        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }
    // *******************************************************************************
    // Prepared Statement to get all SuperPowers
    // *******************************************************************************

    private static final String SQL_GET_ALL_SUPER_POWERS
            = "select * from Super_Powers";

    /**
     * getAllSuperPowers is used to return all the superPowers avaliable in the
     * database.
     *
     * @return
     */
    @Override
    public List<SuperPower> getAllSuperPowers() {
        return jdbcTemplate.query(SQL_GET_ALL_SUPER_POWERS, new SuperPowerMapper());
    }

    // *******************************************************************************
    // Prepared Statement to INSERT A NEW HERO_OR_VILLAIN
    // *******************************************************************************
    private static final String SQL_INSERT_HERO_OR_VILLAIN
            = "insert into Hero_Villain(hero_villain_name, description, isVillain)"
            + " values (?,?,?);";

    /**
     * addHeroOrVillian is the method used to insert a new superHero and and
     * then associate the powers with the superhero. This is a transactional
     * method because the id of the new hero is grabbed from the database after
     * the initial information is inserted.
     *
     * @param heroOrVillian
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void addHeroOrVillian(HeroOrVillain heroOrVillian) {
        // first insert into Heroes_Villains table and get newly generated heroid
        jdbcTemplate.update(SQL_INSERT_HERO_OR_VILLAIN,
                heroOrVillian.getName(),
                heroOrVillian.getDescription(),
                heroOrVillian.getIsVillian());

        int heroOrVillainId
                = jdbcTemplate.queryForObject("select LAST_INSERT_ID()",
                        Integer.class);
        heroOrVillian.setHeroOrVillainId(heroOrVillainId);
        // now update the hero_villains_superPower table
        insertSuperPowers(heroOrVillian);

    }

    // *******************************************************************************
    // Prepared Statement to remove a  HERO OR VILLAIN
    // *******************************************************************************
    private static final String SQL_REMOVE_HERO_OR_VILLAIN
            = " delete from Hero_Villain where hero_villain_id = ?;";
    private static final String SQL_REMOVE_HERO_OR_VILLAIN_FROM_SUPER_POWER_BRIDGE_TABLE
            = "delete from  Hero_Villain_Super_Powers where hero_villain_id = ?;";

    private static final String SQL_REMOVE_HERO_OR_VILLAIN_FROM_ORG_BRIDGE_TABLE
            = "delete from  Hero_Villain_Organizations where hero_villain_id = ?;";
    private static final String SQL_REMOVE_HERO_OR_VILLAIN_FROM_SIGHTING_BRIDGE_TABLE
            = "delete from  Sighting_Hero_Villain where hero_villain_id = ?;";

    /**
     * deleteHeroOrVillian is the method used to delete a HeroOrVillain. This
     * method is Transactional because to delete a HeroOrVillain we need to
     * remove it from several tables. The HeroOrVillain reference will need to
     * be deleted from all Bridge tables. We cannot simply just delete the
     * HeroOrVillain from the main table as all the references to it will not
     * allow it. We first need to delete all the references to the HeroOrVillain
     * in the bridge tables.
     *
     *
     * @param heroOrVillainId
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void deleteHeroOrVillian(int heroOrVillainId) {
        jdbcTemplate.update(SQL_REMOVE_HERO_OR_VILLAIN_FROM_SIGHTING_BRIDGE_TABLE, heroOrVillainId);
        jdbcTemplate.update(SQL_REMOVE_HERO_OR_VILLAIN_FROM_ORG_BRIDGE_TABLE, heroOrVillainId);
        jdbcTemplate.update(SQL_REMOVE_HERO_OR_VILLAIN_FROM_SUPER_POWER_BRIDGE_TABLE, heroOrVillainId);
        jdbcTemplate.update(SQL_REMOVE_HERO_OR_VILLAIN, heroOrVillainId);
    }
    // *******************************************************************************
    // Prepared Statement to UPDATE a  HERO OR VILLAIN and REMOVE SUPERPOWERS
    // *******************************************************************************

    private static final String SQL_UPDATE_HERO_OR_VILLAIN
            = "   update Hero_Villain "
            + "set hero_villain_name = ?, description =? , isVillain = ?"
            + " where hero_villain_id = ? ;";

    private static final String SQL_DELETE_SUPER_POWER_HERO_VILLAIN
            = "delete from  Hero_Villain_Super_Powers where hero_villain_id =?;";

    /**
     * updateHeroOrVillian first will make any changes to the hero table that
     * have been made. It then deletes the superpowers for the specific hero and
     * then resets the relationship by calling the insertSuperPowers method.
     *
     * @param heroOrVillian
     */
    @Override
    public void updateHeroOrVillian(HeroOrVillain heroOrVillian) {
        jdbcTemplate.update(SQL_UPDATE_HERO_OR_VILLAIN,
                heroOrVillian.getName(),
                heroOrVillian.getDescription(),
                heroOrVillian.getIsVillian(),
                heroOrVillian.getHeroOrVillainId());

        // delete SUPER_POWER_HERO_VILLAIN relationships and then reset them
        jdbcTemplate.update(SQL_DELETE_SUPER_POWER_HERO_VILLAIN, heroOrVillian.getHeroOrVillainId());
        insertSuperPowers(heroOrVillian);
    }
    
    // *******************************************************************************
    // Prepared Statement to GET a  HERO OR VILLAIN by Hero_VillainID
    // *******************************************************************************


    private static final String SQL_GET_HERO_OR_VILLAIN_BY_ID
            = "select * from Hero_Villain where hero_villain_id = ?;";
    
    /**
     *  getHeroOrVillainById is used to find a specific heroOrVillain when given an ID.
     * It then maps the  heroOrVillain into the rowmapper. This is wrapped in a try
     * catch as we used queryForObject which only returns one item. If 0 items
     * are returned it will throw an EmptyResultDataAccessException.
     * After a hero is gotten by the query we set the super powers for the
     * heroOrVillain by calling the findSuperPowersByHeroOrVillain and passing it
     * the heroOrVillain.
     * @param heroOrVillainId
     * @return 
     */

    @Override
    public HeroOrVillain getHeroOrVillainById(int heroOrVillainId) {
        try {
            HeroOrVillain heroOrVillain
                    = jdbcTemplate.queryForObject(SQL_GET_HERO_OR_VILLAIN_BY_ID,
                            new HeroOrVillainMapper(),
                            heroOrVillainId);
            // get the Authors for this book and set list on the book

            heroOrVillain.setSuperPowers(findSuperPowersByHeroOrVillain(heroOrVillain));

            return heroOrVillain;

        } catch (EmptyResultDataAccessException ex) {
            return null;
        }

    }
    // *******************************************************************************
    // Prepared Statement to GET all  HERO OR VILLAINS.
    // *******************************************************************************

    private static final String SQL_GET_ALL_HERO_OR_VILLAINS
            = "select * from Hero_Villain;";

    /**
     * 
     */
    @Override
    public List<HeroOrVillain> getAllHeroOrVillains() {
        List<HeroOrVillain> heroOrVillains = jdbcTemplate.query(SQL_GET_ALL_HERO_OR_VILLAINS, new HeroOrVillainMapper());

        return associateSuperPowersWithHeroes(heroOrVillains);
//return jdbcTemplate.query(SQL_GET_ALL_HERO_OR_VILLAINS, new HeroOrVillianMapper());

    }

//    private static final String SQL_GET_ALL_POWERS_BY_HERO_OR_VILLAINS
//            = "Select sp.super_power_id as id , sp.super_power_description as description"
//            + " from Super_Powers sp left join Hero_Villain_Super_Powers hvsp"
//            + " on sp.super_power_id = hvsp.super_power_id left join Hero_Villain hv"
//            + " on hvsp.hero_villain_id = hv.hero_villain_id where hv.hero_villain_id  =?; ";
//
//    @Override
//    public List<SuperPower> getHeroOrVillainsSuperPowers(HeroOrVillain heroOrVillian) {
//        return jdbcTemplate.query(SQL_GET_ALL_POWERS_BY_HERO_OR_VILLAINS, new SuperPowerMapper(), heroOrVillian.getheroOrVillainId());
//    }
}
