/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.superheros.dao;

import com.sg.superheros.mapper.HeroOrVillainMapper;
import com.sg.superheros.mapper.SuperPowerMapper;
import com.sg.superheros.mapper.SightingMapper;
import com.sg.superheros.mapper.LocationRowMapper;
import com.sg.superheros.model.HeroOrVillain;
import com.sg.superheros.model.Location;
import com.sg.superheros.model.Sighting;
import com.sg.superheros.model.SuperPower;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class SightingDaoJdbcTemplateImpl implements SightingDao {

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    // *******************************************************************************
    // Prepared Statement to insert into sightings_hero_villains
    // *******************************************************************************

    private static final String SQL_INSERT_SIGHTINGS_HEROS_VILLAINS
            = " insert into"
            + " Sighting_Hero_Villain(sighting_id, hero_villain_id)"
            + " values (?,?);";

    /**
     * Method Used to run the prepared statement to insert the values into the
     * SIGHTINGS_HEROS_VILLAINS bridge table. It uses an enhanced for loop and
     * final variables to properly set the relationships between the superhero
     * and there power.
     *
     * @param sighting
     */
    private void insertSightingsHerosOrVillains(Sighting sighting) {
        final int SIGHTING_ID = sighting.getSighting_id();
        final List<HeroOrVillain> HERO_OR_VILLAIN = sighting.getHeroOrVillains();

        // update the SIGHTING_HERO_VILLAIN
        for (HeroOrVillain currentHeroOrVillain : HERO_OR_VILLAIN) {
            jdbcTemplate.update(SQL_INSERT_SIGHTINGS_HEROS_VILLAINS,
                    SIGHTING_ID,
                    currentHeroOrVillain.getHeroOrVillainId());
        }

    }
    // *******************************************************************************
    // Prepared Statement to SELECT LOCATION BY SIGHTING ID
    // *******************************************************************************

    private static final String SQL_SELECT_LOCATION_BY_SIGHTING_ID
            = "select l.location_id,l.description, l.city , l.country, "
            + "l.latitude, l.longitude from Locations l"
            + " inner join Sighting_Location sl"
            + " on l.location_id = sl.location_id"
            + " where sighting_id =?;";

    /**
     * Method used to run the SELECT LOCATION BY SIGHTING ID prepared statement.
     * it then maps the location data.
     *
     * @param sighting
     * @return
     */
    private Location findLocationBySighting(Sighting sighting) {
        return jdbcTemplate.queryForObject(SQL_SELECT_LOCATION_BY_SIGHTING_ID,
                new LocationRowMapper(),
                sighting.getSighting_id());
    }
    // *******************************************************************************
    // Prepared Statement to SELECT HEROS BY SIGHTING ID
    // *******************************************************************************
    private static final String SQL_GET_ALL_HEROS_BY_SIGHTING
            = "select hv.hero_villain_id ,hv.hero_villain_name,hv.description, "
            + "hv.isVillain "
            + "from Hero_Villain hv "
            + "inner join Sighting_Hero_Villain shv "
            + "on hv.hero_villain_id = shv.hero_villain_id "
            + "inner join Sighting_Location sl "
            + "on shv.sighting_id = sl.sighting_id "
            + "where  sl.sighting_id =? ;";

    /**
     * Method used to run SELECT HEROS BY SIGHTING ID prepared statement. It
     * then maps the hero data into the hero object and then runs it through the
     * helper method to associate the super powers to the hero.
     *
     * @param sighting
     * @return
     */
    private List<HeroOrVillain> findHeroOrVillainsBySighting(Sighting sighting) {
        List<HeroOrVillain> heroOrVillainsList = jdbcTemplate.query(SQL_GET_ALL_HEROS_BY_SIGHTING,
                new HeroOrVillainMapper(),
                sighting.getSighting_id());

        return associateSuperPowersWithHeroes(heroOrVillainsList);

    }

    /**
     * associatHerosAndLocationsWithSightings Is used to associate SuperHeros
     * and Locations to the sighting. It runs through a loop of all the
     * sightings and then sets the heros and locations by passing them through
     * findHeroOrVillainsBySighting and findLocationBySighting helper methods.
     *
     * @param sightingList
     * @return
     */
    private List<Sighting>
            associatHerosAndLocationsWithSightings(List<Sighting> sightingList) {
        // set the complete list of Heroes and Location ids for each sighting.
        for (Sighting currentSighting : sightingList) {
            // add Heroes to current Sighting
            currentSighting.setHeroOrVillains(findHeroOrVillainsBySighting(currentSighting));
            // Add Location to Current Sighting
            currentSighting.setLocation(findLocationBySighting(currentSighting));
        }
        return sightingList;

    }
    // *******************************************************************************
    // Prepared Statement for getting all powers by heros.
    // *******************************************************************************

    private static final String SQL_GET_ALL_POWERS_BY_HERO
            = "Select sp.super_power_id , sp.super_power_name, sp.super_power_description"
            + " from Super_Powers sp left join Hero_Villain_Super_Powers hvsp"
            + " on sp.super_power_id = hvsp.super_power_id left join Hero_Villain hv"
            + " on hvsp.hero_villain_id = hv.hero_villain_id where hv.hero_villain_id  =?; ";

    /**
     * Helper Method used to Find a HeroOrVillain superpower.
     *
     * @param heroOrVillain
     * @return
     */
    private List<SuperPower> findSuperPowersByHeroOrVillain(HeroOrVillain heroOrVillain) {
        return jdbcTemplate.query(SQL_GET_ALL_POWERS_BY_HERO,
                new SuperPowerMapper(),
                heroOrVillain.getHeroOrVillainId());
    }

    /**
     * Helper method used to associate a HERO with there superpower
     *
     * @param heroOrVillainList
     * @return
     */
    private List<HeroOrVillain>
            associateSuperPowersWithHeroes(List<HeroOrVillain> heroOrVillainList) {
        // set the complete list of superPowers ids for each hero.
        for (HeroOrVillain currentHeroOrVillain : heroOrVillainList) {
            // add authors to current book
            currentHeroOrVillain.setSuperPowers(findSuperPowersByHeroOrVillain(currentHeroOrVillain));

        }
        return heroOrVillainList;
    }

    // *******************************************************************************
    // Prepared Statement for Inserting values into Sighting b
    // *******************************************************************************
    private static final String SQL_INSERT_SIGHTING
            = " insert into Sighting_Location"
            + "(location_id, date_of_sighting, sighting_description) "
            + "values (?,?,?);";

    /**
     * * addSighting is a method used to insert a new sighting. It inserts a
     * locationId, Date and description of a sighting and then gets back the new
     * id from the database. This is wrapped in a Transactional tag so that if
     * either of the database interactions fail the change will be rolled back.
     *
     * @param sighting
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)

    public void addSighting(Sighting sighting) {
        jdbcTemplate.update(SQL_INSERT_SIGHTING,
                sighting.getLocation().getLocationId(),
                sighting.getDateOfSighting().toString(),
                sighting.getSightingDescription());

        int sightingID
                = jdbcTemplate.queryForObject("select LAST_INSERT_ID()",
                        Integer.class);
        sighting.setSighting_id(sightingID);

        // add heros or villains
        insertSightingsHerosOrVillains(sighting);
    }

    // *******************************************************************************
    // Prepared Statement to remove a  Sighting
    // *******************************************************************************
//    private static final String SQL_DELETE_LOCATION
//            = " delete from  Locations where location_id=?;";
    private static final String SQL_DELETE_SIGHTING_HERO_VILLAIN
            = "delete from  Sighting_Hero_Villain where sighting_id = ?;";
    private static final String SQL_DELETE_SIGHTING_LOCATION
            = "delete from  Sighting_Location where sighting_id=?;";

    /**
     * deleteSighting is the method used to delete a Sighting. This method is
     * Transactional because to delete a Sighting we need to remove it from the
     * bridge tables. After those are deleted we can delete the sighting this is
     * wrapped in a transaction tag so if one of the deletes fails it will roll
     * back the change.
     *
     * @param sightingId
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void deleteSighting(int sightingId) {
//        jdbcTemplate.update(SQL_DELETE_LOCATION, sightingId);
        jdbcTemplate.update(SQL_DELETE_SIGHTING_HERO_VILLAIN, sightingId);
        jdbcTemplate.update(SQL_DELETE_SIGHTING_LOCATION, sightingId);
    }
    // *******************************************************************************
    // Prepared Statement to update a SIGHTING
    // *******************************************************************************

    private static final String SQL_UPDATE_SIGHTING_LOCATION
            = "update  Sighting_Location "
            + "set location_id =?, date_of_sighting = ?, sighting_description =? "
            + "where sighting_id=?;";

    /**
     * Method used to update a sighting. It first updates the sighting
     * information it then deletes the entries for it in the sighting hero
     * bridge table and then resets the relationships.
     *
     * @param sighting
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void updateSighting(Sighting sighting) {
        jdbcTemplate.update(SQL_UPDATE_SIGHTING_LOCATION,
                sighting.getLocation().getLocationId(),
                sighting.getDateOfSighting().toString(),
                sighting.getSightingDescription(),
                sighting.getSighting_id());

        // delete  relationships and then reset them
        jdbcTemplate.update(SQL_DELETE_SIGHTING_HERO_VILLAIN, sighting.getSighting_id());
        insertSightingsHerosOrVillains(sighting);
    }

    // *******************************************************************************
    // Prepared Statement to get a Sighting by ID.
    // *******************************************************************************
    private static final String SQL_GET_SIGHTING_LOCATION_BY_ID
            = "select * from Sighting_Location where sighting_id=?;";

    /**
     * getSightingById is used to find a specific Sighting when given an ID. It
     * then maps the sighting into the rowmapper. This is wrapped in a try catch
     * as we used queryForObject which only returns one item. If 0 items are
     * returned it will throw an EmptyResultDataAccessException.
     *
     * Also this method is used to associate the location and the Hero to the
     * specific sighting.
     *
     * @param sightingId
     * @return
     */
    @Override
    public Sighting getSightingById(int sightingId) {
        try {
            Sighting sighting
                    = jdbcTemplate.queryForObject(SQL_GET_SIGHTING_LOCATION_BY_ID,
                            new SightingMapper(),
                            sightingId);
            // get the SuperHeros for this Sighting and set list on the sighting
            sighting.setHeroOrVillains(findHeroOrVillainsBySighting(sighting));

            // set the Location for the sighting
            sighting.setLocation(findLocationBySighting(sighting));

            return sighting;

        } catch (EmptyResultDataAccessException ex) {
            return null;

        }

    }
    // *******************************************************************************
    // Prepared Statement to get all Sightings
    // *******************************************************************************

    private static final String SQL_GET_ALL_SIGHTING_LOCATIONS
            = "select * from Sighting_Location;";

    /**
     * getAllSightings is used to get all sightings from the database. We then
     * pass this through the associatHerosAndLocationsWithSightings so that the
     * object is mapped correctly from the database.
     *
     * @return
     */
    @Override
    public List<Sighting> getAllSightings() {
        List<Sighting> sighting = jdbcTemplate.query(SQL_GET_ALL_SIGHTING_LOCATIONS, new SightingMapper());

        return associatHerosAndLocationsWithSightings(sighting);
    }
    // *******************************************************************************
    // Prepared Statement to get all Sightings by location and Date
    // *******************************************************************************

    private static final String SQL_GET_ALL_SUPERHEROS_BY_LOCATION_AND_DATE
            = "select"
            + " sl.sighting_id, sl.location_id, sl.date_of_sighting, sl.sighting_description"
            + "  from Hero_Villain hv "
            + "inner join Sighting_Hero_Villain shv "
            + "on hv.hero_villain_id = shv.hero_villain_id "
            + "inner join Sighting_Location sl "
            + "on shv.sighting_id = sl.sighting_id "
            + "inner join Locations l "
            + "on sl.location_id = l.location_id "
            + "where  sl.location_id=? "
            + "and sl.date_of_sighting =?;";

    /**
     * getAllSightingsByDate is used to get all sightings from the database That
     * are from the specified date and locationid. We then pass this through the
     * associatHerosAndLocationsWithSightings so that the object is mapped
     * correctly from the database.
     *
     * @param sightingDate
     * @param locationId
     * @return
     */
    @Override
    public List<Sighting> getAllSightingsByLocationAndDate(String sightingDate, int locationId) {
        List<Sighting> sighting = jdbcTemplate.query(SQL_GET_ALL_SUPERHEROS_BY_LOCATION_AND_DATE,
                new SightingMapper(),
                locationId,
                sightingDate);

        return associatHerosAndLocationsWithSightings(sighting);
    }

    // *******************************************************************************
    // Prepared Statement to get all Sightings by location 
    // *******************************************************************************
    private static final String SQL_SELECT_ALL_SIGHTINGS_BY_LOCATION
            = "select sl.sighting_id, sl.location_id, sl.date_of_sighting, sl.sighting_description "
            + "from Locations l "
            + "inner join Sighting_Location sl "
            + "on l.location_id = sl.location_id "
            + "inner join Sighting_Hero_Villain shv"
            + " on shv.sighting_id = sl.sighting_id "
            + "inner join Hero_Villain hv "
            + "on shv.hero_villain_id = hv.hero_villain_id where hv.hero_villain_id = ?;";

    /**
     *
     * getAllSightingsByLocations is used to get all sightings from the database
     * That are from the specified locationid. We then pass this through the
     * associatHerosAndLocationsWithSightings so that the object is mapped
     * correctly from the database.
     *
     * @param locationId
     * @return
     */
    @Override
    public List<Sighting> getAllSightingsByLocations(int locationId) {
        List<Sighting> sighting = jdbcTemplate.query(SQL_SELECT_ALL_SIGHTINGS_BY_LOCATION,
                new SightingMapper(),
                locationId);

        return associatHerosAndLocationsWithSightings(sighting);
    }

    // *******************************************************************************
    // Prepared Statement to GET ALL HERO_VILLAINS BY LOCATION
    // *******************************************************************************
    private static final String SQL_GET_ALL_HEROS_BY_SIGHTING_LOCATION
            = "select hv.hero_villain_id ,hv.hero_villain_name,hv.description, hv.isVillain "
            + "from Hero_Villain hv "
            + "inner join Sighting_Hero_Villain shv"
            + " on hv.hero_villain_id = shv.hero_villain_id"
            + " inner join Sighting_Location sl"
            + " on shv.sighting_id = sl.sighting_id "
            + "inner join Locations l "
            + "on sl.location_id = l.location_id"
            + " where  l.location_id=? ;";

    /**
     *
     * getAllHerosBySightingLocation is used to get all Heros from the database
     * That are from the specified locationid. We then pass this through the
     * associatHerosAndLocationsWithSightings so that the object is mapped
     * correctly from the database.
     *
     * @param locationId
     * @return
     */
    @Override
    public List<HeroOrVillain> getAllHerosBySightingLocation(int locationId) {
        List<HeroOrVillain> heroOrVillains = jdbcTemplate.query(SQL_GET_ALL_HEROS_BY_SIGHTING_LOCATION,
                new HeroOrVillainMapper(),
                locationId);

        return associateSuperPowersWithHeroes(heroOrVillains);
    }

    // *******************************************************************************
    // Prepared Statement to GET ALL HERO_VILLAINS BY DATE
    // *******************************************************************************
    private static final String SQL_SELECT_ALL_SIGHTING_BY_DATE
            = "select"
            + " sl.sighting_id, sl.location_id, sl.date_of_sighting, sl.sighting_description"
            + " from Locations l "
            + "inner join Sighting_Location sl "
            + "on l.location_id = sl.location_id "
            + "inner join Sighting_Hero_Villain shv"
            + " on shv.sighting_id = sl.sighting_id "
            + "inner join Hero_Villain hv"
            + " on shv.hero_villain_id = hv.hero_villain_id "
            + "where sl.date_of_sighting = ?;";

    /**
     * getAllSightingsByLocations is used to get all sightings from the database
     * That are from the specified date. We then pass this through the
     * associatHerosAndLocationsWithSightings so that the object is mapped
     * correctly from the database.
     *
     * @param sightingDate
     * @return
     */
    @Override
    public List<Sighting> getAllSightingsByDate(String sightingDate) {
        List<Sighting> sighting = jdbcTemplate.query(SQL_SELECT_ALL_SIGHTING_BY_DATE,
                new SightingMapper(),
                sightingDate
        );

        return associatHerosAndLocationsWithSightings(sighting);
    }
    private static final String SQL_SELECT_ALL_SIGHTING_BY_HERO
            = "select"
            + " sl.sighting_id, sl.location_id, sl.date_of_sighting, sl.sighting_description"
            + " from Locations l "
            + "inner join Sighting_Location sl "
            + "on l.location_id = sl.location_id "
            + "inner join Sighting_Hero_Villain shv"
            + " on shv.sighting_id = sl.sighting_id "
            + "inner join Hero_Villain hv"
            + " on shv.hero_villain_id = hv.hero_villain_id "
            + "where hv.hero_villain_id = ?;";

    @Override
    public List<Sighting> getAllSightingsByHero(int heroId) {
        List<Sighting> sighting = jdbcTemplate.query(SQL_SELECT_ALL_SIGHTING_BY_HERO,
                new SightingMapper(),
                heroId);

        return associatHerosAndLocationsWithSightings(sighting);
    }

    private static final String SQL_GET_LAST_10_SIGHTINGS
            = "select * from Sighting_Location order by sighting_id desc limit 10;";

    @Override
    public List<Sighting> getLast10Sightings() {
        List<Sighting> sighting = jdbcTemplate.query(SQL_GET_LAST_10_SIGHTINGS,
                new SightingMapper());
        return associatHerosAndLocationsWithSightings(sighting);
    }

}
