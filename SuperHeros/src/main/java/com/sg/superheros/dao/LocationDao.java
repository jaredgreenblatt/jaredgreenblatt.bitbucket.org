/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.superheros.dao;

import com.sg.superheros.service.SightingLocationException;
import com.sg.superheros.model.Location;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface LocationDao {
    
    public void addLocation(Location location);
    
    public void deleteLocation(int locationId)throws SightingLocationException;
    
    public void updateLocation(Location location);
    
    public Location getLocationById(int locationId);
    
    public List<Location> getAllLocations();
}
