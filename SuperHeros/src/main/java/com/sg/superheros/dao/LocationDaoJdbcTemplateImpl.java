/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.superheros.dao;

import com.sg.superheros.service.SightingLocationException;
import com.sg.superheros.mapper.LocationRowMapper;
import com.sg.superheros.model.Location;
import java.util.List;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class LocationDaoJdbcTemplateImpl implements LocationDao {

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    // *******************************************************************************
    // Prepared Statement to insert a  Location
    // *******************************************************************************
    private static final String SQL_INSERT_LOCATION
            = "insert into Locations(description, city, country, latitude, longitude)values(?,?,?,?,?);";

    /**
     * addLocation is a method used to insert a new location. It inserts a
     * description, city,country, latitude and the longitude of a location and
     * then gets back the new id from the database. This is wrapped in a
     * Transactional tag so that if either of the database interactions fail the
     * change will be rolled back.
     *
     * @param location
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void addLocation(Location location) {
        jdbcTemplate.update(SQL_INSERT_LOCATION,
                location.getDescription(),
                location.getCity(),
                location.getCountry(),
                location.getLatitude(),
                location.getLongitude());

        int locationId
                = jdbcTemplate.queryForObject("select LAST_INSERT_ID()",
                        Integer.class);
        location.setLocationId(locationId);
    }

    // *******************************************************************************
    // Prepared Statement to remove a  Location
    // *******************************************************************************
    private static final String SQL_DELETE_LOCATION_FROM_ORG_BRIDGE
            = "delete from  Organizations_Locations where location_id = ?;";
    private static final String SQL_DELETE_LOCATION_FROM_SIGHTING
            = "delete from  Sighting_Location where location_id=?;";
    private static final String SQL_DELETE_LOCATION
            = "delete from  Locations where location_id=?;";

    /**
     * deleteLocation is the method used to delete a Location. This method is
     * Transactional because to delete a Location we need to remove it from any
     * references to it in any other tables. This is wrapped in a transactional
     * tag so if any of the deletes fail the changes will be rolled back.
     *
     * @param locationId
     * @throws com.sg.superheros.service.SightingLocationException
     * @throws com.sg.superheros.dao.SightingLocationException
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void deleteLocation(int locationId) throws SightingLocationException {
        try {
            jdbcTemplate.update(SQL_DELETE_LOCATION_FROM_SIGHTING, locationId);
            jdbcTemplate.update(SQL_DELETE_LOCATION_FROM_ORG_BRIDGE, locationId);
            jdbcTemplate.update(SQL_DELETE_LOCATION, locationId);

        }catch(DataIntegrityViolationException e){
            throw new SightingLocationException("You cannot Delete this location."
                    + "It is assoicated with a super hero sighting.",e);
        }

    }
    // *******************************************************************************
    // Prepared Statement to update a  Location
    // *******************************************************************************

    private static final String SQL_UPDATE_LOCATION
            = "   update  Locations "
            + " set description =?, city = ?, country =?, latitude =?, "
            + "longitude =?"
            + " where location_id=?;";

    /**
     * updateLocation is used to update the location. It will post any changes
     * to the location to the database.
     *
     * @param location
     */
    @Override
    public void updateLocation(Location location) {
        jdbcTemplate.update(SQL_UPDATE_LOCATION,
                location.getDescription(),
                location.getCity(),
                location.getCountry(),
                location.getLatitude(),
                location.getLongitude(),
                location.getLocationId());
    }
// *******************************************************************************
    // Prepared Statement to get a Location by ID.
    // *******************************************************************************

    private static final String SQL_GET_LOCATION_BY_ID
            = " select * from Locations where location_id=?;";

    /**
     * getLocationById is used to find a specific Location when given an ID. It
     * then maps the location into the row mapper. This is wrapped in a try
     * catch as we used queryForObject which only returns one item. If 0 items
     * are returned it will throw an EmptyResultDataAccessException.
     *
     * @param locationId
     * @return
     */
    @Override
    public Location getLocationById(int locationId) {
        try {
            return jdbcTemplate.queryForObject(SQL_GET_LOCATION_BY_ID,
                    new LocationRowMapper(),
                    locationId);

        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }
    // *******************************************************************************
    // Prepared Statement to get all Locations
    // *******************************************************************************

    private static final String SQL_GET_ALL_LOCATIONS
            = "select * from Locations;";

    /**
     * getAllLocation is used to return all the locations avaliable in the
     * database.
     *
     * @return
     */
    @Override
    public List<Location> getAllLocations() {
        return jdbcTemplate.query(SQL_GET_ALL_LOCATIONS, new LocationRowMapper());
    }

}
