/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.superheros.dao;

import com.sg.superheros.mapper.HeroOrVillainMapper;
import com.sg.superheros.mapper.SuperPowerMapper;
import com.sg.superheros.mapper.OrganizationMapper;
import com.sg.superheros.mapper.LocationRowMapper;
import com.sg.superheros.model.HeroOrVillain;
import com.sg.superheros.model.Location;
import com.sg.superheros.model.Organization;
import com.sg.superheros.model.SuperPower;
import java.util.ArrayList;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class OrganizationDaoJdbcTemplateImpl implements OrganizationDao {



    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;

    }
    // *******************************************************************************
    // Prepared Statement to INSERT ORGANIZATIONS_HEROS_VILLAINS
    // *******************************************************************************

    private static final String SQL_INSERT_ORGANIZATIONS_HEROS_VILLAINS
            = "insert into "
            + "Hero_Villain_Organizations(hero_villain_id, organization_id) "
            + "values (?,?);";

    /**
     * insertOrganizationsHerosOrVillains is used to create the bridge table
     * connections for the SuperHero And Organization.
     *
     * @param organization
     */
    private void insertOrganizationsHerosOrVillains(Organization organization) {
        final int ORG_ID = organization.getOrganizationId();
        final List<HeroOrVillain> heroOrVillain = organization.getHeroOrVillains();

        //update the HEROS_VILLAINS_ORGANIZATIONS with an entry for each of the Organizations Members.
        for (HeroOrVillain currentHeroOrVillain : heroOrVillain) {
            jdbcTemplate.update(SQL_INSERT_ORGANIZATIONS_HEROS_VILLAINS,
                    currentHeroOrVillain.getHeroOrVillainId(),
                    ORG_ID);

        }

    }
    // *******************************************************************************
    // Prepared Statement to INSERT ORGANIZATIONS_LOCATIONS
    // *******************************************************************************

    private static final String SQL_INSERT_ORGANIZATIONS_LOCATIONS
            = "insert into"
            + " Organizations_Locations(organization_id, location_id)"
            + " values(?,?);";

    /**
     * insertOrganizationsLocations is used to create the bridge table for
     * organization location.
     *
     * @param organization
     */
    private void insertOrganizationsLocations(Organization organization) {
        final int ORG_ID = organization.getOrganizationId();
        final List<Location> location = organization.getOrganizationLocations();

        //update the HEROS_VILLAINS_ORGANIZATIONS with an entry for each of the location.
        for (Location currentLocations : location) {
            jdbcTemplate.update(SQL_INSERT_ORGANIZATIONS_LOCATIONS,
                    ORG_ID,
                    currentLocations.getLocationId());
        }

    }

    // *******************************************************************************
    // Prepared Statement to GET_ALL_LOCATIONS_By_ORG
    // *******************************************************************************
    private static final String SQL_GET_ALL_LOCATIONS_By_ORG
            = " select"
            + " l.location_id, l.description, l.city, l.country, l.latitude, l.longitude "
            + "from Locations l"
            + " inner join Organizations_Locations ol"
            + " on l.location_id = ol.location_id"
            + " inner join Organizations o "
            + "on ol.organization_id= o.organization_id"
            + " where o.organization_id =?;";

    /**
     * findLocationsByOrg is used to find all locations associated to an
     * organization.
     *
     * @param organization
     * @return
     */
    private List<Location> findLocationsByOrg(Organization organization) {
         List<Location>  locationList = jdbcTemplate.query(SQL_GET_ALL_LOCATIONS_By_ORG, new LocationRowMapper(),
                organization.getOrganizationId());
        return locationList;
    }

    /**
     * associatLocationsWithOrganization Is used to associate Locations with the
     * organization this allows us to repersent a list of locations as part of
     * our organization dto.
     *
     * @param organizationList
     * @return
     */
    private List<Organization>
            associatLocationsWithOrganization(List<Organization> organizationList) {
        // set the complete list of locations ids for each organization.
        for (Organization currentOrganization : organizationList) {
            // add locations to current organization
            currentOrganization.setOrganizationLocations(findLocationsByOrg(currentOrganization));

        }
        return organizationList;
    }

    // *******************************************************************************
    // Prepared Statement to  GET ALL_HEROS_BY_ORG
    // *******************************************************************************
    private static final String SQL_GET_ALL_HEROS_BY_ORG
            = "select hv.hero_villain_id ,hv.hero_villain_name,hv.description, hv.isVillain"
            + " from Hero_Villain hv"
            + " inner join Hero_Villain_Organizations hvo"
            + " on hvo.hero_villain_id = hv.hero_villain_id "
            + "inner join Organizations o "
            + "on hvo.organization_id = o.organization_id "
            + "where o.organization_id = ?;";

    /**
     * findsHeroOrVillainByOrg is used to find all the hero's assoicated with
     * the organization.
     *
     * @param organization
     * @return
     */
    private List<HeroOrVillain> findsHeroOrVillainByOrg(Organization organization) {
           List<HeroOrVillain> heroOrVillainsList = jdbcTemplate.query(SQL_GET_ALL_HEROS_BY_ORG, new HeroOrVillainMapper(),
                organization.getOrganizationId());

        return associateSuperPowersWithHeroes(heroOrVillainsList);
    }

    /**
     * associatHeroesWithOrganization is used to associate HEROS with the
     * organization this allows us to represent a list of heros as part of our
     * Hero dto.
     *
     * @param organizationList
     * @return
     */
    private List<Organization>
            associatHeroesWithOrganization(List<Organization> organizationList) {
        for (Organization currentOrganization : organizationList) {
            currentOrganization.setHeroOrVillains(findsHeroOrVillainByOrg(currentOrganization));

        }
        return organizationList;
    }
    // *******************************************************************************
    // Prepared Statement to GET_ALL_POWERS_BY_HERO
    // *******************************************************************************

    private static final String SQL_GET_ALL_POWERS_BY_HERO
            = "Select sp.super_power_id ,sp.super_power_name, sp.super_power_description"
            + " from Super_Powers sp left join Hero_Villain_Super_Powers hvsp"
            + " on sp.super_power_id = hvsp.super_power_id left join Hero_Villain hv"
            + " on hvsp.hero_villain_id = hv.hero_villain_id where hv.hero_villain_id  =?; ";

    /**
     * findSuperPowersByHeroOrVillain is used to find all of the superPowers
     * associated with the hero.
     *
     * @param heroOrVillain
     * @return
     */
    private List<SuperPower> findSuperPowersByHeroOrVillain(HeroOrVillain heroOrVillain) {
        return jdbcTemplate.query(SQL_GET_ALL_POWERS_BY_HERO,
                new SuperPowerMapper(),
                heroOrVillain.getHeroOrVillainId());
    }

    /**
     * associateSuperPowersWithHeroes is used to map all of the superpowers into
     * the Hero Object.
     *
     * @param heroOrVillainList
     * @return
     */
    private List<HeroOrVillain>
                    
                    
            associateSuperPowersWithHeroes(List<HeroOrVillain> heroOrVillainList) {
        // set the complete list of superPowers ids for each hero.
        for (HeroOrVillain currentHeroOrVillain : heroOrVillainList) {
            // add authors to current book
            currentHeroOrVillain.setSuperPowers(findSuperPowersByHeroOrVillain(currentHeroOrVillain));

        }
        return heroOrVillainList;
    }

    // *******************************************************************************
    // Prepared Statement to INSERT A NEW Organization
    // *******************************************************************************
    private static final String SQL_INSERT_ORGANIZATIONS
            = "   insert into "
            + "Organizations(organization_name, organization_description, contact_information)"
            + " value (?,?,?);";

    // Get All Organizations a Hero Belongs too
    /**
     * addOrganization is the method used to insert a new Organization and and
     * then grab the id from the database. Then it also inserts the hero and
     * location that are assoiciated to the This is a transactional method
     * because there are several query's being run. If any fail the system will
     * roll back the insert.
     *
     * @param organization
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void addOrganization(Organization organization) {
        // first insert into Organizations table and get newly generated org id.
        jdbcTemplate.update(SQL_INSERT_ORGANIZATIONS,
                organization.getName(),
                organization.getDescription(),
                organization.getContactInformation());

        int organizationID
                = jdbcTemplate.queryForObject("select LAST_INSERT_ID()",
                        Integer.class);
        organization.setOrganizationId(organizationID);

        // add heros or villains
        insertOrganizationsHerosOrVillains(organization);

        // add locations
        insertOrganizationsLocations(organization);
    }

    // *******************************************************************************
    // Prepared Statement to Remove A Organization
    // *******************************************************************************
    private static final String SQL_REMOVE_ORGANIZATION
            = "delete from  Organizations where organization_id = ?;";

    private static final String SQL_REMOVE_ORGANIZATION_LOCATION
            = "delete from  Organizations_Locations where organization_id = ?;";

    private static final String SQL_REMOVE_ORGANIZATION_HERO
            = "delete from  Hero_Villain_Organizations where organization_id = ?;";

    /**
     * deleteOrganization is a transactional method that runs all of the deletes
     * needed to remove an organization. If any of the removes fail it will roll
     * back the change.
     *
     * @param organizationId
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void deleteOrganization(int organizationId) {
        jdbcTemplate.update(SQL_REMOVE_ORGANIZATION_HERO, organizationId);
        jdbcTemplate.update(SQL_REMOVE_ORGANIZATION_LOCATION, organizationId);
        jdbcTemplate.update(SQL_REMOVE_ORGANIZATION, organizationId);
    }

    // *******************************************************************************
    // Prepared Statement to Update an Organization
    // *******************************************************************************
    private static final String SQL_UPDATE_ORGANIZATION
            = "   update  Organizations "
            + " set organization_name =?, organization_description =?, contact_information = ? "
            + "where organization_id = ?;";

    /**
     * updateOrganization is used to update an organization. It then deletes the
     * bridge connection between location and hero and then resets them.
     *
     * @param organization
     */
    @Override
    public void updateOrganization(Organization organization) {
        jdbcTemplate.update(SQL_UPDATE_ORGANIZATION,
                organization.getName(),
                organization.getDescription(),
                organization.getContactInformation(),
                organization.getOrganizationId());

        // delete SUPER_POWER_HERO_VILLAIN relationships and then reset them
        jdbcTemplate.update(SQL_REMOVE_ORGANIZATION_HERO, organization.getOrganizationId());
        insertOrganizationsHerosOrVillains(organization);
        jdbcTemplate.update(SQL_REMOVE_ORGANIZATION_LOCATION, organization.getOrganizationId());
        insertOrganizationsLocations(organization);

    }

    // *******************************************************************************
    // Prepared Statement to GET_ORG_BY_ID
    // *******************************************************************************
    private static final String SQL_GET_ORG_BY_ID
            = "select * from Organizations where organization_id = ?;";

    /**
     * getOrganizationrById is used to get an org by id. It is wrapped in a try
     * catch because we are expecting to get an item back. If we don't it fails.
     * It then sets the hero and location into the org dto. By running the find
     * helper methods.
     *
     * @param organizationId
     * @return
     */
    @Override
    public Organization getOrganizationrById(int organizationId) {
        try {
            Organization organization
                    = jdbcTemplate.queryForObject(SQL_GET_ORG_BY_ID,
                            new OrganizationMapper(),
                            organizationId);

            organization.setHeroOrVillains(findsHeroOrVillainByOrg(organization));
            organization.setOrganizationLocations(findLocationsByOrg(organization));

            return organization;

        } catch (EmptyResultDataAccessException ex) {
            return null;

        }
    }
    // *******************************************************************************
    // Prepared Statement to GET_ALL_ORGS
    // *******************************************************************************

    private static final String SQL_GET_ALL_ORGS
            = " select * from Organizations";

    /**
     * getAllOrganizations just returns all of the organizations in the db.
     *
     * @return
     */
    @Override
    public List<Organization> getAllOrganizations() {
        List<Organization> organizations = jdbcTemplate.query(SQL_GET_ALL_ORGS, new OrganizationMapper());
        associatHeroesWithOrganization(organizations);
        associatLocationsWithOrganization(organizations);
        return organizations;

    }

    /**
     * getAllOrganizationsHerosOrVillains is used to get all organizations it
     * then associates the hero with the organization by using the
     * associatHeroesWithOrganization.
     *
     * @return
     */
    // *******************************************************************************
    // Prepared Statement to GET ALL_ORGANIZATIONS_BY_HERO
    // *******************************************************************************
    private static final String SQL_GET_ALL_ORGANIZATIONS_BY_HERO
            = "select o.organization_id, o.organization_name,  o.organization_description, o.contact_information"
            + " from Organizations o "
            + "inner join Hero_Villain_Organizations hvo"
            + " on o.organization_id = hvo.organization_id "
            + "inner join Hero_Villain hv "
            + "on hvo.hero_villain_id  = hv.hero_villain_id "
            + "where hv.hero_villain_id = ?;";

    /**
     * Return a list of Organizations that a specific HeroOrVillain belongs to.
     *
     * @param heroOrVillainId
     * @return
     */
    @Override
    public List<Organization> getAllOrganizationsByHero(int heroOrVillainId) {
        return jdbcTemplate.query(SQL_GET_ALL_ORGANIZATIONS_BY_HERO,
                new OrganizationMapper(),
                heroOrVillainId);
    }

}
