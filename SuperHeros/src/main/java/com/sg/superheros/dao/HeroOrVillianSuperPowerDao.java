/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.superheros.dao;

import com.sg.superheros.model.HeroOrVillain;
import com.sg.superheros.model.SuperPower;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface HeroOrVillianSuperPowerDao {
    
    public void addSuperPower(SuperPower superPower);
    
    public void deleteSuperPower(int superPowerId);
    
    public void updateSuperPower(SuperPower superPower);
    
    public SuperPower getSuperPowerById(int superPowerId);
    
    public List<SuperPower> getAllSuperPowers();
    
    public void addHeroOrVillian(HeroOrVillain heroOrVillian);
    
    public void deleteHeroOrVillian(int heroOrVillainId);
    
    public void updateHeroOrVillian(HeroOrVillain heroOrVillian);
    
    public HeroOrVillain getHeroOrVillainById(int heroOrVillianId);
    
    public List<HeroOrVillain> getAllHeroOrVillains();
    
//    public List<SuperPower> getHeroOrVillainsSuperPowers(HeroOrVillain heroOrVillian);
//    
    
}
