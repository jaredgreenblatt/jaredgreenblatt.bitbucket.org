/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.superheros.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author apprentice
 */
public class Sighting {
    private int sighting_id;
    private Location location;
    private LocalDate dateOfSighting;
    private String sightingDescription;
    private List<HeroOrVillain> heroOrVillains = new ArrayList<>();

    public int getSighting_id() {
        return sighting_id;
    }

    public void setSighting_id(int sighting_id) {
        this.sighting_id = sighting_id;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public LocalDate getDateOfSighting() {
        return dateOfSighting;
    }

    public void setDateOfSighting(LocalDate dateOfSighting) {
        this.dateOfSighting = dateOfSighting;
    }

    public String getSightingDescription() {
        return sightingDescription;
    }

    public void setSightingDescription(String sightingDescription) {
        this.sightingDescription = sightingDescription;
    }

    public List<HeroOrVillain> getHeroOrVillains() {
        return heroOrVillains;
    }

    public void setHeroOrVillains(List<HeroOrVillain> heroOrVillains) {
        this.heroOrVillains = heroOrVillains;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + this.sighting_id;
        hash = 23 * hash + Objects.hashCode(this.location);
        hash = 23 * hash + Objects.hashCode(this.dateOfSighting);
        hash = 23 * hash + Objects.hashCode(this.sightingDescription);
        hash = 23 * hash + Objects.hashCode(this.heroOrVillains);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Sighting other = (Sighting) obj;
        if (this.sighting_id != other.sighting_id) {
            return false;
        }
        if (!Objects.equals(this.sightingDescription, other.sightingDescription)) {
            return false;
        }
        if (!Objects.equals(this.location, other.location)) {
            return false;
        }
        if (!Objects.equals(this.dateOfSighting, other.dateOfSighting)) {
            return false;
        }
        if (!Objects.equals(this.heroOrVillains, other.heroOrVillains)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Sighting{" + "sighting_id=" + sighting_id + ", locations=" + location + ", dateOfSighting=" + dateOfSighting + ", sightingDescription=" + sightingDescription + ", heroOrVillians=" + heroOrVillains + '}';
    }
    
    
}
