/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.superheros.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author apprentice
 */
public class Organization {

    private int organizationId;
    private String name;
    private String description;
    private String contactInformation;
    private List<HeroOrVillain> heroOrVillains = new ArrayList<>();
    private List<Location> organizationLocations = new ArrayList<>();

    public int getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(int organizationId) {
        this.organizationId = organizationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContactInformation() {
        return contactInformation;
    }

    public void setContactInformation(String contactInformation) {
        this.contactInformation = contactInformation;
    }

    public List<HeroOrVillain> getHeroOrVillains() {
        return heroOrVillains;
    }

    public void setHeroOrVillains(List<HeroOrVillain> heroOrVillains) {
        this.heroOrVillains = heroOrVillains;
    }

    public List<Location> getOrganizationLocations() {
        return organizationLocations;
    }

    public void setOrganizationLocations(List<Location> organizationLocations) {
        this.organizationLocations = organizationLocations;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + this.organizationId;
        hash = 59 * hash + Objects.hashCode(this.name);
        hash = 59 * hash + Objects.hashCode(this.description);
        hash = 59 * hash + Objects.hashCode(this.contactInformation);
        hash = 59 * hash + Objects.hashCode(this.heroOrVillains);
        hash = 59 * hash + Objects.hashCode(this.organizationLocations);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Organization other = (Organization) obj;
        if (this.organizationId != other.organizationId) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.contactInformation, other.contactInformation)) {
            return false;
        }
        if (!Objects.equals(this.heroOrVillains, other.heroOrVillains)) {
            return false;
        }
        if (!Objects.equals(this.organizationLocations, other.organizationLocations)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Organization{" + "organizationId=" + organizationId + ", name=" + name + ", description=" + description + ", contactInformation=" + contactInformation + ", heroOrVillians=" + heroOrVillains + ", organizationLocations=" + organizationLocations + '}';
    }

}
