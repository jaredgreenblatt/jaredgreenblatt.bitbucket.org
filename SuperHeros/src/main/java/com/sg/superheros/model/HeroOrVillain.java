/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.superheros.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author apprentice
 */
public class HeroOrVillain {

    private int heroOrVillainId;
    private String name;
    private String description;
    private boolean isVillian;
    private List<SuperPower> superPowers = new ArrayList<>();

    public int getHeroOrVillainId() {
        return heroOrVillainId;
    }

    public void setHeroOrVillainId(int heroOrVillainId) {
        this.heroOrVillainId = heroOrVillainId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getIsVillian() {
        return isVillian;
    }

    public void setIsVillian(boolean isVillian) {
        this.isVillian = isVillian;
    }

    public List<SuperPower> getSuperPowers() {
        return superPowers;
    }

    public void setSuperPowers(List<SuperPower> superPowers) {
        this.superPowers = superPowers;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.heroOrVillainId;
        hash = 97 * hash + Objects.hashCode(this.name);
        hash = 97 * hash + Objects.hashCode(this.description);
        hash = 97 * hash + (this.isVillian ? 1 : 0);
        hash = 97 * hash + Objects.hashCode(this.superPowers);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HeroOrVillain other = (HeroOrVillain) obj;
        if (this.heroOrVillainId != other.heroOrVillainId) {
            return false;
        }
        if (this.isVillian != other.isVillian) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.superPowers, other.superPowers)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "HeroOrVillain{" + "heroOrVillainId=" + heroOrVillainId + ", name=" + name + ", description=" + description + ", isVillian=" + isVillian + ", superPowers=" + superPowers + '}';
    }

}
