/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.superheros.service;

import com.sg.superheros.model.HeroOrVillain;
import com.sg.superheros.model.Location;
import com.sg.superheros.model.Organization;
import com.sg.superheros.model.Sighting;
import com.sg.superheros.model.SuperPower;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface ServiceLayer {

    // Sightings
    public List<Sighting> getLast10Sightings();

    //Heros
    public List<HeroOrVillain> getAllHerosOrVillains();

    public HeroOrVillain getHeroOrVillain(int heroOrVillainId);

    public void addHeroOrVillain(HeroOrVillain heroOrVillain);

    public void deleteHeroOrVillain(int heroOrVillainId);

    public void updateHeroOrVillain(HeroOrVillain heroOrVillain);

    //Powers
    public List<SuperPower> getAllSuperPowers();

    public SuperPower getSuperPower(int powerId);

    public void addSuperPower(SuperPower superPower);

    public void deleteSuperPower(int superPowerId);

    public void updateSuperPower(SuperPower superPower);

    // Locations
    public List<Location> getAllLocation();

    public Location getLocation(int locationId);

    public void addLocation(Location location);

    public void deleteLocation(int locationId) throws SightingLocationException;

    public void updateLocation(Location location);

    // Organizations
    public List<Organization> getAllOrganizations();

    public Organization getOrganization(int organizationId);

    public void addOrganization(Organization organization);

    public void editOrganization(Organization organization);

    public void deleteOrganization(int organizationId);

    //Sightings
    public List<Sighting> getAllSighting();

    public Sighting getSighting(int sightingId);

    public void addSighting(Sighting sighting);

    public void editSighting(Sighting sighting);

    public void deleteSighting(int sightingId);
}
