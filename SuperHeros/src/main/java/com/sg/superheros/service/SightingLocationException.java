/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.superheros.service;

/**
 *
 * @author apprentice
 */
public class SightingLocationException extends Exception {

    public SightingLocationException(String message) {
        super(message);
    }

    public SightingLocationException(String message,
            Throwable cause) {
        super(message, cause);
    }

}
