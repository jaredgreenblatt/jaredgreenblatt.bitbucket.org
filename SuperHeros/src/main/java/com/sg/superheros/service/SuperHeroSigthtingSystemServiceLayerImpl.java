/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.superheros.service;

import com.sg.superheros.dao.HeroOrVillianSuperPowerDao;
import com.sg.superheros.dao.LocationDao;
import com.sg.superheros.dao.OrganizationDao;
import com.sg.superheros.dao.SightingDao;
import com.sg.superheros.model.HeroOrVillain;
import com.sg.superheros.model.Location;
import com.sg.superheros.model.Organization;
import com.sg.superheros.model.Sighting;
import com.sg.superheros.model.SuperPower;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class SuperHeroSigthtingSystemServiceLayerImpl implements ServiceLayer {

    HeroOrVillianSuperPowerDao heroOrVillainDao;
    LocationDao locationDao;
    OrganizationDao organizationDao;
    SightingDao sightingDao;

    public SuperHeroSigthtingSystemServiceLayerImpl(
            HeroOrVillianSuperPowerDao heroOrVillainDao,
            LocationDao locationDao,
            OrganizationDao organizationDao,
            SightingDao sightingDao) {
        this.heroOrVillainDao = heroOrVillainDao;
        this.locationDao = locationDao;
        this.organizationDao = organizationDao;
        this.sightingDao = sightingDao;

    }
    
    

    @Override
    public List<Sighting> getLast10Sightings() {
        return sightingDao.getLast10Sightings();
    }

    @Override
    public List<HeroOrVillain> getAllHerosOrVillains() {
        return heroOrVillainDao.getAllHeroOrVillains();
    }

    @Override
    public HeroOrVillain getHeroOrVillain(int heroOrVillainId) {
        return heroOrVillainDao.getHeroOrVillainById(heroOrVillainId);
    }

    @Override
    public List<SuperPower> getAllSuperPowers() {
        return heroOrVillainDao.getAllSuperPowers();
    }

    @Override
    public void addHeroOrVillain(HeroOrVillain heroOrVillain) {
        heroOrVillainDao.addHeroOrVillian(heroOrVillain);
    }

    @Override
    public SuperPower getSuperPower(int powerId) {
        return heroOrVillainDao.getSuperPowerById(powerId);
    }

    @Override
    public void deleteHeroOrVillain(int heroOrVillainId) {
        heroOrVillainDao.deleteHeroOrVillian(heroOrVillainId);
    }

    @Override
    public void updateHeroOrVillain(HeroOrVillain heroOrVillain) {
        heroOrVillainDao.updateHeroOrVillian(heroOrVillain);
    }

    @Override
    public void addSuperPower(SuperPower superPower) {
        heroOrVillainDao.addSuperPower(superPower);
    }

    @Override
    public void deleteSuperPower(int superPowerId) {
        heroOrVillainDao.deleteSuperPower(superPowerId);
    }

    @Override
    public void updateSuperPower(SuperPower superPower) {
        heroOrVillainDao.updateSuperPower(superPower);
    }

    @Override
    public List<Location> getAllLocation() {
        return locationDao.getAllLocations();
    }

    @Override
    public Location getLocation(int locationId) {
        return locationDao.getLocationById(locationId);
    }

    @Override
    public void addLocation(Location location) {
        locationDao.addLocation(location);
    }

    @Override
    public void deleteLocation(int locationId) throws SightingLocationException {
        
            locationDao.deleteLocation(locationId);
        
    }

    @Override
    public void updateLocation(Location location) {
        locationDao.updateLocation(location);

    }

    @Override
    public List<Organization> getAllOrganizations() {
        return organizationDao.getAllOrganizations();
    }

    @Override
    public Organization getOrganization(int organizationId) {
        return organizationDao.getOrganizationrById(organizationId);
    }

    @Override
    public void addOrganization(Organization organization) {
        organizationDao.addOrganization(organization);
    }

    @Override
    public void editOrganization(Organization organization) {
        organizationDao.updateOrganization(organization);
    }

    @Override
    public void deleteOrganization(int organizationId) {
        organizationDao.deleteOrganization(organizationId);
    }

    @Override
    public List<Sighting> getAllSighting() {
        return sightingDao.getAllSightings();
    }

    @Override
    public Sighting getSighting(int sightingId) {
        return sightingDao.getSightingById(sightingId);
    }

    @Override
    public void addSighting(Sighting sighting) {
        sightingDao.addSighting(sighting);
        
    }
    

    @Override
    public void editSighting(Sighting sighting) {
        sightingDao.updateSighting(sighting);
    }

    @Override
    public void deleteSighting(int sightingId) {
        sightingDao.deleteSighting(sightingId);
        
    }

}
