/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.superheros.controller;

import com.sg.superheros.model.HeroOrVillain;
import com.sg.superheros.model.Location;
import com.sg.superheros.model.Organization;
import com.sg.superheros.model.Sighting;
import com.sg.superheros.model.SuperPower;
import com.sg.superheros.service.ServiceLayer;
import com.sg.superheros.service.SightingLocationException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class SuperHeroController {

    ServiceLayer service;

    @Inject
    public SuperHeroController(ServiceLayer service) {
        this.service = service;
    }

    // Home Page
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String goHome(Model model) {
        // Get All the Sightings from the DAO
        List<Sighting> sightingList = service.getLast10Sightings();
        model.addAttribute("sightingList", sightingList);
        return "index";
    }
    //Create New Sighting end to end.

    @RequestMapping(value = "/createNewSighting", method = RequestMethod.GET)
    public String createNewSighting(Model model) {
        // Get All the Sightings from the DAO
        List<Sighting> sightingList = service.getAllSighting();
        model.addAttribute("sightingList", sightingList);
        List<HeroOrVillain> heroList = service.getAllHerosOrVillains();
        model.addAttribute("heroList", heroList);
        List<SuperPower> powerList = service.getAllSuperPowers();
        model.addAttribute("powerList", powerList);
        List<Organization> organizationList = service.getAllOrganizations();
        model.addAttribute("organizationList", organizationList);
        List<Location> locationList = service.getAllLocation();
        model.addAttribute("locationList", locationList);
        return "CreateNewSighting";
    }

    // Hero Page
    @RequestMapping(value = "/heroOrVillain", method = RequestMethod.GET)
    public String goToHeroOrVillain(Model model) {
        // Get All the Sightings from the DAO
//        List<Sighting> sightingList = service.getLast10Sightings();
//        model.addAttribute("sightingList", sightingList);
        List<HeroOrVillain> heroList = service.getAllHerosOrVillains();
        model.addAttribute("heroList", heroList);
        List<SuperPower> powerList = service.getAllSuperPowers();
        model.addAttribute("powerList", powerList);
        return "HeroOrVillain";
    }

    // Hero Information Page
    @RequestMapping(value = "/displayHeroDetails", method = RequestMethod.GET)
    public String goToHeroOrVillainInformationage(HttpServletRequest request, Model model) {
        // HERO INFORMATION PAGE! getting the hero id from the request parameter.
        String heroIdParameter = request.getParameter("heroOrVillainId");
        int heroId = Integer.parseInt(heroIdParameter);
        HeroOrVillain heroOrVillain = service.getHeroOrVillain(heroId);
        model.addAttribute("heroOrVillain", heroOrVillain);
        return "HeroOrVillainInformation";
    }

    //Create A  new Hero
    @RequestMapping(value = "/createHero", method = RequestMethod.POST)
    public String createHero(HttpServletRequest request, Model model) {

        HeroOrVillain heroOrVillain = new HeroOrVillain();
        heroOrVillain.setName(request.getParameter("name"));
        heroOrVillain.setDescription(request.getParameter("heroDescription"));
        String isVillain = request.getParameter("isVillain");
        if (isVillain.equals("no")) {
            heroOrVillain.setIsVillian(false);
        } else {
            heroOrVillain.setIsVillian(true);
        }

        SuperPower superPower = new SuperPower();
        List<SuperPower> superPowerList = new ArrayList<>();
        String selectedPowerList[] = request.getParameterValues("powerList");
        for (String superPowerString : selectedPowerList) {
            String powerIdString = superPowerString;
            int powerId = Integer.parseInt(powerIdString);
            superPower = service.getSuperPower(powerId);
            superPowerList.add(superPower);

        }
        heroOrVillain.setSuperPowers(superPowerList);
        service.addHeroOrVillain(heroOrVillain);

        List<HeroOrVillain> heroList = service.getAllHerosOrVillains();
        model.addAttribute("heroList", heroList);
        List<SuperPower> powerList = service.getAllSuperPowers();
        model.addAttribute("powerList", powerList);
        return "HeroOrVillain";
    }

    @RequestMapping(value = "/deleteHero", method = RequestMethod.GET)
    public String deleteContact(HttpServletRequest request, Model model) {
        String heroIdParameter = request.getParameter("heroOrVillainId");
        int heroId = Integer.parseInt(heroIdParameter);
        service.deleteHeroOrVillain(heroId);
        List<HeroOrVillain> heroList = service.getAllHerosOrVillains();
        model.addAttribute("heroList", heroList);
        List<SuperPower> powerList = service.getAllSuperPowers();
        model.addAttribute("powerList", powerList);
        return "HeroOrVillain";
    }

    // Hero Information Page
    @RequestMapping(value = "/displayUpdateHero", method = RequestMethod.GET)
    public String updateHero(HttpServletRequest request, Model model) {
        // HERO INFORMATION PAGE! getting the hero id from the request parameter.
        String heroIdParameter = request.getParameter("heroOrVillainId");
        int heroId = Integer.parseInt(heroIdParameter);
        HeroOrVillain heroOrVillain = service.getHeroOrVillain(heroId);
        List<SuperPower> currentHeroPower = heroOrVillain.getSuperPowers();
        List<SuperPower> modifiedPowerList = service.getAllSuperPowers();

        for (SuperPower sp : currentHeroPower) {
            modifiedPowerList.remove(sp);
        }
        model.addAttribute("heroOrVillain", heroOrVillain);

        model.addAttribute("modifiedPowerList", modifiedPowerList);
        return "HeroOrVillainUpdate";
    }

    @RequestMapping(value = "/editHero", method = RequestMethod.POST)
    public String editContact(HttpServletRequest request, Model model) {

        String heroIdParameter = request.getParameter("heroId");
        int heroId = Integer.parseInt(heroIdParameter);
        HeroOrVillain heroOrVillain = service.getHeroOrVillain(heroId);
        heroOrVillain.setName(request.getParameter("heroName"));
        heroOrVillain.setDescription(request.getParameter("heroDescription"));
        String isVillain = request.getParameter("isVillain");
        if (isVillain.equals("no")) {
            heroOrVillain.setIsVillian(false);
        } else {
            heroOrVillain.setIsVillian(true);
        }

        SuperPower superPower = new SuperPower();
        List<SuperPower> superPowerList = new ArrayList<>();
        String selectedPowerList[] = request.getParameterValues("powerList");
        for (String superPowerString : selectedPowerList) {
            String powerIdString = superPowerString;
            int powerId = Integer.parseInt(powerIdString);
            superPower = service.getSuperPower(powerId);
            superPowerList.add(superPower);

        }
        heroOrVillain.setSuperPowers(superPowerList);

        service.updateHeroOrVillain(heroOrVillain);

        List<HeroOrVillain> heroList = service.getAllHerosOrVillains();
        model.addAttribute("heroList", heroList);
        List<SuperPower> powerList = service.getAllSuperPowers();
        model.addAttribute("powerList", powerList);
        return "HeroOrVillain";

    }

    // SuperHero Information Page
    @RequestMapping(value = "/superPower", method = RequestMethod.GET)
    public String superPowerInfo(HttpServletRequest request, Model model) {
        List<SuperPower> powerList = service.getAllSuperPowers();
        model.addAttribute("powerList", powerList);

        return "SuperPower";
    }

    // create super power
    @RequestMapping(value = "/createSuperPower", method = RequestMethod.POST)
    public String createSuperPower(HttpServletRequest request, Model model) {
        SuperPower superPower = new SuperPower();
        superPower.setName(request.getParameter("powerName"));
        superPower.setDescription(request.getParameter("powerDescription"));
        service.addSuperPower(superPower);
        List<SuperPower> powerList = service.getAllSuperPowers();
        model.addAttribute("powerList", powerList);

        return "SuperPower";
    }

    @RequestMapping(value = "/deleteSuperPower", method = RequestMethod.GET)
    public String deleteSuperPower(HttpServletRequest request, Model model) {
        String powerIdParameter = request.getParameter("superPowerId");
        int powerId = Integer.parseInt(powerIdParameter);
        service.deleteSuperPower(powerId);
        List<SuperPower> powerList = service.getAllSuperPowers();
        model.addAttribute("powerList", powerList);

        return "SuperPower";
    }

    @RequestMapping(value = "/displaySuperPower", method = RequestMethod.GET)
    public String displaySuperPower(HttpServletRequest request, Model model) {
        String powerIdParameter = request.getParameter("superPowerId");
        int powerId = Integer.parseInt(powerIdParameter);
        SuperPower superPower = service.getSuperPower(powerId);

        model.addAttribute("superPower", superPower);

        return "SuperPowerDisplay";
    }

    @RequestMapping(value = "/UpdateSuperPower", method = RequestMethod.GET)
    public String updateSuperPower(HttpServletRequest request, Model model) {
        String powerIdParameter = request.getParameter("superPowerId");
        int powerId = Integer.parseInt(powerIdParameter);
        SuperPower superPower = service.getSuperPower(powerId);

        model.addAttribute("superPower", superPower);

        return "SuperPowerUpdate";
    }

    @RequestMapping(value = "/editSuperPower", method = RequestMethod.POST)
    public String editSuperPower(HttpServletRequest request, Model model) {

        String powerIdParameter = request.getParameter("powerId");
        int powerId = Integer.parseInt(powerIdParameter);
        SuperPower superPower = service.getSuperPower(powerId);
        superPower.setName(request.getParameter("powerName"));
        superPower.setDescription(request.getParameter("powerDescription"));
        service.updateSuperPower(superPower);

        List<SuperPower> powerList = service.getAllSuperPowers();
        model.addAttribute("powerList", powerList);
        return "SuperPower";

    }

    // Location Page
    @RequestMapping(value = "/locations", method = RequestMethod.GET)
    public String goToLocation(Model model) {
        List<Location> locationList = service.getAllLocation();
        model.addAttribute("locationList", locationList);

        return "Location";
    }

    @RequestMapping(value = "/deleteLocation", method = RequestMethod.GET)
    public String deleteLocation(HttpServletRequest request, Model model) {
        String errorMessage;
        String locationIdParameter = request.getParameter("locationId");
        int locationId = Integer.parseInt(locationIdParameter);
        Location l = service.getLocation(locationId);
        try {
            service.deleteLocation(locationId);
        } catch (SightingLocationException e) {
            errorMessage = "You cannot delete " + l.getDescription() + " located in " + l.getCity() + ", " + l.getCountry() + ". This location is "
                    + "associated with a SuperHero Or Villain sighting!";
            model.addAttribute("errorMessage", errorMessage);

        }
        List<Location> locationList = service.getAllLocation();
        model.addAttribute("locationList", locationList);

        return "Location";
    }

    @RequestMapping(value = "/displayLocation", method = RequestMethod.GET)
    public String displayLocation(HttpServletRequest request, Model model) {
        String locationIdParameter = request.getParameter("locationId");
        int locationId = Integer.parseInt(locationIdParameter);
        Location location = service.getLocation(locationId);
        model.addAttribute("location", location);

        return "LocationInformation";
    }

    // create  createLocation
    @RequestMapping(value = "/createLocation", method = RequestMethod.POST)

    public String createLocation(HttpServletRequest request, Model model) {
        String errorMessageInput = "";
        Location location = new Location();
        BigDecimal latitude;
        BigDecimal longitude;

        location.setDescription(request.getParameter("description"));
        location.setCity(request.getParameter("city"));
        location.setCountry(request.getParameter("country"));
        String latitudeString = request.getParameter("latitude");
        String longitudeString = request.getParameter("longitude");
        try {
            latitude = new BigDecimal(latitudeString);
            location.setLatitude(latitude);
            longitude = new BigDecimal(longitudeString);
            location.setLongitude(longitude);
            service.addLocation(location);
        } catch (NumberFormatException e) {
            errorMessageInput = "Latitude and Longitude need to be in the correct format."
                    + "Example(0.00000)";
        }

        List<Location> locationList = service.getAllLocation();
        model.addAttribute("locationList", locationList);
        model.addAttribute("errorMessageInput", errorMessageInput);

        return "Location";

    }

    @RequestMapping(value = "/updateLocation", method = RequestMethod.GET)
    public String updateLocation(HttpServletRequest request, Model model) {
        String locationIdParameter = request.getParameter("locationId");
        int locationId = Integer.parseInt(locationIdParameter);
        Location location = service.getLocation(locationId);

        model.addAttribute("location", location);

        return "LocationUpdate";
    }

    @RequestMapping(value = "/editLocation", method = RequestMethod.POST)
    public String editLocation(HttpServletRequest request, Model model) {
        BigDecimal latitude;
        BigDecimal longitude;
        String errorMessageInput = null;

        String locationIdParameter = request.getParameter("locId");
        int locationId = Integer.parseInt(locationIdParameter);
        Location location = service.getLocation(locationId);
        location.setDescription(request.getParameter("description"));
        location.setCity(request.getParameter("city"));
        location.setCountry(request.getParameter("country"));
        String latitudeString = request.getParameter("latitude");
        String longitudeString = request.getParameter("longitude");

        service.updateLocation(location);

        try {
            latitude = new BigDecimal(latitudeString);
            location.setLatitude(latitude);
            longitude = new BigDecimal(longitudeString);
            location.setLongitude(longitude);
            service.updateLocation(location);
        } catch (NumberFormatException e) {
            errorMessageInput = "Latitude and Longitude need to be in the correct format."
                    + "Example(0.00000)";
        }

        List<Location> locationList = service.getAllLocation();
        model.addAttribute("locationList", locationList);
        model.addAttribute("errorMessageInput", errorMessageInput);

        return "Location";
    }

    // Organization Page
    @RequestMapping(value = "/organizations", method = RequestMethod.GET)
    public String goToOrganizations(Model model) {
        List<Organization> organizationList = service.getAllOrganizations();

        model.addAttribute("organizationList", organizationList);

        List<HeroOrVillain> heroList = service.getAllHerosOrVillains();
        model.addAttribute("heroList", heroList);

        List<Location> locationList = service.getAllLocation();
        model.addAttribute("locationList", locationList);

        return "Organization";
    }

    @RequestMapping(value = "/deleteOrganization", method = RequestMethod.GET)
    public String deleteOrganization(Model model, HttpServletRequest request) {

        String orgIdParameter = request.getParameter("organizationId");
        int orgId = Integer.parseInt(orgIdParameter);
        service.deleteOrganization(orgId);
        List<Organization> organizationList = service.getAllOrganizations();

        model.addAttribute("organizationList", organizationList);

        List<HeroOrVillain> heroList = service.getAllHerosOrVillains();
        model.addAttribute("heroList", heroList);

        List<Location> locationList = service.getAllLocation();
        model.addAttribute("locationList", locationList);

        return "Organization";
    }

    @RequestMapping(value = "/displayOrganization", method = RequestMethod.GET)
    public String displayOrganization(Model model, HttpServletRequest request) {

        String orgIdParameter = request.getParameter("organizationId");
        int orgId = Integer.parseInt(orgIdParameter);
        Organization organization = service.getOrganization(orgId);

        model.addAttribute("organization", organization);

        return "OrganizationInformation";
    }

    @RequestMapping(value = "/createOrganization", method = RequestMethod.POST)
    public String createOrganization(Model model, HttpServletRequest request) {

        Organization organization = new Organization();
        organization.setName(request.getParameter("name"));
        organization.setDescription(request.getParameter("description"));
        organization.setContactInformation(request.getParameter("contact"));
        HeroOrVillain heroOrVillain = new HeroOrVillain();
        List<HeroOrVillain> addHeroList = new ArrayList<>();
        String selectedHeroList[] = request.getParameterValues("heroList");
        for (String superHeroString : selectedHeroList) {
            String heroIdString = superHeroString;
            int heroId = Integer.parseInt(heroIdString);
            heroOrVillain = service.getHeroOrVillain(heroId);
            addHeroList.add(heroOrVillain);

        }
        Location location = new Location();
        List<Location> addLocationList = new ArrayList<>();
        String selectedlocationList[] = request.getParameterValues("locationList");
        for (String locationString : selectedlocationList) {
            String locationIdString = locationString;
            int locationId = Integer.parseInt(locationIdString);
            location = service.getLocation(locationId);
            addLocationList.add(location);

        }
        organization.setHeroOrVillains(addHeroList);
        organization.setOrganizationLocations(addLocationList);
        service.addOrganization(organization);

        List<HeroOrVillain> heroList = service.getAllHerosOrVillains();
        model.addAttribute("heroList", heroList);

        List<Location> locationList = service.getAllLocation();
        model.addAttribute("locationList", locationList);

        List<Organization> organizationList = service.getAllOrganizations();

        model.addAttribute("organizationList", organizationList);
        return "Organization";
    }

    // Hero Information Page
    @RequestMapping(value = "/displayUpdateOrganization", method = RequestMethod.GET)
    public String displayUpdateOrganization(HttpServletRequest request, Model model) {
        // HERO INFORMATION PAGE! getting the hero id from the request parameter.
        String orgIdParameter = request.getParameter("organizationId");
        int orgId = Integer.parseInt(orgIdParameter);
        Organization organization = service.getOrganization(orgId);
        List<HeroOrVillain> currentOrganizationHeroList
                = organization.getHeroOrVillains();
        List<HeroOrVillain> modifiedHeroOrvillains
                = service.getAllHerosOrVillains();
        for (HeroOrVillain hv : currentOrganizationHeroList) {
            modifiedHeroOrvillains.remove(hv);
        }
        List<Location> currentOrganizationLocations
                = organization.getOrganizationLocations();
        List<Location> modifiedLocations
                = service.getAllLocation();
        for (Location l : currentOrganizationLocations) {
            modifiedLocations.remove(l);
        }

        model.addAttribute("organization", organization);

        model.addAttribute("modifiedHeroOrvillains", modifiedHeroOrvillains);
        model.addAttribute("modifiedLocations", modifiedLocations);

        return "OrganizationUpdate";
    }

    @RequestMapping(value = "/editOrganization", method = RequestMethod.POST)
    public String editOrganization(HttpServletRequest request, Model model) {

        String orgIdParameter = request.getParameter("orgId");
        int orgId = Integer.parseInt(orgIdParameter);
        Organization organization = service.getOrganization(orgId);

        organization.setName(request.getParameter("name"));
        organization.setDescription(request.getParameter("description"));
        organization.setContactInformation(request.getParameter("contact"));
        HeroOrVillain heroOrVillain = new HeroOrVillain();
        List<HeroOrVillain> addHeroList = new ArrayList<>();
        String selectedHeroList[] = request.getParameterValues("heroList");
        for (String superHeroString : selectedHeroList) {
            String heroIdString = superHeroString;
            int heroId = Integer.parseInt(heroIdString);
            heroOrVillain = service.getHeroOrVillain(heroId);
            addHeroList.add(heroOrVillain);

        }
        Location location = new Location();
        List<Location> addLocationList = new ArrayList<>();
        String selectedlocationList[] = request.getParameterValues("locationList");
        for (String locationString : selectedlocationList) {
            String locationIdString = locationString;
            int locationId = Integer.parseInt(locationIdString);
            location = service.getLocation(locationId);
            addLocationList.add(location);

        }

        organization.setHeroOrVillains(addHeroList);
        organization.setOrganizationLocations(addLocationList);

        service.editOrganization(organization);

        List<HeroOrVillain> heroList = service.getAllHerosOrVillains();
        model.addAttribute("heroList", heroList);

        List<Location> locationList = service.getAllLocation();
        model.addAttribute("locationList", locationList);

        List<Organization> organizationList = service.getAllOrganizations();

        model.addAttribute("organizationList", organizationList);
        return "Organization";

    }

    // Sighting Page
    @RequestMapping(value = "/sightings", method = RequestMethod.GET)
    public String goToSighting(Model model) {

        List<HeroOrVillain> heroList = service.getAllHerosOrVillains();
        model.addAttribute("heroList", heroList);

        List<Location> locationList = service.getAllLocation();
        model.addAttribute("locationList", locationList);

        List<Sighting> sightingList = service.getAllSighting();
        model.addAttribute("sightingList", sightingList);

        return "Sighting";
    }

    @RequestMapping(value = "/deleteSighting", method = RequestMethod.GET)
    public String deleteSighting(HttpServletRequest request, Model model) {

        String sightingIdParameter = request.getParameter("sightingId");

        int sightingId = Integer.parseInt(sightingIdParameter);
        service.deleteSighting(sightingId);

        List<HeroOrVillain> heroList = service.getAllHerosOrVillains();
        model.addAttribute("heroList", heroList);

        List<Location> locationList = service.getAllLocation();
        model.addAttribute("locationList", locationList);

        List<Sighting> sightingList = service.getAllSighting();
        model.addAttribute("sightingList", sightingList);

        return "Sighting";
    }

    @RequestMapping(value = "/displaySighting", method = RequestMethod.GET)
    public String displaySighting(HttpServletRequest request, Model model) {

        String sightingIdParameter = request.getParameter("sightingId");

        int sightingId = Integer.parseInt(sightingIdParameter);
        Sighting sighting = service.getSighting(sightingId);

        model.addAttribute("sighting", sighting);

        return "SightingInformation";
    }

    @RequestMapping(value = "/createSighting", method = RequestMethod.POST)

    public String createSighting(HttpServletRequest request, Model model) {

        Sighting sighting = new Sighting();
        String getDate = request.getParameter("sightingDate");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate sightingDate = LocalDate.parse(getDate, formatter);
        sighting.setSightingDescription(request.getParameter("description"));
        sighting.setDateOfSighting(sightingDate);
        HeroOrVillain heroOrVillain = new HeroOrVillain();
        List<HeroOrVillain> addHeroList = new ArrayList<>();
        String selectedHeroList[] = request.getParameterValues("heroList");
        for (String superHeroString : selectedHeroList) {
            String heroIdString = superHeroString;
            int heroId = Integer.parseInt(heroIdString);
            heroOrVillain = service.getHeroOrVillain(heroId);
            addHeroList.add(heroOrVillain);

        }
        sighting.setHeroOrVillains(addHeroList);
        String locationIdString = request.getParameter("locationList");
        int locationId = Integer.parseInt(locationIdString);
        Location location = service.getLocation(locationId);
        sighting.setLocation(location);
        service.addSighting(sighting);

        List<HeroOrVillain> heroList = service.getAllHerosOrVillains();
        model.addAttribute("heroList", heroList);

        List<Location> locationList = service.getAllLocation();
        model.addAttribute("locationList", locationList);

        List<Sighting> sightingList = service.getAllSighting();
        model.addAttribute("sightingList", sightingList);

        return "Sighting";
    }

    @RequestMapping(value = "/updateSighting", method = RequestMethod.GET)
    public String updateSighting(HttpServletRequest request, Model model) {

        String sightingIdParameter = request.getParameter("sightingId");

        int sightingId = Integer.parseInt(sightingIdParameter);
        Sighting sighting = service.getSighting(sightingId);

        List<HeroOrVillain> currentSightingHeroList
                = sighting.getHeroOrVillains();
        List<HeroOrVillain> modifiedHeroOrvillains
                = service.getAllHerosOrVillains();
        for (HeroOrVillain hv : currentSightingHeroList) {
            modifiedHeroOrvillains.remove(hv);
        }

        Location sightingLocation = sighting.getLocation();

        List<Location> modifiedLocations
                = service.getAllLocation();

        modifiedLocations.remove(sightingLocation);

        model.addAttribute("sighting", sighting);
        model.addAttribute("modifiedLocations", modifiedLocations);
        model.addAttribute("modifiedHeroOrvillains", modifiedHeroOrvillains);

        return "SightingUpdate";
    }

    @RequestMapping(value = "/editSighting", method = RequestMethod.POST)
    public String editSighting(HttpServletRequest request, Model model) {

        String sightingIdParameter = request.getParameter("sightingId");

        int sightingId = Integer.parseInt(sightingIdParameter);
        Sighting sighting = service.getSighting(sightingId);

        String getDate = request.getParameter("sightingDate");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate sightingDate = LocalDate.parse(getDate, formatter);
        sighting.setSightingDescription(request.getParameter("description"));
        sighting.setDateOfSighting(sightingDate);
        HeroOrVillain heroOrVillain = new HeroOrVillain();
        List<HeroOrVillain> addHeroList = new ArrayList<>();
        String selectedHeroList[] = request.getParameterValues("heroList");
        for (String superHeroString : selectedHeroList) {
            String heroIdString = superHeroString;
            int heroId = Integer.parseInt(heroIdString);
            heroOrVillain = service.getHeroOrVillain(heroId);
            addHeroList.add(heroOrVillain);

        }
        sighting.setHeroOrVillains(addHeroList);
        String locationIdString = request.getParameter("locationList");
        int locationId = Integer.parseInt(locationIdString);
        Location location = service.getLocation(locationId);
        sighting.setLocation(location);
        service.editSighting(sighting);

        List<HeroOrVillain> heroList = service.getAllHerosOrVillains();
        model.addAttribute("heroList", heroList);

        List<Location> locationList = service.getAllLocation();
        model.addAttribute("locationList", locationList);

        List<Sighting> sightingList = service.getAllSighting();
        model.addAttribute("sightingList", sightingList);

        return "Sighting";

    }

    /// Create New Sighting From Home Page
    @RequestMapping(value = "/createSightingFromHomePage", method = RequestMethod.POST)

    public String createSightingFromHomePage(HttpServletRequest request, Model model) {

        Sighting sighting = new Sighting();
        String getDate = request.getParameter("sightingDate");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate sightingDate = LocalDate.parse(getDate, formatter);
        sighting.setSightingDescription(request.getParameter("description"));
        sighting.setDateOfSighting(sightingDate);
        HeroOrVillain heroOrVillain = new HeroOrVillain();
        List<HeroOrVillain> addHeroList = new ArrayList<>();
        String selectedHeroList[] = request.getParameterValues("heroList");
        for (String superHeroString : selectedHeroList) {
            String heroIdString = superHeroString;
            int heroId = Integer.parseInt(heroIdString);
            heroOrVillain = service.getHeroOrVillain(heroId);
            addHeroList.add(heroOrVillain);

        }
        sighting.setHeroOrVillains(addHeroList);
        String locationIdString = request.getParameter("locationList");
        int locationId = Integer.parseInt(locationIdString);
        Location location = service.getLocation(locationId);
        sighting.setLocation(location);
        service.addSighting(sighting);

        List<Sighting> sightingList = service.getLast10Sightings();
        model.addAttribute("sightingList", sightingList);

        return "index";

    }

    @RequestMapping(value = "/createHeroNewSighting", method = RequestMethod.POST)

    public String createHeroNewSighting(HttpServletRequest request, Model model) {

        HeroOrVillain heroOrVillain = new HeroOrVillain();
        heroOrVillain.setName(request.getParameter("name"));
        heroOrVillain.setDescription(request.getParameter("heroDescription"));
        String isVillain = request.getParameter("isVillain");
        if (isVillain.equals("no")) {
            heroOrVillain.setIsVillian(false);
        } else {
            heroOrVillain.setIsVillian(true);
        }

        SuperPower superPower = new SuperPower();
        List<SuperPower> superPowerList = new ArrayList<>();
        String selectedPowerList[] = request.getParameterValues("powerList");
        for (String superPowerString : selectedPowerList) {
            String powerIdString = superPowerString;
            int powerId = Integer.parseInt(powerIdString);
            superPower = service.getSuperPower(powerId);
            superPowerList.add(superPower);

        }
        heroOrVillain.setSuperPowers(superPowerList);
        service.addHeroOrVillain(heroOrVillain);

        List<Sighting> sightingList = service.getAllSighting();
        model.addAttribute("sightingList", sightingList);
        List<HeroOrVillain> heroList = service.getAllHerosOrVillains();
        model.addAttribute("heroList", heroList);
        List<SuperPower> powerList = service.getAllSuperPowers();
        model.addAttribute("powerList", powerList);
        List<Organization> organizationList = service.getAllOrganizations();
        model.addAttribute("organizationList", organizationList);
        List<Location> locationList = service.getAllLocation();
        model.addAttribute("locationList", locationList);
        return "CreateNewSighting";

    }

    // create super power
    @RequestMapping(value = "/createSuperPowerNewSighting", method = RequestMethod.POST)
    public String createSuperPowerNewSighting(HttpServletRequest request, Model model) {
        SuperPower superPower = new SuperPower();
        superPower.setName(request.getParameter("powerName"));
        superPower.setDescription(request.getParameter("powerDescription"));
        service.addSuperPower(superPower);
        List<Sighting> sightingList = service.getAllSighting();
        model.addAttribute("sightingList", sightingList);
        List<HeroOrVillain> heroList = service.getAllHerosOrVillains();
        model.addAttribute("heroList", heroList);
        List<SuperPower> powerList = service.getAllSuperPowers();
        model.addAttribute("powerList", powerList);
        List<Organization> organizationList = service.getAllOrganizations();
        model.addAttribute("organizationList", organizationList);
        List<Location> locationList = service.getAllLocation();
        model.addAttribute("locationList", locationList);
        return "CreateNewSighting";
    }

    @RequestMapping(value = "/createLocationNewSighting", method = RequestMethod.POST)

    public String createLocationNewSighting(HttpServletRequest request, Model model) {
        String errorMessageInput = "";
        Location location = new Location();
        BigDecimal latitude;
        BigDecimal longitude;

        location.setDescription(request.getParameter("description"));
        location.setCity(request.getParameter("city"));
        location.setCountry(request.getParameter("country"));
        String latitudeString = request.getParameter("latitude");
        String longitudeString = request.getParameter("longitude");
        try {
            latitude = new BigDecimal(latitudeString);
            location.setLatitude(latitude);
            longitude = new BigDecimal(longitudeString);
            location.setLongitude(longitude);
            service.addLocation(location);
        } catch (NumberFormatException e) {
            errorMessageInput = "Latitude and Longitude need to be in the correct format."
                    + "Example(0.00000)";
        }
        List<Sighting> sightingList = service.getAllSighting();
        model.addAttribute("sightingList", sightingList);
        List<HeroOrVillain> heroList = service.getAllHerosOrVillains();
        model.addAttribute("heroList", heroList);
        List<SuperPower> powerList = service.getAllSuperPowers();
        model.addAttribute("powerList", powerList);
        List<Organization> organizationList = service.getAllOrganizations();
        model.addAttribute("organizationList", organizationList);
        List<Location> locationList = service.getAllLocation();
        model.addAttribute("locationList", locationList);
        return "CreateNewSighting";
    }

}
