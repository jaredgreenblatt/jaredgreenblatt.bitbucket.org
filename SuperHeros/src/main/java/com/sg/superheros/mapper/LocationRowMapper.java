/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.superheros.mapper;

import com.sg.superheros.model.Location;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author apprentice
 */
public class LocationRowMapper implements RowMapper<Location> {

    @Override
    public Location mapRow(ResultSet rs, int i) throws SQLException {
        Location l = new Location();
        l.setLocationId(rs.getInt("location_id"));
        l.setDescription(rs.getString("description"));
        l.setCity(rs.getString("city"));
        l.setCountry(rs.getString("country"));
        l.setLatitude(rs.getBigDecimal("latitude"));
        l.setLongitude(rs.getBigDecimal("longitude"));
        return l;
    }

}

