/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.superheros.mapper;

import com.sg.superheros.model.Organization;
import com.sg.superheros.model.Sighting;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author apprentice
 */
public class SightingMapper implements RowMapper<Sighting> {

    @Override
    public Sighting mapRow(ResultSet rs, int i) throws SQLException {
        Sighting si = new Sighting();
        si.setSighting_id(rs.getInt("sighting_id"));
        si.setDateOfSighting(rs.getTimestamp("date_of_sighting").
                toLocalDateTime().toLocalDate());
        si.setSightingDescription(rs.getString("sighting_description"));
        return si;
    }

 
}
