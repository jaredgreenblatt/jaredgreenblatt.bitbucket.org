/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.superheros.mapper;

import com.sg.superheros.model.HeroOrVillain;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author apprentice
 */
public class
    HeroOrVillainMapper implements RowMapper<HeroOrVillain> {

        @Override
        public HeroOrVillain mapRow(ResultSet rs, int i) throws SQLException {
            HeroOrVillain hv = new HeroOrVillain();
            hv.setHeroOrVillainId(rs.getInt("hero_villain_id"));
            hv.setName(rs.getString("hero_villain_name"));
            hv.setDescription(rs.getString("description"));
            hv.setIsVillian(rs.getBoolean("isVillain"));
            return hv;
        }
    }


