/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.superheros.mapper;

import com.sg.superheros.model.Organization;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author apprentice
 */
public class OrganizationMapper implements RowMapper<Organization> {

    @Override
    public Organization mapRow(ResultSet rs, int i) throws SQLException {
        Organization org = new Organization();
        org.setOrganizationId(rs.getInt("organization_id"));
        org.setName(rs.getString("organization_name"));
        org.setDescription(rs.getString("organization_description"));
        org.setContactInformation(rs.getString("contact_information"));
        return org;

    }

}
