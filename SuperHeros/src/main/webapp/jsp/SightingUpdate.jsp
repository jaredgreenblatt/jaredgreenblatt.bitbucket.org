<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Super Hero Tracking System</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">  
        <link href="${pageContext.request.contextPath}/css/superHero.css" rel="stylesheet">   
        <link href="https://fonts.googleapis.com/css?family=Bungee+Shade" rel="stylesheet">

    </head>
    <body>
        <div class="container">
            <div class="row VendingMachineTitleRow header">
                <div class="col-xs-offset-2 col-xs-8">
                    <h1 class ="text-center">Super Hero Tracking System</h1> 

                </div>
            </div>
            <hr />
            <hr />
            <div class="col-xs-12">
                <center>
                    <div class="navbar text-center">
                        <ul class="nav nav-pills">
                            <li role="presentation"><a href="${pageContext.request.contextPath}/">Home</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/heroOrVillain">Heros</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/superPower">SuperPowers</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/locations">Locations</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/organizations">Organizations</a></li>
                            <li role="presentation"class="active"><a href="${pageContext.request.contextPath}/sightings">Sightings</a></li>

                        </ul>    
                    </div>
                </center>
            </div>


            <h2 class ="col-xs-12 text-center">Hero Or Villain Sightings</h2> 

            <div class="col-xs-12">
                <div class ="col-xs-6">

                    <h3 class = "text-center">Edit Sighting</h3>
                    <form class ="form-horizontal"
                          role ="form" method ="POST"
                          action="editSighting">


                        <div class ="form-group">
                            <label for="add-email" class ="col-md-4 control-label">
                                Heros Or Villains at Sighting:
                            </label>
                            <div class ="col-md-8">
                                <select multiple class="form-control" id="heroList" name ="heroList" locationList>
                                    <c:forEach var="sightingHero"  items="${sighting.heroOrVillains}">

                                        <option value="${sightingHero.heroOrVillainId}" selected='true'> <c:out value="${sightingHero.name}"/>   </option>

                                    </c:forEach>

                                    <c:forEach var="allHero"  items="${modifiedHeroOrvillains}">

                                        <option value="${allHero.heroOrVillainId}"> <c:out value="${allHero.name}"/>   </option>

                                    </c:forEach>

                                </select>
                            </div>
                        </div>
                        <div class ="form-group">
                            <label for="add-email" class ="col-md-4 control-label">
                                Sighting Location:
                            </label>
                            <div class ="col-md-8">
                                <select  class="form-control" id="locationList" name ="locationList"required>


                                    <option value="${sighting.location.locationId}" selected='true'> <c:out value="${sighting.location.description}"/>   </option>




                                    <c:forEach var="allLocations"  items="${modifiedLocations}">

                                        <option value="${allLocations.locationId}"> <c:out value="${allLocations.description}"/>   </option>

                                    </c:forEach>

                                </select>
                            </div>
                        </div>
                        <div class ="form-group">
                            <label for="add-city" class ="col-md-4 control-label">
                                Sighting Description
                            </label>
                            <div class ="col-md-8">
                                <textarea class="form-control" id="exampleTextarea" rows="3" placeholder="Enter a Description of Sighting" name="description" required>${sighting.sightingDescription}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-date" class="col-md-4 control-label">
                                Date Of Sighting:
                            </label>
                            <div class ="col-md-8">
                                <input type="date" class ="form-control" name ="sightingDate" placeholder="Enter Date of Sighting" value ="${sighting.dateOfSighting}" required />
                            </div>
                        </div>

                        <input type="hidden" id="sightingId" name="sightingId" value=<c:out value="${sighting.sighting_id}" />

                               <div class="form-group">
                            <div class="col-md-offset-4 col-md-8">
                                <input type="submit" class="btn btn-default" value="Edit Sighting"/>
                            </div>
                        </div>


                    </form>
                </div>
            </div>



        </div> 






    <!-- Placed at the end of the document so the pages load faster -->
    <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

</body>
</html>

