<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Super Hero Tracking System</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">  
        <link href="${pageContext.request.contextPath}/css/superHero.css" rel="stylesheet">   
        <link href="https://fonts.googleapis.com/css?family=Bungee+Shade" rel="stylesheet">

    </head>
    <body>
        <div class="container">
            <div class="row VendingMachineTitleRow header">
                <div class="col-xs-offset-2 col-xs-8">
                    <h1 class ="text-center">Super Hero Tracking System</h1> 

                </div>
            </div>
            <hr />
            <hr />
            <div class="col-xs-12">
                <center>
                    <div class="navbar text-center">
                        <ul class="nav nav-pills">
                            <li role="presentation"><a href="${pageContext.request.contextPath}/">Home</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/heroOrVillain">Heros</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/superPower">SuperPowers</a></li>
                            <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/locations">Locations</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/organizations">Organizations</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/sightings">Sightings</a></li>

                        </ul>    
                    </div>
                </center>
            </div>


            <h2 class ="col-xs-12 text-center">Locations</h2> 

            <div class="col-xs-12">
                <div class ="col-xs-7">
                    <h4 class="alert"><c:out value="${errorMessage}"/> <h4>
                            <h3 class = "text-center">Locations List</h3>
                            <table id="contactTable" class="table table-hover">
                                <tr>
                                    <th width="40%">Location Description</th>
                                    <th width="30%"> Location City</th>
                                    <th width="15%"></th>
                                    <th width="15%"></th>

                                </tr>
                                <c:forEach var="currentLocation"  items="${locationList}">
                                    <tr>
                                        <td>
                                            <a href="displayLocation?locationId=${currentLocation.locationId}">

                                                <c:out value="${currentLocation.description}"/>  
                                            </a>

                                        </td>
                                        <td>
                                            <c:out value="${currentLocation.city}"/>
                                        </td>
                                        <td>
                                            <a href="updateLocation?locationId=${currentLocation.locationId}">
                                                update
                                            </a>
                                        </td>
                                        <td>
                                            <a href="deleteLocation?locationId=${currentLocation.locationId}">
                                                delete
                                            </a>
                                        </td>

                                    </tr>
                                </c:forEach>
                            </table>

                            </div>
                            <div class ="col-xs-5">
                                <h4 class="alert"><c:out value="${errorMessageInput}"/> <h4>

                                        <h3 class = "text-center">Add Location</h3>
                                        <form class ="form-horizontal"
                                              role ="form" method ="POST"
                                              action="createLocation">
                                            <div class="form-group">
                                                <label for="add-description" class="col-md-4 control-label">
                                                    Description:
                                                </label>
                                                <div class ="col-md-8">
                                                    <input type="text" class ="form-control" name ="description" placeholder="Enter a Description" required />
                                                </div>
                                            </div>
                                            <div class ="form-group">
                                                <label for="add-city" class ="col-md-4 control-label">
                                                    City:
                                                </label>
                                                <div class ="col-md-8">
                                                    <input type="text" class ="form-control" name ="city" placeholder="Enter a City" required/>
                                                </div>
                                            </div>
                                            <div class ="form-group">
                                                <label for="add-country" class ="col-md-4 control-label">
                                                    Country:
                                                </label>
                                                <div class ="col-md-8">
                                                    <input type="text" class ="form-control" name ="country" placeholder="Enter a Country" required />
                                                </div>
                                            </div>
                                            <div class ="form-group">
                                                <label for="add-latitude" class ="col-md-4 control-label">
                                                    Latitude:
                                                </label>
                                                <div class ="col-md-8">
                                                    <input type="number" min="-180" max="180" step="any" class ="form-control" name ="latitude" placeholder="Enter a Latitude"  required/>
                                                </div>
                                            </div>
                                            <div class ="form-group">
                                                <label for="add-latitude" class ="col-md-4 control-label">
                                                    Longitude:
                                                </label>
                                                <div class ="col-md-8">
                                                    <input type="number" min="-180" max="180"  step="any"  class ="form-control" name ="longitude" placeholder="Enter a Longitude" required />
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <div class="col-md-offset-4 col-md-8">
                                                    <input type="submit" class="btn btn-default" value="Create Location" required/>
                                                </div>
                                            </div>


                                        </form>
                                        </div>
                                        </div>



                                        </div> 






                                        </div>
                                        <!-- Placed at the end of the document so the pages load faster -->
                                        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
                                        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

                                        </body>
                                        </html>

