<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <title>Super Hero Tracking System</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">  
        <link href="${pageContext.request.contextPath}/css/superHero.css" rel="stylesheet">   
        <link href="https://fonts.googleapis.com/css?family=Bungee+Shade" rel="stylesheet">


    </head>
    <body>
        <div class="container">
            <div class="row VendingMachineTitleRow header">
                <div class="col-xs-offset-2 col-xs-8">
                    <h1 class ="text-center">Super Hero Tracking System</h1> 

                </div>
            </div>
            <hr />
            <hr />
            <div class="col-xs-12">
                <center>
                    <div class="navbar text-center">
                        <ul class="nav nav-pills">
                            <li role="presentation"><a href="${pageContext.request.contextPath}/">Home</a></li>
                            <li role="presentation" class ="active"><a href="${pageContext.request.contextPath}/heroOrVillain">Heros</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/superPower">SuperPowers</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/locations">Locations</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/organizations">Organizations</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/sightings">Sightings</a></li>

                        </ul>    
                    </div>
                </center>
            </div>


            <div class="col-xs-6">
                <h3 class = "text-center">Edit Hero or Villain</h3>
                <form id="heroOrVillain" role="form" class="form-horizontal" action="editHero" method="POST">
                    <input type="hidden" id="heroId" name="heroId" value=<c:out value="${heroOrVillain.heroOrVillainId}" />
                           
                    <div class ="form-group">
                        <label for="add-heroOrVillain-name" class="col-md-4 control-label">
                            Hero Or Villain Name: 
                        </label> 
                        <div class="col-md-8">
                            <input id="add-hero-name" name="heroName" placeholder="Hero Or Villain Name" type="text" class="form-control" value="<c:out value="${heroOrVillain.name}"/>" required=""/>

                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-description" class="col-md-4 control-label">Description:</label>
                        <div class="col-md-8">
                            <textarea required class="form-control" id="heroDescription" rows="3" placeholder="Enter a Description of Hero Or Villain" name="heroDescription" >${heroOrVillain.description}</textarea>



                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-isVillain" class="col-md-4 control-label">Are They a  Villain?</label>  
                        <div class="col-md-8">

                            <c:set var="isVillain" value="${heroOrVillain.isVillian}" />
                            <select class="form-control" id="villainSelect" name="isVillain" required >
                                <c:if test="${fn:contains(isVillain, 'true')}">
                                    <option>no</option>
                                    <option selected>yes</option>
                                </c:if>
                                <c:if test="${fn:contains(isVillain, 'false')}">
                                    <option selected>no</option>
                                    <option>yes</option>
                                </c:if>


                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-powers" class="col-md-4 control-label">List Of Powers:</label>
                        <div class="col-md-8">



                            <select multiple class="form-control" id="powerSelectList" name ="powerList" required>

                                <c:forEach var="currentHeroPowers"  items="${heroOrVillain.superPowers}">
                                    <option value="${currentHeroPowers.superPowerId}" selected ="true">  <c:out value="${currentHeroPowers.name}"/>   </option>



                                </c:forEach>

                                <c:forEach var="currentPowers"  items="${modifiedPowerList}">


                                    <option value="${currentPowers.superPowerId}"><c:out value="${currentPowers.name}"/>   </option>

                                </c:forEach>

                            </select>








                        </div>

                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-4 col-md-8">
                            <input type="submit" class="btn btn-default" value="Update Hero or Villain"/>
                        </div>



                    </div>
                </form>
                <!--              <sf:form class="form-horizontal" role ="form"
                                     modelAttribute="heroOrVillain"
                                     action="editHero" method="POST">
                                <div class ="form-group">
                                    <label for="add-first-name" class="col-md-4 control-label">
                                        Hero Or Villain Name:
                                    </label> 
                                    <div class="col-md-8">
                                        <sf:input type="text" class="form-control" id="add-hero-name"
                                                  path="name" placeholder="Hero Or Villain Name"/>
                                        <sf:errors path="name" cssclass="error"></sf:errors>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-last-name" class="col-md-4 control-label">Description:</label>
                                        <div class="col-md-8">
                                            
                                        <sf:input type="textarea" class="form-control" id="add-description"
                                                  path="description" placeholder="Description"/>
                                        <sf:errors path="description" cssclass="error"></sf:errors>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-is-Villain" class="col-md-4 control-label">Is Villain?</label>
                                        <div class="col-md-8">
                                        <sf:input type="text" class="form-control" id="add-description"
                                                  path="isVillian" placeholder="Hero or Villian"/>
                                        <sf:errors path="isVillian" cssclass="error"></sf:errors>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="add-list-of-Powers" class="col-md-4 control-label">List Of Powers:</label>
                                        <div class="col-md-8">
                                        <sf:input type="text" class="form-control" id="add-superPowers"
                                                  path="superPowers" placeholder="Hero or Villian"/>
                                        <sf:errors path="superPowers" cssclass="error"></sf:errors>
                                        </div>
                                    </div>
                
                                    <div class="form-group">
                                        <div class="col-md-offset-4 col-md-8">
                                            <input type="submit" class="btn btn-default" value="Update Hero Or Villain"/>
                                        </div>
                
                
                
                                    </div>
                            </sf:form>-->
            </div>
            <!-- Placed at the end of the document so the pages load faster -->
            <script src="/ContactListSpringMVC/js/jquery-3.1.1.min.js"></script>
            <script src="/ContactListSpringMVC/js/bootstrap.min.js"></script>

    </body>
</html>




