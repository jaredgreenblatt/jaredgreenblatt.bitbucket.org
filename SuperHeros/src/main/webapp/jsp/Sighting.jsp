<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Super Hero Tracking System</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">  
        <link href="${pageContext.request.contextPath}/css/superHero.css" rel="stylesheet">   
        <link href="https://fonts.googleapis.com/css?family=Bungee+Shade" rel="stylesheet">

    </head>
    <body>
        <div class="container">
            <div class="row VendingMachineTitleRow header">
                <div class="col-xs-offset-2 col-xs-8">
                    <h1 class ="text-center">Super Hero Tracking System</h1> 

                </div>
            </div>
            <hr />
            <hr />
            <div class="col-xs-12">
                <center>
                    <div class="navbar text-center">
                        <ul class="nav nav-pills">
                            <li role="presentation"><a href="${pageContext.request.contextPath}/">Home</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/heroOrVillain">Heros</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/superPower">SuperPowers</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/locations">Locations</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/organizations">Organizations</a></li>
                            <li role="presentation"class="active"><a href="${pageContext.request.contextPath}/sightings">Sightings</a></li>

                        </ul>    
                    </div>
                </center>
            </div>


            <h2 class ="col-xs-12 text-center">Hero Or Villain Sightings</h2> 

            <div class="col-xs-12">
                <div class ="col-xs-7">
                    <h3 class = "text-center">Sightings List</h3>
                    <table id="contactTable" class="table table-hover">
                        <tr>
                            <th width="20%">Sighting Description</th>
                            <th width="20%">Sighting Hero</th>
                            <th width="20%">Sighting Location</th>
                            <th width="20%">Sighting Date</th>
                            <th width="10%"></th>
                            <th width="10%"></th>

                        </tr>
                        <c:forEach var="currentSighting"  items="${sightingList}">
                            <tr>
                                <td>
                                    <a href="displaySighting?sightingId=${currentSighting.sighting_id}">
                                        ${currentSighting.sightingDescription}
                                    </a>
                                </td>
                                <td>
                                    <c:forEach var="poweredPeople" varStatus ="i" items="${currentSighting.heroOrVillains}">
                                        <a href="displayHeroDetails?heroOrVillainId=${poweredPeople.heroOrVillainId}">
                                            <c:out value="${poweredPeople.name}"/> 

                                        </a>
                                    </c:forEach>
                                    </a>



                                </td>
                                <td>
                                    <a href="displayLocation?locationId=${currentSighting.location.locationId}">
                                        <c:out value="${currentSighting.location.city}"/> , <c:out value="${currentSighting.location.country}"/>          
                                    </a>                                </td>
                                <td>
                                    <c:out value="${currentSighting.dateOfSighting}"/>   
                                </td>
                                <td>

                                    <a href="updateSighting?sightingId=${currentSighting.sighting_id}">
                                        update
                                    </a>
                                </td>
                                <td>
                                    <a href="deleteSighting?sightingId=${currentSighting.sighting_id}">
                                        delete
                                    </a>
                                </td>

                            </tr>
                        </c:forEach>
                    </table>

                </div>
                <div class ="col-xs-5">
                            <h3 class = "text-center">Add Sighting</h3>
                            <form class ="form-horizontal"
                                  role ="form" method ="POST"
                                  action="createSighting">

                                <div class ="form-group">
                                    <label for="add-email" class ="col-md-4 control-label">
                                        Heros Or Villains at Sighting:
                                    </label>
                                    <div class ="col-md-8">
                                        <select multiple class="form-control" id="heroList" name ="heroList" required>

                                            <c:forEach var="sightingHero"  items="${heroList}">

                                                <option value="${sightingHero.heroOrVillainId}"> <c:out value="${sightingHero.name}"/>   </option>

                                            </c:forEach>

                                        </select>
                                    </div>
                                </div>
                                <div class ="form-group">
                                    <label for="add-email" class ="col-md-4 control-label">
                                        Sighting Location:
                                    </label>
                                    <div class ="col-md-8">
                                        <select  class="form-control" id="powerSelectList" name ="locationList" required>

                                            <c:forEach var="sightingLocations"  items="${locationList}">

                                                <option value="${sightingLocations.locationId}"> <c:out value="${sightingLocations.description}"/>   </option>

                                            </c:forEach>

                                        </select>
                                    </div>
                                </div>
                                <div class ="form-group">
                                    <label for="add-city" class ="col-md-4 control-label">
                                        Sighting Description
                                    </label>
                                    <div class ="col-md-8">
                                        <textarea class="form-control" id="exampleTextarea" rows="3" placeholder="Enter a Description of Sighting" name="description" required></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="add-date" class="col-md-4 control-label">
                                        Date Of Sighting:
                                    </label>
                                    <div class ="col-md-8">
                                        <input type="date" class ="form-control" name ="sightingDate" placeholder="Enter Date of Sighting" required />
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-md-offset-4 col-md-8">
                                        <input type="submit" class="btn btn-default" value="Create Sighting"/>
                                    </div>
                                </div>


                            </form>
                            </div>
                            </div>



                            </div> 






                            </div>
                            <!-- Placed at the end of the document so the pages load faster -->
                            <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
                            <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

                            </body>
                            </html>

