<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Super Hero Tracking System</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">  
        <link href="${pageContext.request.contextPath}/css/superHero.css" rel="stylesheet">   
        <link href="https://fonts.googleapis.com/css?family=Bungee+Shade" rel="stylesheet">

    </head>
    <body>
        <div class="container">
            <div class="row VendingMachineTitleRow header">
                <div class="col-xs-offset-2 col-xs-8">
                    <h1 class ="text-center">Super Hero Tracking System</h1> 

                </div>
            </div>
            <hr />
            <hr />
            <div class="col-xs-12">
                <center>
                    <div class="navbar text-center">
                        <ul class="nav nav-pills">
                            <li role="presentation" class ="active"><a href="${pageContext.request.contextPath}/">Home</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/heroOrVillain">Heros</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/superPower">SuperPowers</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/locations">Locations</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/organizations">Organizations</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/sightings">Sightings</a></li>

                        </ul>    
                    </div>
                </center>
            </div>
            <div class="welcomeArea ">
                <div class="col-xs-12">
                    <h2 class ="text-center">Welcome to the Super Hero
                        Tracking System </h1> 
                        <div class="col-xs-offset-2 col-xs-8"> <center>
                                <img src ="${pageContext.request.contextPath}/images/shwarma.gif" class="img-fluid img-responsive img-circle" alt="Responsive image" />
                            </center></div>

                </div> 

            </div>
            <div class="col-xs-12"><hr/>
                <div class="col-xs-4" id="GeneralInfo">
                    <h2 class = "text-center">Hero Education and Relationship Organization (HERO)</h2>
                    <h3 class = "text-center"><a href="${pageContext.request.contextPath}/createNewSighting">Enter a New Hero Sighting Hero Sighting Here!</a></h3>
                    <p class = "text-center">Hero is used to track SuperHero sightings throughout the
                        Universe. Any sighting of any SuperHero or Villain can be tracked using hero. We 
                        Also track the powers and group affiliation of these powered individuals. If you are
                        looking for any information on SuperHero Sighting, Powers, General Information. HERO 
                        is your one stop shop.</p>
                </div>


                <div class="row GeneralInfoLatestSightings">
                    <div class="col-xs-7" id="LatestSighting">
                        <h2 class = "text-center">Last 10 Sightings</h2>
                        <center>
                            <table id="contactTable" class="table table-hover">
                                <tr>
                                    <th width="25%">Sighting Description</th>
                                    <th width="25%">Sighting Hero</th>
                                    <th width="25%">Sighting Location</th>
                                    <th width="25%">Sighting Date</th>

                                </tr>
                                <c:forEach var="currentSighting"  items="${sightingList}">
                                    <tr>
                                        <td>
                                            <a href="displaySighting?sightingId=${currentSighting.sighting_id}">
                                                ${currentSighting.sightingDescription}
                                            </a>
                                        </td>
                                        <td>


                                            <c:forEach var="poweredPeople" varStatus ="i" items="${currentSighting.heroOrVillains}">
                                                <a href="displayHeroDetails?heroOrVillainId=${poweredPeople.heroOrVillainId}">
                                                    <c:out value="${poweredPeople.name}"/> 

                                                </a>
                                            </c:forEach>
                                            </a>

                                        </td>
                                        <td>
                                            <a href="displayLocation?locationId=${currentSighting.location.locationId}">
                                                <c:out value="${currentSighting.location.city}"/> , <c:out value="${currentSighting.location.country}"/>          
                                            </a>
                                        </td>
                                        <td>
                                            <c:out value="${currentSighting.dateOfSighting}"/>   
                                        </td>


                                    </tr>
                                </c:forEach>
                            </table>
                        </center>

                    </div> 
                </div>
            </div>




        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

