<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Super Hero Tracking System</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">  
        <link href="${pageContext.request.contextPath}/css/superHero.css" rel="stylesheet">   
        <link href="https://fonts.googleapis.com/css?family=Bungee+Shade" rel="stylesheet">

    </head>
    <body>
        <div class="container">
            <div class="row VendingMachineTitleRow header">
                <div class="col-xs-offset-2 col-xs-8">
                    <h1 class ="text-center">Super Hero Tracking System</h1> 

                </div>
            </div>
            <hr />
            <hr />
            <div class="col-xs-12">
                <center>
                    <div class="navbar text-center">
                        <ul class="nav nav-pills">
                            <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/">Home</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/heroOrVillain">Heros</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/superPower">SuperPowers</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/locations">Locations</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/organizations">Organizations</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/sightings">Sightings</a></li>

                        </ul>    
                    </div>
                </center>
            </div>


            <h2 class ="col-xs-12 text-center">Create New Hero Or Villain Sighting</h2> 
            <!--
            **************************************************************************************************
            **************************************************************************************************
            **************************************************************************************************
            add new Sighting
            **************************************************************************************************
            **************************************************************************************************
            **************************************************************************************************
            
            -->
            <div class="col-xs-12">
                <div class ="col-xs-5" id="addSighting">
                    <h3 class = "text-center">Add Sighting</h3>
                    <form class ="form-horizontal"
                          role ="form" method ="POST"
                          action="createSightingFromHomePage">

                        <div class ="form-group">
                            <label for="add-email" class ="col-md-4 control-label">
                                Heros Or Villains at Sighting:
                            </label>
                            <div class ="col-md-8">
                                <p>
                                    <button type="button" id="addHero" class="btn btn-default">
                                        Don't See the Hero or Villain You Sighted?
                                    </button>

                                </p>
                                <br/>
                                <select multiple class="form-control" id="heroList" name ="heroList" required>

                                    <c:forEach var="sightingHero"  items="${heroList}">

                                        <option value="${sightingHero.heroOrVillainId}"> <c:out value="${sightingHero.name}"/>   </option>

                                    </c:forEach>

                                </select>
                            </div>
                        </div>
                        <div class ="form-group">
                            <label for="add-email" class ="col-md-4 control-label">
                                Sighting Location:
                            </label>
                            <div class ="col-md-8">
                                <p>
                                    <input type="button" id='addLocation' class="btn btn-default" value="Don't See the Sighting Location?"/>
                                </p>
                                <br/>
                                <select  class="form-control" id="powerSelectList" name ="locationList" required>

                                    <c:forEach var="sightingLocations"  items="${locationList}">

                                        <option value="${sightingLocations.locationId}"> <c:out value="${sightingLocations.description}"/>   </option>

                                    </c:forEach>

                                </select>
                            </div>
                        </div>
                        <div class ="form-group">
                            <label for="add-city" class ="col-md-4 control-label">
                                Sighting Description
                            </label>
                            <div class ="col-md-8">
                                <textarea class="form-control" id="exampleTextarea" rows="3" placeholder="Enter a Description of Sighting" name="description" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-date" class="col-md-4 control-label">
                                Date Of Sighting:
                            </label>
                            <div class ="col-md-8">
                                <input type="date" class ="form-control" name ="sightingDate" placeholder="Enter Date of Sighting" required />
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-8">
                                <input type="submit" class="btn btn-default" value="Create Sighting"/>
                            </div>
                        </div>


                    </form>
                </div>
            </div>

            <!--
      **************************************************************************************************
      **************************************************************************************************
      **************************************************************************************************
      add new hero
      **************************************************************************************************
      **************************************************************************************************
      **************************************************************************************************
      
            -->
            <div class="col-xs-12" id="addHeroForm"> 
                <div class ="col-xs-6">
                    <h3 class = "text-center">Add Hero</h3>
                    <form class ="form-horizontal"
                          role ="form" method ="POST"
                          action="createHeroNewSighting">
                        <div class="form-group">
                            <label for="add-name" class="col-md-4 control-label">
                                Name:
                            </label>
                            <div class ="col-md-8">
                                <input type="text" class ="form-control" name ="name" placeholder="Hero Or Villain Name"  required/>
                            </div>
                        </div>
                        <div class ="form-group">
                            <label for="add-description" class ="col-md-4 control-label">
                                Description
                            </label>
                            <div class ="col-md-8">
                                <textarea class="form-control" id="heroDescription" rows="4"  name="heroDescription"placeholder="Enter a Description of Hero Or Villain" required></textarea>


                            </div>
                        </div>
                        <div class ="form-group">
                            <label for="add-VillainOrHero" class ="col-md-4 control-label">
                                Are They a  Villain? 
                            </label>
                            <div class ="col-md-8">

                                <select class="form-control" id="villainSelect" name="isVillain" required>
                                    <option>no</option>
                                    <option>yes</option>

                                </select>
                            </div>
                        </div>

                        <div class ="form-group">
                            <p>


                            </p>
                            <label for="add-email" class ="col-md-4 control-label">
                                Super Powers:
                            </label>
                            <div class ="col-md-8">
                                <p>
                                <button type="button" id="addPower" class="btn btn-default">
                                    Don't See the Hero's SuperPower?
                                </button>
                                </p>
                                <br/>
                                <select multiple class="form-control" id="powerSelectList" name ="powerList" required>

                                    <c:forEach var="currentPowers"  items="${powerList}">

                                        <option value="${currentPowers.superPowerId}"> <c:out value="${currentPowers.name}"/>   </option>

                                    </c:forEach>

                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-8">
                                <input type="submit" class="btn btn-default" value="Create Hero Or Villain"/>
                            </div>
                        </div>


                    </form>
                </div>
            </div>
            <!--
**************************************************************************************************
**************************************************************************************************
**************************************************************************************************
add new Power
**************************************************************************************************
**************************************************************************************************
**************************************************************************************************

            -->
            <div class ="col-xs-12" id='addPowerForm'>
                <div class ="col-xs-5">
                    <h3 class = "text-center">Add SuperPower</h3>
                    <form class ="form-horizontal"
                          role ="form" method ="POST"
                          action="createSuperPowerNewSighting">
                        <div class="form-group">
                            <label for="add-powerName" class="col-md-4 control-label">
                                SuperPower Name:
                            </label>
                            <div class ="col-md-8">
                                <input type="text" class ="form-control" name ="powerName" placeholder="SuperPower Name" required />
                            </div>
                        </div>
                        <div class ="form-group">
                            <label for="add-description" class ="col-md-4 control-label">
                                SuperPower Description:
                            </label>
                            <div class ="col-md-8">
                                <input type="text" class ="form-control" name ="powerDescription" placeholder="Super Power Description" required />
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-8">
                                <input type="submit" id="superPowerButton" class="btn btn-default" value="Create SuperPower"/>
                            </div>
                        </div>


                    </form>
                </div>
            </div>

            <!--
  **************************************************************************************************
  **************************************************************************************************
  **************************************************************************************************
  add new Location
  **************************************************************************************************
  **************************************************************************************************
  **************************************************************************************************
  
            -->
            <div class="col-xs-12" id="locationForm">
                <div class ="col-xs-5">


                    <h3 class = "text-center">Add Location</h3>
                    <form class ="form-horizontal"
                          role ="form" method ="POST"
                          action="createLocationNewSighting">
                        <div class="form-group">
                            <label for="add-description" class="col-md-4 control-label">
                                Description:
                            </label>
                            <div class ="col-md-8">
                                <input type="text" class ="form-control" name ="description" placeholder="Enter a Description" required />
                            </div>
                        </div>
                        <div class ="form-group">
                            <label for="add-city" class ="col-md-4 control-label">
                                City:
                            </label>
                            <div class ="col-md-8">
                                <input type="text" class ="form-control" name ="city" placeholder="Enter a City" required/>
                            </div>
                        </div>
                        <div class ="form-group">
                            <label for="add-country" class ="col-md-4 control-label">
                                Country:
                            </label>
                            <div class ="col-md-8">
                                <input type="text" class ="form-control" name ="country" placeholder="Enter a Country" required />
                            </div>
                        </div>
                        <div class ="form-group">
                            <label for="add-latitude" class ="col-md-4 control-label">
                                Latitude:
                            </label>
                            <div class ="col-md-8">
                                <input type="number" min="-180" max="180" step="any" class ="form-control" name ="latitude" placeholder="Enter a Latitude"  required/>
                            </div>
                        </div>
                        <div class ="form-group">
                            <label for="add-latitude" class ="col-md-4 control-label">
                                Longitude:
                            </label>
                            <div class ="col-md-8">
                                <input type="number" min="-180" max="180"  step="any"  class ="form-control" name ="longitude" placeholder="Enter a Longitude" required />
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-8">
                                <input type="submit" class="btn btn-default" value="Create Location" required/>
                            </div>
                        </div>


                    </form>
                </div>
            </div>
            <!--Container Div-->   
        </div>













        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>    
        <script src="${pageContext.request.contextPath}/js/superHero.js"></script>


    </body>
</html>

