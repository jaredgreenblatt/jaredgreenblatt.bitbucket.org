<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Super Hero Tracking System</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">  
        <link href="${pageContext.request.contextPath}/css/superHero.css" rel="stylesheet">   
        <link href="https://fonts.googleapis.com/css?family=Bungee+Shade" rel="stylesheet">

    </head>
    <body>
        <div class="container">
            <div class="row VendingMachineTitleRow header">
                <div class="col-xs-offset-2 col-xs-8">
                    <h1 class ="text-center">Super Hero Tracking System</h1> 

                </div>
            </div>
            <hr />
            <hr />
            <div class="col-xs-12">
                <center>
                    <div class="navbar text-center">
                        <ul class="nav nav-pills">
                            <li role="presentation"><a href="${pageContext.request.contextPath}/">Home</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/heroOrVillain">Heros</a></li>
                            <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/superPower">SuperPowers</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/locations">Locations</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/organizations">Organizations</a></li>
                            <li role="presentation"><a href="${pageContext.request.contextPath}/sightings">Sightings</a></li>

                        </ul>    
                    </div>
                </center>
            </div>


            <h2 class ="col-xs-12 text-center">SuperPowers</h2> 


            <div class="col-xs-7">
                <h3 class = "text-center">Edit SuperPower</h3>
                <form class ="form-horizontal"
                      role ="form" method ="POST"
                      action="editSuperPower">
                    <input type="hidden" id="powerId" name="powerId" value="${superPower.superPowerId}"/>

                    <div class ="form-group">               
                        <label for="add-powerName" class="col-xs-5 control-label">
                            SuperPower Name:
                        </label>
                        <div class ="col-xs-7">
                            <input type="text" class ="form-control" name ="powerName" placeholder="SuperPower Name"  value="${superPower.name}" required/>
                        </div>
                    </div>  


                    <div class ="form-group">
                        <label for="add-description" class ="col-xs-5 control-label">
                            SuperPower Description:
                        </label>
                        <div class ="col-xs-7">
                            <input type="text" class ="form-control" name ="powerDescription" placeholder="Super Power Description" value="${superPower.description}" required/>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-xs-offset-4 col-xs-8">
                            <input type="submit" class="btn btn-default" value="Update SuperPower"/>
                        </div>
                    </div>


                </form>
            </div>
        </div>










    </div>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

</body>
</html>

