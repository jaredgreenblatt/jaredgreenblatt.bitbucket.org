/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.classroster;

import com.sg.classroster.controller.ClassRosterController;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * Add code to instantiate the Service Layer (and pass the DAO into its
 * constructor) Modify the code that instantiates the Controller to pass the
 * Service Layer instance to the constructor. Modify the main method of your App
 * class so that it looks like this:
 */
public class App {

    public static void main(String[] args) {
//        // Instantiate the UserIo implementation
//        UserIO myIo = new UserIOConsoleImpl();
//        // Instantiate the View and wire the UserIO implementation into it
//        ClassRosterView myView= new ClassRosterView(myIo);
//        //Intstantiate the DAO
//        ClassRosterDao myDao = new ClassRosterDaoFileImpl();
//        //Instantiate the Audit Dao
//        ClassRosterAuditDao myAuditDao = new ClassRosterAuditDaoFileImpl();
//        //Instantiate the service layer and wire the dao and the audit dao into it
//        ClassRosterServiceLayer myService = new
//            ClassRosterServiceLayerImpl(myDao, myAuditDao);
//        //Instantiate the conroller and wire the service layer into it
//        ClassRosterController controller = new ClassRosterController(myService,
//                myView);
//        //Kick off the Controller
//        controller.run();
//        
        ApplicationContext ctx
                = new ClassPathXmlApplicationContext("applicationContext.xml");
        ClassRosterController controller = ctx.getBean("controller", ClassRosterController.class);
        controller.run();

    }

}
