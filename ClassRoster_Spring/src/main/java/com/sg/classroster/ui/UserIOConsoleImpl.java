/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.classroster.ui;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class UserIOConsoleImpl implements UserIO {

    Scanner sc = new Scanner(System.in);

    @Override
    public void print(String message) {
        System.out.println(message);
    }

    @Override
    public double readDouble(String prompt) {
        double x;
        System.out.println(prompt);
        x = Double.parseDouble(sc.nextLine());
        return x;
    }

    @Override
    public double readDouble(String prompt, double min, double max) {
        double x, y;

        while (true) {
            System.out.println(prompt);
            x = Double.parseDouble(sc.nextLine());
            if (x >= min && x <= max) {
                y = x;

                break;

            }
        }
        return y;
    }

    @Override
    public float readFloat(String prompt) {
        float x;
        System.out.println(prompt);
        x = Float.parseFloat(sc.nextLine());
        return x;

    }

    @Override
    public float readFloat(String prompt, float min, float max) {
        float x, y;

//        
//        
        while (true) {
            System.out.println(prompt);
            x = Float.parseFloat(sc.nextLine());
            if (x >= min && x <= max) {
                y = x;
                break;

            }

        }

        return y;

    }

    @Override
    public int readInt(String prompt) {
        int x;
        System.out.println(prompt);
        x = Integer.parseInt(sc.nextLine());
        return x;
    }

    @Override
    public int readInt(String prompt, int min, int max) {
        int x, y;
        while (true) {
            System.out.println(prompt);
            x = Integer.parseInt(sc.nextLine());
            if (x >= min && x <= max) {
                y = x;
                break;
            }

        }
        return y;

    }

    @Override
    public long readLong(String prompt) {
        long x;
        System.out.println(prompt);
        x = Long.parseLong(sc.nextLine());
        return x;

    }

    @Override
    public long readLong(String prompt, long min, long max) {
        long x, y;
        while (true) {
            System.out.println(prompt);
            x = Long.parseLong(sc.nextLine());
            if (x >= min && x <= max) {
                y = x;
                break;

            }

        }
        return y;

    }

    @Override
    public String readString(String prompt) {
        String x;
        System.out.println(prompt);
        x = sc.nextLine();
        return x;

    }

}
