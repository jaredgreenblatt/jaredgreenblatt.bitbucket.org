/*
The Controller file is the MAESTRO of the application. All of the methods are controlled from
 */
package com.sg.dvd;

import com.sg.DVD.ui.DVDView;
import com.sg.DVD.ui.UserIO;
import com.sg.DVD.ui.UserIOConsoleImpl;
import com.sg.dvd.controller.DVDController;
import com.sg.dvd.dao.DVDDao;
import com.sg.dvd.dao.DVDDaoFileImpl;

/** 
 *
 * The APP file will kick off the program.
 * 
 */
public class App {
    public static void main(String[] args) {
        UserIO myIo= new UserIOConsoleImpl();
        DVDView myView = new DVDView(myIo);
        DVDDao myDao =new DVDDaoFileImpl();
        DVDController controller = new DVDController(myDao, myView);
        
        
        controller.run();
    }
    
}
