/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvd.controller;

import com.sg.DVD.dto.DVD;
import com.sg.DVD.ui.DVDView;
import com.sg.DVD.ui.UserIO;
import com.sg.DVD.ui.UserIOConsoleImpl;
import com.sg.dvd.dao.DVDDao;
import com.sg.dvd.dao.DVDDaoException;
import com.sg.dvd.dao.DVDDaoFileImpl;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apprentice The controller is the MAESTRO of the application. The
 * methods to use the view area that can print to screen or take user inputs of
 * the MVC as well as using the DAO to work with the DVD Object and file.
 *
 */
public class DVDController {
//
//    DVDView view = new DVDView();
//    private UserIO io = new UserIOConsoleImpl();
//    DVDDao dao = new DVDDaoFileImpl();

    private UserIO io = new UserIOConsoleImpl();
    DVDView view;
    DVDDao dao;

    public void run() {
        boolean keepGoing = true;
        int menuSelection = 0;
        try {
            while (keepGoing) {
                menuSelection = getMenuSelection();

                switch (menuSelection) {
                    case 1:
                        listDvds();
                        break;
                    case 2:
                        createDvd();
                        break;
                    case 3:
                        viewDvd();
                        break;
                    case 4:
                        searchDvds();
                        break;
                    case 5:
                        editDVD();
                        break;
                    case 6:
                        removeDvd();
                        break;
                    case 7:
                        keepGoing = false;
                        break;
                    default:
                        unknownCommand();

                }

            }
            exitMessage();
        } catch (DVDDaoException e) {
            view.displayErrorMessage(e.getMessage());
        }
    }

    public DVDController(DVDDao dao, DVDView view) {
        this.dao = dao;
        this.view = view;
    }

    private int getMenuSelection() {
        return view.printMenuAndGetSelection();
    }

    /**
     * Method used to create dvds in controller. It calls on the view to display
     * the banners and then to create a new student then it calls on the dao to
     * store the student.
     */
    private void createDvd() throws DVDDaoException {
        view.displayCreateDvdBanner();
        DVD newDvd = view.getNewDvdInfo();
        dao.addDvd(newDvd.getDvdId(), newDvd);
        view.displayCreateSuccessBanner();

    }

    /**
     * Method used to edit dvds in controller. It calls on the view to display
     * the banners and then prompts for the DVD you want to search. It then
     * displays that information. It ill then launch the EDIT dvd view. Which will
     * allow the user to edit the DVD item by item. Once completed it will pass
     * the edited DVD object to the DAO for Storage.
     */
    private void editDVD() throws DVDDaoException {
        view.displayEditDvdBanner();
        String dvdId = view.getDvdIdChoice();
        DVD dvd = dao.getDvd(dvdId);
        DVD editedDvd = view.makeDvdEdit(dvd);
        dao.addDvd(editedDvd.getDvdId(), editedDvd);
        view.displayEditDvdSuccessBanner();

    }
    /**
     * Method used to list dvds in controller. It calls on the view to display
     * the banners and then creates a list that gets al DVDs from the DAO. It 
     * then calls on the view to display the list to the end user.
     */
 
    private void listDvds() throws DVDDaoException {
        view.displayDisplayAllBanner();
        List<DVD> dvdList = dao.getAllDvds();
        view.displayDvdList(dvdList);
    }
    /**
     * Method used to search for  dvds  by DVD Title. We get a DVD list from the
     * DAO. We call on the getAllDvds method. we then use the view to search the
     * DVD list to display the movies that match the title.
     *
     */

    private void searchDvds() throws DVDDaoException {
        view.displayDVDTitleSearch();
        String titleQuery =view.getDvdTitleForWildCardSearch();
        List<String> titleList = dao.SearchDvdTitle(titleQuery);
        view.wildCardDvdSearch(titleList);

    }
    /**
     * Method used to view one dvd in controller. It calls on the view to display
     * the banners and then calls on the view to ask the user which DVD they want
     * to look at it will then go to the dao and grab that DVD info. It will then 
     * use the view to display that DVD.
     */

    private void viewDvd() throws DVDDaoException {
        view.displayDisplayDvdBanner();
        String dvdId = view.getDvdIdChoice();
        DVD dvd = dao.getDvd(dvdId);
        view.displayDvd(dvd);
    }
/**
     * Method used to remoce dvds in controller. It calls on the view to display
     * the banners and then using the view it prompts the user for which DVD we
     * want to remove. the controller will then ask the dao to delete that user
     * based on the DvdID
     */
    private void removeDvd() throws DVDDaoException {
        view.displayRemoveDvdBanner();
        String DvdId = view.getDvdIdChoice();
        dao.removeDvd(DvdId);
        view.displayRemoveSuccessBanner();
    }

    private void unknownCommand() {
        view.displayUnkownCommandBanner();

    }

    private void exitMessage() {
        view.displyExitBanner();
    }

}
