/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvd.dao;

import com.sg.DVD.dto.DVD;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface DVDDao {

    /**
     * Adds the DVD to the library and it associates it with the DVD ID If there
     * is already a dvd associated with the given DVD id it will return the dvd
     * object, otherwise it will return null.
     *
     * @param dvdID id with which dvd is to be associated
     * @param dvd dvd to be added to the roster
     * @return the DVD object previously associated withe the given dvd id if it
     * exists, null otherwise
     */
    DVD addDvd(String dvdId, DVD dvd) throws DVDDaoException;

    /**
     * Returns a String array containing the DVD Id's of all the dvd's in the
     * collection
     *
     * @return String array containing the id's of all the dvds in the roster.
     */
    List<DVD> getAllDvds() throws DVDDaoException;
 
    /**
     * Returns a String array containing the DVD ID and Title of the Matching 
     * the DVD title being Searched
     * collection
     *
     * @return String array containing the list of movies matching the title 
     * being searched.
     */
    List<String> SearchDvdTitle(String dvdTitle) throws DVDDaoException;

    /**
     * Returns the dvd object assoicated with the given dvd id. returns null if
     * no such student exist
     *
     * @param dvdId id of the dvd to retrieve
     * @return Dvd object associated with the given dvd id, null if no such
     * student exists
     */
    DVD getDvd(String dvdId) throws DVDDaoException;

    /**
     * Removes from the collection the dvd associated with the given id. Returns
     * the student object that is being removed or null if there is no student
     * associated with the given id
     *
     * @param dvdId id of the dvd to be removed
     * @return Dvd object that was removed or null if no student was associated
     * with the given DVD id.
     */
    DVD removeDvd(String dvdId) throws DVDDaoException;

}
