/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.DVD.ui;

import com.sg.DVD.dto.DVD;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import static java.time.temporal.TemporalQueries.localDate;

import java.util.List;

/**
 *
 * @author apprentice
 */
public class DVDView {

    private UserIO io;
    int menuSelection = 0;
    LocalDate ld = LocalDate.now();

    public DVDView(UserIO io) {
        this.io = io;
    }

    //
    /**
     * Method used to print the menu items
     *
     * @return an int used in the menu selection.
     */
    public int printMenuAndGetSelection() {
        io.print("Main Menu");
        io.print("1. List DVD's");
        io.print("2. Create a DVD");
        io.print("3. View a DVD");
        io.print("4. Search DVD by Title");
        io.print("5. Edit a DVD");
        io.print("6. Remove a DVD");
        io.print("7. Exit");

        return io.readInt("Please Select from the above choices", 1, 7);
    }

    /**
     * This method is used to get the new DVD information. When the information
     * is completely filled out we then set the DVD object properties and then
     * return a DVD object. This is then sent to the controller that then sends
     * it to the DAO to store object in the DVD class and Collections Text file.
     *
     * @return DVD Object is return full of the information that was provided in
     * the method.
     */
    public DVD getNewDvdInfo() {

        String dvdId = io.dvdInput("Please enter DVD ID");
        String dvdTitle = io.dvdInput("Please enter the Dvd title");
        String dvdReleaseDate = io.dvdInput("Please enter the DVD Release Date");
        String dvdMppaRating = io.dvdInput("Please enter the DVD MPPA Rating");
        String dvdDirectorsName = io.dvdInput("Please enter the Directors Name");
        String dvdStudio = io.dvdInput("Please enter the DVD Studio");
        String dvdUserRating = io.dvdInput("Please enter the DVD user rating");

        DVD currentDvd = new DVD(dvdId);
        currentDvd.setDvdTitle(dvdTitle);
        currentDvd.setDvdReleaseDate(LocalDate.parse(dvdReleaseDate+"-01-01"));
        currentDvd.setDvdMppaRating(dvdMppaRating);
        currentDvd.setDvdDirectorsName(dvdDirectorsName);
        currentDvd.setDvdStudio(dvdStudio);
        currentDvd.setDvdUserRating(dvdUserRating);

        return currentDvd;
    }

    /**
     * Method that takes a list of DVD objects as parameter and displays the
     * info for each DVD to the screen.
     *
     * @param dvdList
     */
    public void displayDvdList(List<DVD> dvdList) {
        for (DVD currentDvd : dvdList) {
            io.print("DVD ID: " + currentDvd.getDvdId() + " "
                    + "DVD TITLE: " + currentDvd.getDvdTitle() + " "
                    + "DVD RELEASE DATE: " + currentDvd.getDvdReleaseDate().format(DateTimeFormatter.ofPattern("MM")) + " "
                    + "DVD MPPA RATING: " + currentDvd.getDvdMppaRating() + " "
                    + "DVD DIRECTORS NAME: " + currentDvd.getDvdDirectorsName() + " "
                    + "DVD STUDIO: " + currentDvd.getDvdStudio() + " "
                    + "DVD USER RATING: " + currentDvd.getDvdUserRating()
            );
        }
        io.readString("Please hit enter to continue.");
    }

    /**
     *
     * Wildcard Search for tile. Returning the DVD information for users if they
     * match the QUERY string inputed by the user. It will then print out the
     * properties of the DVD object for the matches. I am also counting each
     * time an item is added so that I can report if the search returned no
     * matches.
     *
     * @param dvdList
     * @param strList
     */
    public void wildCardDvdSearch(List<String> dvdList) {
        for (String dvd : dvdList) {
            io.print(dvd);

        }
        if (dvdList.isEmpty()) {
            io.print("There are no DVD's in you library that match your"
                    + " query.");

        }
        io.readString("Please hit enter to continue.");

//        int count = 0;
//
//        displayDVDTitleSearchResult();
//        for (DVD dvd : dvdList) {
//            if (dvd.getDvdTitle().toLowerCase().contains(queryStr.toLowerCase())) {
//                io.print("DVD ID: " + dvd.getDvdId() + " "
//                        + "DVD TITLE: " + dvd.getDvdTitle() + " "
//                        + "DVD RELEASE DATE: " + dvd.getDvdReleaseDate() + " "
//                        + "DVD MPPA RATING: " + dvd.getDvdMppaRating() + " "
//                        + "DVD DIRECTORS NAME: " + dvd.getDvdDirectorsName() + " "
//                        + "DVD STUDIO: " + dvd.getDvdStudio() + " "
//                        + "DVD USER RATING: " + dvd.getDvdUserRating()
//                );
//                count++;
//
//            }
//
//        }
//        if (count == 0) {
//            System.out.println("There are no DVD's in you library with " + queryStr
//                    + " in it's title.");
//
//        }
//
//        io.readString("Please hit enter to continue.");
//    }
        /**
         * Used to display one dvd. If no matches.are found there will be no
         * dvds returned.
         *
         * @param dvd
         */
    }

    public void displayDvd(DVD dvd) {
        if (dvd != null) {
            io.print("DVD ID: " + dvd.getDvdId());
            io.print("DVD TITLE: " + dvd.getDvdTitle());
            io.print("DVD RELEASE DATE: " + dvd.getDvdReleaseDate());
            io.print("DVD MPPA RATING: " + dvd.getDvdMppaRating());
            io.print("DVD DIRECTOR NAME: " + dvd.getDvdDirectorsName());
            io.print("DVD STUDIO: " + dvd.getDvdStudio());
            io.print("DVD USER RATING: " + dvd.getDvdUserRating());
        } else {
            io.print("No such DVD.");
        }
        io.readString("Please hit enter to continue.");

    }

    /**
     * This Make DVD Edit Method is used to edit DVDS that are already Stored in
     * the collection. We show the search result for the DVD that they want to
     * edit we then ask for each item of the DVD if they would like to edit. At
     * the end we return a new DVD object that contains the users EDITS
     *
     * @param dvd
     * @return editedDVD
     */
    public DVD makeDvdEdit(DVD dvd) {
// Setting Temporary Variables to Store the values of the current DVD object.
        String dvdIdTemp = "";
        String dvdTitleTemp = "";
        String dvdReleaseDateTemp = "";
        String dvdMppaRating = "";
        String dvdDirectorsName = "";
        String dvdStudio = "";
        String dvdUserRating = "";
        //Search that will return the current values of a DVD in the collection.
        if (dvd != null) {
            io.print("DVD ID: " + dvd.getDvdId());
            dvdIdTemp = dvd.getDvdId();
            io.print("DVD TITLE: " + dvd.getDvdTitle());
            dvdTitleTemp = dvd.getDvdTitle();
            io.print("DVD RELEASE DATE: " + dvd.getDvdReleaseDate());
            dvdReleaseDateTemp = dvd.getDvdReleaseDate().format(DateTimeFormatter.ISO_DATE);
            io.print("DVD RATING: " + dvd.getDvdMppaRating());
            dvdMppaRating = dvd.getDvdMppaRating();
            io.print("DVD DIRECTOR NAME: " + dvd.getDvdDirectorsName());
            dvdDirectorsName = dvd.getDvdDirectorsName();
            io.print("DVD STUDIO: " + dvd.getDvdStudio());
            dvdStudio = dvd.getDvdStudio();
            io.print("DVD USER RATING: " + dvd.getDvdUserRating());
            dvdUserRating = dvd.getDvdUserRating();
            io.print("_______________________________________________________");
// If no match found it will return no matches.
        } else {
            io.print("No such DVD.");

        }
        /* Prompt to ask if user wants to edit the DVD if YES it will launch 
        nested if statement that will go through each item in the DVD object 
        asking if the user would like to replace. If no is selected it will exit 
        and provide the user a message stating that the DVD was not edited.
         */
        String edit = io.readString("Do you want to edit this DVD's "
                + "information?");
        edit = edit.toUpperCase();
        if (edit.equals("Y") || (edit.equals("YES"))) {

            edit = io.readString("Do you want to edit the title");
            edit = edit.toUpperCase();
            if (edit.equals("Y") || (edit.equals("YES"))) {
                dvdTitleTemp = io.dvdInput("Please enter the Dvd title");

            } else if (edit.equals("N") || (edit.equals("No"))) {
                io.print("The title will remain " + dvdTitleTemp);
            } else {
                io.print("The title will remain " + dvdTitleTemp);
            }

            edit = io.readString("Do you want to edit the Release Date?");
            edit = edit.toUpperCase();
            if (edit.equals("Y") || (edit.equals("YES"))) {
                dvdReleaseDateTemp = io.dvdInput("Please enter the Release Date");

            } else if (edit.equals("N") || (edit.equals("No"))) {
                io.print("The release Date will remain " + dvdReleaseDateTemp);
            }

            edit = io.readString("Do you want to edit the MPAA RATING?");
            edit = edit.toUpperCase();
            if (edit.equals("Y") || (edit.equals("YES"))) {
                dvdMppaRating = io.dvdInput("Please enter the Rating");

            } else if (edit.equals("N") || (edit.equals("No"))) {
                io.print("The rating will remain " + dvdMppaRating);
            } else {
                io.print("The rating will remain " + dvdMppaRating);
            }

            edit = io.readString("Do you want to edit the Director?");
            edit = edit.toUpperCase();
            if (edit.equals("Y") || (edit.equals("YES"))) {
                dvdDirectorsName = io.dvdInput("Please enter the new Director");

            } else if (edit.equals("N") || (edit.equals("No"))) {
                io.print("The Director will remain " + dvdDirectorsName);
            } else {
                io.print("The Director will remain " + dvdDirectorsName);
            }
            edit = io.readString("Do you want to edit the Studio Name?");
            edit = edit.toUpperCase();
            if (edit.equals("Y") || (edit.equals("YES"))) {
                dvdStudio = io.dvdInput("Please enter the new Studio");

            } else if (edit.equals("N") || (edit.equals("No"))) {
                io.print("The Studio will remain " + dvdStudio);
            } else {
                io.print("The Studio will remain " + dvdStudio);
            }
            edit = io.readString("Do you want to edit the User Rating?");
            edit = edit.toUpperCase();
            if (edit.equals("Y") || (edit.equals("YES"))) {
                dvdUserRating = io.dvdInput("Please enter the new User Rating");

            } else if (edit.equals("N") || (edit.equals("No"))) {
                io.print("The User Rating will remain " + dvdUserRating);
            } else {
                io.print("The User Rating will remain " + dvdUserRating);
            }
            /*
            Will create a new DVD object with the edited fields and return a new 
            DVD object back to the controller so that the changes can be sent to 
            the dao.
             */

            DVD editDvd = new DVD(dvdIdTemp);
            editDvd.setDvdTitle(dvdTitleTemp);
            editDvd.setDvdReleaseDate(LocalDate.parse(dvdReleaseDateTemp+"-01-01", DateTimeFormatter.ISO_DATE));
            editDvd.setDvdMppaRating(dvdMppaRating);
            editDvd.setDvdDirectorsName(dvdDirectorsName);
            editDvd.setDvdStudio(dvdStudio);
            editDvd.setDvdUserRating(dvdUserRating);

            return editDvd;
            /*
            If the user elects not to make a change or simply does not input yes
            or no the user will be given a message that no edits where made and 
            the original DVD object will be returned to the controller to put
            back into the DAO.
            
             */
        } else if (edit.equals("N") || (edit.equals("No"))) {
            io.print("No edits have been made.");

        } else {
            io.print("No edits have been made because you didn't select yes.");

        }
        return dvd;
    }

    public String getDvdIdChoice() {
        return io.readString("Please Enter the DVD ID.");
    }

    public String getDvdTitleForWildCardSearch() {
        return io.readString("Please Enter the DVD Title you would like to search.");
    }

    public void displayCreateDvdBanner() {
        io.print("=== Create DVD ===");
    }

    public void displayCreateSuccessBanner() {
        io.readString("DVD succesfully created. Please hit enter to continue");

    }

    public void displayDisplayAllBanner() {
        io.print("=== Display All DVD's ===");
    }

    public void displayDVDTitleSearch() {
        io.print("=== Please Enter the Title you wish to search ===");
    }

    public void displayDVDTitleSearchResult() {
        io.print("=== DVD Search Results ===");
    }

    public void displayDisplayDvdBanner() {
        io.print("=== Display DVD ===");
    }

    public void displayEditDvdBanner() {
        io.print("=== Edit  DVD ===");
    }

    public void displayEditDvdSuccessBanner() {
        io.readString("DVD succesfully edited. Please hit enter to continue.");
    }

    public void displayRemoveDvdBanner() {
        io.print("=== Remove DVD ===");
    }

    public void displayRemoveSuccessBanner() {
        io.readString("DVD succesfully removed. Please hit enter to continue.");
    }

    public void displyExitBanner() {
        io.print("Good Bye!!!");

    }

    public void displayUnkownCommandBanner() {
        io.print("Unknown Command!!!");
    }

    public void displayErrorMessage(String errorMsg) {
        io.print("=== ERROR ===");
        io.print(errorMsg);
    }

}
