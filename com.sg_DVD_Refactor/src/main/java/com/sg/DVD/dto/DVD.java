/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.DVD.dto;

import java.time.LocalDate;

/**
 *
 * DVD CLASS is created here. This File defines what the DVD object will be made
 * up of.
 */
public class DVD {

    private String dvdId;
    private String dvdTitle;
    private LocalDate dvdReleaseDate;
    private String dvdMppaRating;

    private String dvdDirectorsName;
    private String dvdStudio;
    private String dvdUserRating;

//ID is read only it is passed in as a constructor. 
    public DVD(String dvdId) {
        this.dvdId = dvdId;
    }

    public String getDvdId() {
        return dvdId;
    }

    public String getDvdTitle() {
        return dvdTitle;
    }

    public void setDvdTitle(String dvdTitle) {
        this.dvdTitle = dvdTitle;
    }

    public String getDvdMppaRating() {
        return dvdMppaRating;
    }

    public void setDvdMppaRating(String dvdMppaRating) {
        this.dvdMppaRating = dvdMppaRating;
    }

    public String getDvdDirectorsName() {
        return dvdDirectorsName;
    }

    public void setDvdDirectorsName(String dvdDirectorsName) {
        this.dvdDirectorsName = dvdDirectorsName;
    }

    public String getDvdStudio() {
        return dvdStudio;
    }

    public void setDvdStudio(String dvdStudio) {
        this.dvdStudio = dvdStudio;
    }

    public String getDvdUserRating() {
        return dvdUserRating;
    }

    public void setDvdUserRating(String dvdUserRating) {
        this.dvdUserRating = dvdUserRating;
    }

    public LocalDate getDvdReleaseDate() {
        return dvdReleaseDate;
    }

    public void setDvdReleaseDate(LocalDate dvdReleaseDate) {
        this.dvdReleaseDate = dvdReleaseDate;
    }

}
