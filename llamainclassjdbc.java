private static final class LllamaMapper implements RowMapper<Llama> {

    @Override
    public Llama mapRow(ResultSet rs, int i) throws SQLException {
        Llama ll = new Llama();
        au.setMyLlamaName(rs.getString("myLlamaName"));
        au.seMyLlamaAge(rs.getInt("myLLamaAge"));
        au.setMyLlamaId(rs.getInt(MyLlamaId"));
        return au;
    }

}


private static final String SQL_SELECT_ALL_LLAMAS
         = "select * from Llamas";


         @Override
  public List<Author> getAllLlamas() {
      return jdbcTemplate.query(SQL_SELECT_ALL_LLAMAS, new LlamaMapper());
  }



  private static final class LllamaMapperArgs implements RowMapper<Llama> {

      @Override
      public Llama mapRow(ResultSet rs, int i) throws SQLException {
          Llama ll = new Llama();
          au.setMyLlamaName(rs.getString("myLlamaName"));
          au.seMyLlamaAge(rs.getInt("myLLamaAge"));
          au.setMyLlamaId(rs.getInt("author_id"));
          return au;
      }

  }
