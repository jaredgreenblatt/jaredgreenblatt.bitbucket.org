/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.basicsprogrammingconcepts;

/**
 *
 * @author apprentice
 */
public class SummativeSums {

    public static void main(String[] args) {

        //building the array of ints.
        int[] arrayofInts1 = {1, 90, -33, -55, 67, -16, 28, -55, 15};
        int[] arrayofInts2 = {999, -60, -77, 14, 160, 301};
        int[] arrayofInts3 = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120,
            130, 140, 150, 160, 170, 180, 190, 200, -99};

        // screen printouts calling the sum of array methods.
        System.out.println("#1 Array Sum: " + sumOfArrays(arrayofInts1));
        System.out.println("#2 Array Sum: " + sumOfArrays(arrayofInts2));
        System.out.println("#3 Array Sum: " + sumOfArrays(arrayofInts3));

    }
//method that returns int. This is returning the sum of the array elements.

    public static int sumOfArrays(int[] sum) {
        //x variable that will be returned by this methods. Sum of arrays.
        int x = 0;
        //For Loop that will iterate throught the array.
        for (int i = 0; i < sum.length; i++) {
            //adding the value of each array spot to X.
            x += sum[i];

        }
        //returns the  final sum so that it can be printed to screen in the main.

        return x;

    }
 
}
