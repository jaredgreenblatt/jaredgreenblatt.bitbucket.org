/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.basicsprogrammingconcepts;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class HealthyHeart {

    public static void main(String[] args) {
        /*Declaring variables that are going to be used in this exercise*/
        int age, maxHeartRate;
        float targetZoneOne = .5f, targetZoneTwo = .85f,
                targetZoneOneSum, targetZoneTwoSum;
        //varible used to calc max heart rate.
        int calcMaxHeartRate = 220;
        int intZoneOneSum, intZoneTwoSum;
        boolean inputCheck;
        // Scanner used to read in user input .
        Scanner inputReader = new Scanner(System.in);

        //Prompting and recieving the user input.
        System.out.println("How old Are you?");
        do {
            if (!(inputReader.hasNextInt())) {
                inputReader.next();
                System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
                System.out.println("ERROR Please Input an Age greater than 0!");
                System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
                System.out.println("");
                System.out.println("How old Are you?");
            }
            inputCheck = true;

        } while (!(inputReader.hasNextInt()));

        age = inputReader.nextInt();

        //Calculating the max heart rate.
        maxHeartRate = calcMaxHeartRate - age;
        //Calculating the heart rate zones.
        targetZoneOneSum = maxHeartRate * targetZoneOne;
        targetZoneTwoSum = maxHeartRate * targetZoneTwo;
        intZoneOneSum = (int) targetZoneOneSum;
        intZoneTwoSum = (int) targetZoneTwoSum;

        //Printing out the max hr and zones of HR.
        System.out.println("Your maximum heart rate should be  " + maxHeartRate + " beats per minute");
        System.out.println("Your target HR Zone is " + intZoneOneSum + " - " + intZoneTwoSum + " beats per minute");

    }
 
}
