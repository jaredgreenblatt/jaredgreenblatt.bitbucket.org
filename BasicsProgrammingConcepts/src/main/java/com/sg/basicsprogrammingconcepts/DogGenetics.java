/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.basicsprogrammingconcepts;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class DogGenetics {

    public static void main(String[] args) {
        // Creating the varibles.
        String dogName;
        Scanner inputeReader = new Scanner(System.in);
        Random randy = new Random();
        // Scanner used to get the dogName input.
        System.out.println("What is your dogs name? ");
        dogName = inputeReader.nextLine();
        //Sout Messages 
        System.out.println("Well then, I have this highly reliable"
                + " report on " + dogName);
        System.out.println("Esquire's prestigious background right here.");
        System.out.println("");
        System.out.println(dogName + " is..");
        // First Random  will generate a random number between 1 -100

        //int dog1 = randy.nextInt(100)+1;
        // dog 1 will be a random value from 1-9.
        int dog1 = randy.nextInt(9)+1 ;
        //dog 2 will be a random value from 1 to 19.
        int dog2 = randy.nextInt(19 )+1 ;
        //dog 3 will be a random value from 1 to 29.
        int dog3 = randy.nextInt(29)+1;
        //dog 4 will be a random value from 1 to 39.  
        int dog4 = randy.nextInt(39 )+1;
        // dog 5 will be the remaining number of the sum of the other 4 dogs.
        int dog5 = (100 -( dog1 + dog2 + dog3 + dog4));
 
        int sum = dog1 + dog2 + dog3 + dog4 + dog5;
        if (sum == 100) {
            System.out.println(dog1 + "% St. Bernard");
            System.out.println(dog2 + "% Chihuahua");
            System.out.println(dog3 + "% Dramatic RedNosed Asian Pug");
            System.out.println(dog4 + "% Common Cur");
            System.out.println(dog5 + "% King Doberman");

        }
        System.out.println("Wow, that's QUITE the dog!");

    }

}
