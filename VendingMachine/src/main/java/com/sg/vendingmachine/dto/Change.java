/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.dto;

import java.math.BigDecimal;

/**
 *
 * 
 * @author apprentice
 */
public class Change {
    // Setting the Values of the Change Properties.

    private final BigDecimal ONE_HUNDRED_DOLLAR_BILL = new BigDecimal("100");
    private final BigDecimal TWENTY_DOLLAR_BILL = new BigDecimal("20");
    private final BigDecimal TEN_DOLLAR_BILL = new BigDecimal("10");
    private final BigDecimal FIVE_DOLLAR_BILL = new BigDecimal("5");
    private final  BigDecimal ONE_DOLLAR_BILL = new BigDecimal("1");
    private final BigDecimal QUARTER = new BigDecimal("0.25");
    private final  BigDecimal DIME = new BigDecimal("0.10");
    private final BigDecimal NICKLE = new BigDecimal("0.05");
    private final BigDecimal PENNY = new BigDecimal("0.01");
    private int quarterCount = 0;
    private int dimeCount = 0;
    private int nickleCount = 0;
    private int pennyCount = 0;
    private int oneHundredDollarBillCount = 0;
    private int twentyDollarBillCount = 0;
    private int tenDollarBillCount = 0;
    private int fiveDollarBillCount = 0;
    private int oneDollarBillCount = 0;
    
    
    /**
     * Constuctor used to return the change object after the remaining amount
     * is run through a while loop to evaluate for each money denomination.
     * While the the amount is greater than or equal the counter for that amount 
     * will increment by one. Getters and setter will then be used by the view to
     * print the amount of change owed to the user.
     * @param changeAmount 
     */

    public Change(BigDecimal changeAmount) {

        while (changeAmount.compareTo(ONE_HUNDRED_DOLLAR_BILL) == 1 || changeAmount.compareTo(ONE_HUNDRED_DOLLAR_BILL) == 0) {
            changeAmount = changeAmount.subtract(ONE_HUNDRED_DOLLAR_BILL);
            oneHundredDollarBillCount++;
        }
        while (changeAmount.compareTo(TWENTY_DOLLAR_BILL) == 1 || changeAmount.compareTo(TWENTY_DOLLAR_BILL) == 0) {
            changeAmount = changeAmount.subtract(TWENTY_DOLLAR_BILL);
            twentyDollarBillCount++;
        }
         while (changeAmount.compareTo(TEN_DOLLAR_BILL) == 1 || changeAmount.compareTo(TEN_DOLLAR_BILL) == 0) {
            changeAmount = changeAmount.subtract(TEN_DOLLAR_BILL);
            tenDollarBillCount++;
        }
         while (changeAmount.compareTo(FIVE_DOLLAR_BILL) == 1 || changeAmount.compareTo(FIVE_DOLLAR_BILL) == 0) {
            changeAmount = changeAmount.subtract(FIVE_DOLLAR_BILL);
            fiveDollarBillCount++;
        }
        while (changeAmount.compareTo(ONE_DOLLAR_BILL) == 1 || changeAmount.compareTo(ONE_DOLLAR_BILL) == 0) {
            changeAmount = changeAmount.subtract(ONE_DOLLAR_BILL);
            oneDollarBillCount++;
        }
       

      

        while (changeAmount.compareTo(QUARTER) == 1 || changeAmount.compareTo(QUARTER) == 0) {
            changeAmount = changeAmount.subtract(QUARTER);
            quarterCount++;

        }
        while (changeAmount.compareTo(DIME) == 1 || changeAmount.compareTo(DIME) == 0) {
            changeAmount = changeAmount.subtract(DIME);
            dimeCount++;

        }
        while (changeAmount.compareTo(NICKLE) == 1 || changeAmount.compareTo(NICKLE) == 0) {
            changeAmount = changeAmount.subtract(NICKLE);
            nickleCount++;

        }
        while (changeAmount.compareTo(PENNY) == 1 || changeAmount.compareTo(PENNY) == 0) {
            changeAmount = changeAmount.subtract(PENNY);
            pennyCount++;

        }

    }

    public BigDecimal getONE_HUNDRED_DOLLAR_BILL() {
        return ONE_HUNDRED_DOLLAR_BILL;
    }

    public BigDecimal getTWENTY_DOLLAR_BILL() {
        return TWENTY_DOLLAR_BILL;
    }

    public BigDecimal getTEN_DOLLAR_BILL() {
        return TEN_DOLLAR_BILL;
    }

    public BigDecimal getFIVE_DOLLAR_BILL() {
        return FIVE_DOLLAR_BILL;
    }

    public BigDecimal getONE_DOLLAR_BILL() {
        return ONE_DOLLAR_BILL;
    }

    public BigDecimal getQUARTER() {
        return QUARTER;
    }

    public BigDecimal getDIME() {
        return DIME;
    }

    public BigDecimal getNICKLE() {
        return NICKLE;
    }

    public BigDecimal getPENNY() {
        return PENNY;
    }

    public int getQuarterCount() {
        return quarterCount;
    }

    public int getDimeCount() {
        return dimeCount;
    }

    public int getNickleCount() {
        return nickleCount;
    }

    public int getPennyCount() {
        return pennyCount;
    }

    public int getOneHundredDollarBillCount() {
        return oneHundredDollarBillCount;
    }

    public int getTwentyDollarBillCount() {
        return twentyDollarBillCount;
    }

    public int getTenDollarBillCount() {
        return tenDollarBillCount;
    }

    public int getFiveDollarBillCount() {
        return fiveDollarBillCount;
    }

    public int getOneDollarBillCount() {
        return oneDollarBillCount;
    }

}
