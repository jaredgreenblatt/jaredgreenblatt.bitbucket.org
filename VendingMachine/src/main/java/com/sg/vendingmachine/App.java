/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine;

import com.sg.vendingmachine.controller.VendingMachineController;
import com.sg.vendingmachine.dao.VendingMachineDao;
import com.sg.vendingmachine.dao.VendingMachineDaoImpl;
import com.sg.vendingmachine.service.InsufficientFundsException;
import com.sg.vendingmachine.service.ItemDoesNotExistException;
import com.sg.vendingmachine.service.NoItemInventoryException;
import com.sg.vendingmachine.service.VendingMachineDataValidationException;
import com.sg.vendingmachine.service.VendingMachineServiceLayer;
import com.sg.vendingmachine.service.VendingMachineServiceLayerImpl;
import com.sg.vendingmachine.ui.UserIO;
import com.sg.vendingmachine.ui.UserIOConsoleImpl;
import com.sg.vendingmachine.ui.VendingMachineView;

/**
 *The App Class is used to launch the application. It fires off the components
 * needed for the vending machine to run.
 * @author apprentice
 */
public class App {

    public static void main(String[] args) throws NoItemInventoryException, InsufficientFundsException, VendingMachineDataValidationException, ItemDoesNotExistException {
        UserIO MyIo = new UserIOConsoleImpl();
        VendingMachineView myView = new VendingMachineView(MyIo);
        VendingMachineDao myDao = new VendingMachineDaoImpl();
        VendingMachineServiceLayer myService = 
                new VendingMachineServiceLayerImpl(myDao);
        VendingMachineController controller
                = new VendingMachineController(myService, myView);
        
        controller.run();
        

    }
}


