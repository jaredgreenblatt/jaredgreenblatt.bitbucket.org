/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.controller;

import com.sg.vendingmachine.dao.VendingMachinePersistenceException;
import com.sg.vendingmachine.dto.Change;
import com.sg.vendingmachine.dto.VendingItem;
import com.sg.vendingmachine.service.InsufficientFundsException;
import com.sg.vendingmachine.service.ItemDoesNotExistException;
import com.sg.vendingmachine.service.NoItemInventoryException;
import com.sg.vendingmachine.service.VendingMachineDataValidationException;
import com.sg.vendingmachine.service.VendingMachineServiceLayer;
import com.sg.vendingmachine.ui.VendingMachineView;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * Controller Is used to tell orchestrate all aspects of the application.
 */
public class VendingMachineController {

    private VendingMachineView view;
    private VendingMachineServiceLayer service;

    String selection = null;

    public VendingMachineController(VendingMachineServiceLayer service, VendingMachineView view) {
        this.service = service;
        this.view = view;

    }

    /**
     *
     * @throws NoItemInventoryException
     * @throws InsufficientFundsException
     * @throws VendingMachineDataValidationException
     * @throws ItemDoesNotExistException
     *
     * Run Method is used to run the applications menu and then also used to
     * call the methods that perform the different aspects of the application.
     * This contains the application Main Menu as well as the Admin Sub Menu.
     */
    public void run() throws NoItemInventoryException, InsufficientFundsException, VendingMachineDataValidationException, ItemDoesNotExistException {
        boolean keepGoing = true;
        int menuSelection = 0;

        try {
            /*
            Open wil open the File that the application writes too. We only 
            do this at the beggining of the app in the Vending Machine.
             */

            service.Open();
            // if Keep Going is true the menu will keep coming up.
            while (keepGoing) {

                selection = newMenu();

                selection = selection.toUpperCase();
                /*
                 MENU Depending on what you choose it will launch different Methods
                V* will try to purchase an Item.
                C or Change Will give you change.
                $ will add money.
                Exit will quit the loop by chaning the boolean it will also give 
                back change using the vendingMachine Exit method. Also it closes
                and Saves the FIle that is opened at the start of the loop.
                @ will launch the admin Menu used to control the vendingItem Object.
                
                 */

                if (selection.contains("V")) {
                    purchaseItem();

                } else if (selection.equals("CHANGE") || selection.equals("C")) {
                    makeChange();
                } else if (selection.equals("$")) {
                    addMoney();
                } else if (selection.equals("EXIT")) {
                    exitVendingMachine();
                    service.Close();
                    keepGoing = false;
                    break;
                } else if (selection.equals("@")) {
                    boolean adminMenu = true;
                    while (adminMenu) {
                        menuSelection = getMenuSelection();
                        switch (menuSelection) {
                            case 1:
                                listVendingItem();
                                break;
                            case 2:
                                createVendingItem();
                                break;
                            case 3:
                                viewVendingItem();
                                break;
                            case 4:
                                removeVendingItem();
                                break;
                            case 5:
                                adminMenu = false;
                                break;
                            default:
                                unknownCommand();

                        }

                    }

                } else {
                    unknownCommand();
                }

            }
            exitMessage();

        } catch (VendingMachinePersistenceException e) {
            view.displayErrorMessage(e.getMessage());
        }
    }
//Used for the admin Menu to Catch the user input

    private int getMenuSelection() {
        return view.printMenuAndGetSelection();
    }

    /*
    used for Capturing the String Inputed in the main Menu.
    Also Pull in a list of Items and the Running Total from the Service.
     */
    private String newMenu() throws VendingMachinePersistenceException {
        List<VendingItem> vendingItemList = service.getAllVendingItem();
        BigDecimal money = view.MoneyAmount(service.getrunningTotal());
        view.displayVendingMachine();
        return view.vendingMachineItem(vendingItemList, money);

    }
    //Method used by admin Function to add Item.

    private void createVendingItem() throws VendingMachinePersistenceException {
        view.displayCreateVendingItemBanner();
        boolean hasErrors = false;
        do {
            VendingItem currentVendingItem = view.getNewVendingItemInfo();
            try {
                service.createVendingItem(currentVendingItem);
                view.displayCreateSuccessBanner();
                hasErrors = false;
            } catch (VendingMachineDataValidationException e) {
                hasErrors = true;
                view.displayErrorMessage(e.getMessage());
            }

        } while (hasErrors);

    }
//Method used by admin Function to List all Items.

    private void listVendingItem() throws VendingMachinePersistenceException {
        view.displayDisplayAllBanner();
        List<VendingItem> vendingItemList = service.getAllVendingItem();
        view.displayVendingItemList(vendingItemList);
    }
//Method used by admin Function to List one item.

    private void viewVendingItem() throws VendingMachinePersistenceException, ItemDoesNotExistException {
        view.displayDisplayVendingBanner();
        String itemId = view.getVendingItemIdChoice();
        VendingItem vendingItem = service.getVendingItem(itemId);
        view.displayVendingItem(vendingItem);
    }
//Method used by admin Function to remove one item.

    private void removeVendingItem() throws VendingMachinePersistenceException {
        view.displayRemoveVendingItemBanner();
        String vendingItemId = view.getVendingItemIdChoice();
        service.removeVendingItem(vendingItemId);
        view.displayRemoveSuccessBanner();
    }

    //Called by Main Menu to add money.
    private void addMoney() {

        //Calls on the service to add the money to the services running total.
        view.displayAddMoneyBanner();
        service.setrunningTotal((view.getMoney()));
        if (service.getrunningTotal().compareTo(BigDecimal.ZERO) > 0) {

            view.displayAddMoneySuccessBanner();
        } else {
            view.displayAddMoneyFailureBanner();
            service.setrunningTotal(BigDecimal.ZERO);
        }

    }

    //Method called when Exiting. Will return money still in the machine.
    private void exitVendingMachine() {
        view.displayVendingExit();
        if (service.getrunningTotal().compareTo(new BigDecimal("0")) == 1) {
            view.PrintChangeTotalAmount(service.getrunningTotal());
            makeChange();

        }

    }
//Controller Used to make change. Calls on the Service to handle the logic and
// the view to print it.

    private void makeChange() {
        view.displayMakeChangeBanner();
        view.PrintChangeTotalAmount(service.getrunningTotal());
        Change change = service.makeChange();
        view.PrintChange(change);
        view.PrintChangeTotalAmount(service.getrunningTotal());
        view.displayMakeChangeSuccessBanner();

    }
//Method to Purhcase an item.

    private void purchaseItem() {
        //view.displayVending();

// 
        try {
            //Creates a Vending Item Object based on the user selection.
            VendingItem purchaseVendingItem = service.getVendingItem(selection);
            //Calls on view to display the purchased item.
            view.displayPurchasedItemName(purchaseVendingItem);
            //creates an edited item after the item is purchased
            VendingItem editedItem = service.PurchaseItem(purchaseVendingItem);
            //Add the item changes to the object.
            service.createVendingItem(editedItem);
            view.displayVending();
            view.displayVendingComplete();
            makeChange();

        } catch (NoItemInventoryException | InsufficientFundsException | ItemDoesNotExistException | VendingMachinePersistenceException | VendingMachineDataValidationException | NullPointerException e) {
            view.displayErrorMessage(e.getMessage());
        }

    }

    private void unknownCommand() {
        view.displayUnknownCommandBanner();
    }

    private void exitMessage() {
        view.displayExitBanner();
    }
}
