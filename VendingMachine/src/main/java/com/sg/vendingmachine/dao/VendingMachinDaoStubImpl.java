/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.dao;

import com.sg.vendingmachine.dto.VendingItem;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class VendingMachinDaoStubImpl implements VendingMachineDao {

    VendingItem onlyItem;
    List<VendingItem> vendingItemList = new ArrayList<>();

    public VendingMachinDaoStubImpl() {
        onlyItem = new VendingItem("0001");
        onlyItem.setItemName("SNACK SNACK");
        onlyItem.setItemCost(new BigDecimal("1.00"));
        onlyItem.setNumberOfItemsInInventory(1);

        vendingItemList.add(onlyItem);
    }

    @Override
    public VendingItem addVendingItem(String itemId, VendingItem vendingItem) throws VendingMachinePersistenceException {
        if (itemId.equals(onlyItem.getItemId())) {
            return onlyItem;
        } else {
            return null;
        }
    }

    @Override
    public List<VendingItem> getAllVendingItems() throws VendingMachinePersistenceException {
        return vendingItemList;
    }

    @Override
    public VendingItem getVendingItem(String itemId) throws VendingMachinePersistenceException {
        if (itemId.equals(onlyItem.getItemId())) {
            return onlyItem;
        } else {
            return null;
        }
    }

    @Override
    public VendingItem removeItem(String itemId) throws VendingMachinePersistenceException {
        if (itemId.equals(onlyItem.getItemId())) {
            return onlyItem;
        } else {
            return null;
        }
    }

    @Override
    public void Open() throws VendingMachinePersistenceException {
    }

    @Override
    public void Close() throws VendingMachinePersistenceException {
   }

}
