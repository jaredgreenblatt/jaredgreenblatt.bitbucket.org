/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.dao;

import com.sg.vendingmachine.dto.VendingItem;
import java.math.BigDecimal;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class VendingMachineDaoTest {
    private VendingMachineDao dao = new VendingMachineDaoImpl();
    

    public VendingMachineDaoTest() {
        
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws VendingMachinePersistenceException {
        List<VendingItem>vendingItemList = dao.getAllVendingItems();
        for (VendingItem item: vendingItemList) {
            dao.removeItem(item.getItemId());
            
        }
        
    }

    @After
    public void tearDown() {
    }

  
    /**
     * Test of getAllVendingItems method, of class VendingMachineDao.
     */
    @Test
    public void testGetAllVendingItems() throws Exception {
        VendingItem item1= new VendingItem("001");
        item1.setItemName("FOOD");
        item1.setItemCost(new BigDecimal("1.50"));
        item1.setNumberOfItemsInInventory(5);
        dao.addVendingItem(item1.getItemId(), item1);
        
        VendingItem item2= new VendingItem("002");
        item2.setItemName("Snack");
        item2.setItemCost(new BigDecimal("2.75"));
        item2.setNumberOfItemsInInventory(7);
        dao.addVendingItem(item2.getItemId(), item2);
        
        assertEquals(2, dao.getAllVendingItems().size());
        
    }

    /**
     * Test of getVendingItem method, of class VendingMachineDao.
     */
    @Test
    public void testADDGetVendingItem() throws Exception {
        VendingItem itemTest = new VendingItem("001");
        itemTest.setItemName("FOOD");
        itemTest.setItemCost(new BigDecimal("1.50"));
        itemTest.setNumberOfItemsInInventory(5);
        
        dao.addVendingItem(itemTest.getItemId(), itemTest);
        
        VendingItem fromDao = dao.getVendingItem(itemTest.getItemId());
        
        assertEquals(itemTest,fromDao);
    }
    
     @Test
    public void testRemoveVendingItem() throws Exception {
        VendingItem item1= new VendingItem("001");
        item1.setItemName("FOOD");
        item1.setItemCost(new BigDecimal("1.50"));
        item1.setNumberOfItemsInInventory(5);
        dao.addVendingItem(item1.getItemId(), item1);
        
        VendingItem item2= new VendingItem("002");
        item2.setItemName("Snack");
        item2.setItemCost(new BigDecimal("2.75"));
        item2.setNumberOfItemsInInventory(7);
        dao.addVendingItem(item2.getItemId(), item2);
        
        assertEquals(2, dao.getAllVendingItems().size());
        dao.removeItem("001");
        assertEquals(1, dao.getAllVendingItems().size());
        dao.removeItem("002");
        assertEquals(0, dao.getAllVendingItems().size());
        
    }

  

}
