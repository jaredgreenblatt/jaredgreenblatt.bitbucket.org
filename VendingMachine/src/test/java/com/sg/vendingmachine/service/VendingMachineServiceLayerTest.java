/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.service;

import com.sg.vendingmachine.dao.VendingMachinDaoStubImpl;
import com.sg.vendingmachine.dao.VendingMachineDao;
import com.sg.vendingmachine.dto.VendingItem;
import java.math.BigDecimal;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class VendingMachineServiceLayerTest {

    private VendingMachineServiceLayer service;

    public VendingMachineServiceLayerTest() {
        VendingMachineDao dao = new VendingMachinDaoStubImpl();

        service = new VendingMachineServiceLayerImpl(dao);
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of createVendingItem method, of class VendingMachineServiceLayer.
     */
    @Test
    public void testCreateVendingItem() throws Exception {
        VendingItem item = new VendingItem("0003");
        item.setItemName("B");
        item.setItemCost(new BigDecimal("1.00"));
        item.setNumberOfItemsInInventory(3);
        service.createVendingItem(item);

    }

    /**
     * Test of getAllVendingItem method, of class VendingMachineServiceLayer.
     */
    @Test
    public void testGetAllVendingItem() throws Exception {
        assertEquals(1, service.getAllVendingItem().size());
    }

    /**
     * Test of getVendingItem method, of class VendingMachineServiceLayer.
     */
    @Test
    public void testGetVendingItem() throws Exception {
        VendingItem item = service.getVendingItem("0001");
        assertNotNull(item);

    }

    /**
     * Test of removeVendingItem method, of class VendingMachineServiceLayer.
     */
    @Test
    public void testRemoveVendingItem() throws Exception {
        VendingItem item = service.removeVendingItem("0001");
        assertNotNull(item);

    }

    /**
     * Test of PurchaseItem method, of class VendingMachineServiceLayer.
     */
    @Test
    public void testPurchaseItem() throws Exception {
        VendingItem item = service.getVendingItem("0001");
        BigDecimal runningTotal= new BigDecimal("1.0");
        
         
        BigDecimal tempCost = item.getItemCost();
        String tempName = item.getItemName();
        int tempInventory = item.getNumberOfItemsInInventory();
        tempInventory = tempInventory - 1;
        runningTotal = runningTotal.subtract(tempCost);
        VendingItem edititedClass = new VendingItem(item.getItemId());
        edititedClass.setItemName(tempName);
        edititedClass.setItemCost(tempCost);
        edititedClass.setNumberOfItemsInInventory(tempInventory);

        
        
        assertEquals(0,edititedClass.getNumberOfItemsInInventory());
       
      
    }

    /**
     * Test of makeChange method, of class VendingMachineServiceLayer.
     */
}
