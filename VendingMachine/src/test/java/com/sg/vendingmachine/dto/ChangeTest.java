/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.dto;

import java.math.BigDecimal;
import java.math.RoundingMode;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class ChangeTest {

    Change changeTest = new Change(new BigDecimal(0));

    public ChangeTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getONE_HUNDRED_DOLLAR_BILL method, of class Change.
     */
    @Test
    public void testChangefor10Dollars() {
        Change tenDollarTest = new Change(new BigDecimal(10.00));
        assertEquals(tenDollarTest.getOneHundredDollarBillCount(), 0);
        assertEquals(tenDollarTest.getTwentyDollarBillCount(), 0);
        assertEquals(tenDollarTest.getTenDollarBillCount(), 1);
        assertEquals(tenDollarTest.getFiveDollarBillCount(), 0);
        assertEquals(tenDollarTest.getOneDollarBillCount(), 0);
        assertEquals(tenDollarTest.getQuarterCount(), 0);
        assertEquals(tenDollarTest.getDimeCount(), 0);
        assertEquals(tenDollarTest.getNickleCount(), 0);
        assertEquals(tenDollarTest.getPennyCount(), 0);

    }

    @Test
    public void testChangeforOneDoller77() {
        Change tenDollarTest = new Change(new BigDecimal(1.77));
        assertEquals(tenDollarTest.getOneHundredDollarBillCount(), 0);
        assertEquals(tenDollarTest.getTwentyDollarBillCount(), 0);
        assertEquals(tenDollarTest.getTenDollarBillCount(), 0);
        assertEquals(tenDollarTest.getFiveDollarBillCount(), 0);
        assertEquals(tenDollarTest.getOneDollarBillCount(), 1);
        assertEquals(tenDollarTest.getQuarterCount(), 3);
        assertEquals(tenDollarTest.getDimeCount(), 0);
        assertEquals(tenDollarTest.getNickleCount(), 0);
        assertEquals(tenDollarTest.getPennyCount(), 2);

    }

    @Test
    public void testChangefor88Cents() {
        Change tenDollarTest = new Change(new BigDecimal(0.88));
        assertEquals(tenDollarTest.getOneHundredDollarBillCount(), 0);
        assertEquals(tenDollarTest.getTwentyDollarBillCount(), 0);
        assertEquals(tenDollarTest.getTenDollarBillCount(), 0);
        assertEquals(tenDollarTest.getFiveDollarBillCount(), 0);
        assertEquals(tenDollarTest.getOneDollarBillCount(), 0);
        assertEquals(tenDollarTest.getQuarterCount(), 3);
        assertEquals(tenDollarTest.getDimeCount(), 1);
        assertEquals(tenDollarTest.getNickleCount(), 0);
        assertEquals(tenDollarTest.getPennyCount(), 3);

    }

    @Test
    public void testChangefor35() {
        Change tenDollarTest = new Change(new BigDecimal(35));
        assertEquals(tenDollarTest.getOneHundredDollarBillCount(), 0);
        assertEquals(tenDollarTest.getTwentyDollarBillCount(), 1);
        assertEquals(tenDollarTest.getTenDollarBillCount(), 1);
        assertEquals(tenDollarTest.getFiveDollarBillCount(), 1);
        assertEquals(tenDollarTest.getOneDollarBillCount(), 0);
        assertEquals(tenDollarTest.getQuarterCount(), 0);
        assertEquals(tenDollarTest.getDimeCount(), 0);
        assertEquals(tenDollarTest.getNickleCount(), 0);
        assertEquals(tenDollarTest.getPennyCount(), 0);

    }

    @Test
    public void testChange66Cents() {
        Change tenDollarTest = new Change(new BigDecimal(0.66));
        assertEquals(tenDollarTest.getOneHundredDollarBillCount(), 0);
        assertEquals(tenDollarTest.getTwentyDollarBillCount(), 0);
        assertEquals(tenDollarTest.getTenDollarBillCount(), 0);
        assertEquals(tenDollarTest.getFiveDollarBillCount(), 0);
        assertEquals(tenDollarTest.getOneDollarBillCount(), 0);
        assertEquals(tenDollarTest.getQuarterCount(), 2);
        assertEquals(tenDollarTest.getDimeCount(), 1);
        assertEquals(tenDollarTest.getNickleCount(), 1);
        assertEquals(tenDollarTest.getPennyCount(), 1);

    }

    @Test
    public void testChange37() {
        Change tenDollarTest = new Change(new BigDecimal(37.00));
        assertEquals(tenDollarTest.getOneHundredDollarBillCount(), 0);
        assertEquals(tenDollarTest.getTwentyDollarBillCount(), 1);
        assertEquals(tenDollarTest.getTenDollarBillCount(), 1);
        assertEquals(tenDollarTest.getFiveDollarBillCount(), 1);
        assertEquals(tenDollarTest.getOneDollarBillCount(), 2);
        assertEquals(tenDollarTest.getQuarterCount(), 0);
        assertEquals(tenDollarTest.getDimeCount(), 0);
        assertEquals(tenDollarTest.getNickleCount(), 0);
        assertEquals(tenDollarTest.getPennyCount(), 0);

    }

    @Test
    public void testChange3867() {
        Change tenDollarTest = new Change(new BigDecimal(38.67).setScale(2, RoundingMode.FLOOR));
        assertEquals(tenDollarTest.getOneHundredDollarBillCount(), 0);
        assertEquals(tenDollarTest.getTwentyDollarBillCount(), 1);
        assertEquals(tenDollarTest.getTenDollarBillCount(), 1);
        assertEquals(tenDollarTest.getFiveDollarBillCount(), 1);
        assertEquals(tenDollarTest.getOneDollarBillCount(), 3);
        assertEquals(tenDollarTest.getQuarterCount(), 2);
        assertEquals(tenDollarTest.getDimeCount(), 1);
        assertEquals(tenDollarTest.getNickleCount(), 1);
        assertEquals(tenDollarTest.getPennyCount(), 2);

    }

    @Test
    public void testChange3899967() {
        Change tenDollarTest = new Change(new BigDecimal(38.67).setScale(2, RoundingMode.FLOOR));
        assertEquals(tenDollarTest.getOneHundredDollarBillCount(), 0);
        assertEquals(tenDollarTest.getTwentyDollarBillCount(), 1);
        assertEquals(tenDollarTest.getTenDollarBillCount(), 1);
        assertEquals(tenDollarTest.getFiveDollarBillCount(), 1);
        assertEquals(tenDollarTest.getOneDollarBillCount(), 3);
        assertEquals(tenDollarTest.getQuarterCount(), 2);
        assertEquals(tenDollarTest.getDimeCount(), 1);
        assertEquals(tenDollarTest.getNickleCount(), 1);
        assertEquals(tenDollarTest.getPennyCount(), 2);

    }

    @Test
    public void testChange3666() {
        Change tenDollarTest = new Change(new BigDecimal(36.66).setScale(2, RoundingMode.HALF_EVEN));
        System.out.println("--------------------------------------------------");
        System.out.println("36.66");
        assertEquals(tenDollarTest.getOneHundredDollarBillCount(), 0);
        System.out.println("Hundo " + tenDollarTest.getOneHundredDollarBillCount());
        assertEquals(tenDollarTest.getTwentyDollarBillCount(), 1);
        System.out.println("Twenty " + tenDollarTest.getTwentyDollarBillCount());
        assertEquals(tenDollarTest.getTenDollarBillCount(), 1);
        System.out.println("Ten " + tenDollarTest.getTenDollarBillCount());
        assertEquals(tenDollarTest.getFiveDollarBillCount(), 1);
        System.out.println("Five " + tenDollarTest.getFiveDollarBillCount());
        assertEquals(tenDollarTest.getOneDollarBillCount(), 1);
        System.out.println("One " + tenDollarTest.getOneDollarBillCount());
        assertEquals(tenDollarTest.getQuarterCount(), 2);
        System.out.println("Quarter " + tenDollarTest.getQuarterCount());
        assertEquals(tenDollarTest.getDimeCount(), 1);
        System.out.println("Dime " + tenDollarTest.getDimeCount());
        assertEquals(tenDollarTest.getNickleCount(), 1);
        System.out.println("Nickle " + tenDollarTest.getNickleCount());
        System.out.println("Penny " + tenDollarTest.getPennyCount());
        assertEquals(tenDollarTest.getPennyCount(), 1);

        System.out.println("--------------------------------------------------");

    }

}
