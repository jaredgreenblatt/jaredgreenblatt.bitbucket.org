//home.js
$(document).ready(function() {
    loadDvds();

    $('#create-dvd-button').click(function(event) {
        // check for errors and display any that we have
        // pass the input associated with the add form to the validation function
        //  var haveValidationErrors = checkAndDisplayValidationErrors($('#add-form').find('input'));

        // if we have errors, bail out by returning false
        //if (haveValidationErrors) {
        //        return false;
        //  }

        $.ajax({
            type: 'POST',
            url: 'http://localhost:8080/dvd',
            data: JSON.stringify({
                title: $('#new-dvd-title').val(),
                releaseYear: $('#new-release-date').val(),
                director: $('#new-director').val(),
                rating: $('#new-rating').val(),
                note: $('#new-notes').val()

            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json',
            success: function() {
                $('#errorMessages').empty();
                $('#new-dvd-title').val('');
                $('#new-release-date').val('');
                $('#new-director').val('');
                $('#new-rating').val('');
                $('#new-notes').val('');
                loadDvds();

            },
            error: function() {
                $('#errorMessages')
                    .append($('<li>')
                        .attr({
                            class: 'list-group-item list-group-item-danger'
                        })
                        .text('Error Calling Web Service. Please try again later.'));

            }
        })

    });
    $('#edit-dvd-button').click(function(event) {
        // check for errors and display any that we have
        // pass the input associated with the edit form to the validation function
        //  var haveValidationErrors = checkAndDisplayValidationErrors($('#edit-form').find('input'));

        // if we have errors, bail out by returning false
        //  if (haveValidationErrors) {
        //    return false;
        //}
        $.ajax({
            type: 'PUT',
            url: 'http://localhost:8080/dvd/' + $('#edit-dvd-id').val(),
            data: JSON.stringify({
                dvdId: $('#edit-dvd-id').val(),
                title: $('#edit-dvd-title').val(),
                releaseYear: $('#edit-release-date').val(),
                director: $('#edit-director').val(),
                rating: $('#edit-rating').val(),
                note: $('#edit-notes').val()

            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },

            'dataType': 'json',
            success: function() {
                //$('#errorMessages').empty();
                $('#edit-dvd-title').val('');
                $('#edit-release-date').val('');
                $('#edit-director').val('');
                $('#edit-notes').val('');
                $('#editDvd').hide();

                loadDvds();

            },
            error: function() {
                $('#errorMessages')
                    .append($('<li>')
                        .attr({
                            class: 'list-group-item list-group-item-danger'
                        })
                        .text('Error Calling Web Service. Please try again later.'));

            }
        })

    });

    $('#create-Dvd').click(function(event) {
        $('#editDvd').hide();
        $('#navbar').hide();
        $('#dvdTable').hide();
        $('#createDVD').show();
    });
    $('#create-dvd-cancel-button').click(function(event) {
        $('#new-dvd-title').val('');
        $('#new-release-date').val('');
        $('#new-director').val('');
        $('#new-notes').val('');
        $('#createDVD').hide();

        loadDvds();
    });

    $('#edit-cancel-button').click(function(event) {
        $('#edit-dvd-title').val('');
        $('#edit-release-date').val('');
        $('#edit-director').val('');
        $('#edit-notes').val('');
        $('#editDvd').hide();

        loadDvds();
    });


});

function showEditForm(dvdId) {
    $('#errorMessages').empty();

    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/dvd/' + dvdId,
        success: function(dvd, status) {
            $('#edit-dvd-title').val(dvd.title);
            $('#edit-release-date').val(dvd.realeaseYear);
            $('#edit-director').val(dvd.director);
            $('#edit-rating').val(dvd.rating);
            $('#edit-notes').val(dvd.notes);
            $('#edit-dvd-id').val(dvd.dvdId);



        },
        error: function() {
            $('#errorMessages')
                .append($('<li>')
                    .attr({
                        class: 'list-group-item list-group-item-danger'
                    })
                    .text('Error Calling Web Service. Please try again later.'));

        }
    })

    $('#editDvd').show();
    $('#navbar').hide();
    $('#dvdTable').hide();
    $('#createDVD').hide();
}


function deleteDvd(dvdId) {
    $.ajax({
        type: 'DELETE',
        url: 'http://localhost:8080/dvd/' + dvdId,
        success: function() {
            loadDvds();
        }
    });
}

function loadDvds() {
    clearContactTable();
    $('#navbar').show();
    $('#dvdTable').show();
    //clearContactTable();
    var contentRows = $('#contentRows');

    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/dvds',
        success: function(dvdsArray) {
            //alert('SUCCESS');
            $.each(dvdsArray, function(index, dvd) {
                var title = dvd.title;
                var releaseYear = dvd.realeaseYear;
                var director = dvd.director;
                var rating = dvd.rating;
                var dvdId = dvd.dvdId;
                //  var notes = dvd.notes;

                var row = '<tr>';
                row += '<td>' + title + '</td>';
                row += '<td>' + releaseYear + '</td>';
                row += '<td>' + director + '</td>';
                row += '<td>' + rating + '</td>';
                //  row += '<td><a onclick="showEditForm(' + dvdId + ')">Edit</a></td>';
                row += '<td><a onclick="showEditForm(' + dvdId + ')">Edit</a> | <a onclick="deleteDvd(' + dvdId + ')">Delete</a></td>';
                row += '</tr>';

                contentRows.append(row);

                $('#editDvd').hide();
                $('#createDVD').hide();




            });

        },

        error: function() {
            $('#errorMessages')
                .append($('<li>')
                    .attr({
                        class: 'list-group-item list-group-item-danger'
                    })
                    .text('Error Calling Web Service. Please try again later.'));

        }



    });

    function clearContactTable() {
        $('#contentRows').empty();
    }

    //
    // function hideEditForm() {
    //     //  $('#errorMessages').empty();
    //     $('#edit-dvd-title').val('');
    //     $('#edit-release-date').val('');
    //     $('#edit-director').val('');
    //     $('#edit-notes').val('');
    //     $('#editDvd').hide();
    //
    //     loadDvds();





    function hideEditForm() {
        //  $('#errorMessages').empty();
        $('#edit-dvd-title').val('');
        $('#edit-release-date').val('');
        $('#edit-director').val('');
        $('#edit-notes').val('');
        $('#editDvd').hide();

        loadDvds();



    }




}
