//home.js
$(document).ready(function() {
    $('#fiveDayForcast').hide();
    $('#currentCondition').hide();
    $('#invalidZip').hide();


    //


    //api.openweathermap.org/data/2.5/weather?zip=94040&appid=2ab2b6043bb8b54e7212089d5f4316da&units=imperial


});

// $.ajax({
//     type: 'GET',
//     url: 'http://api.openweathermap.org/data/2.5/weather?zip=94040&appid=2ab2b6043bb8b54e7212089d5f4316da&units=imperial',
//     success: function(checkConection) {
//         alert('SUCCESS');
//
//     },
//     error: function() {
//         alert('FAILURE');
//     }
//
// });
var webSiteCurrentWeather = 'http://api.openweathermap.org/data/2.5/weather?zip=';
var SiteFiveDayForeCastWeather = 'http://api.openweathermap.org/data/2.5/forecast/daily?zip='
var zipCode; // = $('#zipCode').val();
var apiCode = '&appid=2ab2b6043bb8b54e7212089d5f4316da';
var unit = '&units=';
var measurement; // = $('unit').val();
var concatSiteCurrentWeather;
var concatSiteFiveDay;

//alert(concatSite);
$('#get-weather').on('click', function() {

    //clear elements
    $('#currentWeatherInfo').html('');
    $('#currentCityH1').html('');
    $('#currentWeatherDescription').html('');
    $('#printFiveDay').html('');



    zipCode = $('#zipcode').val();
    measurement = $('#unit').val();
    //concatSiteCurrentWeatherSite
    concatSiteCurrentWeather = webSiteCurrentWeather;
    concatSiteCurrentWeather += zipCode;
    concatSiteCurrentWeather += apiCode;
    concatSiteCurrentWeather += unit;
    concatSiteCurrentWeather += measurement;
    //concat five day site
    concatSiteFiveDay = SiteFiveDayForeCastWeather;
    concatSiteFiveDay += zipCode;
    concatSiteFiveDay += apiCode;
    concatSiteFiveDay += unit;
    concatSiteFiveDay += measurement;
    concatSiteFiveDay += '&cnt=5';
    $.ajax({
        type: 'GET',
        url: concatSiteCurrentWeather,


        success: function(currentWeather) {
            $('#invalidZip').hide();
            $('#currentCondition').show();

            //alert('SUCCESS');
            // current City
            var currentCityWeatherDiv = $('#currentCityH1');
            var curentTempWeatherInfo = '<h1>';
            curentTempWeatherInfo += 'Current Conditions in ' + currentWeather.name;
            curentTempWeatherInfo += '</h1>'
            currentCityWeatherDiv.append(curentTempWeatherInfo);
            //currentWeatheDescription
            var currentConditionWeatherDiv = $('#currentWeatherDescription');
            var currentConditionImageUrl = 'http://openweathermap.org/img/w/'
            currentConditionImageUrl += currentWeather.weather[0].icon;
            currentConditionImageUrl += '.png'
            var currentConditionWeatherInfo = '<p>';
            currentConditionWeatherInfo += '<img src="' + currentConditionImageUrl + '"/>'

            currentConditionWeatherInfo += currentWeather.weather[0].main + ' ' +
                currentWeather.weather[0].description;
            currentConditionWeatherInfo += '</p>';
            currentConditionWeatherDiv.append(currentConditionWeatherInfo);


            //currentTemp
            if (measurement == 'Imperial') {
                var currentTempWeatherDiv = $('#currentWeatherInfo');
                var curentTempWeatherInfo = '<p>';
                curentTempWeatherInfo += 'Temperature: ' + currentWeather.main.temp + ' F' + '<br>';
                curentTempWeatherInfo += 'Humidity: ' + currentWeather.main.humidity + '%' + '<br>';
                curentTempWeatherInfo += 'Wind: ' + currentWeather.wind.speed + ' miles/hour' + '<br>';
                curentTempWeatherInfo += '</p>';
                currentTempWeatherDiv.append(curentTempWeatherInfo);

            } else if (measurement == 'Metric') {
                var currentTempWeatherDiv = $('#currentWeatherInfo');
                var curentTempWeatherInfo = '<p>';
                curentTempWeatherInfo += 'Temperature: ' + currentWeather.main.temp + ' C' + '<br>';
                curentTempWeatherInfo += 'Humidity: ' + currentWeather.main.humidity + '%' + '<br>';
                curentTempWeatherInfo += 'Wind: ' + currentWeather.wind.speed + ' kilometers /hour' + '<br>';
                curentTempWeatherInfo += '</p>';
                currentTempWeatherDiv.append(curentTempWeatherInfo);
            }





            //open divs


            //  in(concatSite);



        },

        error: function() {
            $('#invalidZip').show();
        }

    });
    $.ajax({
        type: 'GET',
        url: concatSiteFiveDay,
        success: function(forecastArray) {
            $('#fiveDayForcast').show();
            var fiveDayDiv = $('#printFiveDay');
            var fivedayImageUrl = 'http://openweathermap.org/img/w/'

            $.each(forecastArray.list, function(index, forecast) {

                var dateUnix = forecast.dt * 1000;
                var date = new Date(dateUnix);
                var n = date.toString();
                n = n.substring(4, 7);
                if (measurement == 'Imperial') {
                    var forecastInfo = '<div class="col-lg-2"> ';
                    forecastInfo += '<p>';
                    forecastInfo += n + ' ' + date.getDate() + '</br>';
                    forecastInfo += '<img src="';
                    forecastInfo += fivedayImageUrl;
                    forecastInfo += forecast.weather[0].icon + '.png"' + '</br>';
                    forecastInfo += forecast.weather[0].main + '</br>';
                    forecastInfo += 'H ' + forecast.temp.max + 'F' + ' ' + 'L ' +
                        forecast.temp.min + 'F';
                    forecastInfo += '</p>';
                    forecastInfo += '</div>';

                    fiveDayDiv.append(forecastInfo);



                } else if (measurement == 'Metric') {
                  var forecastInfo = '<div class="col-lg-2"> ';
                  forecastInfo += '<p>';
                  forecastInfo += n + ' ' + date.getDate() + '</br>';
                  forecastInfo += '<img src="';
                  forecastInfo += fivedayImageUrl;
                  forecastInfo += forecast.weather[0].icon + '.png"' + '</br>';
                  forecastInfo += forecast.weather[0].main + '</br>';
                  forecastInfo += 'H ' + forecast.temp.max + 'C' + ' ' + 'L ' +
                      forecast.temp.min + 'C';
                  forecastInfo += '</p>';
                  forecastInfo += '</div>';

                  fiveDayDiv.append(forecastInfo);

                }
                //
                // var forecastInfo = '<div class="col-lg-2"> ';
                // forecastInfo += '<p>';
                // forecastInfo += n + ' ' + date.getDate() + '</br>';
                // forecastInfo += '<img src="';
                // forecastInfo += fivedayImageUrl;
                // forecastInfo += forecast.weather[0].icon + '.png"' + '</br>';
                // forecastInfo += forecast.weather[0].main + '</br>';
                // forecastInfo += 'H ' + forecast.temp.max + 'F' + ' ' + 'L ' +
                //     forecast.temp.min + 'F';
                // forecastInfo += '</p>';
                // forecastInfo += '</div>';
                //
                // fiveDayDiv.append(forecastInfo);


            });

        },
        error: function() {
            $('#invalidZip').show();
            $('#fiveDayForcast').hide();

        }


    });
    //open divs

    //  alert(concatSite);

});
