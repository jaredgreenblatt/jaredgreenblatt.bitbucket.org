/*
   Select the Suppliers whose contact has a title that starts with the word 
   Sales
*/

USE Northwind;

Select * 
from Suppliers
where  ContactTitle LIKE 'Sales%';
