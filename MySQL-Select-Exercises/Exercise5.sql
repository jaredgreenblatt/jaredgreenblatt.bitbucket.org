/*
   Select the orders whose freight is more than $100.00
*/

USE Northwind;
Select *
from Orders
where freight > 100;