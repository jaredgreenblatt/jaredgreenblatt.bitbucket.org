/*
   Select the orders shipping to the USA whose freight is 
   between $10 and $20
*/

USE Northwind;
Select * 
from Orders 
where Freight BETWEEN 10 AND 20
and ShipCountry ='USA';