/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine;

import com.sg.VendingMachine.ui.UserIO;
import com.sg.VendingMachine.ui.UserIOConsoleImpl;
import java.math.BigDecimal;

/**
 *
 * @author apprentice
 */
public class App {
    public static void main(String[] args) {
        UserIO io = new UserIOConsoleImpl();
        BigDecimal test;
        test = new BigDecimal("1");
        test = io.readBigDecimal("Please Enter a  Big Decimal");
        System.out.println(test);
        test = io.readBigDecimal("Please Enter a  Big Decimal",new BigDecimal("10"), new BigDecimal("100"));
        System.out.println(test);
    }
    
}
