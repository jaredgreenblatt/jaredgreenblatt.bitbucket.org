/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.interestcalculater;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

/**
 *
 * @author apprentice
 */
public class iCalculater {

    public BigDecimal iCalculater(Compounding compound, BigDecimal interestRate, BigDecimal interestPrincipal, int numberOfYears) {
        BigDecimal convertToPercentage = new BigDecimal("100");
        BigDecimal addOne = new BigDecimal("1");
        BigDecimal currentBalance = interestPrincipal;
        BigDecimal yearStartPrincipal;
        BigDecimal compoundingRate;
        BigDecimal compundedRate;
        switch (compound) {
            case MONTHLY:
                compoundingRate = new BigDecimal("12");

                for (int i = 1; i <= numberOfYears; i++) {
                    yearStartPrincipal = currentBalance;
                    for (int j = 1; j <= 12; j++) {

                        compundedRate = interestRate.divide(compoundingRate, 2, RoundingMode.HALF_UP);
                        compundedRate = compundedRate.divide(convertToPercentage, 3, RoundingMode.HALF_UP);
                        compundedRate = compundedRate.add(addOne);
                        currentBalance = currentBalance.multiply(compundedRate);
                        currentBalance = currentBalance.setScale(2, RoundingMode.HALF_UP);
                       

                    }
                }
                return currentBalance;

            case QUATERLY:

                compoundingRate = new BigDecimal("4");

                for (int i = 1; i <= numberOfYears; i++) {
                    yearStartPrincipal = currentBalance;
                    for (int j = 1; j <= 4; j++) {

                        compundedRate = interestRate.divide(compoundingRate, 2, RoundingMode.HALF_UP);
                        compundedRate = compundedRate.divide(convertToPercentage, 3, RoundingMode.HALF_UP);
                        compundedRate = compundedRate.add(addOne);
                        currentBalance = currentBalance.multiply(compundedRate);
                        currentBalance = currentBalance.setScale(2, RoundingMode.HALF_UP);
                       

                    }
                }
                return currentBalance;

            case BIYEARLY:
                compoundingRate = new BigDecimal("2");

                for (int i = 1; i <= numberOfYears; i++) {
                    yearStartPrincipal = currentBalance;
                    for (int j = 1; j <= 2; j++) {

                        compundedRate = interestRate.divide(compoundingRate, 2, RoundingMode.HALF_UP);
                        compundedRate = compundedRate.divide(convertToPercentage, 3, RoundingMode.HALF_UP);
                        compundedRate = compundedRate.add(addOne);
                        currentBalance = currentBalance.multiply(compundedRate);
                        currentBalance = currentBalance.setScale(2, RoundingMode.HALF_UP);
                        

                    }
                }
                return currentBalance;
            case YEARLY:
                compoundingRate = new BigDecimal("1");

                for (int i = 1; i <= numberOfYears; i++) {
                    yearStartPrincipal = currentBalance;

                    // currentBalance = currentBalance * (1 + ((interestRate * .25) / 100));
                    compundedRate = interestRate.divide(compoundingRate, 2, RoundingMode.HALF_UP);
                    compundedRate = compundedRate.divide(convertToPercentage, 3, RoundingMode.HALF_UP);
                    compundedRate = compundedRate.add(addOne);
                    currentBalance = currentBalance.multiply(compundedRate);
                    currentBalance = currentBalance.setScale(2, RoundingMode.HALF_UP);
                    

                }
                return currentBalance;
            default:
                throw new UnsupportedOperationException("Unknown Interest Calculation");
        }

    }

}
