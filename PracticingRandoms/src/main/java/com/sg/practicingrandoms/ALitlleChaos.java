/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.practicingrandoms;

import java.util.Random;

/**
 *
 * @author apprentice
 */
public class ALitlleChaos {

    public static void main(String[] args) {
        Random randomizer = new Random();

        System.out.println("Random  can make integers: " + randomizer.nextInt());
        System.out.println("Or a Double " + randomizer.nextDouble());
        System.out.println("Or a boolean " + randomizer.nextBoolean());

        int num = randomizer.nextInt(100);
        System.out.println("You can store a randomized result and user it over " + num);
        System.out.println("and  over " + num);
        System.out.println("Or just keep generating new values");
        System.out.println("Here's a bunch of numbers from 0 - 100: ");

        System.out.print(randomizer.nextInt(101) + ", ");
        System.out.print(randomizer.nextInt(101) + ", ");
        System.out.print(randomizer.nextInt(101) + ", ");
        System.out.print(randomizer.nextInt(101) + ", ");
        System.out.print(randomizer.nextInt(101) + ", ");
        System.out.println(randomizer.nextInt(101));
    }

}
