/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WhileLoops;

/**
 *
 * @author apprentice
 */
public class WaitAWhile {
    public static void main(String[] args) {
        int timeNow =5;
        int bedTime =10;
        
        while(timeNow< bedTime){
            System.out.println("It's only "+timeNow+" oclock");
            System.out.println("Might as well stay up longer");
            timeNow++; //Time Passes
            
        }
        System.out.println("oh it's "+timeNow+" oclock");
            System.out.println("must sleep");
    }
    
}
