drop  database  if exists  BalloonBlog;
--  Creates Database if SuperHero exists.
Create database if not exists  BalloonBlog;

--  Create Tables
use BalloonBlog;
-- blog Tables

  CREATE TABLE IF NOT EXISTS `Blog` (
 `BlogId` int(11) NOT NULL AUTO_INCREMENT,
 `Title` varchar(50) NOT NULL,
 `Article` text NOT NULL,
 `StartDate` date NOT NULL,
 `EndDate` date ,
 `Featured` boolean NOT NULL,
 `Enabled` boolean NOT NULL,
 `AuthorId` int(11) NOT NULL,
 `CategoryId` int(11) NOT NULL,
 PRIMARY KEY (`BlogId`)
 );
 CREATE TABLE IF NOT EXISTS `Category` (
 `CategoryId` int(11) NOT NULL AUTO_INCREMENT,
 `Category` varchar(50) NOT NULL,
 
 PRIMARY KEY (`CategoryId`)
 );


 
   CREATE TABLE IF NOT EXISTS `BlogAuthor` (
 `AuthorId` int(11) NOT NULL AUTO_INCREMENT,
 `DisplayName` varchar(50) NOT NULL,
 `FirstName` varchar(50) NOT NULL,
 `LastName` varchar(50) NOT NULL,

 PRIMARY KEY (`AuthorId`)
 );
 
   CREATE TABLE IF NOT EXISTS `Tag` (
 `TagId` int(11) NOT NULL AUTO_INCREMENT,
 `Tag` varchar(50) NOT NULL,
 

 PRIMARY KEY (`TagId`)
 );
 
   CREATE TABLE IF NOT EXISTS `Blog_Tag` (
 `BlogId` int(11) NOT NULL,
 `TagId` int(11) NOT NULL,
 

 PRIMARY KEY (`BlogId`,`TagId`)
 );
   CREATE TABLE IF NOT EXISTS `Comment` (
 `CommentId` int(11) NOT NULL AUTO_INCREMENT,
 `BlogId` int(11) NOT NULL,
 `Comment` text NOT NULL,
 `DisplayName` varchar(50) NOT NULL,
 `CommentDate` date not null,

 PRIMARY KEY (`CommentId`)
 );
    CREATE TABLE IF NOT EXISTS `GenericPage` (
 `PageId` int(11) NOT NULL AUTO_INCREMENT,
 `CategoryId` int(11) NOT NULL ,
 `Title` varchar(50) NOT NULL,
 `PageText` text NOT NULL,
 `Enabled` boolean NOT NULL,
 `StartDate` date not null,
  `EndDate` date ,


 PRIMARY KEY (`PageId`)
 );
 
 
 
 
 -- Alter Balloon Tables

 -- Blog_Tag
  ALTER TABLE `Blog_Tag`
 ADD CONSTRAINT `Blog_Tag_BlogId`
 FOREIGN KEY (`BlogId`)
 REFERENCES `Blog` (`BlogId`);
 
 ALTER TABLE `Blog_Tag`
 ADD CONSTRAINT `Blog_Tag_TagId`
 FOREIGN KEY (`TagId`)
 REFERENCES `Tag` (`TagId`);
 

 

 -- Category
   ALTER TABLE `Blog`
 ADD CONSTRAINT `Blog_CategoryId`
 FOREIGN KEY (`CategoryId`)
 REFERENCES `Category` (`CategoryId`);
 
  -- Category
   ALTER TABLE `GenericPage`
 ADD CONSTRAINT `GenericPage_CategoryId`
  FOREIGN KEY (`CategoryId`)
 REFERENCES `Category` (`CategoryId`);
 
 
 -- insert Data
 -- Category
 INSERT INTO `Category` (`Category`) VALUES
('Hot Air Balloons'),
('FireWorks Balloons'),
('Generic Pages')

;
 -- Blog Author
  INSERT INTO `BlogAuthor` (`DisplayName`, `FirstName`, `LastName`) VALUES
('BBalloons','Bobby', 'Balloons'),
('Great','Greta', 'G');
 
 -- Blog
 INSERT INTO `Blog` (`Title`, `Article`, `StartDate`, `EndDate`, `Featured`, `Enabled`, `AuthorId`,`CategoryId`) VALUES
('Big Bad  Balloons', 'Big Bad  Balloons Article Big Bad  Balloons Article', '2017/04/01','2017/04/01',true,true, 1,1),
('99 Red Balloon', '99 Red Balloon Article 99 Red Balloon Article', '2017/04/01','2017/04/01',true,true, 2,2);
 
 -- Comment
 
  INSERT INTO `Comment` (`BlogId`, `Comment`, `DisplayName`, `CommentDate`) VALUES
('1', 'Great Read','AirHead', '2017/04/01'),
('2', 'VeryInteresting','HotAir', '2017/04/01');

 INSERT INTO `Tag` (`Tag`) VALUES
('Balloons'),
('Baskets'),
('Hot Air');

 INSERT INTO `Blog_Tag` (`BlogId`,`TagId`) VALUES
(1,1),
(1,2),
(1,3),
(2,1),
(2,2),
(2,3);




 INSERT INTO `GenericPage` (`CategoryId`,`Title`,`PageText`,`Enabled`, `StartDate`, `EndDate`) VALUES
(3,'Disclaimer Page','Disclaimer PageDisclaimer PageDisclaimer PageDisclaimer PageDisclaimer Page', true, '2017-04-01', '2050-04-01'),
(3,'About Us','About UsAbout UsAbout UsAbout UsAbout UsAbout UsAbout UsAbout Us', true, '2017-04-01', '2050-04-01');





