use BalloonBlog;

-- Blog

-- Select Last 10 Blogs;
select * from Blog
limit 10;

-- Select Last 1 FeaturedBlog;
select * from Blog
where featured=true
order by blogId desc
limit 1;

-- Select last 3 Featured Blogs
select * from Blog
where featured=true
order by blogId desc
limit 3;
-- Create
 INSERT INTO Blog (Title, Article, StartDate, EndDate, Featured, Enabled, AuthorId,CategoryId) VALUES
('Test', 'Test', '2017/04/01',null,true,true, 1,1);

 INSERT INTO Blog (Title, Article, StartDate, EndDate, Featured, Enabled, AuthorId,CategoryId) VALUES
(?, ?, ?, ?, ?, ?, ?, ?);

-- Update
update Blog set Title ='Test 2', Article='Test 2',StartDate='2017/04/01',EndDate='2017/04/10',Featured=true,Enabled=true,AuthorId= 1,CategoryId=1 
where blogId=1;

update Blog set Title =?, Article=?,StartDate=?,EndDate=?,Featured=?,Enabled=?,AuthorId= ?,CategoryId=? 
where blogId=1;
-- Remove
delete from Blog_Tag
where blogId =1;
delete from `Comment`
where blogId =1;
delete from Blog
where blogId =1;

delete from Blog_Tag
where blogId =?;
delete from `Comment`
where blogId =?;
delete from Blog
where blogId =?;
-- Get By ID
select * from  Blog
where blogId =1;
select * from  Blog
where blogId =?;
-- Get AlL
select * from Blog;


-- BlogAuthor


-- Create
  INSERT INTO BlogAuthor (DisplayName, FirstName, LastName) VALUES
('Test','Test', 'Test');
 
 INSERT INTO BlogAuthor (DisplayName, FirstName, LastName) VALUES
(?, ?, ?);

-- Update
update BlogAuthor set DisplayName ='Test 2', FirstName='Test 2',FirstName='Test2'
where authorId=1;

update BlogAuthor set DisplayName =?, FirstName=?,FirstName=?
where authorId=?;


-- Remove
delete from BlogAuthor
where authorId =3;
delete from BlogAuthor
where authorId =?;
-- Get By ID
select * from  BlogAuthor
where authorId =1;
select * from  BlogAuthor
where authorId =?;
-- Get AlL
select * from BlogAuthor;


-- Tag


-- Create
  INSERT INTO Tag (Tag) VALUES
('Test');
 
 INSERT INTO Tag (Tag) VALUES
(?);

-- Update
update Tag set Tag ='Test 2'
where TagId=1;

update Tag set Tag =?
where TagId=?;


-- Remove
delete from Tag
where TagId =1;
delete from Tag
where TagId =?;
-- Get By ID
select * from  Tag
where TagId =1;
select * from  Tag
where TagId =?;
-- Get AlL
select * from Tag;

-- BlogTag


-- Create
  INSERT INTO Blog_Tag (BlogId,TagID) VALUES
(4,1);
 INSERT INTO Blog_Tag (BlogId,TagID) VALUES
(?,?);
 


-- Update
update Blog_Tag set BlogId =4 ,TagId=2
where BlogId=4 and 
TagId=1;

update Blog_Tag set BlogId =? ,TagId=?
where BlogId=? and 
TagId=?;


-- Remove
delete from Blog_Tag
where BlogId=4 and 
TagId=1;
delete from Blog_Tag
where BlogId=? and 
TagId=?;
delete from Blog_Tag
where BlogId=? ;
delete from Blog_Tag
where TagId=?;
-- Get By ID
select * from Blog_Tag
where BlogId=4 and 
TagId=1;
select * from Blog_Tag
where BlogId=? and 
TagId=?;
-- Get AlL
select * from Blog_Tag;


-- Category

-- create
 INSERT INTO Category (Category) VALUES
('Test');
INSERT INTO Category (Category) VALUES(?);
-- update

update Category set Category='Test2' where CategoryId =1;
update Category set Category=? where CategoryId =?;

-- delete 
delete from Category where CategoryId =4;
delete from Category where CategoryId =?;

-- get 
Select * from Category where CategoryId =1;
Select * from Category where CategoryId =?;

-- get all

select * from Category;



-- Comment

-- create
 INSERT INTO `Comment` (blogId, `Comment`, displayName, CommentDate) VALUES
(1,'Bird', 'BirdMan', '2014/01/01');
 INSERT INTO `Comment` (blogId, `Comment`, displayName, CommentDate) VALUES
(?, ?, ?, ? );
-- update

update `Comment` set blogId=2, `Comment`='2 Birds', displayName='the Birdman', commentDate ='2012/01/01'
where CommentId=2;

update `Comment` set blogId=?, `Comment`=?, displayName=?, commentDate =?
where CommentId=?;

-- delete 
delete from `Comment` where CommentId=1;
delete  from `Comment`  where CommentId=?;

-- get 
select * from `Comment` where CommentId=1;
select * from `Comment`  where CommentId=?;

-- get all

select * from Comment;


-- GenericPage

-- Select Category by  Page Id
Select c.CategoryId, c.Category from
Category c 
inner join GenericPage gp
on c.CategoryId=gp.CategoryId
where pageId=1;

Select c.CategoryId, c.Category from Category c inner join GenericPage gp on c.CategoryId=gp.CategoryId where pageId=?;
-- create
 INSERT INTO `GenericPage` (`CategoryId`,`Title`,`PageText`,`Enabled`, `StartDate`, `EndDate`) VALUES
(3,'Disclaimer Page','Disclaimer PageDisclaimer PageDisclaimer PageDisclaimer PageDisclaimer Page', true, '2017-04-01', null);

INSERT INTO `GenericPage` (`CategoryId`,`Title`,`PageText`,`Enabled`, `StartDate`, `EndDate`) VALUES (?, ?, ?, ?, ?, ?);



-- update

update GenericPage set CategoryId=2, Title='2 Birds', PageText='the Birdman', Enabled =True, StartDate ='2017-04-01', EndDate ='2017-04-01'
where pageId=2;

update GenericPage set CategoryId=?, Title=?, PageText=?, Enabled =?, StartDate =?, EndDate =? where pageId=?;

-- delete 
-- 
delete from GenericPage where pageId=1;
delete from GenericPage where pageId=?;

-- get 
select * from GenericPage where pageId=1;
select * from GenericPage where pageId=?;

-- get all

select * from GenericPage;





-- Security
insert into users (username, password, enabled) values (?, ?, 1);
insert into authorities (username, authority) values (?, ?);
delete from users where username = ?;
delete from authorities where username = ?;
select * from users;
