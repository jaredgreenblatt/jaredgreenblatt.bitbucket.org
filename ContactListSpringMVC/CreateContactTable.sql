drop  database  if exists  address_book;
--  Creates Database if HollywoodTowerHotel exists.
Create database if not exists  address_book;

use contact_list;
CREATE TABLE IF NOT EXISTS `contacts` (
 `contact_id` int(11) NOT NULL AUTO_INCREMENT,
 `full_name` varchar(100) NOT NULL,
 `street_address` varchar(100) NOT NULL,
 `city` varchar(50) NOT NULL,
 `state` varchar(10) NOT NULL,
 `zip` varchar(10) NOT NULL,
 PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;



drop  database  if exists  contact_list_test;
--  Creates Database if HollywoodTowerHotel exists.
Create database if not exists  contact_list_test;

use contact_list_test;

CREATE TABLE IF NOT EXISTS `contacts` (
 `contact_id` int(11) NOT NULL AUTO_INCREMENT,
 `first_name` varchar(50) NOT NULL,
 `last_name` varchar(50) NOT NULL,
 `company` varchar(50) NOT NULL,
 `phone` varchar(10) DEFAULT NULL,
 `email` varchar(50) NOT NULL,
 PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;