//contactList.js
$(document).ready(function() {
    $('#runningTotal').val(0);
    loadMachine();
});

function selectVendingItem(id) {
    clearErrorMessages();
    var itemNumber = id;
    var itemSelection = $('#itemChoice');
    itemSelection.val(itemNumber);
}

function clearVendingMachine() {
    $('#VendingItems').empty();
    $('#itemChoice').val('');
    $('#moneyIn').val('');
    $('#runningTotal').val(parseFloat(0.00));
    //$('#messages').val('');

}

function clearErrorMessages() {
    $('#messages').empty();


}


function makeChange() {
    //    alert('SUCCESS');
    var moneyInVendingMachine = parseFloat($('#runningTotal').val());
    var quarterCount = 0;
    var dimeCount = 0;
    var nickleCount = 0;
    var pennyCount = 0;
    var printChange = $('#change')

    while (moneyInVendingMachine >= 0.25) {
        quarterCount++;
        moneyInVendingMachine = moneyInVendingMachine - .25;
        moneyInVendingMachine = moneyInVendingMachine.toFixed(2);
    }
    while (moneyInVendingMachine >= 0.10) {
        dimeCount++;
        moneyInVendingMachine = moneyInVendingMachine - .1;
        moneyInVendingMachine = moneyInVendingMachine.toFixed(2);
    }
    while (moneyInVendingMachine >= 0.05) {
        nickleCount++;
        moneyInVendingMachine = moneyInVendingMachine - .05;
        moneyInVendingMachine = moneyInVendingMachine.toFixed(2);
    }
    while (moneyInVendingMachine >= 0.01) {
        pennyCount++;
        moneyInVendingMachine = moneyInVendingMachine - .01;
        moneyInVendingMachine = moneyInVendingMachine.toFixed(2);
    }

    var changeString = '';

    if (quarterCount == 1) {
        changeString += quarterCount + ' Quarter. '

    } else if (quarterCount > 1) {
        changeString += quarterCount + ' Quarters. '

    }
    if (dimeCount == 1) {
        changeString += dimeCount + ' Dime. '

    } else if (dimeCount > 1) {
        changeString += pennyCount + ' Dimes. '

    }
    if (nickleCount == 1) {
        changeString += nickleCount + ' Nickle. '

    } else if (nickleCount > 1) {
        changeString += nickleCount + ' Nickles. '

    }
    if (pennyCount == 1) {
        changeString += pennyCount + ' Penny. '

    } else if (pennyCount > 1) {
        changeString += pennyCount + ' Pennies. '

    }
    printChange.val(changeString);
    clearVendingMachine();
    loadMachine();



}

function makePurchase() {
    clearErrorMessages();
    //clearErrorMessages();
    var printChange = $('#change')
    var amount = parseFloat($('#runningTotal').val());
    var id = $('#itemChoice').val();
    var concatUrl = 'http://localhost:8080/money/';
    concatUrl += +amount + '/item/' + id;


    $.ajax({
        type: 'GET',
        url: concatUrl,
        success: function(purchaseItem) {
            //  $.each(purchaseItem, function(index, changeArray) {
            var quarters = purchaseItem.quarters;
            var dimes = purchaseItem.dimes;
            var nickels = purchaseItem.nickels;
            var pennies = purchaseItem.pennies;

            var changeString = '';

            if (quarters == 1) {
                changeString += quarters + ' Quarter. '

            } else if (quarters > 1) {
                changeString += quarters + ' Quarters. '

            }
            if (dimes == 1) {
                changeString += dimes + ' Dime. '

            } else if (dimes > 1) {
                changeString += dimes + ' Dimes. '

            }
            if (nickels == 1) {
                changeString += nickels + ' Nickle. '

            } else if (nickels > 1) {
                changeString += nickels + ' Nickles. '

            }
            if (pennies == 1) {
                changeString += pennies + ' Penny. '

            } else if (pennies > 1) {
                changeString += pennies + ' Pennies. '

            }
            printChange.val(changeString);
            clearVendingMachine();
            loadMachine();

            //  });
        },
        //  alert("Success");


        error: function(data) {
            var errorMessage = data.responseJSON.message;

            $('#errorMessages')
                .append($('#messages')
                    // .attr({
                    //     class: 'form-control noresize',
                    //     rows: '4',
                    //     id: 'messages',
                    //   //  readOnly
                    //     style:"resize:none",
                    // })
                    .text(errorMessage));

        }


    });





}

function addMoney(inputMoneyType) {
    clearErrorMessages();

    $('#change').val('');
    var moneyAmount = 0.00;
    var dollarAmount = $('#moneyIn');

    if (inputMoneyType == 'dollar') {
        moneyAmount = parseFloat($('#runningTotal').val());
        moneyAmount += 1.00;
        moneyAmount = moneyAmount.toFixed(2);
        $('#runningTotal').val(moneyAmount);

    } else if (inputMoneyType == 'quarter') {
        moneyAmount = parseFloat($('#runningTotal').val());
        moneyAmount += 0.25;
        moneyAmount = moneyAmount.toFixed(2);
        $('#runningTotal').val(moneyAmount);
    } else if (inputMoneyType == 'dime') {
        moneyAmount = parseFloat($('#runningTotal').val());
        moneyAmount += 0.10;
        moneyAmount = moneyAmount.toFixed(2);
        $('#runningTotal').val(moneyAmount);
    } else if (inputMoneyType == 'nickle') {
        moneyAmount = parseFloat($('#runningTotal').val());
        moneyAmount += 0.05;
        moneyAmount = moneyAmount.toFixed(2);
        $('#runningTotal').val(moneyAmount);
    }

    dollarAmount.val(moneyAmount);



}





function loadMachine() {

    var row1 = $('#row1');
    var row2 = $('#row2');
    var row3 = $('#row3');
    var countItem = 0;
    var rowCount = 1;
    var innerRowCount = 0;

    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/items',
        success: function(vendingItemArray) {
            $.each(vendingItemArray, function(index, vendingItem) {
                var id = vendingItem.id;
                var name = vendingItem.name;
                var price = vendingItem.price;
                price = price.toFixed(2);
                var quantity = vendingItem.quantity;


                var rowItem1 = '<div class="col-sm-4">';
                rowItem1 += '<button type="button" id="button' + id + '" class="btn btn-secondary btn-lg" onclick="selectVendingItem(' + id + ') " style=" width:160px; height:160px; margin-bottom:4px; word-wrap:break-word;">';
                rowItem1 += '<p class="text-left">' + id + '</p>';
                rowItem1 += '<p class="text-center">' + name + '</p>';
                rowItem1 += '<p class="text-center">$ ' + price + '</p>';
                rowItem1 += '<p class="text-center">Quantity Left: ' + quantity + '</p>';
                rowItem1 += '</button>'
                rowItem1 +='<br />'
                rowItem1 +='<br />'
                rowItem1 +='<br />'


                var row = $('#VendingItems');





                row.append(rowItem1);






            });







        },

        error: function(data) {
            $('#errorMessages')
                .append($('<li>')
                    .attr({
                        class: 'list-group-item list-group-item-danger'
                    })
                    .text('Error Calling Web Service. Please try again later.'));

        }


    });



}
