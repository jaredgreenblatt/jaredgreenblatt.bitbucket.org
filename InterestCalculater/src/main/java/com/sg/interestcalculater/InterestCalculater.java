/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.interestcalculater;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class InterestCalculater {

    public static void main(String[] args) {

        System.out.print("Enter the interest rate: ");
        double interestRate = userInputDouble();

        System.out.print("Enter the initial principal: ");
        double initialPrincipal = userInputDouble();

        System.out.print("Enter number of years: ");
        int numberOfYears = userInputInt();

//        System.out.println(interestRate);
//        System.out.println(initialPrincipal);
//        System.out.println(numberOfYears);
        boolean x = validateInputs(interestRate, initialPrincipal, numberOfYears);

        while (x == false) {
            System.out.println("Please reenter the user inputs");

            System.out.print("Enter the interest rate: ");
            interestRate = userInputDouble();
            System.out.print("Enter the initial principal: ");
            initialPrincipal = userInputDouble();

            System.out.print("Enter number of years: ");
            numberOfYears = userInputInt();

            x = validateInputs(interestRate, initialPrincipal, numberOfYears);

        }

        finacialCalculater(interestRate, initialPrincipal, numberOfYears);

    }

    public static int userInputInt() {

        // scanner
        Scanner sc = new Scanner(System.in);
        // Read in inputs are an integer
        int value = sc.nextInt();

        return value;
    }

    public static double userInputDouble() {

        //scanner
        Scanner sc = new Scanner(System.in);
        //Read input as a double
        double value = sc.nextDouble();

        return value;

    }

    public static boolean validateInputs(double interestRate, double initialPrincipal, int numberOfYears) {

        boolean x;
        if (interestRate > 0 && initialPrincipal > 0 && numberOfYears > 0) {
            x = true;

        } else {
            x = false;
        }

        return x;

    }

    public static void finacialCalculater(double iRate, double iPrincipal, int numYears) {
        double currentBalance = iPrincipal;
        double yearStartPrincipal;
        
       
        
        for (int i = 1; i <= numYears; i++) {
            yearStartPrincipal =currentBalance;
            

            for (int j = 1; j <= 4; j++) {
                //interest calculation.
                //
                currentBalance = currentBalance * (1 + ((iRate * .25) / 100));
            }
            System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
            System.out.println("");
            System.out.println("The Year Number "+i);
            System.out.println("");
            System.out.println("Principal at the Beginning of Year "+yearStartPrincipal );
            System.out.println("");
            System.out.println("Total amount of Interest earned at End of Year "+(currentBalance-yearStartPrincipal));
            System.out.println("");
            System.out.println("Prinicipal at the end of year "+currentBalance);
            System.out.println("");
            System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
            System.out.println("");

        }

    }

}
