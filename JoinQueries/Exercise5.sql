/*
	Find a list of all the Employees who have never found a Grant
*/

USE SWCCorp;





SELECT FirstName, LastName
FROM Employee
	LEFT JOIN `Grant` ON
		Employee.EmpID = `Grant`.EmpID
WHERE `Grant`.EmpID IS NULL;



