/*
	Write a query to show every combination of employee and location.
*/

USE SWCCorp;

select e.firstName, e.lastName ,l.city
from Employee e cross join Location l;

