/*
	Get a list of each employee first name and lastname
	and the territory names they are associated with
*/

USE Northwind;
-- Select * from  Employees;
-- Select * from  EmployeeTerritories;
-- Select * from  Territories;



Select FirstName, LastName, TerritoryDescription
From Employees
inner join EmployeeTerritories
 on  EmployeeTerritories.EmployeeID = Employees.EmployeeID 
inner join Territories 
on EmployeeTerritories.TerritoryID = Territories.TerritoryID 
