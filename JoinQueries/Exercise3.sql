/*
	Get all the order information for any order where Chai was sold.
*/

USE Northwind;

Select * from Orders o
inner join Order_Details od
on o.OrderID = od.OrderId
inner join Products p
on od.ProductId = p.ProductId
where p.ProductName = 'Chai';




