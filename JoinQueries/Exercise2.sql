/*
	Get the Company Name, Order Date, and each order details 
	Product name for USA customers only.
*/

USE Northwind;




Select c.CompanyName , o.OrderDate , p.ProductName

from Customers c
inner join Orders o
on o.CustomerID =c.customerId
inner join Order_Details od
on o.OrderId = od.OrderID
inner join Products p
on od.productID = p.productId
where c.Country ='USA';


