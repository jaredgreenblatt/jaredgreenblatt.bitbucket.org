/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachinespringmvc.dao;

import com.sg.vendingmachinespringmvc.model.VendingItem;
import java.util.List;

/**
 *
 * @author apprentice
 */

public interface VendingMachineDao {
    
        /**
     * Adds the given Vending Item to the Inventory and associates it with the
     * given Vending Item ID. If there is already a vending item associated with
     * the given Vending Item ID it will return that VendingItem object,
     * otherwise it will return null.
     *
     *
     * @param itemId
     * @param vendingItem
     * @return
     * @throws com.sg.vendingmachine.dao.VendingItemPersistenceException
     */
    VendingItem addVendingItem(VendingItem vendingItem) ;
    /**
     * Returns a String array containing the Item ids of all Items in the
     * Inventory.
     *
     *
     * @return
     * @throws com.sg.vendingmachine.dao.VendingItemPersistenceException
     */
    List<VendingItem> getAllVendingItems() ;

    /**
     * Returns the VendingItem object associated with the given item id. Returns
     * null if no such item exists
     *
     * @param itemId
     * @return
     * @throws com.sg.vendingmachine.dao.VendingItemPersistenceException
     */

    VendingItem getVendingItem(long itemId) ;
    /**
     * Removes from the inventory the VendingItem associated with the given id.
     * Returns the vending item object that is being removed or null if there is
     * no vendingItem associated with the given id
     *
     *
     * @param itemId
     * @return
     * @throws com.sg.vendingmachine.dao.VendingItemPersistenceException
     */
    VendingItem removeItem(long itemId) ;
    
    void editVendingItem(VendingItem vendingItem);
    

    
        
    
}
