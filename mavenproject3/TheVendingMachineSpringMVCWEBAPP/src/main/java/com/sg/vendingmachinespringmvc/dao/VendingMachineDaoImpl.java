/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachinespringmvc.dao;

import com.sg.vendingmachinespringmvc.model.VendingItem;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apprentice
 */

public class VendingMachineDaoImpl implements VendingMachineDao {

    private Map<Long, VendingItem> vendingItemsMap = new HashMap<>();
    private static long vendingItemIdCounter = 1;

    public VendingMachineDaoImpl() {
        VendingItem vi = new VendingItem();
        vi.setItemCost(new BigDecimal("1.66"));
        vi.setItemName("Skittles");
        vi.setNumberOfItemsInInventory(13);
        addVendingItem(vi);
        vi = new VendingItem();
        vi.setItemCost(new BigDecimal("1.65"));
        vi.setItemName("Coke");
        vi.setNumberOfItemsInInventory(7);
        addVendingItem(vi);
        vi = new VendingItem();
        vi.setItemCost(new BigDecimal("1.95"));
        vi.setItemName("Starburst");
        vi.setNumberOfItemsInInventory(6);
        addVendingItem(vi);
        vi = new VendingItem();
        vi.setItemCost(new BigDecimal("1.85"));
        vi.setItemName("Snickers");
        vi.setNumberOfItemsInInventory(0);
        addVendingItem(vi);
        vi = new VendingItem();
        vi.setItemCost(new BigDecimal("1.46"));
        vi.setItemName("Reeces");
        vi.setNumberOfItemsInInventory(2);
        addVendingItem(vi);
        vi = new VendingItem();
        vi.setItemCost(new BigDecimal("1.33"));
        vi.setItemName("Diet Coke");
        vi.setNumberOfItemsInInventory(4);
        addVendingItem(vi);
        vi = new VendingItem();
        vi.setItemCost(new BigDecimal("2.22"));
        vi.setItemName("Sweedish Fish");
        vi.setNumberOfItemsInInventory(8);
        addVendingItem(vi);
        vi = new VendingItem();
        vi.setItemCost(new BigDecimal("1.88"));
        vi.setItemName("Gold Fish");
        vi.setNumberOfItemsInInventory(14);
        addVendingItem(vi);
        vi = new VendingItem();
        vi.setItemCost(new BigDecimal("1.77"));
        vi.setItemName("M&M's");
        vi.setNumberOfItemsInInventory(9);
        addVendingItem(vi);
          vi = new VendingItem();
        vi.setItemCost(new BigDecimal("2.75"));
        vi.setItemName("Ice Cream");
        vi.setNumberOfItemsInInventory(8);
        addVendingItem(vi);
        vi = new VendingItem();
        vi.setItemCost(new BigDecimal("3.66"));
        vi.setItemName("Dells Slush");
        vi.setNumberOfItemsInInventory(14);
        addVendingItem(vi);
        vi = new VendingItem();
        vi.setItemCost(new BigDecimal("1.27"));
        vi.setItemName("Pop Rocks");
        vi.setNumberOfItemsInInventory(9);
        addVendingItem(vi);

    }

    @Override
    public VendingItem addVendingItem(VendingItem vendingItem) {
        vendingItem.setItemId(vendingItemIdCounter);
        vendingItemIdCounter++;
        vendingItemsMap.put(vendingItem.getItemId(), vendingItem);

        return vendingItem;
    }

    @Override
    public List<VendingItem> getAllVendingItems() {
        return new ArrayList<>(vendingItemsMap.values());
    }

    @Override
    public VendingItem getVendingItem(long itemId) {
        return vendingItemsMap.get(itemId);

    }

    @Override
    public VendingItem removeItem(long itemId) {
        VendingItem removedVendingItem = vendingItemsMap.remove(itemId);
        return removedVendingItem;
    }

    @Override
    public void editVendingItem(VendingItem vendingItem) {
        removeItem(vendingItem.getItemId());
        vendingItemsMap.put(vendingItem.getItemId(), vendingItem);
    }

}
