/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachinespringmvc.service;

import com.sg.vendingmachinespringmvc.dao.VendingMachineDao;
import com.sg.vendingmachinespringmvc.model.Change;
import com.sg.vendingmachinespringmvc.model.VendingItem;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author apprentice
 */

public class VendingMachineServiceLayerImpl implements VendingMachineServiceLayer {

    private VendingMachineDao dao;
    private BigDecimal runningTotal = new BigDecimal("0.00");

    public VendingMachineServiceLayerImpl(VendingMachineDao dao) {
        this.dao = dao;

    }
    //Validation used to force the vending machine objects to have all there fields.

    private void validateVendingItem(VendingItem vendingItem) throws
            VendingMachineDataValidationException {

        if (vendingItem.getItemName() == null
                || vendingItem.getItemName().trim().length() == 0
                || vendingItem.getItemCost() == null
                || vendingItem.getItemCost().toString().trim().length() == 0) {

            throw new VendingMachineDataValidationException(
                    "ERROR: All fields [Item Name and Item Price] are required.");
        }
    }

    @Override
    public void createVendingItem(VendingItem vendingItem) throws VendingMachineDataValidationException {
        validateVendingItem(vendingItem);

        dao.addVendingItem(vendingItem);
    }

    @Override
    public void editVendingItem(VendingItem vendingItem) throws VendingMachineDataValidationException {
        validateVendingItem(vendingItem);
        dao.editVendingItem(vendingItem);
    }

    @Override
    public List<VendingItem> getAllVendingItem() {
        return dao.getAllVendingItems();
    }

    @Override
    public VendingItem getVendingItem(long itemId) {

        return dao.getVendingItem(itemId);
    }

    @Override
    public VendingItem removeVendingItem(long itemId) {
        return dao.removeItem(itemId);
    }

    @Override
    public VendingItem purchaseItem(VendingItem vendingItem) throws NoItemInventoryException, InsufficientFundsException {
        // Throwing an Exception if the inventory is out of Stock.
        if (vendingItem.getNumberOfItemsInInventory() == 0) {
            throw new NoItemInventoryException(
                    "SOLD OUT!!!");
        }

        // Throwing an Exception if you try to buy something without enough money.
        if (vendingItem.getItemCost().compareTo(runningTotal) == 1) {
            BigDecimal addThisAmount = vendingItem.getItemCost().subtract(runningTotal);
            String addThisAmountString = addThisAmount.toString();
            throw new InsufficientFundsException(
                    "Please deposit: $"
                    + addThisAmountString);

        }
//Code Blockif it makes it through the catches.
// setting up temp variables to store the value of the current ITEM that
// we are purchasing.
        BigDecimal tempCost = vendingItem.getItemCost();
        String tempName = vendingItem.getItemName();
        int tempInventory = vendingItem.getNumberOfItemsInInventory();
        //Subtracting one from the inventory.
        tempInventory = tempInventory - 1;
        //subtracting the cost of the item from the running total in the machine.
        runningTotal = runningTotal.subtract(tempCost);
        //Instating a new object for us to return to the updated item 
        //With the change in inventory.
        VendingItem editedItem = getVendingItem(vendingItem.getItemId());
        editedItem.setItemName(tempName);
        editedItem.setItemCost(tempCost);
        editedItem.setNumberOfItemsInInventory(tempInventory);

        return editedItem;

    }

    @Override
    public Change makeChange() {
        Change change = new Change(runningTotal);
        runningTotal = runningTotal.subtract(runningTotal);

        return change;
    }

    @Override
    public BigDecimal getrunningTotal() {
        return runningTotal;
    }

    @Override
    public void setrunningTotal(BigDecimal runningTotal) {
        this.runningTotal = runningTotal;
    }

}

