/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachinespringmvc;

import com.sg.vendingmachinespringmvc.model.Change;
import com.sg.vendingmachinespringmvc.model.VendingItem;
import com.sg.vendingmachinespringmvc.service.InsufficientFundsException;
import com.sg.vendingmachinespringmvc.service.NoItemInventoryException;
import com.sg.vendingmachinespringmvc.service.VendingMachineServiceLayer;
import java.math.BigDecimal;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 *
 * @author apprentice
 */
@Controller
public class VendingMachineController {

    private VendingMachineServiceLayer service;
    //Spring injection for the controller. Passing in the service layer.

    @Inject
    public VendingMachineController(VendingMachineServiceLayer service) {
        this.service = service;
    }

    /**
     * This controller method used when the the home page of the application is
     * hit. When the Users request is sent to application This method will run.
     * It grabs a list of the vending items, The applications running total and
     * then passes them to index.jsp. This used to load the vending machine
     * contents and to get the applications running total. If it succeeds it
     * will make the purchase and then rebuild the index page. this is done by
     * calling on the service layer. If the purchase fails it will throw an
     * error message. These are caught by the controller method. It will then
     * send the message back to the jsp. As well as rebuild the index page.
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String vendingMachine(Model model) {

        List<VendingItem> vendingItemList = service.getAllVendingItem();
        String runningTotal = service.getrunningTotal().toString();
        model.addAttribute("runningTotal", runningTotal);
        model.addAttribute("vendingItemList", vendingItemList);
        return "index";
    }

    /**
     * purchase Item is used when the make purchase button is clicked. First it
     * gets the item Choice and the running total from the JSP. Next it
     * validates that the VendingIdInput is not empty. If it's empty it will
     * return an error message and then rebuild the index page. If there is a
     * valid item the controller method will now try to make a purchase
     *
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value = "/PurchaseItem", method = RequestMethod.POST)
    public String makePurchase(HttpServletRequest request, Model model) {
        String errorMessage;
        BigDecimal runningTotal;
        String vendingIdInput = request.getParameter("itemChoice");
        String runningTotalInput = request.getParameter("runningTotal");
        
     
            try {
                   if (vendingIdInput.isEmpty() || vendingIdInput == null) {

            errorMessage = "You have made an invalid Selection!";
            model.addAttribute("errorMessage", errorMessage);

        } else {

                long vendingItemId = Long.parseLong(vendingIdInput);
                runningTotal = new BigDecimal(runningTotalInput);
                VendingItem purchaseVendingItem = service.getVendingItem(vendingItemId);
                service.setrunningTotal(runningTotal);
                service.purchaseItem(purchaseVendingItem);

                Change purchaseChange = service.makeChange();
                model.addAttribute("purchaseChange", purchaseChange);

                String thankYou = "Thank You!!!";
                model.addAttribute("thankYou", thankYou);

            } 

            
        }catch (NoItemInventoryException | InsufficientFundsException e) {
                errorMessage = e.getMessage();
                model.addAttribute("errorMessage", errorMessage);
        }catch ( NumberFormatException e) {
                errorMessage = "Number Format Exception.";
                model.addAttribute("errorMessage", errorMessage);
        }
    
        runningTotal = service.getrunningTotal();
        model.addAttribute("runningTotal", runningTotal);
        List<VendingItem> vendingItemList = service.getAllVendingItem();
        model.addAttribute("vendingItemList", vendingItemList);

        return "index";} 

    }


