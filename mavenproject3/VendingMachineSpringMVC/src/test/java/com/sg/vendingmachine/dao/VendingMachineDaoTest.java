/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.dao;

import com.sg.vendingmachinespringmvc.dao.VendingMachineDao;
import com.sg.vendingmachinespringmvc.model.VendingItem;
import java.math.BigDecimal;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class VendingMachineDaoTest {

    private VendingMachineDao dao;

    public VendingMachineDaoTest() {

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
         ApplicationContext ctx
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = ctx.getBean("vendingMachineDao", VendingMachineDao.class);
   
        }
    @After
    public void tearDown() {
    }
    
     @Test
    public void addGetDeleteContact() {
        // Create new contact
        VendingItem vi = new VendingItem();
        vi.setItemCost(new BigDecimal(1.50));
        vi.setItemName("Candy");
        vi.setNumberOfItemsInInventory(10);
        dao.addVendingItem(vi);
        VendingItem fromDb = dao.getVendingItem(vi.getItemId());
        assertEquals(fromDb, vi);
        dao.removeItem(vi.getItemId());
        assertNull(dao.getVendingItem(vi.getItemId()));
    }
    
      @Test
    public void addUpdateContact() {
        // Create new contact
        VendingItem vi = new VendingItem();
        vi.setItemCost(new BigDecimal(1.50));
        vi.setItemName("Candy");
        vi.setNumberOfItemsInInventory(10);
        dao.addVendingItem(vi);
       vi.setItemName("Skittles");
        dao.editVendingItem(vi);
        VendingItem fromDb = dao.getVendingItem(vi.getItemId());
        assertEquals(fromDb, vi);
    }

}
