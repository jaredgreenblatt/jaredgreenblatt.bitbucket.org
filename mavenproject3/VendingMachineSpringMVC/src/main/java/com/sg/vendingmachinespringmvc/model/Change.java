/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachinespringmvc.model;

import java.math.BigDecimal;

/**
 *
 * @author apprentice
 */
public class Change {
    // Setting the Values of the Change Properties.

    private final BigDecimal QUARTER = new BigDecimal("0.25");
    private final BigDecimal DIME = new BigDecimal("0.10");
    private final BigDecimal NICKLE = new BigDecimal("0.05");
    private final BigDecimal PENNY = new BigDecimal("0.01");
    private int quarterCount = 0;
    private int dimeCount = 0;
    private int nickleCount = 0;
    private int pennyCount = 0;

    public int getQuarterCount() {
        return quarterCount;
    }

    public void setQuarterCount(int quarterCount) {
        this.quarterCount = quarterCount;
    }

    public int getDimeCount() {
        return dimeCount;
    }

    public void setDimeCount(int dimeCount) {
        this.dimeCount = dimeCount;
    }

    public int getNickleCount() {
        return nickleCount;
    }

    public void setNickleCount(int nickleCount) {
        this.nickleCount = nickleCount;
    }

    public int getPennyCount() {
        return pennyCount;
    }

    public void setPennyCount(int pennyCount) {
        this.pennyCount = pennyCount;
    }

    public Change(BigDecimal changeAmount) {

        while (changeAmount.compareTo(QUARTER) == 1 || changeAmount.compareTo(QUARTER) == 0) {
            changeAmount = changeAmount.subtract(QUARTER);
            quarterCount++;

        }
        while (changeAmount.compareTo(DIME) == 1 || changeAmount.compareTo(DIME) == 0) {
            changeAmount = changeAmount.subtract(DIME);
            dimeCount++;

        }
        while (changeAmount.compareTo(NICKLE) == 1 || changeAmount.compareTo(NICKLE) == 0) {
            changeAmount = changeAmount.subtract(NICKLE);
            nickleCount++;

        }
        while (changeAmount.compareTo(PENNY) == 1 || changeAmount.compareTo(PENNY) == 0) {
            changeAmount = changeAmount.subtract(PENNY);
            pennyCount++;

        }

    }

    @Override
    public String toString() {
        String changeString = "";
        if (quarterCount == 1) {
            changeString = changeString.concat(quarterCount + " Quarter ");
        } else if (quarterCount > 1) {
            changeString = changeString.concat(quarterCount + " Quarters ");

        }
        if (dimeCount == 1) {
            changeString = changeString.concat(dimeCount + " Dime ");
        } else if (dimeCount > 1) {
            changeString = changeString.concat(dimeCount + " Dimes ");

        }
        if (nickleCount == 1) {
            changeString = changeString.concat(nickleCount + " Nickle ");
        } else if (nickleCount > 1) {
            changeString = changeString.concat(nickleCount + " Nickles ");

        }
        if (pennyCount == 1) {
            changeString = changeString.concat(pennyCount + " Penny ");
        } else if (pennyCount > 1) {
            changeString = changeString.concat(pennyCount + " Pennies ");

        }

        return changeString;
    }

    /**
     * Constuctor used to return the change object after the remaining amount is
     * run through a while loop to evaluate for each money denomination. While
     * the the amount is greater than or equal the counter for that amount will
     * increment by one. Getters and setter will then be used by the view to
     * print the amount of change owed to the user.
     *
     * @param changeAmount
     */
}
