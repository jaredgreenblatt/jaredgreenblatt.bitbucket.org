<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Index Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">       
        <link href="${pageContext.request.contextPath}/css/vendingMachine.css" rel="stylesheet">   
        <link href="https://fonts.googleapis.com/css?family=Bungee+Shade" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <div class="row VendingMachineTitleRow header">
                <div class="col-xs-offset-4 col-xs-4">
                    <h1>Vending Machine</h1>

                </div>
            </div>
            <hr />
            <div class="row VendingMachineItemRow1">
                <div class="col-xs-8" id="VendingItems">
                    <c:forEach var="currentVendingItem" items="${vendingItemList}">

                        <div class="col-sm-4">
                            <button type="button" id="button<c:out value="${currentVendingItem.itemId}"/>" class="btn btn-secondary btn-lg" onclick="selectVendingItem(<c:out value="${currentVendingItem.itemId}"/>)" style=" width:160px; height:160px; margin-bottom:4px; word-wrap:break-word;">
                                <p class="text-left">
                                    <c:out value="${currentVendingItem.itemId}"/>
                                </p>
                                <p class="text-center">
                                    <c:out value="${currentVendingItem.itemName}"/>
                                </p>
                                <p class="text-center">$
                                    <c:out value="${currentVendingItem.itemCost}"/>
                                </p>
                                <p class="text-center">Quantity Left: 
                                    <c:out value="${currentVendingItem.numberOfItemsInInventory}"/>
                                </p>


                            </button>
                        </div>

                    </c:forEach>


                </div>
                <div class="col-xs-4" style="width:300px;height:200px" id="vendingMachineControls">
                    <div class="row VendingMoneyControls">
                        <form class ="form-horizontal"
                              role ="form" method ="POST"
                              action="PurchaseItem">
                            <div class="form-group">
                                <label for="moneyIn"><h4>Total $ In</h4></label>
                                <input type="hidden" id="runningTotal" name="runningTotal" value="${runningTotal}">
                                <input type="text" class="form-control" id="moneyIn" value="${runningTotal}" readOnly >
                            </div>
                    </div>

                    <div class="row VendingMoneyControl row2 ">



                        <button type="button" class="btn btn-secondary col-xs-5" onclick="addMoney('dollar')">Add Dollar</button>
                        <button type="button" class="btn btn-secondary  col-xs-offset-2 col-xs-5 " onclick="addMoney('quarter')">Add Quarter</button>
                    </div>
                    <br />
                    <div class="row VendingMoneyControl row2 ">
                        <button type="button" class="btn btn-secondary col-xs-5" onclick="addMoney('dime')">Add Dime</button>
                        <button type="button" class="btn btn-secondary col-xs-offset-2 col-xs-5" onclick="addMoney('nickle')">Add Nickle</button>
                    </div>
                    <hr />
                    <div class="row VendingMachineMessagesRow1">
                        <div class="form-group">


                            <label for="messages"><h4>Messages</h4></label>

                            <textarea class="form-control noresize" rows="4" id="messages"  style="resize: none" readonly><c:out value ="${errorMessage}"/><c:out value ="${thankYou}"/></textarea>

                        </div>
                    </div>
                    <div class="row VendingMachineMessagesRow2">
                        <label for="itemChoice">Item:</label>

                        <input input type="text" class="form-control" id="itemChoice" name="itemChoice" readonly></input>
                    </div>
                    <br />
                    <div class="row VendingMachineMessagesRow3">
                        <button type="submit" class="btn btn-secondary" onclick="makePurchase()">Make Purchase</button>
                        </form>
                    </div>
                    <hr />


                    <div class="row VendingMachineChangeRow1">
                        <div class="form-group">
                            <label for="change"><h4>Change</h4></label>
                            <input type="text" class="form-control" id="change" readOnly value="${purchaseChange}">





                            <br />
                            <button type="button" class="btn btn-secondary" onclick="makeChange()">Change Return</button>
                        </div>

                    </div>
                    <hr />
                </div>


            </div>
            <!-- onclick="selectVendingItem(3) " -->


        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/home.js"></script>

    </body>
</html>

