-- regular 
Select  Total
from Billing b 

order by Total desc
limit 0,10;

-- bonus
Select  Total, concat(c.FirstName," " ,c.LastName) as 'Name'
from Billing b 
inner join Reservation r
on b.reservationId = r.reservationId
inner join Customer c 
on r.CustomerID = c.CustomerID
where concat(c.FirstName," " ,c.LastName)='Joe Smith'
order by Total desc
limit 0,10;
