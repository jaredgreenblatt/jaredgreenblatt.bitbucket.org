-- Drops Database if HollywoodTowerHotel exists.
drop  database  if exists HollywoodTowerHotel;
--  Creates Database if HollywoodTowerHotel exists.
Create database if not exists HollywoodTowerHotel;
-- 
use HollywoodTowerHotel;
-- Create Tables
-- 
-- Room Table Primary Key Room ID
create table if not exists Rooms(
RoomID int not null auto_increment,
RoomNumber int not null,
Floor int not null,
RoomTypeID int not null,
primary key(RoomID)

);


-- RoomType Table Primary Key RoomType ID
create table if not exists RoomType(
RoomTypeID int not null auto_increment,
RoomTypeDescription varchar(45) not null,
OccupencyLimit int not null,
primary key(RoomTypeID)

);

-- RoomRates table Primary Key RoomRateID
create table if not exists RoomRates(
RoomRateID int not null auto_increment,
RoomTypeID int not null,
Rate decimal(10,2) not null,
RateDescription varchar(45) not null,
RoomRatesStartDate date not null,
RoomRatesEndDate date,
primary key(RoomRateID)

);

-- RoomRoomAmenities Table Composite Key Room ID and Amenities ID
create table if not exists RoomRoomAmenities(
RoomID int not null,
AmenitiesID int not null,

primary key(RoomID,AmenitiesID)

);

-- RoomAmmenties Table AmenitiesID Primary Key
create table if not exists RoomAmenities(
AmenitiesID int not null auto_increment,
AmenitiesDescription varchar(45) not null,
primary key(AmenitiesID)
);

-- RoomReservation Table Composite Key Room Id and Reservation ID.
create table if not exists RoomReservation(
RoomID int not null,
ReservationID int not null,

primary key(RoomID,ReservationID)

);

-- RoomReservationAddOns Table  Primary Key RoomReservationAddOnID
create table if not exists RoomReservationAddOns(
RoomReservationAddOnID int not null auto_increment,
RoomID int not null,
ReservationID int not null,
AddOnID int not null,
primary key(RoomReservationAddOnID)
);

-- AddOns Table  Primary Key AddOnId
create table if not exists AddOns(
AddOnID int not null auto_increment,
AddOnDescription varchar(140),
primary key(AddOnID)
);

-- AddOnPriceTable  Table Primary Key AddOnPriceID.
create table if not exists AddOnPriceTable(
AddOnPriceID int not null auto_increment,
AddOnID int not null,
AddOnPriceStartDate date not null,
AddOnPriceEndDate date,
AddOnPrice decimal(10,2),

primary key(AddOnPriceID)
);

-- Reservastion Table Primary Key ReservationID
create table if not exists Reservation(
ReservationID int not null auto_increment,
ReservationBookingDate date not null,
ReservationStartDate date not null,
ReservationEndDate date,
CustomerID int not null ,
NumberOfGuest int not null ,

primary key(ReservationID)
);


-- ReservationPromoCode Table Composite Key ReservationID, PromoCode

create table if not exists ReservationPromoCode(
ReservationID int not null,
PromoCode int not null,

primary key(ReservationID,PromoCode)
);


-- PromoCode Table Primary Key PromoCode
create table if not exists PromoCode(
PromoCode int not null auto_increment,
PromocodeDescription varChar(70),
PromoCodeStartDate date not null,
PromoCodeEndDate date,
PromocodeType varChar(40) not null,
AmountOff Decimal(10,2)  not null,

primary key(PromoCode)
);

-- ReservationGuest Table Composite Key ReservationID and GuestID.
create table if not exists ReservationGuest(
ReservationID int not null ,
GuestID int not null ,
primary key(ReservationID,GuestID)
);


-- Guest Table Primary Key GuestID
create table if not exists Guest(
GuestID int not null auto_increment,
FirstName varchar(45),
LastName varchar(45),
Age int,

primary key(GuestID)
);

-- Billing Table PrimaryKey BillingID.
create table if not exists Billing(
BillingID int not null auto_increment,
ReservationID int not null ,
BillingDate date,
SubTotal decimal(10,2),
Tax decimal(10,2),
Total decimal(10,2),


primary key(BillingID)
);

-- BillingDetails Primary Key BillingDetailsID
create table if not exists BillingDetails(
BillingDetailsID int not null auto_increment,
BillingID int not null ,
Price decimal(10,2),
Description varchar(140),



primary key(BillingDetailsID)
);


-- Customer Table Primary Key CustomerID
create table if not exists Customer(
CustomerID int not null auto_increment,
FirstName varchar(45) not null,
LastName varchar(45) not null,
Phone varchar(10) not null,
Email varchar(100) not null,



primary key(CustomerID)
);


-- Add Foreign Keys constraints

Alter Table RoomRates
add constraint fk_RoomTypeID
foreign key (RoomTypeID)
references RoomType(RoomTypeID);

Alter Table Rooms
add constraint fk_Rooms_RoomTypeID
foreign key (RoomTypeID)
references RoomType(RoomTypeID);

Alter Table RoomRoomAmenities
add constraint fk_RoomAmenities_RoomID
foreign key (RoomID)
references Rooms(RoomID);

Alter Table RoomRoomAmenities
add constraint fk_RoomAmenities_AmenitiesID
foreign key (AmenitiesID)
references RoomAmenities(AmenitiesID);



Alter Table RoomReservation
add constraint fk_RoomReservation_RoomID
foreign key (RoomID)
references Rooms(RoomID);

Alter Table RoomReservation
add constraint fk_RoomReservation_ReservationID
foreign key (ReservationID)
references Reservation(ReservationID);


Alter Table RoomReservationAddOns
add constraint fk_RoomReservationAddOns_RoomID
foreign key (RoomID)
references RoomReservation(RoomID);

Alter Table RoomReservationAddOns
add constraint fk_RoomReservationAddOns_ReservationID
foreign key (ReservationID)
references RoomReservation(ReservationID);

Alter Table RoomReservationAddOns
add constraint fk_RoomReservationAddOns_AddOnID
foreign key (AddOnID)
references AddOns(AddOnID);


Alter Table AddOnPriceTable
add constraint fk_AddOnPriceTable_AddOnID
foreign key (AddOnID)
references AddOns(AddOnID);

Alter Table ReservationPromoCode
add constraint fk_ReservationPromoCode_ReservationID
foreign key (ReservationID)
references Reservation(ReservationID);

Alter Table ReservationPromoCode
add constraint fk_ReservationPromoCode_PromoCode
foreign key (PromoCode)
references PromoCode(PromoCode);


Alter Table ReservationGuest
add constraint fk_ReservationGuest_GuestID
foreign key (GuestID)
references Guest(GuestID);

Alter Table ReservationGuest
add constraint fk_ReservationGuest_ReservationID
foreign key (ReservationID)
references Reservation(ReservationID);

Alter Table Reservation
add constraint fk_Reservation_CustomerID
foreign key (CustomerID)
references Customer(CustomerID);

Alter Table Billing
add constraint fk_Billing_ReservationId
foreign key (ReservationID)
references Reservation(ReservationID);

Alter Table BillingDetails
add constraint fk_BillingDetails_BillingID
foreign key (BillingID)
references Billing(BillingID);


-- Add Data
insert into 
RoomType(RoomTypeDescription, OccupencyLimit)
values
('King',2),
('Queen',4),
('Suite', 6);

insert into 
RoomRates(RoomTypeID, Rate, RateDescription,RoomRatesStartDate, RoomRatesEndDate)
values
(1,120,'King Standard Rate', '2017/01/01',null),
(2,100,'Queen Standard Rate', '2017/01/01',null),
(1,200,'Suite Standard Rate', '2017/01/01',null),
(1,240,'King Conference Rate', '2017/03/01','2017/03/03'),
(1,200,'Queen Conference Rate', '2017/03/01','2017/03/03'),
(1,400,'Suite Conference Rate', '2017/03/01','2017/03/03');

insert into 
Rooms(RoomNumber, Floor, RoomTypeID)
values
(101,1, 1),
(102,1, 2),
(103,1, 3),
(104,1, 1),
(105,1, 2),
(201,2,3),
(202,2, 1),
(203,2, 2),
(204,2, 3),
(205,2, 1);

insert into 
RoomAmenities(AmenitiesDescription)
values
('Refridge'),
('Spa'),
('MiniBar'),
('Free In Room Wifi'),
('Helicopter');

insert into 
RoomRoomAmenities(RoomID, AmenitiesID)
values
(1,1),
(1,3),
(1,4),
(2,1),
(2,3),
(2,4),
(3,1),
(3,2),
(3,3),
(3,4),
(3,5),
(4,1),
(4,3),
(4,4),
(5,1),
(5,3),
(5,4),
(6,1),
(6,2),
(6,3),
(6,4),
(6,5),
(7,1),
(7,3),
(7,4),
(8,1),
(8,3),
(8,4),
(9,1),
(9,2),
(9,3),
(9,4),
(9,5),
(10,1),
(10,3),
(10,4);


Insert Into 
Customer(FirstName, LastName, Phone, eMail)
values
('Joe', 'Smith', '7815551234','Joe@Smith.com'),
('John', 'Doe', '7815558929','John@Doe.com'),
('Tim', 'Tom', '7815559012','Tim@Tom.com'),
('Sally', 'Jones', '7815559876','Sally@Jones.com'),
('Bob', 'Llamastein', '7815551111','Bob@Llamastein.com');



Insert Into
Reservation(ReservationBookingDate, ReservationStartDate, ReservationEndDate, CustomerID, NumberOfGuest)
values
('2017/01/04', '2017/02/01','2017/02/08',1,2),
('2017/02/14', '2017/03/01','2017/03/03',2,4),
('2017/03/01', '2017/03/29','2017/04/03',3,6),
('2017/03/01', '2017/03/15','2017/03/18',4,1),
('2017/03/20', '2017/03/20','2017/03/25',5,3);



insert into 
RoomReservation(RoomID, ReservationID)
values
(1,1),
(2,2),
(3,3),
(1,4),
(1,5),
(2,5);

insert into
AddOns(AddOnDescription)
values
('Room Charge'),
('Hotel Bar Charge'),
('Water Sports'),
('In Room Movie'),
('Transportation');


insert into
AddOnPriceTable(AddOnID, AddOnPriceStartDate, AddOnPriceEndDate, AddOnPrice)
values
(1, '2017/01/01',null, 50),
(2, '2017/01/01',null, 25),
(3, '2017/01/01',null, 150),
(4, '2017/01/01',null, 12.50),
(5, '2017/01/01',null, 75.50);


insert into
RoomReservationAddOns(RoomID,ReservationID, AddOnID)
values
(1,1,1),
(1,1,2),
(1,1,4),
(1,1,5),
(2,2,5),
(3,3,3),
(3,3,4),
(1,4,2),
(1,4,1),
(1,4,1),
(1,4,2),
(1,5,1),
(1,5,1),
(1,5,1),
(1,5,2),
(1,5,2),
(2,5,4);




insert into 
PromoCode(PromocodeDescription,PromoCodeStartDate, PromoCodeEndDate, PromocodeType, AmountOff)
values
('50 Dollars Off','2017/02/01','2017/02/15','MoneyOff', 50 ),
('20% Off','2017/03/01','2017/03/18','PercentOff', .20 );


insert into 
ReservationPromoCode(ReservationID, PromoCode)
values
(1,1),
(4,2);


insert into 
Guest(FirstName, LastName, Age)
values('John', 'Smith', 35),
('Brenda', 'Smith', 35),
('John', 'Doe', 55),
('Jill', 'Doe', 42),
('Jack', 'Doe', 15),
('Billy', 'Doe', 13),
('Tim', 'Tom', 44),
('Tom', 'Tom', 36),
('Tammy', 'Tom', 41),
('Tanner', 'Tom', 39),
('Tyler', 'Tom', 14),
('Tony', 'Tom', 11),
('Sally', 'Smith', 64),
('Bob', 'Llamastein', 66),
('Ryan', 'Llamastein', 64),
('Jim', 'Llamastein', 33);







insert into
ReservationGuest
(ReservationID, GuestID)
values
(1,1),
(1,2),
(2,3),
(2,4),
(2,5),
(2,6),
(3,7),
(3,8),
(3,9),
(3,10),
(3,11),
(3,12),
(4,13),
(5,14),
(5,15),
(5,16);


insert into
Billing(ReservationId, BillingDate, SubTotal, Tax, Total)
values
(1,'2017/02/08',953,47.65,1000.65),
(2,'2017/03/09',475.50,23.77,499.27),
(4,'2017/03/18',408,20.40,428.40),
(5,'2017/03/25',1312.50,65.65,1378.15),
(3,'2017/04/03',562.50,28.12,590.62);

insert into
BillingDetails(BillingId, Price, Description)
values
(1,840, '7 Nights Standard King'),
(1,50, 'Room Charge'),
(1,25, 'Bar Charge'),
(1,12.50,'In Room Movie'),
(1,75.50,'Transportation'),
(1,50.00,'50 Dollar Off Promo Code'),
(2,400,'2 Nights Conference Queen'),
(2,75.50,'Transportation'),
(3,360,'King Standard 3 Nights'),
(3,50,'Room Charge'),
(3,25,'Bar Charge'),
(3,25,'Bar Charge'),
(3,50,'Room Charge'),
(3,0.20,'20 Percent Off'),
(4,600,'5 NightsKing Room Standard'),
(4,500,'5 Nights Queen Room Standard'),
(4,50,'Room Charge Room1'),
(4,50,'Room Charge Room1'),
(4,50,'Room Charge Room1'),
(4,25,'Bar Charge Room1'),
(4,25,'Bar Charge Room1'),
(4,12.50,'Movie'),
(5,400,'Standard Suite'),
(5,12.50,'In Room Movie'),
(5,150,'Water Sports');





