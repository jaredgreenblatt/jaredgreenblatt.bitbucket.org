use HollywoodTowerHotel;
Select distinct RoomNumber
from Rooms ro
inner Join RoomRoomAmenities rra 
on ro.RoomId = rra.RoomID
where rra.AmenitiesID in(Select AmenitiesID from RoomAmenities as ra
where ra.AmenitiesDescription in('Spa' ,'Helicopter','Refridge','Minibar'))
group by RoomNumber
having count(RoomNumber) = 4;