/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.luckysevens;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class LuckSevens {

    public static void main(String[] args) {
        //declare Variables that are used for LuckySevens
        int betAmount;
        //declare and init the scanner
        Scanner sc = new Scanner(System.in);

        betAmount = betInput("Please Enter your Initial Bet!");
        System.out.println(betAmount);
        boolean x = validateInputs(betAmount);
        while (x == false) {
            System.out.println("Please Enter a valid ");
            betAmount = betInput("Please Enter your Initial Bet!");

            x = validateInputs(betAmount);

        }

        playGame(betAmount);

    }

    public static int betInput(String prompt) {
        //Declere a a  new Scanner
        Scanner sc = new Scanner(System.in);
        // Print Prompt
        System.out.println(prompt);
        // Read Value Into String
        String input = sc.nextLine();
        // Converts String to Int
        int intVal = Integer.parseInt(input);
        //returns the int
        return intVal;

    }

    public static void playGame(int bet) {
        int bigWin = bet;
        int originalBet = bet;
        int highRoll = 1;
        int dice1;
        int dice2;
        int diceSum;
        int rollCount = 0;

        Random dice = new Random();

        int currentBet = bet;
        while (currentBet >= 1) {
            dice1 = dice.nextInt(6) + 1;
            dice2 = dice.nextInt(6) + 1;
            diceSum = dice1 + dice2;

            if (diceSum == 7) {
                currentBet += 4;
                rollCount++;
                if (currentBet > bigWin) {
                    bigWin = currentBet;
                    highRoll = rollCount;

                }

            } else {
                currentBet -= 1;
                rollCount++;

            }

        }
        printResults(rollCount, highRoll, bigWin);
        playAgain(currentBet);

    }

    public static void printResults(int countRolls, int highRoll, int largeWin) {
        System.out.println("You are broke after " + countRolls + " rolls");
        System.out.println("You should have quit after " + highRoll + " when you had $" + largeWin);

    }

    public static void playAgain(int endAmount) {
        int betAmount;
        int playAgain;
        Scanner sc = new Scanner(System.in);

        System.out.println("Would You Like to Play Again? 1 for yes 2 for no");
        playAgain = sc.nextInt();

        if (playAgain == 1) {
            betAmount = betInput("Please Enter your Initial Bet!");
            playGame(betAmount);

        }else{
            System.out.println("Thanks for Playing!");
        }

    }

    public static boolean validateInputs(int bet) {
        // method to validate that the number is greater than 0
        boolean x;
        if (bet > 0) {
            x = true;

        } else {
            x = false;
        }

        return x;
    }

}
