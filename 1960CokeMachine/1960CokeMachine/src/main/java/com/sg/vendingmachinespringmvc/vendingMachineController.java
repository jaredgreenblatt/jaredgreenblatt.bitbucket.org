/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachinespringmvc;

import com.sg.vendingmachinespringmvc.model.Change;
import com.sg.vendingmachinespringmvc.model.VendingItem;
import com.sg.vendingmachinespringmvc.service.InsufficientFundsException;
import com.sg.vendingmachinespringmvc.service.NoItemInventoryException;
import com.sg.vendingmachinespringmvc.service.VendingMachineServiceLayer;
import java.math.BigDecimal;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class vendingMachineController {

    VendingMachineServiceLayer service;

    @Inject
    public vendingMachineController(VendingMachineServiceLayer service) {
        this.service = service;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String vendingMachine(Model model) {
        //loadVendingMachine();
        List<VendingItem> vendingItemList = service.getAllVendingItem();
        model.addAttribute("vendingItemList", vendingItemList);
        return "index";
    }

    @RequestMapping(value = "/PurchaseItem", method = RequestMethod.POST)
    public String makePurchase(HttpServletRequest request, Model model) {
        //String displayItemId = requestcurrentVendingItem.itemId
        String vendingIdInput = request.getParameter("itemChoice");
        String runningTotalInput = request.getParameter("runningTotal");
        if (vendingIdInput.isEmpty()) {

            String errorMessage = "You have made an invalid Selection!";
            model.addAttribute("errorMessage", errorMessage);
            List<VendingItem> vendingItemList = service.getAllVendingItem();
            model.addAttribute("vendingItemList", vendingItemList);
            return "index";

        } else {
            try {

                long vendingItemId = Long.parseLong(vendingIdInput);
                BigDecimal runningTotal = new BigDecimal(runningTotalInput);
                VendingItem purchaseVendingItem = service.getVendingItem(vendingItemId);
                service.setrunningTotal(runningTotal);
                service.PurchaseItem(purchaseVendingItem);
                Change purchaseChange = service.makeChange();
                List<VendingItem> vendingItemList = service.getAllVendingItem();
                String thankYou = "Thank You!!!";
                model.addAttribute("vendingItemList", vendingItemList);
                model.addAttribute("purchaseChange", purchaseChange);
                model.addAttribute("thankYou", thankYou);
                return "index";

            } catch (NoItemInventoryException | InsufficientFundsException e) {
                String errorMessage = e.getMessage();
                model.addAttribute("errorMessage", errorMessage);
                List<VendingItem> vendingItemList = service.getAllVendingItem();
                model.addAttribute("vendingItemList", vendingItemList);
                return "index";
            }
        }

    }

    @RequestMapping(value = "/AddVendingItem", method = RequestMethod.GET)
    public String addVendingItem() {
        return "AddVendingItem";
    }

}
