/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SimpleCalcTwo;

import com.sg.userio.UserIOClass;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class App {

    public static void main(String[] args) {
        String playAgain;
        UserIOClass uio = new UserIOClass();

        do {
            boolean anotherOperation = false;

            String operation;
            double operand1 = 0;
            double operand2 = 0;

            uio.print("Please enter in your operation.");

            operation = uio.readString("Addition enter A, Subtraction enter S, Multiplication enter M , Division enter D or Exit");

            switch (operation) {
                case "EXIT":
                    uio.print("Thank you!");
                    anotherOperation = false;
                    break;
                case "A":
                case "S":
                case "M":
                case "D":
                    
                    operand1 = uio.readDouble("Please Enter Operand 1");
                  
                    operand2 = uio.readDouble("Please Enter Operand 2");
                    anotherOperation = true;
                    break;

            }

            if (operation.equals("A")) {
                double result = SimpleCalculator.addition(operand1, operand2);
                System.out.println(SimpleCalculator.addition(operand1, operand2));
                

            } else if (operation.equals("S")) {
                System.out.println(SimpleCalculator.subtraction(operand1, operand2));

            } else if (operation.equals("M")) {
                System.out.println(SimpleCalculator.multiplication(operand1, operand2));

            } else if (operation.equals("D")) {
                System.out.println(SimpleCalculator.division(operand1, operand2));

            }
     
            playAgain = uio.readString("Do want to do any more calculations?");
            playAgain = playAgain.toUpperCase();

        } while (playAgain.equals("Y") || playAgain.equals("YES"));
        if (playAgain.equals("N") || playAgain.equals("NO")) {
            uio.print("Thanks for adding stuff.");

        }

    }

}
