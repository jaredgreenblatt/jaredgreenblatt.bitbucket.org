<%-- 
    Document   : results
    Created on : Mar 20, 2017, 8:53:22 PM
    Author     : apprentice
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Flooring Results Page</title>
    </head>
    <body>
        <h1>Flooring Calculator!</h1>

        <p>
            Total Time Taken<br/>
            <c:out value="${timeToInstall} hrs " />
        </p>
        <p>
            Cost Per Materials</br>
            <c:out value="$ ${materialCost}" />

        </p>
        
        <p>
            Cost Per For Labor</br>
            <c:out value="$ ${laborCost}" />

        </p>
        
        <p>
            Total Cost</br>
            <c:out value="$ ${totalCost}" />

        </p>
        
        <p>
        <a href="index.jsp">Calculate another floor?.</a>
    </p>

    </body>
</html>
