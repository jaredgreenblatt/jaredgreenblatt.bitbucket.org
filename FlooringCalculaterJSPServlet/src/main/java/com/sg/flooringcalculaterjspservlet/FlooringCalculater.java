/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringcalculaterjspservlet;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(name = "FlooringCalculater", urlPatterns = {"/FlooringCalculater"})
public class FlooringCalculater extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        BigDecimal laborCost = new BigDecimal(86.00);
        BigDecimal installPerHour = new BigDecimal(20.00);
        String inputWidth = request.getParameter("widthFlooringArea");
        String inputLength = request.getParameter("lengthFlooringArea");
        String inputMaterialCostPerSquareFoot = request.getParameter("costPer1SquareFootOfMaterial");
        BigDecimal width = new BigDecimal(inputWidth);
        BigDecimal length = new BigDecimal(inputLength);
        BigDecimal materialCostPerSquareFoot = new BigDecimal(inputMaterialCostPerSquareFoot);
        BigDecimal materialCost, area, timeToInstall,totalLaborCost,totalCost;
        area = length.multiply(width);
        materialCost = materialCostPerSquareFoot.multiply(area);
        materialCost = materialCost.setScale(2, RoundingMode.HALF_DOWN);

        // Area to cover divided 20 to calculate how long it takes to install
        timeToInstall =area.divide(installPerHour, 1, RoundingMode.HALF_UP);
        totalLaborCost=timeToInstall.multiply(laborCost);
        totalLaborCost =totalLaborCost.setScale(2, RoundingMode.HALF_DOWN);
        
        totalCost =materialCost.add(totalLaborCost);
        totalCost =totalCost.setScale(2, RoundingMode.HALF_DOWN);
        
        
        request.setAttribute("materialCost", materialCost);
         request.setAttribute("laborCost", totalLaborCost);
        request.setAttribute("timeToInstall", timeToInstall);
        request.setAttribute("totalCost", totalCost);

         RequestDispatcher rd = request.getRequestDispatcher("results.jsp");
        rd.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
