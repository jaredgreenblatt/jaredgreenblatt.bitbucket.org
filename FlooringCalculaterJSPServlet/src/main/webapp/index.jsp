<%-- 
    Document   : index
    Created on : Mar 20, 2017, 8:53:10 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Flooring Calculator</title>
    </head>
    <body>
        <h1>Flooring Calculator</h1>
        <p>
            Flooring Calculator used to calculate the cost of flooring for specified Area.
        </p>
        <p>
            The team can install flooring at a rate of 20 sq feet per hour. 

        </p>
        <p>
            The company bills every 15 minutes at a rate of 86 dollars an hour.
        </p>

        <form method="post" action="FlooringCalculater">
            <p> Width Of Area to Floor</p>
            <input type="text" name ="widthFlooringArea"/><br/>
             <p> Length Of Area to Floor</p>
            <input type="text" name ="lengthFlooringArea"/><br/>
             <p> Cost Per 1 Sq Foot Of Material.</p>
            <input type="text" name ="costPer1SquareFootOfMaterial"/><br/>

            <br/>
            <input type ="submit" value ="Calculate Flooring Cost"/>
        </form> 

    </body>
</html>
