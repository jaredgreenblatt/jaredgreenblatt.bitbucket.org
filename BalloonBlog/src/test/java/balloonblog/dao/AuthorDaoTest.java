/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balloonblog.dao;

import balloonblog.dto.Author;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class AuthorDaoTest {
    AuthorDao dao;
    
    public AuthorDaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        ApplicationContext ctx
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = ctx.getBean("AuthorDao", AuthorDao.class);
        JdbcTemplate cleaner = ctx.getBean(JdbcTemplate.class);
        cleaner.execute("delete from  Blog_Tag where  1= 1");
        cleaner.execute("delete from  Comment where 1 = 1");
        cleaner.execute("delete from BlogAuthor where 1 = 1 ");
        cleaner.execute("delete from  Blog where  1= 1");
        cleaner.execute("delete from  GenericPage where 1=1");
        cleaner.execute("delete from  Category where 1 = 1");
        cleaner.execute("delete from Tag where 1 = 1");
        cleaner.execute("delete from  authorities where 1=1");
        cleaner.execute("delete from  users   where 1 = 1");

    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addAuthor method, of class AuthorDao.
     */
    @Test
    public void testConnection() {
//        dao.getAllAuthors();
    }

    @Test
    public void testAddGetAuthor() {
        Author a = new Author();
        a.setDisplayName("Glendower");
        a.setFirstName("Henry");
        a.setLastName("Thefourth");
        dao.addAuthor(a);
        
        Author fromDao = dao.getAuthorById(a.getAuthorId());
        assertEquals(a, fromDao);

    }
    
    @Test
    public void testAddGetDeleteAuthor() {
        Author a = new Author();
        a.setDisplayName("Glendower");
        a.setFirstName("Henry");
        a.setLastName("Thefourth");
        dao.addAuthor(a);
        
        Author fromDao = dao.getAuthorById(a.getAuthorId());
        assertEquals(a, fromDao);
        
        dao.removeAuthor(a.getAuthorId());
        Author fromDao2 = dao.getAuthorById(a.getAuthorId());
        assertEquals(fromDao2,null);
        assertNull(dao.getAuthorById(a.getAuthorId()));
        
    }

    @Test
    public void testGetAllAuthors() {
        Author d = new Author();
        d.setDisplayName("Glendower");
        d.setFirstName("Henry");
        d.setLastName("Thefourth");
        dao.addAuthor(d);
        
        Author e = new Author();
        e.setDisplayName("Harry");
        e.setFirstName("Harold");
        e.setLastName("Houdini");
        dao.addAuthor(e);        
        
        assertEquals(2, dao.getAllAuthors().size());
        assertEquals(dao.getAllAuthors().contains(d), true);
        assertEquals(dao.getAllAuthors().contains(e), true);
    }
    
    @Test
    public void testUpdateAuthor() {
        Author a = new Author();
        a.setDisplayName("Glendower");
        a.setFirstName("Henry");
        a.setLastName("Thefourth");
        dao.addAuthor(a);
        
        Author fromDao = dao.getAuthorById(a.getAuthorId());

        assertEquals(a, fromDao);

        a.setDisplayName("Jon");
        a.setFirstName("Jonathon");
        a.setLastName("Snow");
        
        dao.updateAuthor(a);

        Author fromDaoAfterEdit = dao.getAuthorById(a.getAuthorId());

        assertFalse(fromDao == fromDaoAfterEdit);
        assertEquals(fromDao.getAuthorId(), fromDaoAfterEdit.getAuthorId());

    }
    
    
}
