/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balloonblog.dao;

import balloonblog.dto.Category;
import balloonblog.dto.GenericPage;
import java.time.LocalDate;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class GenericPageDaoTest {

    GenericPageDao gpDao;
    CategoryDao catDao;

    public GenericPageDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        gpDao = ctx.getBean("GenericPageDao", GenericPageDao.class);

        ApplicationContext ctxCatDao
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        catDao = ctx.getBean("CategoryDao", CategoryDao.class);

        JdbcTemplate cleaner = ctx.getBean(JdbcTemplate.class);
        cleaner.execute("delete from  Blog_Tag where  1= 1");
        cleaner.execute("delete from  Comment where 1 = 1");
        cleaner.execute("delete from BlogAuthor where 1 = 1 ");
        cleaner.execute("delete from  Blog where  1= 1");
        cleaner.execute("delete from  GenericPage where 1=1");
        cleaner.execute("delete from  Category where 1 = 1");
        cleaner.execute("delete from Tag where 1 = 1");
        cleaner.execute("delete from  authorities where 1=1");
        cleaner.execute("delete from  users   where 1 = 1");

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addGenericPage method, of class GenericPageDao.
     */
    @Test
    public void testAddGetGenericPage() {
        Category c = new Category();
        c.setCategory("Test");
        catDao.addCategory(c);

        GenericPage gp = new GenericPage();
        gp.setTitle("Test Title");
        gp.setPageText("Test Page Text");
        gp.setEnabled(true);
        gp.setStartDate(LocalDate.now());
        gp.setEndDate(LocalDate.now());
        gp.setCategory(c);

        gpDao.addGenericPage(gp);

        GenericPage fromDb = gpDao.getGenericPageById(gp.getPageId());

        assertEquals(gp, fromDb);

    }

    @Test
    public void testAddGetRemoveGenericPage() {
        Category c = new Category();
        c.setCategory("Test");
        catDao.addCategory(c);

        GenericPage gp = new GenericPage();
        gp.setTitle("Test Title");
        gp.setPageText("Test Page Text");
        gp.setEnabled(true);
        gp.setStartDate(LocalDate.now());
        gp.setEndDate(LocalDate.now());
        gp.setCategory(c);

        gpDao.addGenericPage(gp);

        GenericPage fromDb = gpDao.getGenericPageById(gp.getPageId());

        assertEquals(gp, fromDb);
        gpDao.removeGenericPage(gp.getPageId());
        assertNull(gpDao.getGenericPageById(gp.getPageId()));

    }

    @Test
    public void testGetAll() {
        Category c = new Category();
        c.setCategory("Test");
        catDao.addCategory(c);

        GenericPage gp = new GenericPage();
        gp.setTitle("Test Title");
        gp.setPageText("Test Page Text");
        gp.setEnabled(true);
        gp.setStartDate(LocalDate.now());
        gp.setEndDate(LocalDate.now());
        gp.setCategory(c);

        gpDao.addGenericPage(gp);

        Category d = new Category();
        d.setCategory("Test");
        catDao.addCategory(d);
        GenericPage gp2 = new GenericPage();
        gp2.setTitle("Test Title");
        gp2.setPageText("Test Page Text");
        gp2.setEnabled(true);
        gp2.setStartDate(LocalDate.now());
        gp2.setEndDate(LocalDate.now());
        gp2.setCategory(d);

        gpDao.addGenericPage(gp2);

        assertEquals(gpDao.getAllGenericPages().size(), 2);
        assertEquals(gpDao.getAllGenericPages().contains(gp2), true);
            assertEquals(gpDao.getAllGenericPages().contains(gp), true);
        
        

    }
    @Test
    public void updateGenericPage(){
               Category c = new Category();
        c.setCategory("Test");
        catDao.addCategory(c);

        GenericPage gp = new GenericPage();
        gp.setTitle("Test Title");
        gp.setPageText("Test Page Text");
        gp.setEnabled(true);
        gp.setStartDate(LocalDate.now());
        gp.setEndDate(LocalDate.now());
        gp.setCategory(c);

        gpDao.addGenericPage(gp);
        
       
        
         GenericPage fromDbBefore = gpDao.getGenericPageById(gp.getPageId());
         
         gp.setPageText("Change Page Text");
         gpDao.updateGenericPage(gp);
         
         GenericPage fromDbAfter = gpDao.getGenericPageById(gp.getPageId());
         
         assertFalse(fromDbBefore==fromDbAfter);
         
         assertEquals(fromDbAfter.getPageId(), fromDbBefore.getPageId());
         
        
    }
}
