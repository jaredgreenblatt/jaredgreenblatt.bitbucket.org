/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balloonblog.dao;

import balloonblog.dto.Author;
import balloonblog.dto.Blog;
import balloonblog.dto.Category;
import balloonblog.dto.Comment;
import balloonblog.dto.Tag;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class BlogDaoTest {

    BlogDao blogDao;
    CommentDao commentDao;
    AuthorDao authorDao;
    TagDao tagDao;
    CategoryDao catDao;

    public BlogDaoTest() {

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctxBlog
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        blogDao = ctxBlog.getBean("BlogDao", BlogDao.class);

        commentDao = ctxBlog.getBean("CommentDao", CommentDao.class);

        catDao = ctxBlog.getBean("CategoryDao", CategoryDao.class);

        authorDao = ctxBlog.getBean("AuthorDao", AuthorDao.class);

        tagDao = ctxBlog.getBean("TagDao", TagDao.class);

        JdbcTemplate cleaner = ctxBlog.getBean(JdbcTemplate.class);
        cleaner.execute("delete from  Blog_Tag where  1= 1");
        cleaner.execute("delete from  Comment where 1 = 1");
        cleaner.execute("delete from BlogAuthor where 1 = 1 ");
        cleaner.execute("delete from  Blog where  1= 1");
        cleaner.execute("delete from  GenericPage where 1=1");
        cleaner.execute("delete from  Category where 1 = 1");
        cleaner.execute("delete from Tag where 1 = 1");
        cleaner.execute("delete from  authorities where 1=1");
        cleaner.execute("delete from  users   where 1 = 1");

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addBlog method, of class BlogDao.
     */
    @Test
    public void addBlog() {
        Author a = new Author();
        a.setDisplayName("Glendower");
        a.setFirstName("Henry");
        a.setLastName("Thefourth");
        authorDao.addAuthor(a);

        Category c = new Category();
        c.setCategory("Test");
        catDao.addCategory(c);

        Tag t = new Tag();
        t.setTag("new tag 2");
        tagDao.addTag(t);

        Tag s = new Tag();
        s.setTag("new tag 3");
        tagDao.addTag(s);

        Blog b = new Blog();
        b.setArticle("Test Article");
        b.setTitle("TITLE");
        b.setEnabled(true);
        b.setFeatured(true);
        b.setStartDate(LocalDate.now());
        b.setEndDate(LocalDate.now());
        b.setAuthor(a);
        b.setCategory(c);

        List<Comment> commentList = new ArrayList<>();

        b.setCommentList(commentList);
        List<Tag> tagList = new ArrayList<>();
        tagList.add(t);
        tagList.add(s);
        b.setTagList(tagList);

        blogDao.addBlog(b);

        Blog fromDb = blogDao.getBlogById(b.getBlogId());

        assertEquals(b, fromDb);

    }

    @Test
    public void addDeleteBlog() {
        Author a = new Author();
        a.setDisplayName("Glendower");
        a.setFirstName("Henry");
        a.setLastName("Thefourth");
        authorDao.addAuthor(a);

        Category c = new Category();
        c.setCategory("Test");
        catDao.addCategory(c);

        Tag t = new Tag();
        t.setTag("new tag 2");
        tagDao.addTag(t);

        Tag s = new Tag();
        s.setTag("new tag 3");
        tagDao.addTag(s);

        Blog b = new Blog();
        b.setArticle("Test Article");
        b.setTitle("TITLE");
        b.setEnabled(true);
        b.setFeatured(true);
        b.setStartDate(LocalDate.now());
        b.setEndDate(LocalDate.now());
        b.setAuthor(a);
        b.setCategory(c);

        List<Comment> commentList = new ArrayList<>();

        b.setCommentList(commentList);
        List<Tag> tagList = new ArrayList<>();
        tagList.add(t);
        tagList.add(s);
        b.setTagList(tagList);

        blogDao.addBlog(b);

        Blog fromDb = blogDao.getBlogById(b.getBlogId());

        assertEquals(b, fromDb);

        blogDao.removeBlog(b.getBlogId());
        assertEquals(blogDao.getBlogById(b.getBlogId()), null);

    }

    @Test
    public void addGetAllBlogs() {
        Author a = new Author();
        a.setDisplayName("Glendower");
        a.setFirstName("Henry");
        a.setLastName("Thefourth");
        authorDao.addAuthor(a);

        Category c = new Category();
        c.setCategory("Test");
        catDao.addCategory(c);

        Tag t = new Tag();
        t.setTag("new tag 2");
        tagDao.addTag(t);

        Tag s = new Tag();
        s.setTag("new tag 3");
        tagDao.addTag(s);

        Blog b = new Blog();
        b.setArticle("Test Article");
        b.setTitle("TITLE");
        b.setEnabled(true);
        b.setFeatured(true);
        b.setStartDate(LocalDate.now());
        b.setEndDate(LocalDate.now());
        b.setAuthor(a);
        b.setCategory(c);

        List<Comment> commentList = new ArrayList<>();

        b.setCommentList(commentList);
        List<Tag> tagList = new ArrayList<>();
        tagList.add(t);
        tagList.add(s);
        b.setTagList(tagList);

        blogDao.addBlog(b);

        Blog fromDb = blogDao.getBlogById(b.getBlogId());

        assertEquals(b, fromDb);

        assertEquals(blogDao.getAllBlogs().size(), 1);
        assertTrue(blogDao.getAllBlogs().contains(b));

    }

    @Test
    public void testBlogAddCommentWithUpdate() {
        Author a = new Author();
        a.setDisplayName("Glendower");
        a.setFirstName("Henry");
        a.setLastName("Thefourth");
        authorDao.addAuthor(a);

        Category c = new Category();
        c.setCategory("Test");
        catDao.addCategory(c);

        Tag t = new Tag();
        t.setTag("new tag 2");
        tagDao.addTag(t);

        Tag s = new Tag();
        s.setTag("new tag 3");
        tagDao.addTag(s);

        Blog b = new Blog();
        b.setArticle("Test Article");
        b.setTitle("TITLE");
        b.setEnabled(true);
        b.setFeatured(true);
        b.setStartDate(LocalDate.now());
        b.setEndDate(LocalDate.now());
        b.setAuthor(a);
        b.setCategory(c);

        List<Comment> commentList = new ArrayList<>();

        b.setCommentList(commentList);
        List<Tag> tagList = new ArrayList<>();
        tagList.add(s);
        tagList.add(t);
        b.setTagList(tagList);

        blogDao.addBlog(b);

        Blog addCommentsToBlog = blogDao.getBlogById(b.getBlogId());

        Comment com1 = new Comment();
        int blogId = addCommentsToBlog.getBlogId();
        com1.setBlogId(blogId);
        com1.setComment("this is the best blog ever");
        
        LocalDate currentDate; 
        currentDate = LocalDate.now();
        com1.setDisplyName("Vin Diesel");
        com1.setCommentDate(currentDate);
        commentDao.addComment(com1, blogId, currentDate);
        
        Comment com2 = new Comment();

        com2.setBlogId(blogId);
        com2.setComment("this is the worst blog ever");
        com2.setDisplyName("Debby Downer");
        com2.setCommentDate(currentDate);
        commentDao.addComment(com2, blogId, currentDate);

        List<Comment> commentListAddComments = new ArrayList<>();
        commentListAddComments.add(com1);
        commentListAddComments.add(com2);

        addCommentsToBlog.setCommentList(commentListAddComments);
        blogDao.updateBlog(addCommentsToBlog);

        Blog fromDb = blogDao.getBlogById(addCommentsToBlog.getBlogId());

        assertEquals(addCommentsToBlog, fromDb);

    }

    @Test
    public void testBlogUpdate() {
        Author a = new Author();
        a.setDisplayName("Glendower");
        a.setFirstName("Henry");
        a.setLastName("Thefourth");
        authorDao.addAuthor(a);

        Category c = new Category();
        c.setCategory("Test");
        catDao.addCategory(c);

        Tag t = new Tag();
        t.setTag("new tag 2");
        tagDao.addTag(t);

        Tag s = new Tag();
        s.setTag("new tag 3");
        tagDao.addTag(s);

        Blog b = new Blog();
        b.setArticle("Test Article");
        b.setTitle("TITLE");
        b.setEnabled(true);
        b.setFeatured(true);
        b.setStartDate(LocalDate.now());
        b.setEndDate(LocalDate.now());
        b.setAuthor(a);
        b.setCategory(c);

        List<Comment> commentList = new ArrayList<>();

        b.setCommentList(commentList);
        List<Tag> tagList = new ArrayList<>();
        tagList.add(s);
        tagList.add(t);
        b.setTagList(tagList);

        blogDao.addBlog(b);

        Blog addCommentsToBlog = blogDao.getBlogById(b.getBlogId());

        LocalDate currentDate; 
        currentDate = LocalDate.now();
        String commentDate = currentDate.format(DateTimeFormatter.ISO_DATE);        
        
        Comment com1 = new Comment();
        int blogId = addCommentsToBlog.getBlogId();
        com1.setBlogId(blogId);
        com1.setComment("this is the best blog ever");
        com1.setDisplyName("Vin Diesel");
        commentDao.addComment(com1, blogId, currentDate);

        Comment com2 = new Comment();

        com2.setBlogId(blogId);
        com2.setComment("this is the worst blog ever");
        com2.setCommentDate(LocalDate.of(2013, 10, 13));
        com2.setDisplyName("Debby Downer");
        commentDao.addComment(com2, blogId, currentDate);

        List<Comment> commentListAddComments = new ArrayList<>();
        commentListAddComments.add(com1);
        commentListAddComments.add(com2);

        addCommentsToBlog.setCommentList(commentListAddComments);
        blogDao.updateBlog(addCommentsToBlog);

        Blog fromDbBefore = blogDao.getBlogById(addCommentsToBlog.getBlogId());

        fromDbBefore.setArticle("UPDATE");
        blogDao.updateBlog(fromDbBefore);

        Blog fromDbAfter = blogDao.getBlogById(fromDbBefore.getBlogId());

        assertFalse(fromDbAfter == fromDbBefore);

        assertEquals(fromDbAfter.getBlogId(), fromDbBefore.getBlogId());

    }

}
