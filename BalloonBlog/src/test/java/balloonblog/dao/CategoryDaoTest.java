/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balloonblog.dao;

import balloonblog.dto.Category;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class CategoryDaoTest {

    CategoryDao dao;

    public CategoryDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

        ApplicationContext ctx
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = ctx.getBean("CategoryDao", CategoryDao.class);
        JdbcTemplate cleaner = ctx.getBean(JdbcTemplate.class);
        cleaner.execute("delete from  Blog_Tag where  1= 1");
        cleaner.execute("delete from  Comment where 1 = 1");
        cleaner.execute("delete from BlogAuthor where 1 = 1 ");
        cleaner.execute("delete from  Blog where  1= 1");
        cleaner.execute("delete from  GenericPage where 1=1");
        cleaner.execute("delete from  Category where 1 = 1");
        cleaner.execute("delete from Tag where 1 = 1");
        cleaner.execute("delete from  authorities where 1=1");
        cleaner.execute("delete from  users   where 1 = 1");

    }

    @After
    public void tearDown() {
    }

    @Test
    public void testAddGetCategory() {
        Category c = new Category();
        c.setCategory("Test");
        dao.addCategory(c);

        Category fromDb = dao.getCategoryById(c.getCategoryId());
        assertEquals(c, fromDb);

    }

    @Test
    public void testAddGetDeleteCategory() {
        Category c = new Category();
        c.setCategory("Test");
        dao.addCategory(c);

        Category fromDb = dao.getCategoryById(c.getCategoryId());
        assertEquals(c, fromDb);

        try {
            dao.removeCategory(c.getCategoryId());
            Category fromDb1 = dao.getCategoryById(c.getCategoryId());
            assertEquals(fromDb1, null);
        } catch (CategoryDeleteException ex) {
            Logger.getLogger(CategoryDaoTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        assertNull(dao.getCategoryById(c.getCategoryId()));

    }

    @Test
    public void testGetAllCategories() {
        Category c = new Category();
        c.setCategory("Test C");
        dao.addCategory(c);
        Category d = new Category();

        d.setCategory("Test D");
        dao.addCategory(d);

        assertEquals(2, dao.getAllCategory().size());
        assertEquals(dao.getAllCategory().contains(c), true);
        assertEquals(dao.getAllCategory().contains(d), true);

    }

    @Test
    public void testUpdateCategory() {
        Category c = new Category();
        c.setCategory("Test C");
        dao.addCategory(c);
        Category fromDb = dao.getCategoryById(c.getCategoryId());
        assertEquals(c, fromDb);
        
        c.setCategory("Edited Test C");
        dao.updateCategory(c);
        
        Category fromDbAfterEdit = dao.getCategoryById(c.getCategoryId());
        
        assertFalse(fromDb==fromDbAfterEdit);
        assertEquals(fromDb.getCategoryId(), fromDbAfterEdit.getCategoryId());
        

    }

}
