/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balloonblog.dao;

import balloonblog.dto.Comment;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class CommentDaoTest {
    CommentDao dao;
    
    public CommentDaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
           ApplicationContext ctx
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = ctx.getBean("CommentDao", CommentDao.class);
        JdbcTemplate cleaner = ctx.getBean(JdbcTemplate.class);
        cleaner.execute("delete from  Blog_Tag where  1= 1");
        cleaner.execute("delete from  Comment where 1 = 1");
        cleaner.execute("delete from BlogAuthor where 1 = 1 ");
        cleaner.execute("delete from  Blog where  1= 1");
        cleaner.execute("delete from  GenericPage where 1=1");
        cleaner.execute("delete from  Category where 1 = 1");
        cleaner.execute("delete from Tag where 1 = 1");
        cleaner.execute("delete from  authorities where 1=1");
        cleaner.execute("delete from  users   where 1 = 1");
        
       
        
        
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addComment method, of class CommentDao.
     */
    @Test
    public void testConnection() {
//        dao.getAllComment();
    }

    @Test
    public void testAddGetComment() {

        LocalDate currentDate; 
        currentDate = LocalDate.now();
        String commentDate = currentDate.format(DateTimeFormatter.ISO_DATE);          
        
        Comment c = new Comment();
        int blogId = 1;
        c.setBlogId(blogId);
        c.setComment("this is the best blog ever");
        c.setDisplyName("Vin Diesel");
        c.setCommentDate(currentDate);
        dao.addComment(c, blogId, currentDate);
        
        Comment fromDao = dao.getCommentById(c.getCommentId());
        assertEquals(c, fromDao);

    }
    
    @Test
    public void testAddGetDeleteComment() {
        

        LocalDate currentDate; 
        currentDate = LocalDate.now();
        String commentDate = currentDate.format(DateTimeFormatter.ISO_DATE);          
                
        Comment c = new Comment();
        int blogId = 1;
        c.setBlogId(blogId);
        c.setComment("this is the best blog ever");
        c.setDisplyName("Vin Diesel");
        c.setCommentDate(currentDate);
        dao.addComment(c,1, currentDate);
        
        Comment fromDao = dao.getCommentById(c.getCommentId());
        assertEquals("Equals Test",c, fromDao);
        
        dao.removeComment(c.getCommentId());
        Comment fromDao2 = dao.getCommentById(c.getCommentId());
        
        assertEquals("Equals Test 2",fromDao2,null);
        assertNull("Null Test",dao.getCommentById(c.getCommentId()));
        
    }    
    
    @Test
    public void testGetAllComments() {
        

        LocalDate currentDate; 
        currentDate = LocalDate.now();
        String commentDate = currentDate.format(DateTimeFormatter.ISO_DATE);          
                
        Comment c = new Comment();
        int blogId = 1;
        c.setBlogId(blogId);
        c.setComment("this is the best blog ever");
        c.setDisplyName("Vin Diesel");
        c.setCommentDate(currentDate);
        dao.addComment(c,1, currentDate);
        
        Comment d = new Comment();
        int secondBlogId = 2;
        d.setBlogId(secondBlogId);
        d.setComment("this is the worst blog ever");
        d.setDisplyName("Debby Downer");
        d.setCommentDate(currentDate);
        
        dao.addComment(d,secondBlogId,currentDate);
        
        assertEquals("2", 2, dao.getAllComments().size());
        assertEquals("c",dao.getAllComments().contains(c), true);
        assertEquals("d",dao.getAllComments().contains(d), true);
    }    
    
    @Test
    public void testUpdateComment() {

        LocalDate currentDate; 
        currentDate = LocalDate.now();
        String commentDate = currentDate.format(DateTimeFormatter.ISO_DATE);          
                
        
        Comment c = new Comment();
        int blogId = 1;
        c.setBlogId(blogId);
        c.setComment("this is the best blog ever");
        c.setCommentDate(LocalDate.of(2012, 10, 13));
        c.setDisplyName("Vin Diesel");
        c.setCommentDate(currentDate);
        dao.addComment(c,1,currentDate);
        
        Comment fromDao = dao.getCommentById(c.getCommentId());
        
        assertEquals(c, fromDao);
        
        c.setComment("this is the worst blog ever");
        c.setCommentDate(LocalDate.of(2013, Month.MARCH, 1));
        c.setDisplyName("Debby Downer");
        c.setCommentDate(currentDate);
        
        dao.updateComment(c);
        
        Comment fromDaoAfterEdit = dao.getCommentById(c.getCommentId());
        
        assertFalse(fromDao == fromDaoAfterEdit);
        assertEquals(fromDao.getCommentId(), fromDaoAfterEdit.getCommentId());
        
       
    }    
    
}
