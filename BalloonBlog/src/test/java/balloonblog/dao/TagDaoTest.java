/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balloonblog.dao;

import balloonblog.dto.Tag;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class TagDaoTest {

    TagDao dao;

    public TagDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = ctx.getBean("TagDao", TagDao.class);
        JdbcTemplate cleaner = ctx.getBean(JdbcTemplate.class);
        cleaner.execute("delete from  Blog_Tag where  1= 1");
        cleaner.execute("delete from  Comment where 1 = 1");
        cleaner.execute("delete from BlogAuthor where 1 = 1 ");
        cleaner.execute("delete from  Blog where  1= 1");
        cleaner.execute("delete from  GenericPage where 1=1");
        cleaner.execute("delete from  Category where 1 = 1");
        cleaner.execute("delete from Tag where 1 = 1");
        cleaner.execute("delete from  authorities where 1=1");
        cleaner.execute("delete from  users   where 1 = 1");

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addTag method, of class TagDao.
     */
    @Test
    public void testConnection() {
//        dao.getAllTags();
    }

    @Test
    public void testAddGetTag() {
        Tag t = new Tag();
        t.setTag("new tag");
        dao.addTag(t);

        Tag fromDb = dao.getTagById(t.getTagId());
        assertEquals(t, fromDb);

    }

    @Test
    public void testAddGetDeleteCategory() {
        Tag t = new Tag();
        t.setTag("new tag");
        dao.addTag(t);

        Tag fromDb = dao.getTagById(t.getTagId());
        assertEquals(t, fromDb);

        try {
            dao.removeTag(t.getTagId());
            Tag fromDb1 = dao.getTagById(t.getTagId());
            assertEquals(fromDb1, null);
        } catch (TagDeleteException ex) {
            Logger.getLogger(CategoryDaoTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        assertNull(dao.getTagById(t.getTagId()));

    }

    @Test
    public void testGetAllTags() {
        Tag t = new Tag();
        t.setTag("new tag 2");
        dao.addTag(t);
        
        Tag s = new Tag();
        s.setTag("new tag 3");
        dao.addTag(s);

        assertEquals(2, dao.getAllTags().size());
        assertEquals(dao.getAllTags().contains(t), true);
        assertEquals(dao.getAllTags().contains(s), true);
    }

    @Test
    public void testUpdateTag() {
        Tag t = new Tag();
        t.setTag("new tag");
        dao.addTag(t);

        Tag fromDb = dao.getTagById(t.getTagId());

        assertEquals(t, fromDb);

        t.setTag("edited new tag");
        dao.updateTag(t);

        Tag fromDbAfterEdit = dao.getTagById(t.getTagId());

        assertFalse(fromDb == fromDbAfterEdit);
        assertEquals(fromDb.getTagId(), fromDbAfterEdit.getTagId());

    }

}
