






<div class="container">
    <div class="col-xs-offset-2 col-xs-8" style="text-align: center" >
        <h3 class = "text-center">Add Category</h3>
        <form class ="form-horizontal"
              role ="form"  role="form" id="add-category-form">
            <div class="form-group">
                <label for="add-category" class="col-md-4 control-label">
                    Category:
                </label>
                <div class ="col-md-8">
                    <input type="text" class ="form-control" id="categoryNameSnip" placeholder="Enter Category" required />
                    <br>
                    <input class="col-xs-6" type="button" class="btn btn-default" value="Add Category" onclick="addCategorySnip()"/>
                </div>
            </div>
            <br><br>
            <sec:authorize access="hasRole('ROLE_ADMIN')">
                <h3 class = "text-center">Remove Category</h3>
                <div class="form-group">
                    <label for="add-category" class="col-xs-4 control-label">Select Category:</label>
                    <div class="col-md-8">
                        <select class="form-control" name="categorySnip"  placeholder="category" id="categorySnip">
                        </select>
                        <br>
                        <input class="col-xs-6"  type="button" class="btn btn-default" value="Remove Category" onclick="removeCategorySnip()"/>
                        </sec:authorize>
                    </div>
                </div>
        </form>

    </div>
</div>


