<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Blogs to Approve</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/HomeBalloons.css" rel="stylesheet"> 
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body style="background-color: #e6ffff">
        <div class="row" id="navbar">
            <div class="col-xs-11">        
                <img class="float" src="http://www.clker.com/cliparts/2/Y/t/a/C/o/hot-air-balloon-outline.svg" alt="" />
                <img class=float1 src="http://www.clker.com/cliparts/2/Y/t/a/C/o/hot-air-balloon-outline.svg" alt="" />
            </div>
            <div class="col-xs-1">  
                <%@include file="sideMenu.jsp" %>
            </div>
        </div>

        <div class="col-xs-offset-2 col-xs-8" style="text-align: center" >
            <h1>Blogs Awaiting Approval</h1>

            <hr/>

            <div class="col-xs-offset-3 col-xs-6">

                <c:forEach var="currentBlog" items="${blogList}">
                    <div style="text-align: center" id="generic">
                        <a href="displayBlog?blogId=${currentBlog.blogId}">
                            <h1><c:out value="${currentBlog.title}"/></h1>
                            <br><br>
                        </a>

                        ${currentBlog.article}<br><br>

                        <i class="glyphicon glyphicon-user"></i> <c:out value="${currentBlog.author.displayName}"/>
                        <i class="glyphicon glyphicon-folder-close"></i> <c:out value="${currentBlog.category.category}" />
                        <i class="glyphicon glyphicon-time"></i> <c:out value="${currentBlog.startDate}" />
                        <input type="hidden" name="blogId" value="${blog.blogId}"/><br>


                        <c:forEach var="currentTag" items="${currentBlog.tagList}">
                            <i class="glyphicon glyphicon-tag"></i><c:out value="${currentTag.tag}" />
                        </c:forEach> 
                        <br><br>
                        <a href="displayEditBlogForm?blogId=${currentBlog.blogId}">Edit</a> | 
                        <a href="deleteBlogApproval?blogId=${currentBlog.blogId}">Delete</a>
                        <br>
                        <br>
                        <br>
                        <form class="form-horizontal" 
                              role="form" method="POST" 
                              action="approveBlog"
                              id="approveBlog"
                              style="text-align:center">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-success" id="approve-blog-button" value="Approve Blog"/>
                                </div>
                            </div>
                            <input type="hidden" name="blogId" value="${currentBlog.blogId}"/>
                        </form>


                    </div>
                </c:forEach> 

            </div>

        </div>


        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/sidemenu.js"></script>


    </body>
</html>

