<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Create a Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/HomeBalloons.css" rel="stylesheet"> 
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png"> 

    </head>
    <body style="background-color: #e6ffff">
        <div class="row" id="navbar">
            <div class="col-xs-11">        
                <img class="float" src="http://www.clker.com/cliparts/2/Y/t/a/C/o/hot-air-balloon-outline.svg" alt="" />
                <img class=float1 src="http://www.clker.com/cliparts/2/Y/t/a/C/o/hot-air-balloon-outline.svg" alt="" />
            </div>
            <div class="col-xs-1">  
                <%@include file="sideMenu.jsp" %>
            </div>

        </div>

        <div class="col-xs-offset-2 col-xs-8" style="text-align: center" >
            <h1>Create a Page</h1>
            <hr/>
        </div>

        <!--            Page form goes here.-->
        <div class="col-xs-offset-2 col-xs-8" style="text-align: center" >
            <div id="pageCreate">
                <form class="form-horizontal" 
                      role="form" method="POST" 
                      action="createPage"
                      id="createPage">
                    <div class="form-group">
                        <label for="add-title" class="col-md-4 control-label">Page Title:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="title" id="title" placeholder="Title" required/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="add-pageText" class="col-md-4 control-label"></label>
                        <div class="col-md-8">
                            <textarea  name="pageText"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="add-category" class="col-md-4 control-label">Category:</label>
                        <div class="col-md-8">
                            <select class="form-control" name="category" id="category" placeholder="Select Category" required>
                                <option value="" disabled selected>--Select Category--</option>
                                <c:forEach var="currentCategory" items="${categoryList}">
                                    <option value="${currentCategory.categoryId}">
                                        <c:out value="${currentCategory.category}"/>
                                    </option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>



                    <div class="form-group">
                        <div class="col-xs-10">
                            <a onclick=showCatSnip()>Click here to add a category</a>


                        </div>
                    </div>

                    <div class="form-group">
                        <label for="add-startDate" class="col-xs-4 control-label">Select Start Date:</label>
                        <div>
                            <div class="col-xs-8">
                                <input type="date" class="form-control" name="startDate" id="startDate" placeholder="Start Date" required/>
                            </div>
                            <label for="add-endDate" class="col-xs-4 control-label">Select End Date:</label>
                            <div class="col-xs-8">
                                <input type="date" class="form-control" name="endDate" id="endDate" placeholder="End Date" required/>
                            </div>

                        </div>
                    </div>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <div class ="form-group">
                            <label for="enablePage" class ="col-md-4 control-label">
                                Enable Page? 
                            </label>
                            <div class ="col-md-8">
                                <select class="form-control" id="pageEnable" name="pageEnable" required>
                                    <option selected>no</option>
                                    <option>yes</option>

                                </select>
                            </div>

                        </div>
                        <br>
                    </sec:authorize> 

                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-8">
                            <input type="submit" class="btn btn-default" id="add-page-button" value="Create Page"/>
                        </div>
                    </div>
                    <input type="hidden" name="pageId" value="${page.pageId}"/>
                </form>
            </div>

            <div class="col-xs-12" style="text-align: center" id ="catSnipDiv">
                <%@include file="catSnip.jsp" %>

            </div>

        </div>

        <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=jupqfvd6xbt805vuo0c45oi7px2yoj56wncg8r5m0oimlg6c"></script>
        <script>tinymce.init({
                                selector: 'textarea',
                                height: 500,
                                theme: 'modern',
                                plugins: [
                                    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                                    'searchreplace wordcount visualblocks visualchars code fullscreen',
                                    'insertdatetime media nonbreaking save table contextmenu directionality',
                                    'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
                                ],
                                toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                                toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
                                image_advtab: true,
                                templates: [
                                    {title: 'Test template 1', content: 'Test 1'},
                                    {title: 'Test template 2', content: 'Test 2'}
                                ],
                                content_css: [
                                    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                                    '//www.tinymce.com/css/codepen.min.css'
                                ]
                            });
        </script>            

        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/sidemenu.js"></script>
        <script src="${pageContext.request.contextPath}/js/category.js"></script>




    </body>
</html>

