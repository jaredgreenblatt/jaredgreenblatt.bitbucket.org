<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>${genericPage.title}</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/HomeBalloons.css" rel="stylesheet"> 
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png"> 
    </head>
    <body style="background-color: #e6ffff">
        <div class="row" id="navbar">
            <div class="col-xs-11">        
                <img class="float" src="http://www.clker.com/cliparts/2/Y/t/a/C/o/hot-air-balloon-outline.svg" alt="" />
                <img class=float1 src="http://www.clker.com/cliparts/2/Y/t/a/C/o/hot-air-balloon-outline.svg" alt="" />
            </div>
            <div class="col-xs-1">  
                <%@include file="sideMenu.jsp" %>
            </div>
        </div>
        <%--<sec:authorize access="hasRole('ROLE_ADMIN')">--%>
        <!--                        <li role="presentation">
                                    <a href="${pageContext.request.contextPath}/displayUserList">
                                        User Admin
                                    </a>
                                </li>                        -->
        <%--</sec:authorize>--%>  
        <div class="container">
            <div class="col-xs-offset-2 col-xs-8" style="text-align: center" >
                <c:if test="${pageContext.request.userPrincipal.name != null}">
                    <h2>Hello : ${pageContext.request.userPrincipal.name}
                        | <a href="<c:url value="/j_spring_security_logout" />" > Logout</a>
                    </h2>
                </c:if>
                <br><br>
                <h1>Users</h1>
                <br>
                <a href="displayUserForm">Add a User</a><br/>
                <hr/>
                <c:forEach var="user" items="${users}">
                    <c:out value="${user.username}"/> |
                    <a href="deleteUser?username=${user.username}">Delete</a><br/><br/>
                </c:forEach>
            </div>
            <!-- Placed at the end of the document so the pages load faster -->
            <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/sidemenu.js"></script>

    </body>
</html>