
function mutipleFunc() {
    validateForm();






}

//Game Function this is where all the calculations happen.
function startGame() {
    //Setting Variables
    //bet Amount variable used throughout the program. Grabs number from form and then used throughout calculations.
    //Initial Bet(original bet value) diceone(dice1 value) dicetwo(dice2 value)dicesum(total of d1&d2)
    //bigWin highest amount won hvRollCount roll count at largest value won. rollcount total number of rolls counter
    var betAmount, initialBet, diceOne, diceTwo, diceSum, bigWin, hvRollCount, rollCount;
    betAmount = parseInt(document.getElementById("startingNumber").value);
    bigWin = betAmount;
    initialBet = betAmount;
    rollCount = 0;
    hvRollCount = 0;

    //Game While Loop
    // This will cotinue to run aslong as the current bet is greater than 1 dollar
    while (betAmount >= 1) {
        //Roll Count will go up by 1 each time this while loop is run.
        rollCount++;
        // d1&d2 are calculating a random number between 1 and 6.
        diceOne = Math.floor((Math.random() * 6) + 1);
        diceTwo = Math.floor((Math.random() * 6) + 1);
        // total of both dices
        diceSum = diceOne + diceTwo;
        //Game Logic
        // if the dice is equal to 7 the user wins four dollars.
        if (diceSum ===7) {
            betAmount += 4;
        }
        // if dice is anything other than 7  user will lose 1 dollar
        else {

            betAmount--;
        }

        // bigwin is set to your starting amount. So once that number is smaller than your winnings
        //we will store  that amount as bigwin we will also store the rollcount as the highest value rollcount
        // if this new value is bigger this will be reset.
        //This logic is evaluated each run.
        if (betAmount >= bigWin) {
            hvRollCount = rollCount;
            bigWin = betAmount;

        }
        //as long as betamount is >1 this loop will start again


    }
    //items below are how we display in the html. The"" items line up with ID's in the p html tags.
    document.getElementById("result").innerHTML = "Results";
    document.getElementById("iBetText").innerHTML = "Starting Bet:";
    document.getElementById("iBet").innerHTML = "$" + initialBet + ".00";
    document.getElementById("totalRollsText").innerHTML = "Total Rolls Before Going Broke:";
    document.getElementById("totalRolls").innerHTML = +rollCount;
    document.getElementById("bWinText").innerHTML = "Highest Amount Won:";
    document.getElementById("bWin").innerHTML = "$" + bigWin + ".00";
    document.getElementById("bWinRollNumberText").innerHTML = "Roll Count at Highest Amount Won:";
    document.getElementById("bWinRollNumber").innerHTML = +hvRollCount;

}
//changes button Value to PlayAgain.
function newButtonValue() {
    document.getElementById("PlayButton").value = "Play Again";
}
//This function validates the form. 
//This will check to see if the starting bet is null. 
//if null the user will be alerted.
// If there is an number it will get through the logic and start the game functions. 
function validateForm() {
    var $startbet = (document.getElementById("startingNumber").value);

    if ($startbet === "") {
        alert("You must enter a starting bet.");
        return false;
    } else {
        startGame();
        newButtonValue();

    }

}