/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    
    loadComments();
    
});
function deleteComment(commentId) {

    
    $.ajax({
        type: 'DELETE',
        url: 'http://localhost:8080/BalloonBlog/restComment/' + commentId,
        success: function () {
            clearComment();
            loadComments();
            
        }



    });
}


function loadComments() {

    var contentRows = $('#contentRows');
    var blogId = $('#blogId').val();
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/BalloonBlog/restCommentByBlog/' + blogId,
        success: function (commentArray) {

            $.each(commentArray, function (index, comment) {
                var commentID = comment.commentId;
                var displyName = comment.displyName;
                var content = comment.comment;
                var row = "<tr><td>" + displyName + "</td>";
                row += "<td>" + content + "</td>";
                row += "<td><a onclick=deleteComment(" + commentID + ") id=(" + commentID + " value=(" + commentID + ")>Delete</a></td></tr>";


                contentRows.append(row);
            });
        },
        error: function () {
            $('#errorMessages')
                    .append($('<h3>')

                            .text('Error Calling Web Service. Please try again later.'));
        }
    });
}
function clearComment() {
    $('#contentRows').empty();
  

}