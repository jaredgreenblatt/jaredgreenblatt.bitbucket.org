/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//category.js
$(document).ready(function () {
    clearAuthor();
    clearTags();
    clearCategory();
    loadCategory();
    loadAuthors();
    loadTags();
    $('#formBlog').show();
    $('#tagSnipDiv').hide();
    $('#authorSnipDiv').hide();
    $('#catSnipDiv').hide();





});
function loadCategory() {

    var contentRows = $('#category');
    var categorySnipList = $('#categorySnip');
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/BalloonBlog/restCategory',
        success: function (contactArray) {
            $.each(contactArray, function (index, category) {
                var categoryID = category.categoryId;
                var category = category.category;
                var select = '<option value =' + categoryID + ' id=' + categoryID + '>';
                select += category + '</option>';
                contentRows.append(select);
                categorySnipList.append(select);
            });
        },
        error: function () {
            $('#errorMessages')
                    .append($('<h3>')

                            .text('Error Calling Web Service. Please try again later.'));
        }


    });
}
function addCategorySnip() {



    $.ajax({
        type: 'POST',
        url: 'http://localhost:8080/BalloonBlog/restCategory',
        data: JSON.stringify({
            category: $('#categoryNameSnip').val()
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
//        'dataType': 'json',
        success: function () {
            $('#errorMessages').empty();
            $('#categoryNameSnip').val('');
            clearCategory();
            loadCategory();
            hideCatSnip();

        },
        error: function () {
            $('#errorMessages')
                    .append($('<h3>')

                            .text('Error Calling Web Service. Please try again later.'));

        }
    });




}
function removeCategorySnip() {

    clearErrorMessage();

    var categoryId = $('#categorySnip').val();

    $.ajax({
        type: 'DELETE',
        url: 'http://localhost:8080/BalloonBlog/restCategory/' + categoryId,
        success: function () {
            $('#errorMessages').empty();
            $('#categoryNameSnip').val('');
            loadCategory();
            clearCategory();
            hideCatSnip();
        },
        error: function () {
            clearError();
            loadCategory();
            clearCategory();
            hideCatSnip();
            $('#errorMessages')
                    .append($('<h3>')

                            .text('Category cannot be deleted as it is associated with live blog or page content.'));

        }
    });
}


function clearCategory() {
    $('#categorySnip').empty();
    $('#category').empty();


}

function clearErrorMessage() {
    $('#errorMessages').empty();
}

function showCatSnip() {
    $('#formBlog').hide();
    $('#tagSnipDiv').hide();
    $('#authorSnipDiv').hide();
    $('#catSnipDiv').show();



}
function hideCatSnip() {
    $('#formBlog').show();
    $('#tagSnipDiv').hide();
    $('#authorSnipDiv').hide();
    $('#catSnipDiv').hide();


}


function loadTags() {

    var contentRows = $('#tagBlog');
    var contentSnip = $('#tagSnip');

    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/BalloonBlog/restTag',
        success: function (contactArray) {
            $.each(contactArray, function (index, tag) {
                var tagID = tag.tagId;
                var tag = tag.tag;
                var select = '<option value =' + tagID + ' id=' + tagID + '>';
                select += tag + '</option>';
                contentRows.append(select);
                contentSnip.append(select);

            });
        },
        error: function () {
            $('#errorMessages')
                    .append($('<h3>')

                            .text('Error Calling Web Service. Please try again later.'));
        }


    });
}
function addTagSnip() {



    $.ajax({
        type: 'POST',
        url: 'http://localhost:8080/BalloonBlog/restTag',
        data: JSON.stringify({
            tag: $('#tagNameSnip').val()
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
//        'dataType': 'json',
        success: function () {
            clearError();
            $('#tagNameSnip').val('');
            clearTags();
            loadTags();
            hideTagSnip();

        },
        error: function () {
            clearError();
            $('#errorMessages')
                    .append($('<h3>')

                            .text('Error Calling Web Service. Please try again later.'));

        }
    });




}
function removeTagSnip() {

    var tagId = $('#tagSnip').val();

    $.ajax({
        type: 'DELETE',
        url: 'http://localhost:8080/BalloonBlog/restTag/' + tagId,
        success: function () {
            clearError();
            clearTags();
            loadTags();
            hideTagSnip();
        },
        error: function () {
            clearError();
            clearTags();
            loadTags();
            hideTagSnip();
            $('#errorMessages')
                    .append($('<h3>')
                            .text('Tag cannot be deleted as it is associated with live blog content.'));

        }


    });


}

function showTagSnip() {
    $('#formBlog').hide();
    $('#tagSnipDiv').show();
    $('#authorSnipDiv').hide();
    $('#catSnipDiv').hide();



}
function hideTagSnip() {
    $('#formBlog').show();
    $('#tagSnipDiv').hide();
    $('#authorSnipDiv').hide();
    $('#catSnipDiv').hide();


}
function clearTags() {
    $('#tagBlog').empty();
    $('#tagSnip').empty();

}

function  clearError() {
    $('#errorMessages').empty();
}





function loadAuthors() {

    var contentRows = $('#removeAuthorSnip');
    var authorRows = $('#author');
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/BalloonBlog/restAuthor',
        success: function (contactArray) {
            $.each(contactArray, function (index, author) {
                var authorId = author.authorId;
                var authorDisplayName = author.displayName;
                var select = '<option value =' + authorId + ' id=' + authorId + '>';
                select += authorDisplayName + '</option>';
                contentRows.append(select);
                authorRows.append(select);
            });
        },
        error: function () {
            $('#errorMessages')
                    .append($('<h3>')

                            .text('Error Calling Web Service. Please try again later.'));
        }


    });
}
function addAuthorSnip() {



    $.ajax({
        type: 'POST',
        url: 'http://localhost:8080/BalloonBlog/restAuthor',
        data: JSON.stringify({
            displayName: $('#displayNameSnip').val(),
            firstName: $('#firstNameSnip').val(),
            lastName: $('#lastNameSnip').val()


        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
//        'dataType': 'json',
        success: function () {
            clearError();
            $('#displayNameSnip').val('');
            $('#firstNameSnip').val('');
            $('#lastNameSnip').val('');
            clearAuthor();
            loadAuthors();
            hideAuthorSnip();

        },
        error: function () {
            clearError();
            $('#errorMessages')
                    .append($('<h3>')

                            .text('Error Calling Web Service. Please try again later.'));


        }
    });




}
function deleteAuthorSnip() {

    var authorId = $('#removeAuthorSnip').val();

    $.ajax({
        type: 'DELETE',
        url: 'http://localhost:8080/BalloonBlog/restAuthor/' + authorId,
        success: function () {
            clearError();
            clearAuthor();
            loadAuthors();
            hideAuthorSnip();

        }
        , error: function () {
            clearError();
            clearAuthor();
            loadAuthors();
            hideAuthorSnip();
            $('#errorMessages')
                    .append($('<h3>')
                            .text('Author cannot be deleted as it is associated with live blog content.'));

        }



    });


}

function showAuthorSnip() {
    $('#formBlog').hide();
    $('#tagSnipDiv').hide();
    $('#authorSnipDiv').show();
    $('#catSnipDiv').hide();



}
function hideAuthorSnip() {
    $('#formBlog').show();
    $('#tagSnipDiv').hide();
    $('#authorSnipDiv').hide();
    $('#catSnipDiv').hide();


}


function clearAuthor() {
    $('#removeAuthorSnip').empty();
    $('#author').empty();


}

function  clearError() {
    $('#errorMessages').empty();
}









