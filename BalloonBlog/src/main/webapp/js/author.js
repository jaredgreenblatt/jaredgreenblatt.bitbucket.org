/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//tag.js
$(document).ready(function () {

    loadAuthors();
});
function loadAuthors() {

    var contentRows = $('#removeAuthor');
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/BalloonBlog/restAuthor',
        success: function (contactArray) {
            $.each(contactArray, function (index, author) {
                var authorId = author.authorId;
                var authorDisplayName = author.displayName;
                var select = '<option value =' + authorId + ' id=' + authorId + '>';
                select += authorDisplayName + '</option>';
                contentRows.append(select);
            });
        },
        error: function () {
            $('#errorMessages')
                    .append($('<h3>')

                            .text('Error Calling Web Service. Please try again later.'));
        }


    });
}
function addAuthor() {



    $.ajax({
        type: 'POST',
        url: 'http://localhost:8080/BalloonBlog/restAuthor',
        data: JSON.stringify({
            displayName: $('#displayName').val(),
            firstName: $('#firstName').val(),
            lastName: $('#lastName').val()


        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
//        'dataType': 'json',
        success: function () {
            clearError();
            $('#displayName').val('');
            $('#firstName').val('');
            $('#lastName').val('');
            clearAuthor();
            loadAuthors();

        },
        error: function () {
            clearError();
            $('#errorMessages')
                    .append($('<h3>')

                            .text('Error Calling Web Service. Please try again later.'));

        }
    });




}
function deleteAuthor() {

    var authorId = $('#removeAuthor').val();

    $.ajax({
        type: 'DELETE',
        url: 'http://localhost:8080/BalloonBlog/restAuthor/' + authorId,
        success: function () {
            clearError();
            clearAuthor();
            loadAuthors();

        }



    });


}


function clearAuthor() {
    $('#removeAuthor').empty();

}

function  clearError() {
    $('#errorMessages').empty();
}





