package balloonblog;

import balloonblog.dto.Author;
import balloonblog.dto.Blog;
import balloonblog.dto.Category;
import balloonblog.dto.Comment;
import balloonblog.dto.GenericPage;
import balloonblog.dto.Tag;
import balloonblog.service.BalloonBlogService;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping({"/"})
public class HelloController {
    
    private BalloonBlogService service;
    
    @Inject
    public HelloController(BalloonBlogService service) {
        this.service = service;
    }
    
        @RequestMapping(value="/", method=RequestMethod.GET)
    public String homePage(Model model) {

        List<Author> authorList = service.getAllAuthors();
        List<Category> categoryList = service.getAllCategory();

        model.addAttribute("categoryList", categoryList);
        
        List<Tag> tagList = service.getAllTags();
        model.addAttribute("tagList", tagList);
        
        List<Comment> commentList = service.getAllComments();
        model.addAttribute("commentList", commentList);
        
        List<Blog> blogList = service.getAllEnabledBlogsByDate();
        model.addAttribute("blogList", blogList);
        
        // Adding SideMenu Content
        List<GenericPage> pageListEnabled = service.getAllEnabledGenericPages();
        model.addAttribute("pageListEnabled", pageListEnabled);
        List<Category> catList = service.getAllCategory();
        model.addAttribute("catList", catList);
        
        return "index";
    }
    
    
    
    
    
}
