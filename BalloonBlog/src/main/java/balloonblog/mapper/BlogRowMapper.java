/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balloonblog.mapper;

import balloonblog.dto.Blog;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author apprentice
 */
public class BlogRowMapper implements RowMapper<Blog> {

    @Override
    public Blog mapRow(ResultSet rs, int i) throws SQLException {
        Blog b = new Blog();
        b.setBlogId(rs.getInt("BlogId"));
        b.setTitle(rs.getString("Title"));
        b.setArticle(rs.getString("Article"));
        b.setStartDate(rs.getTimestamp("StartDate")
                .toLocalDateTime().toLocalDate());
        b.setEndDate(rs.getTimestamp("EndDate")
                .toLocalDateTime().toLocalDate());
        b.setEnabled(rs.getBoolean("Enabled"));
        b.setFeatured(rs.getBoolean("Featured"));

        return b;
    }

}
