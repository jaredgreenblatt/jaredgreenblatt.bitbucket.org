/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balloonblog.mapper;

import balloonblog.dto.GenericPage;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author apprentice
 */
public class GenericPageRowMapper implements RowMapper<GenericPage> {

    @Override
    public GenericPage mapRow(ResultSet rs, int i) throws SQLException {
        GenericPage gp = new GenericPage();
        gp.setPageId(rs.getInt("PageId"));
        gp.setTitle(rs.getString("Title"));
        gp.setPageText(rs.getString("PageText"));
        gp.setEnabled(rs.getBoolean("enabled"));
        gp.setStartDate(rs.getTimestamp("StartDate").
                toLocalDateTime().toLocalDate());
        gp.setEndDate(rs.getTimestamp("EndDate").
                toLocalDateTime().toLocalDate());

        return gp;

    }

}
//  Category c = new Category();
//        c.setCategoryId(rs.getInt("CategoryId"));
//        c.setCategory(rs.getString("Category"));
//        return c;
