/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balloonblog.mapper;

import balloonblog.dto.Category;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author apprentice
 */
public class CategoryRowMapper implements RowMapper<Category>{

    @Override
    public Category mapRow(ResultSet rs, int i) throws SQLException {
        Category c = new Category();
        c.setCategoryId(rs.getInt("CategoryId"));
        c.setCategory(rs.getString("Category"));
        return c;
    }
    
}
