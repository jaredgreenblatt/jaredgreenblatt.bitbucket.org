/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balloonblog.mapper;

import balloonblog.dto.Comment;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author apprentice
 */
public class CommentRowMapper implements RowMapper<Comment> {

    @Override
    public Comment mapRow(ResultSet rs, int i) throws SQLException {
        Comment c  = new Comment();
        c.setCommentId(rs.getInt("commentId"));
        c.setBlogId(rs.getInt("blogId"));
        c.setComment(rs.getString("comment"));
        c.setDisplyName(rs.getString("displayName"));
        c.setCommentDate(rs.getDate("commentDate").toLocalDate());
        return c;
    }
    
    
}

