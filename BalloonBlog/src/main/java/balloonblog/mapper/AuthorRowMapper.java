/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balloonblog.mapper;

import balloonblog.dto.Author;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author apprentice
 */
public class AuthorRowMapper implements RowMapper<Author> {

    @Override
    public Author mapRow(ResultSet rs, int i) throws SQLException {
        Author a  = new Author();
        a.setAuthorId(rs.getInt("authorId"));
        a.setDisplayName(rs.getString("displayName"));
        a.setFirstName(rs.getString("firstName"));
        a.setLastName(rs.getString("lastName"));
        return a;
    }
}
