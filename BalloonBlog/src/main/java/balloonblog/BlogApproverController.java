/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balloonblog;

import balloonblog.dto.Author;
import balloonblog.dto.Blog;
import balloonblog.dto.Category;
import balloonblog.dto.Comment;
import balloonblog.dto.GenericPage;
import balloonblog.dto.Tag;
import balloonblog.service.BalloonBlogService;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.swing.JOptionPane;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class BlogApproverController {

    private BalloonBlogService service;

    @Inject
    public BlogApproverController(BalloonBlogService service) {
        this.service = service;
    }

    @RequestMapping(value = "/blogApproval", method = RequestMethod.GET)
    public String displayBlogApproval(Model model) {

        List<Author> authorList = service.getAllAuthors();
        List<Category> categoryList = service.getAllCategory();

        model.addAttribute("categoryList", categoryList);

        List<Tag> tagList = service.getAllTags();
        model.addAttribute("tagList", tagList);

        List<Comment> commentList = service.getAllComments();
        model.addAttribute("commentList", commentList);

        List<Blog> blogList = service.getAllBlogsToApprove();
        model.addAttribute("blogList", blogList);

        // Adding SideMenu Content
        List<GenericPage> pageListEnabled = service.getAllEnabledGenericPages();
        model.addAttribute("pageListEnabled", pageListEnabled);
        List<Category> catList = service.getAllCategory();
        model.addAttribute("catList", catList);

        return "blogApproval";
    }

    @RequestMapping(value = "/approveBlog", method = RequestMethod.POST)
    public String approveBlog(HttpServletRequest request, Model model) {

        try {
            String stringId = request.getParameter("blogId");
            int blogId = Integer.parseInt(stringId);

            Blog blogToUpdate = service.getBlogById(blogId);
            blogToUpdate.setEnabled(true);

            service.updateBlog(blogToUpdate);

            return "redirect:blogApproval";

        } catch (NullPointerException e) {
            JOptionPane.showMessageDialog(null, "Error", "Error",
                    JOptionPane.ERROR_MESSAGE);
            return "redirect:blogApproval";

        }

    }
    
        @RequestMapping(value = "/deleteBlogApproval", method = RequestMethod.GET)
    public String deleteBlogApprovalSite(HttpServletRequest request
    ) {
        String blogIdString = request.getParameter("blogId");
        int blogId = Integer.parseInt(blogIdString);
        service.removeBlog(blogId);
        return "redirect:blogApproval";
    }

}
