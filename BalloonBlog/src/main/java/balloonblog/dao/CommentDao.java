/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balloonblog.dao;

import balloonblog.dto.Comment;
import java.time.LocalDate;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface CommentDao {
         // add the given Comment to the data store
    public void addComment(Comment comment, int blogId, LocalDate currentDate);
    
    // remove the Comment with the give id from the data store
    public void removeComment(int commentId);
    
    //update the given Comment in the data store
    public void updateComment(Comment comment);
    
    // retrieve all Comment from the data store
    public List<Comment> getAllComments();
    
    // retrieve the Comment with the given id from the data store.
    public Comment getCommentById(int commentId);
    
    // retrieve the Comment with the given blog id from the data store.
    public List<Comment> getCommentByBlogId(int blogId);    
    
    
}
