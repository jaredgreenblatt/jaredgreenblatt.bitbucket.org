/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balloonblog.dao;

import balloonblog.dto.Author;
import balloonblog.mapper.AuthorRowMapper;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class AuthorDaoJdbcImpl implements AuthorDao{
        private JdbcTemplate jdbcTemplate;
        

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    
    // *******************************************************************************
    // Prepared Statement to insert a Author
    // *******************************************************************************
   
     private static final String SQL_INSERT_AUTHOR
             ="INSERT INTO BlogAuthor (DisplayName, FirstName, LastName) VALUES(?,?,?);";    
    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false) 
    @Override
    public void addAuthor(Author author) {
        jdbcTemplate.update(SQL_INSERT_AUTHOR, 
                author.getDisplayName(),author.getFirstName(),author.getLastName());
        int authorId = jdbcTemplate.queryForObject("select LAST_INSERT_ID()",
                        Integer.class);
        author.setAuthorId(authorId);        
        
    }
    
    // *******************************************************************************
    // Prepared Statement to delete a Author, only deletes if associated blog_tags are deleted
    // *******************************************************************************
   
    
    private static final String SQL_DELETE_AUTHOR
            ="DELETE FROM BlogAuthor WHERE AuthorId = ? ;";      

    @Override
    public void removeAuthor(int authorId) {
           jdbcTemplate.update(SQL_DELETE_AUTHOR, authorId); 
    }

    private static final String SQL_UPDATE_AUTHOR
            = "UPDATE BlogAuthor set DisplayName = ?, FirstName = ?, LastName = ? where AuthorId = ?;";
    
    
    @Override
    public void updateAuthor(Author author) {
        jdbcTemplate.update(SQL_UPDATE_AUTHOR,
                author.getDisplayName(),
                author.getFirstName(),
                author.getLastName(),
                author.getAuthorId());
        
    }
    // *******************************************************************************
    // Prepared Statement to get all Authors
    // *******************************************************************************

    private static final String SQL_GET_ALL_AUTHORS
            = "SELECT * from BlogAuthor;";    
    
    @Override
    public List<Author> getAllAuthors() {
        return jdbcTemplate.query(SQL_GET_ALL_AUTHORS, new AuthorRowMapper());

    }

    private static final String SQL_GET_AUTHOR_BY_ID
            = "SELECT * from BlogAuthor where AuthorId =?;";
    

    
    @Override
    public Author getAuthorById(int authorId) {
        try {
            return jdbcTemplate.queryForObject(SQL_GET_AUTHOR_BY_ID,
                    new AuthorRowMapper(),
                    authorId);

        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }
    
}
