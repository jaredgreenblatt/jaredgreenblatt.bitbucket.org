/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balloonblog.dao;

import balloonblog.dto.Blog;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface BlogDao {

    // add the given Blog to the data store
    public void addBlog(Blog blog);

    // remove the Blog with the give id from the data store
    public void removeBlog(int blogId);

    //update the blog Author in the data store
    public void updateBlog(Blog blog);

    // retrieve all Blog from the data store
    public List<Blog> getAllBlogs();

    // retrieve the Blog with the given id from the data store.
    public Blog getBlogById(int blogId);

    // retrieve all enabled Blogs from the data store by date descending
    public List<Blog> getAllEnabledBlogsByDate();

    //retrieve all Blog from the data store where Enabled = false;
    public List<Blog> getBlogsToApprove();

    // retrieve  Blogs with from the given author
    public List<Blog> getBlogsByAuthorId(int authorId);

}
