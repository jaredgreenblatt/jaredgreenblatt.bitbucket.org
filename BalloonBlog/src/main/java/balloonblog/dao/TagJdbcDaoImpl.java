/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balloonblog.dao;

import balloonblog.dto.Tag;
import balloonblog.mapper.TagRowMapper;
import java.util.List;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class TagJdbcDaoImpl implements TagDao{
         private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    
    // *******************************************************************************
    // Prepared Statement to insert a Tag
    // *******************************************************************************
   
     private static final String SQL_INSERT_TAG
             ="INSERT INTO Tag (Tag) VALUES(?);";    
     
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false) 
    @Override
    public void addTag(Tag tag) {
        jdbcTemplate.update(SQL_INSERT_TAG, 
                tag.getTag());
        int tagId = jdbcTemplate.queryForObject("select LAST_INSERT_ID()",
                        Integer.class);
        tag.setTagId(tagId);
        
    }
    
    // *******************************************************************************
    // Prepared Statement to delete a Tag, only deletes if associated blog_tags are deleted
    // *******************************************************************************
   
    
    private static final String SQL_DELETE_TAG
            ="DELETE FROM Tag WHERE TagId = ? ;";   
    
    @Override
    public void removeTag(int tagId) throws TagDeleteException{
        try {
           jdbcTemplate.update(SQL_DELETE_TAG, tagId); 
        } catch (DataAccessException ex) {
            throw new TagDeleteException("You cannot Delete this Tag."
                    + "It is assoicated with a Blog.", ex);
            
        }
        jdbcTemplate.update(SQL_DELETE_TAG, tagId); 
    }


    private static final String SQL_UPDATE_TAG
            = "update Tag set Tag=? where TagId =?;";
    
    @Override
    public void updateTag(Tag tag) {
        jdbcTemplate.update(SQL_UPDATE_TAG,
                tag.getTag(),
                tag.getTagId());
    }

    // *******************************************************************************
    // Prepared Statement to get all Tags
    // *******************************************************************************

    private static final String SQL_GET_ALL_TAG
            = "select * from Tag;";    
    
    @Override
    public List<Tag> getAllTags() {
        return jdbcTemplate.query(SQL_GET_ALL_TAG, new TagRowMapper());
    }
    

    // *******************************************************************************
    // Prepared Statement to get a Tag by ID.
    // *******************************************************************************
    private static final String SQL_GET_TAG_BY_ID
            = "Select * from Tag where TagId =?;";
    

    @Override
    public Tag getTagById(int tagId) {
        
        try {
            return jdbcTemplate.queryForObject(SQL_GET_TAG_BY_ID,
                    new TagRowMapper(),
                    tagId);

            
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }        
    
}
