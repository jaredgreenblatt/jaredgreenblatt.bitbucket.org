/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balloonblog.dao;

import balloonblog.dto.GenericPage;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface GenericPageDao {
    // add the given  GenericPage to the data store

    public void addGenericPage(GenericPage genericPage);

    // remove the  GenericPage with the give id from the data store
    public void removeGenericPage(int pageId);

    //update the given  GenericPage in the data store
    public void updateGenericPage(GenericPage genericPage);

    // retrieve all  GenericPage from the data store
    public List<GenericPage> getAllGenericPages();

    // retrieve the  GenericPage with the given id from the data store.
    public GenericPage getGenericPageById(int pageId);

    // retrieve all  enabled GenericPage from the data store
    public List<GenericPage> getAllEnabledGenericPages();

    // retrieve all  disbaled GenericPage from the data store
    public List<GenericPage> getAllDisabledGenericPages();

}
