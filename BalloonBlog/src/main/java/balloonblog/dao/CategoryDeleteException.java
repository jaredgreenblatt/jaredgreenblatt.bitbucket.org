/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balloonblog.dao;

/**
 *
 * @author apprentice
 */
public class CategoryDeleteException extends Exception {

    public CategoryDeleteException(String message) {
        super(message);
    }

    public CategoryDeleteException(String message,
            Throwable cause) {
        super(message, cause);
    }
    
}
