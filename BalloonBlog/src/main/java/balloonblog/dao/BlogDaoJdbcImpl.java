/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balloonblog.dao;

import balloonblog.dto.Author;
import balloonblog.dto.Blog;
import balloonblog.dto.Category;
import balloonblog.dto.Comment;
import balloonblog.dto.Tag;
import balloonblog.mapper.AuthorRowMapper;
import balloonblog.mapper.BlogRowMapper;
import balloonblog.mapper.CategoryRowMapper;
import balloonblog.mapper.CommentRowMapper;
import balloonblog.mapper.TagRowMapper;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class BlogDaoJdbcImpl implements BlogDao {

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    // *******************************************************************************
    // Prepared Statement to INSERT BLOG_TAG
    // *******************************************************************************
    private static final String SQL_INSERT_BLOG_TAG
            = "INSERT INTO Blog_Tag (BlogId,TagID) VALUES (?,?);";

    private void insertBlogTag(Blog blog) {
        final int BLOG_ID = blog.getBlogId();
        final List<Tag> TAG_LIST = blog.getTagList();

        for (Tag currentTag : TAG_LIST) {
            jdbcTemplate.update(SQL_INSERT_BLOG_TAG,
                    BLOG_ID,
                    currentTag.getTagId());

        }

    }
    // *******************************************************************************
    // Prepared Statement to GET_ALL_TAGS_BY_BLOG
    // *******************************************************************************
    private static final String SQL_GET_TAG_BY_BLOG_ID
            = "select t.tagId, t.Tag "
            + "from Tag t "
            + "inner join Blog_Tag bt "
            + "on t.TagId = bt.TagId "
            + "inner join Blog b "
            + "on bt.BlogId=b.BlogId"
            + " where bt.blogId = ?;";

    private List<Tag> findTagsByBlog(Blog blog) {
        List<Tag> tagList = jdbcTemplate.query(SQL_GET_TAG_BY_BLOG_ID,
                new TagRowMapper(), blog.getBlogId());
        return tagList;
    }
    // *******************************************************************************
    // Prepared Statement to SELECT CATEGORY BY BY_BLOG
    // *******************************************************************************
    private static final String SQL_SELECT_CATEGORY_BY_BLOG_ID
            = "Select"
            + " c.CategoryId, c.Category"
            + " from Category c "
            + "inner join Blog b "
            + "on c.CategoryId=b.CategoryId "
            + "where BlogId= ?;";

    private Category findCategoryByBlog(Blog blog) {
        return jdbcTemplate.queryForObject(SQL_SELECT_CATEGORY_BY_BLOG_ID,
                new CategoryRowMapper(),
                blog.getBlogId());
    }

    // *******************************************************************************
    // Prepared Statement to SELECT AUTHOR BY BY_BLOG
    // *******************************************************************************
    private static final String SQL_SELECT_AUTHOR_BY_BLOG_ID
            = "select "
            + "ba.AuthorId, ba.DisplayName, ba.FirstName, ba.LastName "
            + "from BlogAuthor ba "
            + "inner join Blog b"
            + " on ba.AuthorId = b.AuthorId"
            + " where b.blogId =?;";

    private Author findAuthorByBlog(Blog blog) {
        return jdbcTemplate.queryForObject(SQL_SELECT_AUTHOR_BY_BLOG_ID,
                new AuthorRowMapper(),
                blog.getBlogId());
    }

    // *******************************************************************************
    // Prepared Statement to SELECT COMMENTS BY BY_BLOG
    // *******************************************************************************
    private static final String SQL_SELECT_COMMENT_BY_BLOG_ID
            = "select c.CommentId,c.`Comment`, c.BlogId, c.DisplayName, c.CommentDate from `Comment` c inner join Blog b on c.BlogId = b.BlogId where b.BlogId =?;";

    private List<Comment> findCommentsByBlog(Blog blog) {
        List<Comment> commentList
                = jdbcTemplate.query(SQL_SELECT_COMMENT_BY_BLOG_ID,
                        new CommentRowMapper(), blog.getBlogId());
        return commentList;
    }

    private List<Blog>
            associateBlogAuthorCategoryCommentTag(List<Blog> blogList) {

        for (Blog currentBlog : blogList) {
            currentBlog.setAuthor(findAuthorByBlog(currentBlog));
            currentBlog.setCategory(findCategoryByBlog(currentBlog));
            currentBlog.setCommentList(findCommentsByBlog(currentBlog));
            currentBlog.setTagList(findTagsByBlog(currentBlog));

        }
        return blogList;
    }

    // *******************************************************************************
    // Prepared Statement to ADD BLOG
    // *******************************************************************************
    private static final String SQL_INSERT_BLOG
            = "INSERT "
            + "INTO Blog (Title, Article, StartDate, EndDate, Featured, Enabled, AuthorId,CategoryId)"
            + " VALUES (?, ?, ?, ?, ?, ?, ?, ?);";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)

    public void addBlog(Blog blog) {
        jdbcTemplate.update(SQL_INSERT_BLOG,
                blog.getTitle(),
                blog.getArticle(),
                blog.getStartDate().toString(),
                blog.getEndDate().toString(),
                blog.getFeatured(),
                blog.getEnabled(),
                blog.getAuthor().getAuthorId(),
                blog.getCategory().getCategoryId());

        int blogId
                = jdbcTemplate.queryForObject("select LAST_INSERT_ID()",
                        Integer.class);
        blog.setBlogId(blogId);

        insertBlogTag(blog);

    }
    // *******************************************************************************
    // Prepared Statement to REMOVE BLOG
    // *******************************************************************************

    private static final String SQL_DELETE_BLOG_TAG
            = "delete from Blog_Tag where BlogId=?;";
    private static final String SQL_DELETE_BLOG_COMMENTS
            = "delete  from `Comment`  where BlogId=?;";
    private static final String SQL_DELETE_BLOG
            = "delete from Blog where BlogId=?";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void removeBlog(int blogId) {
        jdbcTemplate.update(SQL_DELETE_BLOG_TAG, blogId);
        jdbcTemplate.update(SQL_DELETE_BLOG_COMMENTS, blogId);
        jdbcTemplate.update(SQL_DELETE_BLOG, blogId);

    }

    // *******************************************************************************
    // Prepared Statement to UPDATE BLOG
    // *******************************************************************************
    private static final String SQL_BLOG_UPDATE
            = "update Blog set Title =?, Article=?, StartDate= ?,EndDate= ?,Featured= ? ,Enabled=? ,AuthorId= ? ,CategoryId=? where blogId=?;";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void updateBlog(Blog blog) {
        jdbcTemplate.update(SQL_BLOG_UPDATE,
                blog.getTitle(),
                blog.getArticle(),
                blog.getStartDate().toString(),
                blog.getEndDate().toString(),
                blog.getFeatured(),
                blog.getEnabled(),
                blog.getAuthor().getAuthorId(),
                blog.getCategory().getCategoryId(),
                blog.getBlogId());

        jdbcTemplate.update(SQL_DELETE_BLOG_TAG, blog.getBlogId());
        insertBlogTag(blog);
    }

    // *******************************************************************************
    // Prepared Statement to GET ALL BLOGS
    // *******************************************************************************
    private static final String SQL_GET_ALL_BLOGS
            = "select * from Blog;";

    @Override
    public List<Blog> getAllBlogs() {
        List<Blog> blogList = jdbcTemplate.query(SQL_GET_ALL_BLOGS,
                new BlogRowMapper());
        associateBlogAuthorCategoryCommentTag(blogList);

        return blogList;
    }

    // *******************************************************************************
    // Prepared Statement to GET BLOG
    // *******************************************************************************
    private static final String SQL_GET_BLOG_BY_ID
            = "select * from Blog where BlogId=?;";

    @Override
    public Blog getBlogById(int blogId) {
        try {
            Blog blog
                    = jdbcTemplate.queryForObject(SQL_GET_BLOG_BY_ID,
                            new BlogRowMapper(),
                            blogId);
            blog.setAuthor(findAuthorByBlog(blog));
            blog.setCategory(findCategoryByBlog(blog));
            blog.setCommentList(findCommentsByBlog(blog));
            blog.setTagList(findTagsByBlog(blog));

            return blog;

        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    // *******************************************************************************
    // Prepared Statement to GET ENABLED BLOGS BY DESCENDING DATE
    // *******************************************************************************
    private static final String SQL_GET_ALL_ENABLED_BLOGS_BY_DATE_DESC
            = "select * from Blog "
            + "Where Enabled = 1 "
            + "ORDER BY StartDate DESC; ";

    @Override
    public List<Blog> getAllEnabledBlogsByDate() {
        List<Blog> blogList = jdbcTemplate.query(SQL_GET_ALL_ENABLED_BLOGS_BY_DATE_DESC,
                new BlogRowMapper());
        associateBlogAuthorCategoryCommentTag(blogList);

        return blogList;
    }

    // *******************************************************************************
    // Prepared Statement to GET BLOGS TO APPROVE
    // *******************************************************************************
    private static final String SQL_GET_ALL_BLOGS_TO_APPROVE
            = "select * from Blog "
            + "Where Enabled = 0; ";

    @Override
    public List<Blog> getBlogsToApprove() {
        List<Blog> blogList = jdbcTemplate.query(SQL_GET_ALL_BLOGS_TO_APPROVE,
                new BlogRowMapper());
        associateBlogAuthorCategoryCommentTag(blogList);

        return blogList;
    }

    private static final String SQL_GET_ALL_BLOGS_BY_AUTHOR_ID
            = "select * from Blog where authorId=?;";

    @Override
    public List<Blog> getBlogsByAuthorId(int authorId) {
        List<Blog> blogList = jdbcTemplate.query(SQL_GET_ALL_BLOGS_BY_AUTHOR_ID,
                new BlogRowMapper(),
                authorId);
        associateBlogAuthorCategoryCommentTag(blogList);

        return blogList;

    }

}
