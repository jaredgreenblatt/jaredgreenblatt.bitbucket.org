/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balloonblog.dao;

import balloonblog.dto.Comment;
import balloonblog.mapper.CommentRowMapper;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class CommentDaoJdbcImpl implements CommentDao {

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String SQL_INSERT_COMMENT
            = "INSERT INTO Comment (BlogId, Comment, DisplayName, CommentDate) VALUES(?, ?, ?, ?);";

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public void addComment(Comment comment, int blogId, LocalDate currentDate) {
        jdbcTemplate.update(SQL_INSERT_COMMENT,
                blogId,
                comment.getComment(),
                comment.getDisplyName(),
                currentDate.format(DateTimeFormatter.ofPattern("yyyy/MM/dd")));
                //comment.getCommentDate().format(DateTimeFormatter.ofPattern("yyyy/MM/dd")));
        int commentId = jdbcTemplate.queryForObject("select LAST_INSERT_ID()",
                Integer.class);
        comment.setCommentId(commentId);

    }

    private static final String SQL_DELETE_COMMENT
            = "DELETE FROM Comment WHERE CommentId = ? ;";

    @Override
    public void removeComment(int commentId) {
        jdbcTemplate.update(SQL_DELETE_COMMENT, commentId);
    }

    private static final String SQL_UPDATE_COMMENT
            = "UPDATE Comment set BlogId = ?, Comment = ?, DisplayName = ?, CommentDate = ? where CommentId = ?;";

    @Override
    public void updateComment(Comment comment) {
        jdbcTemplate.update(SQL_UPDATE_COMMENT,
                comment.getBlogId(),
                comment.getComment(),
                comment.getDisplyName(),
                comment.getCommentDate().format(DateTimeFormatter.ofPattern("yyyy/MM/dd")),
                comment.getCommentId());

    }

    private static final String SQL_GET_ALL_COMMENTS
            = "SELECT * from Comment;";

    @Override
    public List<Comment> getAllComments() {
        return jdbcTemplate.query(SQL_GET_ALL_COMMENTS, new CommentRowMapper());
    }

    private static final String SQL_GET_COMMENT_BY_ID
            = "SELECT * from Comment where CommentId =?;";

    @Override
    public Comment getCommentById(int commentId) {
        try {
            return jdbcTemplate.queryForObject(SQL_GET_COMMENT_BY_ID,
                    new CommentRowMapper(),
                    commentId);

        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }
    
    private static final String SQL_GET_COMMENT_BY_BLOG_ID
            = "SELECT c.CommentId, c.BlogId, c.`Comment`, c.DisplayName, "
            + "c.CommentDate FROM `Comment` c inner join Blog b on "
            + "c.blogId=b.blogId where b.blogId=?;";

    @Override
    public List<Comment> getCommentByBlogId(int blogId) {
            return jdbcTemplate.query(SQL_GET_COMMENT_BY_BLOG_ID,
                    new CommentRowMapper(),
                    blogId);

    }
    

}
