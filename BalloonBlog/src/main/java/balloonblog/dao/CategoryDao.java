/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balloonblog.dao;

import balloonblog.dto.Category;
import java.util.List;

/**
 *
 * @author apprentice
 */

public interface CategoryDao {
    
    
      // add the given Category to the data store
    public void addCategory(Category category);
    
    // remove the Category with the give id from the data store
    public void removeCategory(int categoryId) throws CategoryDeleteException;
    
    //update the given Category in the data store
    public void updateCategory(Category category );
    
    // retrieve all Category from the data store
    public List<Category> getAllCategory();
    
    // retrieve the Category with the given id from the data store.
    public Category getCategoryById(int categoryId);

    
    
    
}
