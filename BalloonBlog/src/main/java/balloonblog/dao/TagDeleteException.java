/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package balloonblog.dao;

/**
 *
 * @author apprentice 
 */
public class TagDeleteException extends Exception{
    
    public TagDeleteException(String message) {
        super(message);
    }

    public TagDeleteException(String message,
            Throwable cause) {
        super(message, cause);
    }
        
    

}
