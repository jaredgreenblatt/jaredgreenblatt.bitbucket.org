/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balloonblog.dao;

import balloonblog.dto.Category;
import balloonblog.mapper.CategoryRowMapper;
import java.util.List;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class CategoryDaoJdbcImpl implements CategoryDao {

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    // *******************************************************************************
    // Prepared Statement to insert a  Category
    // *******************************************************************************
    private static final String SQL_INSERT_CATEGORY
            = "INSERT INTO Category (Category) VALUES(?);";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void addCategory(Category category) {
        jdbcTemplate.update(SQL_INSERT_CATEGORY,
                category.getCategory());
        int categoryId
                = jdbcTemplate.queryForObject("select LAST_INSERT_ID()",
                        Integer.class);
        category.setCategoryId(categoryId);

    }
    // *******************************************************************************
    // Prepared Statement to remove a  Category
    // *******************************************************************************
    private static final String SQL_DELETE_CATEGORY
            = "delete from Category where CategoryId =?;";

    @Override
    public void removeCategory(int categoryId) throws CategoryDeleteException {
        try {
            jdbcTemplate.update(SQL_DELETE_CATEGORY, categoryId);
        } catch (DataIntegrityViolationException e) {
            throw new CategoryDeleteException("You cannot Delete this Category."
                    + "It is assoicated with Blogs and Webpages.", e);
        }
        jdbcTemplate.update(SQL_DELETE_CATEGORY, categoryId);
    }
    // *******************************************************************************
    // Prepared Statement to update a  Category
    // *******************************************************************************
    private static final String SQL_UPDATE_CATEGORY
            = "update Category set Category=? where CategoryId =?;";

    @Override
    public void updateCategory(Category category) {
        jdbcTemplate.update(SQL_UPDATE_CATEGORY,
                category.getCategory(),
                category.getCategoryId());

    }
    // *******************************************************************************
    // Prepared Statement to get all Categorys
    // *******************************************************************************

    private static final String SQL_GET_ALL_CATEGORY
            = "select * from Category;";

    @Override
    public List<Category> getAllCategory() {
        return jdbcTemplate.query(SQL_GET_ALL_CATEGORY, new CategoryRowMapper());
    }

    // *******************************************************************************
    // Prepared Statement to get a Category by ID.
    // *******************************************************************************
    private static final String SQL_GET_CATEGORY_BY_ID
            = "Select * from Category where CategoryId =?;";

    @Override
    public Category getCategoryById(int categoryId) {

        try {
            return jdbcTemplate.queryForObject(SQL_GET_CATEGORY_BY_ID,
                    new CategoryRowMapper(),
                    categoryId);

        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

}
