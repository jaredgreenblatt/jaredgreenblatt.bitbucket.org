/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balloonblog.dao;

import balloonblog.dto.Author;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface AuthorDao {
    
         // add the given Author to the data store
    public void addAuthor(Author author);
    
    // remove the Author with the give id from the data store
    public void removeAuthor(int authorId);
    
    //update the given Author in the data store
    public void updateAuthor(Author author);
    
    // retrieve all Author from the data store
    public List<Author> getAllAuthors();
    
    // retrieve the Author with the given id from the data store.
    public Author getAuthorById(int authorId);

    
}
