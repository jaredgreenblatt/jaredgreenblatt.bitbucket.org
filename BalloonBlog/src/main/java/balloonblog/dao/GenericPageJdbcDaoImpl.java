/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balloonblog.dao;

import balloonblog.dto.Category;
import balloonblog.dto.GenericPage;
import balloonblog.mapper.CategoryRowMapper;
import balloonblog.mapper.GenericPageRowMapper;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class GenericPageJdbcDaoImpl implements GenericPageDao {

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    // *******************************************************************************
    // Prepared Statement to SELECT CATEGORY BY PAGE
    // *******************************************************************************
    private static final String SQL_SELECT_CATEGORY_BY_PAGE_ID
            = "Select c.CategoryId, c.Category"
            + " from Category c "
            + "inner join GenericPage gp "
            + "on c.CategoryId=gp.CategoryId "
            + "where pageId=?;";

    private Category findCategoryByGenericPage(GenericPage genericPage) {
        return jdbcTemplate.queryForObject(SQL_SELECT_CATEGORY_BY_PAGE_ID,
                new CategoryRowMapper(),
                genericPage.getPageId());
    }

    private List<GenericPage>
            associateCategorysWithGenericPages(List<GenericPage> genericPageList) {
        for (GenericPage currentPage : genericPageList) {
            currentPage.setCategory(findCategoryByGenericPage(currentPage));
        }
        return genericPageList;
    }

    private static final String SQL_INSERT_GENERIC_PAGE
            = "INSERT INTO `GenericPage` "
            + "(`CategoryId`,`Title`,`PageText`,`Enabled`, `StartDate`, `EndDate`) "
            + "VALUES (?, ?, ?, ?, ?, ?);";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void addGenericPage(GenericPage genericPage) {
        jdbcTemplate.update(SQL_INSERT_GENERIC_PAGE,
                genericPage.getCategory().getCategoryId(),
                genericPage.getTitle(),
                genericPage.getPageText(),
                genericPage.getEnabled(),
                genericPage.getStartDate().toString(),
                genericPage.getEndDate().toString());

        int pageId
                = jdbcTemplate.queryForObject("select LAST_INSERT_ID()",
                        Integer.class);
        genericPage.setPageId(pageId);
    }
    // *******************************************************************************
    // Prepared Statement to remove a  GenericPage
    // *******************************************************************************
    private static final String SQL_DELETE_GENERIC_PAGE
            = "delete from GenericPage where pageId=?;";

    @Override
    public void removeGenericPage(int pageId) {
        jdbcTemplate.update(SQL_DELETE_GENERIC_PAGE, pageId);
    }

    // *******************************************************************************
    // Prepared Statement to update a Generic Page
    // *******************************************************************************
    private static final String SQL_UPDATE_GENERIC_PAGE
            = "update"
            + " GenericPage set CategoryId=?, Title=?, PageText=?, Enabled =?, "
            + "StartDate =?, EndDate =? "
            + "where pageId=?;";

    @Override
    public void updateGenericPage(GenericPage genericPage) {
        jdbcTemplate.update(SQL_UPDATE_GENERIC_PAGE,
                genericPage.getCategory().getCategoryId(),
                genericPage.getTitle(),
                genericPage.getPageText(),
                genericPage.getEnabled(),
                genericPage.getStartDate().toString(),
                genericPage.getEndDate().toString(),
                genericPage.getPageId());
    }

    // *******************************************************************************
    // Prepared Statement to get all Generic Pages
    // *******************************************************************************
    private static final String SQL_GET_ALL_GENERIC_PAGE
            = "select * from GenericPage;";

    @Override
    public List<GenericPage> getAllGenericPages() {
        List<GenericPage> genericPageList = jdbcTemplate.query(SQL_GET_ALL_GENERIC_PAGE,
                new GenericPageRowMapper());
        return associateCategorysWithGenericPages(genericPageList);
    }
// *******************************************************************************
    // Prepared Statement to get a PAGE by ID.
    // *******************************************************************************
    private static final String SQL_GET_GENERIC_PAGE_BY_ID
            = "select * from GenericPage where pageId=?;";

    @Override
    public GenericPage getGenericPageById(int pageId) {
        try {
            GenericPage genericPage
                    = jdbcTemplate.queryForObject(SQL_GET_GENERIC_PAGE_BY_ID,
                            new GenericPageRowMapper(),
                            pageId);
            genericPage.setCategory(findCategoryByGenericPage(genericPage));

            return genericPage;
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    private static final String SQL_GET_ALL_ENABLED_GENERIC_PAGE
            = "SELECT gp.PageId, gp.CategoryId, gp.Title, gp.PageText, "
            + "gp.Enabled, gp.StartDate, gp.EndDate FROM GenericPage gp "
            + " inner join Category c on gp.categoryId = c.categoryId "
            + "where enabled = true "
            + "order by c.Category asc;";

    @Override
    public List<GenericPage> getAllEnabledGenericPages() {
        List<GenericPage> genericPageList = jdbcTemplate.query(SQL_GET_ALL_ENABLED_GENERIC_PAGE,
                new GenericPageRowMapper());
        return associateCategorysWithGenericPages(genericPageList);
    }
    private static final String SQL_GET_ALL_DISABLED_GENERIC_PAGE
            = "SELECT gp.PageId, gp.CategoryId, gp.Title, gp.PageText, "
            + "gp.Enabled, gp.StartDate, gp.EndDate FROM GenericPage gp "
            + " inner join Category c on gp.categoryId = c.categoryId "
            + "where enabled = false "
            + "order by c.Category asc;";

    @Override
    public List<GenericPage> getAllDisabledGenericPages() {
        List<GenericPage> genericPageList = jdbcTemplate.query(SQL_GET_ALL_DISABLED_GENERIC_PAGE,
                new GenericPageRowMapper());
        return associateCategorysWithGenericPages(genericPageList);
    }

}
