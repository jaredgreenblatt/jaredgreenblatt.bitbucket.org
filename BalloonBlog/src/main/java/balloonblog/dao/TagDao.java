/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balloonblog.dao;

import balloonblog.dto.Tag;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface TagDao {
        
      // add the given Tag to the data store
    public void addTag(Tag tag);
    
    // remove the Tag with the give id from the data store
    public void removeTag(int tagId) throws TagDeleteException;
    
    //update the given Tag in the data store
    public void updateTag(Tag tag );
    
    // retrieve all Tag from the data store
    public List<Tag> getAllTags();
    
    // retrieve the Tag with the given id from the data store.
    public Tag getTagById(int tagId);

    
    
}
