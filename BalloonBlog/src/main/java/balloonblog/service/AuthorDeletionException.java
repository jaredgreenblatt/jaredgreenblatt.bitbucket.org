/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balloonblog.service;

/**
 *
 * @author apprentice
 */
public class AuthorDeletionException extends Exception {

    public AuthorDeletionException(String message) {
        super(message);
    }

    public AuthorDeletionException(String message,
            Throwable cause) {
        super(message, cause);
    }

}
