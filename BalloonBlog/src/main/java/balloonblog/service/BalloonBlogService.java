/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balloonblog.service;

import balloonblog.dao.CategoryDeleteException;
import balloonblog.dao.TagDeleteException;
import balloonblog.dto.Author;
import balloonblog.dto.Blog;
import balloonblog.dto.Category;
import balloonblog.dto.Comment;
import balloonblog.dto.GenericPage;
import balloonblog.dto.Tag;
import balloonblog.dto.User;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface BalloonBlogService {

    // Author DAO Methods
    // add the given Author to the data store
    public void addAuthor(Author author);

    // remove the Author with the give id from the data store
    public void removeAuthor(int authorId) throws AuthorDeletionException;

    //update the given Author in the data store
    public void updateAuthor(Author author);

    // retrieve all Author from the data store
    public List<Author> getAllAuthors();

    // retrieve the Author with the given id from the data store.
    public Author getAuthorById(int authorId);

    // Blog DAO Methods
    // add the given Blog to the data store
    public void addBlog(Blog blog);

    // remove the Blog with the give id from the data store
    public void removeBlog(int blogId);

    //update the blog Author in the data store
    public void updateBlog(Blog blog);

    // retrieve all Blog from the data store
    public List<Blog> getAllBlogs();

    // retrieve the Blog with the given id from the data store.
    public Blog getBlogById(int blogId);

    // retrieve all enabled Blogs from the data store by date descending
    public List<Blog> getAllEnabledBlogsByDate();

    // retrieve all Blogs from the data store with enabled equal to false.
    public List<Blog> getAllBlogsToApprove();

    // Category DAO Methods
    // add the given Category to the data store
    public void addCategory(Category category);

    // remove the Category with the give id from the data store
    public void removeCategory(int categoryId) throws CategoryDeleteException;

    //update the given Category in the data store
    public void updateCategory(Category category);

    // retrieve all Category from the data store
    public List<Category> getAllCategory();

    // retrieve the Category with the given id from the data store.
    public Category getCategoryById(int categoryId);

    // Comment DAO Methods
    // add the given Comment to the data store
    public void addComment(Comment comment, int blogId);

    // remove the Comment with the give id from the data store
    public void removeComment(int commentId);

    //update the given Comment in the data store
    public void updateComment(Comment comment);

    // retrieve all Comment from the data store
    public List<Comment> getAllComments();

    // retrieve the Comment with the given id from the data store.
    public Comment getCommentById(int commentId);

    // retrieve the Comment with the given blog id from the data store.
    public List<Comment> getCommentByBlogId(int blogId);

    // Generic DAO Methods
    // add the given  GenericPage to the data store
    public void addGenericPage(GenericPage genericPage);

    // remove the  GenericPage with the give id from the data store
    public void removeGenericPage(int pageId);

    //update the given  GenericPage in the data store
    public void updateGenericPage(GenericPage genericPage);

    // retrieve all  GenericPage from the data store
    public List<GenericPage> getAllGenericPages();

    // retrieve the  GenericPage with the given id from the data store.
    public GenericPage getGenericPageById(int pageId);

    // retrieve all  enabled GenericPage from the data store
    public List<GenericPage> getAllEnabledGenericPages();

    // retrieve all  disabled GenericPage from the data store
    public List<GenericPage> getAllDisabledGenericPages();

    // Tag DAO Methods
    // add the given Tag to the data store
    public void addTag(Tag tag);

    // remove the Tag with the give id from the data store
    public void removeTag(int tagId) throws TagDeleteException;

    //update the given Tag in the data store
    public void updateTag(Tag tag);

    // retrieve all Tag from the data store
    public List<Tag> getAllTags();

    // retrieve the Tag with the given id from the data store.
    public Tag getTagById(int tagId);

    //UserDao
    public User addUser(User newUser);

    public void deleteUser(String username);

    public List<User> getAllUsers();

}
