/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balloonblog.service;

import balloonblog.dao.AuthorDao;
import balloonblog.dao.BlogDao;
import balloonblog.dao.CategoryDao;
import balloonblog.dao.CategoryDeleteException;
import balloonblog.dao.CommentDao;
import balloonblog.dao.GenericPageDao;
import balloonblog.dao.TagDao;
import balloonblog.dao.TagDeleteException;
import balloonblog.dao.UserDao;
import balloonblog.dto.Author;
import balloonblog.dto.Blog;
import balloonblog.dto.Category;
import balloonblog.dto.Comment;
import balloonblog.dto.GenericPage;
import balloonblog.dto.Tag;
import balloonblog.dto.User;
import java.time.LocalDate;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class BalloonBlogServiceImpl implements BalloonBlogService {

    BlogDao blogDao;
    AuthorDao authorDao;
    CategoryDao categoryDao;
    CommentDao commentDao;
    TagDao tagDao;
    GenericPageDao genericPageDao;
    UserDao userDao;

    public BalloonBlogServiceImpl(BlogDao blogDao, AuthorDao authorDao,
            CategoryDao categoryDao, CommentDao commentDao, TagDao tagDao,
            GenericPageDao genericPageDao, UserDao userDao) {

        this.blogDao = blogDao;
        this.authorDao = authorDao;
        this.categoryDao = categoryDao;
        this.commentDao = commentDao;
        this.tagDao = tagDao;
        this.genericPageDao = genericPageDao;
        this.userDao = userDao;

    }

    @Override
    public void addAuthor(Author author) {
        authorDao.addAuthor(author);
    }

    @Override
    public void removeAuthor(int authorId) throws AuthorDeletionException {

        List<Blog> blogList = blogDao.getBlogsByAuthorId(authorId);
        Author author = authorDao.getAuthorById(authorId);
        if (blogList.size() > 0) {
            throw new AuthorDeletionException(
                    "ERROR: Author Cannot Be Deleted blogs are assoicated with "
                    +author.getDisplayName()+"."
            );
        }

        authorDao.removeAuthor(authorId);
    }

    @Override
    public void updateAuthor(Author author) {
        authorDao.updateAuthor(author);
    }

    @Override
    public List<Author> getAllAuthors() {
        return authorDao.getAllAuthors();
    }

    @Override
    public Author getAuthorById(int authorId) {
        return authorDao.getAuthorById(authorId);
    }

    @Override
    public void addBlog(Blog blog) {
        blogDao.addBlog(blog);
    }

    @Override
    public void removeBlog(int blogId) {
        blogDao.removeBlog(blogId);
    }

    @Override
    public void updateBlog(Blog blog) {
        blogDao.updateBlog(blog);
    }

    @Override
    public List<Blog> getAllBlogs() {
        return blogDao.getAllBlogs();
    }

    @Override
    public Blog getBlogById(int blogId) {
        return blogDao.getBlogById(blogId);
    }

    @Override
    public List<Blog> getAllEnabledBlogsByDate() {
        return blogDao.getAllEnabledBlogsByDate();
    }

    @Override
    public List<Blog> getAllBlogsToApprove() {
        return blogDao.getBlogsToApprove();
    }

    @Override
    public void addCategory(Category category) {
        categoryDao.addCategory(category);
    }

    @Override
    public void removeCategory(int categoryId) throws CategoryDeleteException {
        categoryDao.removeCategory(categoryId);
    }

    @Override
    public void updateCategory(Category category) {
        categoryDao.updateCategory(category);
    }

    @Override
    public List<Category> getAllCategory() {
        return categoryDao.getAllCategory();
    }

    @Override
    public Category getCategoryById(int categoryId) {
        return categoryDao.getCategoryById(categoryId);
    }

    @Override
    public void addComment(Comment comment, int blogId) {
        LocalDate currentDate;
        currentDate = LocalDate.now();
        // String commentDate = currentDate.format(DateTimeFormatter.ISO_DATE);

        commentDao.addComment(comment, blogId, currentDate);
    }

    @Override
    public void removeComment(int commentId) {
        commentDao.removeComment(commentId);
    }

    @Override
    public void updateComment(Comment comment) {
        commentDao.updateComment(comment);
    }

    @Override
    public List<Comment> getAllComments() {
        return commentDao.getAllComments();
    }

    @Override
    public Comment getCommentById(int commentId) {
        return commentDao.getCommentById(commentId);
    }

    @Override
    public List<Comment> getCommentByBlogId(int blogId) {
        return commentDao.getCommentByBlogId(blogId);
    }

    @Override
    public void addGenericPage(GenericPage genericPage) {
        genericPageDao.addGenericPage(genericPage);
    }

    @Override
    public void removeGenericPage(int pageId) {
        genericPageDao.removeGenericPage(pageId);
    }

    @Override
    public void updateGenericPage(GenericPage genericPage) {
        genericPageDao.updateGenericPage(genericPage);
    }

    @Override
    public List<GenericPage> getAllGenericPages() {
        return genericPageDao.getAllGenericPages();
    }

    @Override
    public GenericPage getGenericPageById(int pageId) {
        return genericPageDao.getGenericPageById(pageId);
    }

    @Override
    public List<GenericPage> getAllEnabledGenericPages() {
        return genericPageDao.getAllEnabledGenericPages();
    }

    @Override
    public List<GenericPage> getAllDisabledGenericPages() {
        return genericPageDao.getAllDisabledGenericPages();
    }

    @Override
    public void addTag(Tag tag) {
        tagDao.addTag(tag);
    }

    @Override
    public void removeTag(int tagId) throws TagDeleteException {
        tagDao.removeTag(tagId);
    }

    @Override
    public void updateTag(Tag tag) {
        tagDao.updateTag(tag);
    }

    @Override
    public List<Tag> getAllTags() {
        return tagDao.getAllTags();
    }

    @Override
    public Tag getTagById(int tagId) {
        return tagDao.getTagById(tagId);
    }

    @Override
    public User addUser(User newUser) {
        return userDao.addUser(newUser);
    }

    @Override
    public void deleteUser(String username) {
        userDao.deleteUser(username);
    }

    @Override
    public List<User> getAllUsers() {
        return userDao.getAllUsers();
    }

}
