/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balloonblog;

import balloonblog.dto.Category;
import balloonblog.dto.GenericPage;
import balloonblog.dto.Tag;
import balloonblog.service.BalloonBlogService;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import static jdk.nashorn.internal.objects.Global.getDate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class PageController {

    BalloonBlogService service;

    @Inject
    public PageController(BalloonBlogService service) {
        this.service = service;
    }

    @RequestMapping(value = "/createPage", method = RequestMethod.GET)
    public String createPage(Model model) {
        List<Category> categoryList = service.getAllCategory();

        model.addAttribute("categoryList", categoryList);

        // Adding SideMenu Content
        List<GenericPage> pageListEnabled = service.getAllEnabledGenericPages();
        model.addAttribute("pageListEnabled", pageListEnabled);
        List<Category> catList = service.getAllCategory();
        model.addAttribute("catList", catList);

        return "createPage";
    }

    @RequestMapping(value = "/createPage", method = RequestMethod.POST)
    public String createPage(HttpServletRequest request, Model model) {

        GenericPage genericPage = new GenericPage();
        genericPage.setTitle(request.getParameter("title"));
        genericPage.setPageText(request.getParameter("pageText"));
        genericPage.setEnabled(true);
        String categoryIdString = request.getParameter("category");
        int categoryId = Integer.parseInt(categoryIdString);
        Category category = service.getCategoryById(categoryId);
        genericPage.setCategory(category);
        String getStartDate = request.getParameter("startDate");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate startDate = LocalDate.parse(getStartDate, formatter);
        genericPage.setStartDate(startDate);
        String getEndDate = request.getParameter("endDate");
        LocalDate endDate = LocalDate.parse(getEndDate, formatter);
        genericPage.setEndDate(endDate);

        String enabled = request.getParameter("pageEnable");

        if (enabled != null && !enabled.isEmpty()) {
            if (enabled.equals("yes")) {
                genericPage.setEnabled(true);
            } else {
                genericPage.setEnabled(false);
            }
            // doSomething
        } else {
            genericPage.setEnabled(false);
        }

        service.addGenericPage(genericPage);

        List<GenericPage> pageListEnabled = service.getAllEnabledGenericPages();

        model.addAttribute("pageListEnabled", pageListEnabled);

        List<Category> catList = service.getAllCategory();
        model.addAttribute("catList", catList);

        return "redirect:/";
    }

    @RequestMapping(value = "/displayPages", method = RequestMethod.GET)
    public String displayPageList(HttpServletRequest request, Model model) {
        List<GenericPage> pageListEnabled = service.getAllEnabledGenericPages();

        model.addAttribute("pageListEnabled", pageListEnabled);

        List<Category> catList = service.getAllCategory();

        model.addAttribute("catList", catList);

        return "displayPage";
    }

    @RequestMapping(value = "/displayPage", method = RequestMethod.GET)
    public String displayPage(HttpServletRequest request, Model model) {

        String pageIdString = request.getParameter("pageId");
        int pageId = Integer.parseInt(pageIdString);
        GenericPage genericPage = service.getGenericPageById(pageId);

//        Adding SideMenu Content
        model.addAttribute("genericPage", genericPage);
        List<GenericPage> pageListEnabled = service.getAllEnabledGenericPages();
        model.addAttribute("pageListEnabled", pageListEnabled);
        List<Category> catList = service.getAllCategory();
        model.addAttribute("catList", catList);

        return "displayPage";
    }

    @RequestMapping(value = "/sideMenu", method = RequestMethod.GET)
    public String displaysideMenu(HttpServletRequest request, Model model) {

        List<GenericPage> pageListEnabled = service.getAllEnabledGenericPages();

        model.addAttribute("pageListEnabled", pageListEnabled);

        List<Category> catList = service.getAllCategory();

        model.addAttribute("catList", catList);

        return "sideMenu";
    }

    @RequestMapping(value = "/staticPageAdmin", method = RequestMethod.GET)
    public String displaystaticPageAdmin(HttpServletRequest request, Model model) {

        List<GenericPage> pageListEnabled = service.getAllEnabledGenericPages();
        model.addAttribute("pageListEnabled", pageListEnabled);
        List<GenericPage> pageListDisabled = service.getAllDisabledGenericPages();
        model.addAttribute("pageListDisabled", pageListDisabled);

        model.addAttribute("pageListEnabled", pageListEnabled);
        List<GenericPage> pageList = service.getAllGenericPages();

        model.addAttribute("pageList", pageList);

        List<Category> catList = service.getAllCategory();

        model.addAttribute("catList", catList);

        return "pageAdmin";
    }

    @RequestMapping(value = "/deletePage", method = RequestMethod.GET)
    public String deletePage(HttpServletRequest request, Model model) {

        String pageIdString = request.getParameter("pageId");
        int pageId = Integer.parseInt(pageIdString);
        service.removeGenericPage(pageId);

        List<GenericPage> pageListEnabled = service.getAllEnabledGenericPages();
        model.addAttribute("pageListEnabled", pageListEnabled);
        List<GenericPage> pageListDisabled = service.getAllDisabledGenericPages();
        model.addAttribute("pageListDisabled", pageListDisabled);

        model.addAttribute("pageListEnabled", pageListEnabled);
        List<GenericPage> pageList = service.getAllGenericPages();

        model.addAttribute("pageList", pageList);

        List<Category> catList = service.getAllCategory();

        model.addAttribute("catList", catList);

        return "pageAdmin";
    }

    @RequestMapping(value = "/enablePage", method = RequestMethod.GET)
    public String enablePage(HttpServletRequest request, Model model) {

        String pageIdString = request.getParameter("pageId");
        int pageId = Integer.parseInt(pageIdString);
        GenericPage genericPage = service.getGenericPageById(pageId);
        genericPage.setEnabled(true);
        service.updateGenericPage(genericPage);

        List<GenericPage> pageListEnabled = service.getAllEnabledGenericPages();
        model.addAttribute("pageListEnabled", pageListEnabled);
        List<GenericPage> pageListDisabled = service.getAllDisabledGenericPages();
        model.addAttribute("pageListDisabled", pageListDisabled);

        model.addAttribute("pageListEnabled", pageListEnabled);
        List<GenericPage> pageList = service.getAllGenericPages();

        model.addAttribute("pageList", pageList);

        List<Category> catList = service.getAllCategory();

        model.addAttribute("catList", catList);

        return "pageAdmin";
    }

    @RequestMapping(value = "/disablePage", method = RequestMethod.GET)
    public String disablePage(HttpServletRequest request, Model model) {

        String pageIdString = request.getParameter("pageId");
        int pageId = Integer.parseInt(pageIdString);
        GenericPage genericPage = service.getGenericPageById(pageId);
        genericPage.setEnabled(false);
        service.updateGenericPage(genericPage);

        List<GenericPage> pageListEnabled = service.getAllEnabledGenericPages();
        model.addAttribute("pageListEnabled", pageListEnabled);
        List<GenericPage> pageListDisabled = service.getAllDisabledGenericPages();
        model.addAttribute("pageListDisabled", pageListDisabled);

        model.addAttribute("pageListEnabled", pageListEnabled);
        List<GenericPage> pageList = service.getAllGenericPages();

        model.addAttribute("pageList", pageList);

        List<Category> catList = service.getAllCategory();

        model.addAttribute("catList", catList);

        return "pageAdmin";
    }

    @RequestMapping(value = "/editPage", method = RequestMethod.GET)
    public String editPage(HttpServletRequest request, Model model) {

        String pageIdString = request.getParameter("pageId");
        int pageId = Integer.parseInt(pageIdString);
        GenericPage genericPage = service.getGenericPageById(pageId);
        model.addAttribute("genericPage", genericPage);
        List<Category> modifiedCategoryList = service.getAllCategory();
        Category cg = genericPage.getCategory();
        modifiedCategoryList.remove(cg);
        model.addAttribute("modifiedCategoryList", modifiedCategoryList);

        List<GenericPage> pageListEnabled = service.getAllEnabledGenericPages();
        model.addAttribute("pageListEnabled", pageListEnabled);
        List<GenericPage> pageListDisabled = service.getAllDisabledGenericPages();
        model.addAttribute("pageListDisabled", pageListDisabled);

        model.addAttribute("pageListEnabled", pageListEnabled);
        List<GenericPage> pageList = service.getAllGenericPages();

        model.addAttribute("pageList", pageList);

        List<Category> categoryList = service.getAllCategory();

        model.addAttribute("categoryList", categoryList);

        return "editPage";
    }

    @RequestMapping(value = "/editPage", method = RequestMethod.POST)
    public String makePageEdit(HttpServletRequest request, Model model) {

        String pageIdString = request.getParameter("pageId");
        int pageId = Integer.parseInt(pageIdString);
        GenericPage genericPage = service.getGenericPageById(pageId);
        genericPage.setTitle(request.getParameter("title"));
        genericPage.setPageText(request.getParameter("pageText"));
        genericPage.setEnabled(true);
        String categoryIdString = request.getParameter("category");
        int categoryId = Integer.parseInt(categoryIdString);
        Category category = service.getCategoryById(categoryId);
        genericPage.setCategory(category);
        String getStartDate = request.getParameter("startDate");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate startDate = LocalDate.parse(getStartDate, formatter);
        genericPage.setStartDate(startDate);
        String getEndDate = request.getParameter("endDate");
        LocalDate endDate = LocalDate.parse(getEndDate, formatter);
        genericPage.setEndDate(endDate);
        String enabled = request.getParameter("pageEnable");
        if (enabled.equals("yes")) {
            genericPage.setEnabled(true);
        } else {
            genericPage.setEnabled(false);
        }
        service.updateGenericPage(genericPage);
        List<GenericPage> pageListEnabled = service.getAllEnabledGenericPages();
        model.addAttribute("pageListEnabled", pageListEnabled);
        List<GenericPage> pageListDisabled = service.getAllDisabledGenericPages();
        model.addAttribute("pageListDisabled", pageListDisabled);

        model.addAttribute("pageListEnabled", pageListEnabled);
        List<GenericPage> pageList = service.getAllGenericPages();

        model.addAttribute("pageList", pageList);

        List<Category> catList = service.getAllCategory();

        model.addAttribute("catList", catList);

        return "pageAdmin";
    }

    @RequestMapping(value = "/tag", method = RequestMethod.GET)
    public String displayTag(Model model) {
        List<Category> categoryList = service.getAllCategory();

        model.addAttribute("categoryList", categoryList);

        // Adding SideMenu Content
        List<GenericPage> pageListEnabled = service.getAllEnabledGenericPages();
        model.addAttribute("pageListEnabled", pageListEnabled);
        List<Category> catList = service.getAllCategory();
        model.addAttribute("catList", catList);
        List<Tag> tagList = service.getAllTags();
        model.addAttribute("tagList", tagList);
        return "tag";
    }

    @RequestMapping(value = "/category", method = RequestMethod.GET)
    public String displayCategory(Model model) {
        List<Category> categoryList = service.getAllCategory();

        model.addAttribute("categoryList", categoryList);

        // Adding SideMenu Content
        List<GenericPage> pageListEnabled = service.getAllEnabledGenericPages();
        model.addAttribute("pageListEnabled", pageListEnabled);
        List<Category> catList = service.getAllCategory();
        model.addAttribute("catList", catList);

        return "category";
    }

    @RequestMapping(value = "/author", method = RequestMethod.GET)
    public String displayAuthor(Model model) {
        List<Category> categoryList = service.getAllCategory();

        model.addAttribute("categoryList", categoryList);

        // Adding SideMenu Content
        List<GenericPage> pageListEnabled = service.getAllEnabledGenericPages();
        model.addAttribute("pageListEnabled", pageListEnabled);
        List<Category> catList = service.getAllCategory();
        model.addAttribute("catList", catList);

        return "author";
    }

}
