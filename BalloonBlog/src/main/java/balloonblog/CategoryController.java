/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balloonblog;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class CategoryController {
    
        @RequestMapping(value = "/displayCategoryPage", method = RequestMethod.GET)
    public String displayCategoryPage(HttpServletRequest request, Model model) {

        return "category";
    }    
    
}
