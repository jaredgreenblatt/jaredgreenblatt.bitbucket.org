/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balloonblog;

import balloonblog.dto.Author;
import balloonblog.dto.Blog;
import balloonblog.dto.Category;
import balloonblog.dto.Comment;
import balloonblog.dto.GenericPage;
import balloonblog.dto.Tag;
import balloonblog.service.BalloonBlogServiceImpl;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.swing.JOptionPane;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class BlogController {

    BalloonBlogServiceImpl sl;

    @Inject
    public BlogController(BalloonBlogServiceImpl service) {
        this.sl = service;
    }

    @RequestMapping(value = "/createBlog", method = RequestMethod.GET)
    public String displayBlog(Model model) {

        List<Category> categoryList = sl.getAllCategory();
        model.addAttribute("categoryList", categoryList);

        List<Tag> tagList = sl.getAllTags();
        model.addAttribute("tagList", tagList);

        List<Author> authorList = sl.getAllAuthors();
        model.addAttribute("authorList", authorList);

        // Adding SideMenu Content
        List<GenericPage> pageListEnabled = sl.getAllEnabledGenericPages();
        model.addAttribute("pageListEnabled", pageListEnabled);
        List<Category> catList = sl.getAllCategory();
        model.addAttribute("catList", catList);

        return "createBlog";
    }

    @RequestMapping(value = "/createBlog", method = RequestMethod.POST)
    public String createBlog(HttpServletRequest request) {

        try {
            Blog blog = new Blog();
            blog.setTitle(request.getParameter("title"));
            blog.setArticle(request.getParameter("article"));

            String authorToGet = request.getParameter("createAuthor");
            int authorId = Integer.parseInt(authorToGet);
            Author author = sl.getAuthorById(authorId);
            blog.setAuthor(author);

            String categoryToGet = request.getParameter("category");
            int categoryId = Integer.parseInt(categoryToGet);
            Category category = sl.getCategoryById(categoryId);
            blog.setCategory(category);

            String[] tagArray = request.getParameterValues("tagList");
            List<Tag> tagList = new ArrayList<>();
            for (String currentTag : tagArray) {
                Tag tagToAdd = new Tag();
                int tagId = Integer.parseInt(currentTag);
                tagToAdd = sl.getTagById(tagId);
                tagList.add(tagToAdd);
            }
            blog.setTagList(tagList);

            String startDate = request.getParameter("startDate");
            LocalDate startDateToAdd = LocalDate.parse(startDate);
            blog.setStartDate(startDateToAdd);

            String endDate = request.getParameter("endDate");
            LocalDate endDateToAdd = LocalDate.parse(endDate);
            blog.setEndDate(endDateToAdd);

            String enabled = request.getParameter("enableBlog");
            if (enabled != null && !enabled.isEmpty()) {
                if (enabled.equals("yes")) {
                    blog.setEnabled(true);
                } else {
                    blog.setEnabled(false);
                }
                // doSomething
            } else {
                blog.setEnabled(false);
            }

            String featured = request.getParameter("featureBlog");
            if (featured.equals("yes")) {
                blog.setFeatured(true);
            } else {
                blog.setFeatured(false);
            }

            sl.addBlog(blog);

            return "redirect:/";

        } catch (NullPointerException e) {

            return "redirect:createBlog";

        }

    }

    @RequestMapping(value = "/deleteBlog", method = RequestMethod.GET)
    public String deleteBlog(HttpServletRequest request
    ) {
        String blogIdString = request.getParameter("blogId");
        int blogId = Integer.parseInt(blogIdString);
        sl.removeBlog(blogId);
        return "redirect:/";
    }

    @RequestMapping(value = "/displayBlog", method = RequestMethod.GET)
    public String displayBlog(HttpServletRequest request, Model model
    ) {

        String blogIdString = request.getParameter("blogId");
        int blogId = Integer.parseInt(blogIdString);
        Blog blog = sl.getBlogById(blogId);

        model.addAttribute("blogPage", blog);

        // Adding SideMenu Content
        List<GenericPage> pageListEnabled = sl.getAllEnabledGenericPages();
        model.addAttribute("pageListEnabled", pageListEnabled);
        List<Category> catList = sl.getAllCategory();
        model.addAttribute("catList", catList);

        return "displayBlog";
    }

    @RequestMapping(value = "/displayEditBlogForm", method = RequestMethod.GET)
    public String displayEditBlogForm(HttpServletRequest request, Model model
    ) {
        String blogIdParameter = request.getParameter("blogId");
        int blogId = Integer.parseInt(blogIdParameter);
        Blog blog = sl.getBlogById(blogId);
        model.addAttribute("blog", blog);

        // Get All Tags from the Service Layer
        List<Tag> tagList = sl.getAllTags();
        //Get just the tags associated with the blog
        List<Tag> blogTags = blog.getTagList();

        for (Tag currentTag : blogTags) {
            tagList.remove(currentTag);
        }

        // Get Category from the Service Layer
        Category currentCategory = blog.getCategory();
        //Get Author for Sighting.
        Author currentAuthor = blog.getAuthor();

        List<Author> authorList = sl.getAllAuthors();
        authorList.remove(currentAuthor);
        model.addAttribute("authorList", authorList);

        List<Category> categoryList = sl.getAllCategory();
        categoryList.remove(currentCategory);
        model.addAttribute("categoryList", categoryList);

        //Put the list of Tags on the Model
        model.addAttribute("tagList", tagList);
        //Put the list of current selected tags on model.
        model.addAttribute("blogTags", blogTags);

        //Put the Category and Author on the Model
        model.addAttribute("currentCategory", currentCategory);
        model.addAttribute("currentAuthor", currentAuthor);

        Boolean enabled = blog.getEnabled();
        model.addAttribute("enabled", enabled);

        Boolean featured = blog.getFeatured();
        model.addAttribute("featured", featured);

        // Adding SideMenu Content
        List<GenericPage> pageListEnabled = sl.getAllEnabledGenericPages();
        model.addAttribute("pageListEnabled", pageListEnabled);
        List<Category> catList = sl.getAllCategory();
        model.addAttribute("catList", catList);

        return "editBlog";
    }

    @RequestMapping(value = "/editBlog", method = RequestMethod.POST)
    public String editBlog(HttpServletRequest request, Model model
    ) {

        try {
            Blog blog = new Blog();
            blog.setTitle(request.getParameter("title"));
            blog.setArticle(request.getParameter("article"));

            String authorToGet = request.getParameter("author");
            int authorId = Integer.parseInt(authorToGet);
            Author author = sl.getAuthorById(authorId);
            blog.setAuthor(author);

            String categoryToGet = request.getParameter("category");
            int categoryId = Integer.parseInt(categoryToGet);
            Category category = sl.getCategoryById(categoryId);
            blog.setCategory(category);

            String[] tagArray = request.getParameterValues("tag");
            List<Tag> tagList = new ArrayList<>();
            for (String currentTag : tagArray) {
                Tag tagToAdd = new Tag();
                int tagId = Integer.parseInt(currentTag);
                tagToAdd = sl.getTagById(tagId);
                tagList.add(tagToAdd);
            }
            blog.setTagList(tagList);

            String startDate = request.getParameter("startDate");
            LocalDate startDateToAdd = LocalDate.parse(startDate);
            blog.setStartDate(startDateToAdd);

            String endDate = request.getParameter("endDate");
            LocalDate endDateToAdd = LocalDate.parse(endDate);
            blog.setEndDate(endDateToAdd);

            String enabled = request.getParameter("enableBlog");
            if (enabled.equals("yes")) {
                blog.setEnabled(true);
            } else {
                blog.setEnabled(false);
            }

            String featured = request.getParameter("featureBlog");
            if (featured.equals("yes")) {
                blog.setFeatured(true);
            } else {
                blog.setFeatured(false);
            }

            String blogToInt = request.getParameter("blogId").toString();
            int BlogId = Integer.parseInt(blogToInt);

            blog.setBlogId(BlogId);

            sl.updateBlog(blog);

            return "redirect:/";

        } catch (NullPointerException e) {
            JOptionPane.showMessageDialog(null, "Error", "Error",
                    JOptionPane.ERROR_MESSAGE);
            return "redirect:editBlog";

        }

    }

}
