/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package balloonblog;

import balloonblog.dao.CategoryDeleteException;
import balloonblog.dao.TagDeleteException;
import balloonblog.dto.Author;
import balloonblog.dto.Blog;
import balloonblog.dto.Category;
import balloonblog.dto.Comment;
import balloonblog.dto.GenericPage;
import balloonblog.dto.Tag;
import balloonblog.service.AuthorDeletionException;
import balloonblog.service.BalloonBlogService;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author apprentice
 */
@CrossOrigin
@Controller
public class RestController {

    private BalloonBlogService service;

    @Inject
    public RestController(BalloonBlogService service) {
        this.service = service;
    }

    /*
   ******************************************************************************
   ******************************************************************************
   Generic Page Methods
   ******************************************************************************
   ******************************************************************************
   ******************************************************************************

     */
    //  GetById
    @RequestMapping(value = "/restPage/{id}", method = RequestMethod.GET)
    @ResponseBody
    public GenericPage getGenericPage(@PathVariable("id") int id) {
        return service.getGenericPageById(id);
    }

    // GetAll
    @RequestMapping(value = "/restPage", method = RequestMethod.GET)
    @ResponseBody
    public List<GenericPage> getAllPages() {
        return service.getAllGenericPages();
    }

    // GetAllEnabled
    @RequestMapping(value = "/restPageEnabled", method = RequestMethod.GET)
    @ResponseBody
    public List<GenericPage> getAllEnabledPages() {
        return service.getAllEnabledGenericPages();
    }
    // GetAllDisabled

    @RequestMapping(value = "/restPageDisabled", method = RequestMethod.GET)
    @ResponseBody
    public List<GenericPage> getAllDisabledPages() {
        return service.getAllEnabledGenericPages();
    }

    // Create 
    @RequestMapping(value = "/restPage", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public void createSuperPage(@RequestBody GenericPage genericPage) {
        service.addGenericPage(genericPage);
    }
    // Remove

    @RequestMapping(value = "/restPage/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletePage(@PathVariable("id") int id) {
        service.removeGenericPage(id);
    }

    // edit 
    @RequestMapping(value = "/restPage/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateRestPage(@PathVariable("id") int id, @RequestBody GenericPage genericPage) {
        // favor the path variable over the id in the object if they differ
        genericPage.setPageId(id);
        service.updateGenericPage(genericPage);
    }

    /*
   ******************************************************************************
   ******************************************************************************
   Author Page Methods
   ******************************************************************************
   ******************************************************************************
   ******************************************************************************

     */
    //  GetById
    @RequestMapping(value = "/restAuthor/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Author getAuthor(@PathVariable("id") int id) {
        return service.getAuthorById(id);
    }

    // GetAll
    @RequestMapping(value = "/restAuthor", method = RequestMethod.GET)
    @ResponseBody
    public List<Author> getAllAuthors() {
        return service.getAllAuthors();
    }

    // Create 
    @RequestMapping(value = "/restAuthor", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public void createAuthor(@RequestBody Author author) {
        service.addAuthor(author);
    }
    // Remove

    @RequestMapping(value = "/restAuthor/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity deleteAuthor(@PathVariable("id") int id) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("MyResponseHeader", "MyValue");
        try {
            service.removeAuthor(id);
            return new ResponseEntity<>("Ok Request", responseHeaders, HttpStatus.OK);
        } catch (AuthorDeletionException ex) {
            String errorMessageOutput = "Author cannot be deleted they are related to live blog content.";
            return new ResponseEntity<>(errorMessageOutput, responseHeaders, HttpStatus.BAD_REQUEST);
        }
    }

    // edit 
    @RequestMapping(value = "/restAuthor/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateAuthor(@PathVariable("id") int id, @RequestBody Author author) {
        // favor the path variable over the id in the object if they differ
        author.setAuthorId(id);
        service.updateAuthor(author);
    }

    /*
   ******************************************************************************
   ******************************************************************************
   Blog Page Methods
   ******************************************************************************
   ******************************************************************************
   ******************************************************************************

     */
    //  GetById
    @RequestMapping(value = "/restBlog/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Blog getBlog(@PathVariable("id") int id) {
        return service.getBlogById(id);
    }

    // GetAll
    @RequestMapping(value = "/restBlog", method = RequestMethod.GET)
    @ResponseBody
    public List<Blog> getAllBlogs() {
        return service.getAllBlogs();
    }

    // Create 
    @RequestMapping(value = "/restBlog", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public void createBlog(@RequestBody Blog blog) {
        service.addBlog(blog);
    }
    // Remove

    @RequestMapping(value = "/restBlog/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteBlog(@PathVariable("id") int id) {
        service.removeBlog(id);
    }

    // edit 
    @RequestMapping(value = "/restBlog/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateBlog(@PathVariable("id") int id, @RequestBody Blog blog) {
        // favor the path variable over the id in the object if they differ
        blog.setBlogId(id);
        service.updateBlog(blog);
    }

    /*
   ******************************************************************************
   ******************************************************************************
   Category Page Methods
   ******************************************************************************
   ******************************************************************************
   ******************************************************************************

     */
    //  GetById
    @RequestMapping(value = "/restCategory/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Category getCategory(@PathVariable("id") int id) {
        return service.getCategoryById(id);
    }

    // GetAll
    @RequestMapping(value = "/restCategory", method = RequestMethod.GET)
    @ResponseBody
    public List<Category> getAllCategorys() {
        return service.getAllCategory();
    }

    // Create 
    @RequestMapping(value = "/restCategory", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public void createCategory(@RequestBody Category category) {
        service.addCategory(category);
    }

    // Remove
    @RequestMapping(value = "/restCategory/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<String> deleteCategory(@PathVariable("id") int id) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("MyResponseHeader", "MyValue");

        try {
            service.removeCategory(id);
            return new ResponseEntity<String>("Ok Request", responseHeaders, HttpStatus.OK);

        } catch (CategoryDeleteException ex) {
            String errorMessageOutput = "Category cannot be deleted.";
            return new ResponseEntity<String>(errorMessageOutput, responseHeaders, HttpStatus.BAD_REQUEST);
        }

    }

    // edit 
    @RequestMapping(value = "/restCategory/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateCategory(@PathVariable("id") int id, @RequestBody Category category) {
        // favor the path variable over the id in the object if they differ
        category.setCategoryId(id);
        service.updateCategory(category);
    }

    /*
   ******************************************************************************
   ******************************************************************************
   Tag Page Methods
   ******************************************************************************
   ******************************************************************************
   ******************************************************************************

     */
    //  GetById
    @RequestMapping(value = "/restTag/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Tag getTag(@PathVariable("id") int id) {
        return service.getTagById(id);
    }

    // GetAll
    @RequestMapping(value = "/restTag", method = RequestMethod.GET)
    @ResponseBody
    public List<Tag> getAllTags() {
        return service.getAllTags();
    }

    // Create 
    @RequestMapping(value = "/restTag", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public void createTag(@RequestBody Tag tag) {
        service.addTag(tag);
    }
    // Remove

    @RequestMapping(value = "/restTag/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<String> deleteTag(@PathVariable("id") int id) {
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("MyResponseHeader", "MyValue");

        try {
            service.removeTag(id);
            return new ResponseEntity<String>("Ok Request", responseHeaders, HttpStatus.OK);

        } catch (TagDeleteException ex) {
            String errorMessageOutput = "Tag cannot be deleted.";
            return new ResponseEntity<String>(errorMessageOutput, responseHeaders, HttpStatus.BAD_REQUEST);
        }

    }

    // edit 
    @RequestMapping(value = "/restTag/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateTag(@PathVariable("id") int id, @RequestBody Tag tag) {
        // favor the path variable over the id in the object if they differ
        tag.setTagId(id);
        service.updateTag(tag);
    }

    /*
   ******************************************************************************
   ******************************************************************************
   Comment Page Methods
   ******************************************************************************
   ******************************************************************************
   ******************************************************************************

     */
    //  GetById
    @RequestMapping(value = "/restComment/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Comment getComment(@PathVariable("id") int id) {
        return service.getCommentById(id);
    }

    //  GetByBlogId
    @RequestMapping(value = "/restCommentByBlog/{id}", method = RequestMethod.GET)
    @ResponseBody
    public List<Comment> getCommentByBlog(@PathVariable("id") int id) {
        return service.getCommentByBlogId(id);
    }

    // GetAll
    @RequestMapping(value = "/restComment", method = RequestMethod.GET)
    @ResponseBody
    public List<Comment> getAllComments() {
        return service.getAllComments();
    }

    // Create 
    @RequestMapping(value = "/restComment", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public void createComment(@RequestBody Comment comment) {
        service.addComment(comment, comment.getBlogId());
    }
    // Remove

    @RequestMapping(value = "/restComment/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteComment(@PathVariable("id") int id) {
        service.removeComment(id);
    }

    // edit 
    @RequestMapping(value = "/restComment/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateComment(@PathVariable("id") int id, @RequestBody Comment comment) {
        // favor the path variable over the id in the object if they differ
        comment.setCommentId(id);
        service.updateComment(comment);
    }

}
