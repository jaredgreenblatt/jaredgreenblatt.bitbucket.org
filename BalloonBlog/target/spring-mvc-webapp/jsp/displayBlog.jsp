<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>${genericPage.title}</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/HomeBalloons.css" rel="stylesheet"> 
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png"> 
    </head>
    <body style="background-color: #e6ffff">
        <div class="row" id="navbar">
            <div class="col-xs-11">        
                <img class="float" src="http://www.clker.com/cliparts/2/Y/t/a/C/o/hot-air-balloon-outline.svg" alt="" />
                <img class=float1 src="http://www.clker.com/cliparts/2/Y/t/a/C/o/hot-air-balloon-outline.svg" alt="" />
            </div>
            <div class="col-xs-1">  
                <%@include file="sideMenu.jsp" %>
            </div>
        </div>


        <div class="col-xs-12" style="text-align: center" >

                <div class="container">
                    <div style="text-align: center" id="generic">
                        <h2 class="text-center">${blogPage.title}</h2>

                        <p>${blogPage.article}</p><br><br>

                        <i class="glyphicon glyphicon-user"></i> <c:out value="${blogPage.author.displayName}"/>
                        <i class="glyphicon glyphicon-folder-close"></i> <c:out value="${blogPage.category.category}" />
                        <i class="glyphicon glyphicon-time"></i> <c:out value="${blogPage.startDate}" /><br>
                        <i class="glyphicon glyphicon-tags"></i>
                        <c:forEach var="currentTag" items="${blogPage.tagList}" varStatus="loop">
                            <c:out value="${currentTag.tag}"/>
                            <c:if test="${!loop.last}">|</c:if>
                        </c:forEach> 
                        <br>
                        <c:if test="${blogPage.featured==true}"><i class="glyphicon glyphicon-star"></i>Featured Blog</c:if>
                        <br>
                        <div style="text-align: left" id="comment"></div>    

                    </div>



                    <div style="text-align: center" id="generic">

                        <div class="form-group">
                            <input type="text" class ="form-control" id="commentName" placeholder="Add Comment"/>
                            <input type="text" class ="form-control" id="commentAuthor" placeholder="Your Name"/>                        
                            <br>    
                            <input type="button" class="btn btn-default" value="Add Comment" onclick="addComment()"/>
                            <br>
                        </div>                    
                    </div>           
                </div>                    
                    
            </div>
            <input type="hidden" id="blogId" value="${blogPage.blogId}"/>

    <!-- Placed at the end of the document so the pages load faster -->
    <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/sidemenu.js"></script>
    <script src="${pageContext.request.contextPath}/js/comment.js"></script>
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=jupqfvd6xbt805vuo0c45oi7px2yoj56wncg8r5m0oimlg6c"></script>
    <script>tinymce.init({selector: 'textarea'});</script>


</body>
</html>

