



<div class="container">
    <div class="col-xs-offset-2 col-xs-8" style="text-align: center" >
        <h3 class = "text-center">Add Author</h3>
        <form class ="form-horizontal"
              role ="form"  role="form" id="add-tag-form">
            <div class="form-group">
                <label for="add-first-name" class="col-md-4 control-label">
                    First Name:
                </label>
                <div class ="col-md-8">
                    <input type="text" class ="form-control" id="firstNameSnip" placeholder="First Name" required />
                </div>
            </div>
            <div class="form-group">
                <label for="add-last-name" class="col-md-4 control-label">
                    Last Name:
                </label>
                <div class ="col-md-8">
                    <input type="text" class ="form-control" id="lastNameSnip" placeholder="First Name" required />
                </div>
            </div>
            <div class="form-group">
                <label for="add-display-name" class="col-md-4 control-label">
                    Display Name:
                </label>
                <div class ="col-xs--2 col-md-8">
                    <input type="text" class ="form-control" id="displayNameSnip" placeholder="Display Name" required />
                    <br>
                    <input class="col-xs-6" type="button" class="btn btn-default" value="Add Author" onclick="addAuthorSnip()"/>
                </div>
            </div>
            <br><br>
            <sec:authorize access="hasRole('ROLE_ADMIN')">
                <h3 class = "text-center">Remove Author</h3>
                <div class="form-group">
                    <label for="remove_author" class="col-xs-4 control-label">Display Name:</label>
                    <div class="col-md-8">
                        <select class="form-control" name="remA"  placeholder="removeAuthor" id="removeAuthorSnip">
                        </select>
                        <br>
                        <input class="col-xs-6"  type="button" class="btn btn-default" value="Remove Author" onclick="deleteAuthorSnip()"/>
                    </div>
                </div>
            </sec:authorize>

        </form>

    </div>
</div>
