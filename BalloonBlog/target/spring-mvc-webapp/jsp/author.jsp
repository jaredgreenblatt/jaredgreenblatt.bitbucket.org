<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>${genericPage.title}</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/HomeBalloons.css" rel="stylesheet"> 
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png"> 
    </head>
    <body style="background-color: #e6ffff">
        <div class="row" id="navbar">
            <div class="col-xs-11">        
                <img class="float" src="http://www.clker.com/cliparts/2/Y/t/a/C/o/hot-air-balloon-outline.svg" alt="" />
                <img class=float1 src="http://www.clker.com/cliparts/2/Y/t/a/C/o/hot-air-balloon-outline.svg" alt="" />
            </div>
            <div class="col-xs-1">  
                <%@include file="sideMenu.jsp" %>
            </div>
        </div>




        <div class="container">
            <div class="col-xs-offset-2 col-xs-8" style="text-align: center" >
                <h3 class = "text-center" id="errorMessages"></h3>
                <h3 class = "text-center">Add Author</h3>
                <form class ="form-horizontal"
                      role ="form"  role="form" id="add-tag-form">
                    <div class="form-group">
                        <label for="add-first-name" class="col-md-4 control-label">
                            First Name:
                        </label>
                        <div class ="col-md-8">
                            <input type="text" class ="form-control" id="firstName" placeholder="First Name" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-last-name" class="col-md-4 control-label">
                            Last Name:
                        </label>
                        <div class ="col-md-8">
                            <input type="text" class ="form-control" id="lastName" placeholder="First Name" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-display-name" class="col-md-4 control-label">
                            Display Name:
                        </label>
                        <div class ="col-xs--2 col-md-8">
                            <input type="text" class ="form-control" id="displayName" placeholder="Display Name" required />
                            <br>
                            <input class="col-xs-6" type="button" class="btn btn-default" value="Add Author" onclick="addAuthor()"/>
                        </div>
                    </div>
                    <br><br>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <h3 class = "text-center">Remove Author</h3>
                        <div class="form-group">
                            <label for="remove_author" class="col-xs-4 control-label">Display Name:</label>
                            <div class="col-md-8">
                                <select class="form-control" name="remA"  placeholder="removeAuthor" id="removeAuthor">
                                </select>
                                <br>
                                <input class="col-xs-6"  type="button" class="btn btn-default" value="Remove Author" onclick="deleteAuthor()"/>
                            </div>
                        </div>
                    </sec:authorize>

                </form>

            </div>
        </div>




        <!--            menu of generic pages.-->








    </div>


    <!-- Placed at the end of the document so the pages load faster -->
    <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/sidemenu.js"></script>
    <script src="${pageContext.request.contextPath}/js/author.js"></script>
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=jupqfvd6xbt805vuo0c45oi7px2yoj56wncg8r5m0oimlg6c"></script>
    <script>tinymce.init({selector: 'textarea'});</script>


</body>
</html>

