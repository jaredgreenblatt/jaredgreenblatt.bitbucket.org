<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Index Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
        <link href="${pageContext.request.contextPath}/css/HomeBalloons.css" rel="stylesheet"> 
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png"> 
    </head>
    <body>
        <div class='container'>
            <br><br><br><br><br>
            <div class="row col-xs-12">
                <div class="col-xs-10 " id="itemRow"></div>
                <div class="col-xs-2 " id="itemRow">
                </div>
                <div class='sky'>
                    <div class='cloud-1 cloud'>
                        <div class='cloud--inner'></div>
                    </div>
                    <div class='cloud-2 cloud'>
                        <div class='cloud--inner'></div>
                    </div>
                    <div class='cloud-3 cloud'>
                        <div class='cloud--inner'></div>
                    </div>
                </div>
                <sec:authorize access="hasRole('ROLE_USER')">
                <div class="col-xs-offset-10">
                    <h3>Hello : ${pageContext.request.userPrincipal.name}</h3>
                </div>
                </sec:authorize>
                <div id="mySidenav" class="sidenav">
                    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                    <a href="${pageContext.request.contextPath}/">Home</a>
                    <sec:authorize access="hasRole('ROLE_USER')">
                        <a href="${pageContext.request.contextPath}/createBlog">Create Blog</a>
                        <a href="${pageContext.request.contextPath}/createPage">Create Page</a>
                    </sec:authorize>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <a href="${pageContext.request.contextPath}/blogApproval">Approve Blog</a>
                        <a href="${pageContext.request.contextPath}/staticPageAdmin">Static Page Administration</a>

                    </sec:authorize>
                    <a href="${pageContext.request.contextPath}/login">Login</a>
                    <c:forEach var="currentCat" items="${catList}">
                        <nav class="navbar" id="item">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" 
                                    data-target="#${currentCat.categoryId}" aria-controls="navbarToggleExternalContent" 
                                    aria-expanded="false" aria-label="Toggle navigation">
                                <span>  <a>${currentCat.category}</a></span>
                            </button>
                        </nav>
                        <div class="pos-f-t">
                            <div class="collapse" id="${currentCat.categoryId}">
                                <c:forEach var="currentPage" items="${pageListEnabled}">
                                    <c:if test="${fn:contains(currentPage.category, currentCat.category)}">

                                        <div>
                                            <a href= "displayPage?pageId=${currentPage.pageId}">
                                                ${currentPage.title}</a>
                                        </div>

                                    </c:if>
                                </c:forEach>
                            </div>
                        </div>
                    </c:forEach>
                    <sec:authorize access="hasRole('ROLE_USER')">
                        <a href="${pageContext.request.contextPath}/category">Add Category</a>
                        <a href="${pageContext.request.contextPath}/tag">Add Tag</a>
                        <a href="${pageContext.request.contextPath}/author">Add Author</a>
                    </sec:authorize>
                    <c:if test="${pageContext.request.userPrincipal.name != null}">
                       
                             <a href="<c:url value="/j_spring_security_logout" />" > Logout</a>
                        </p>
                    </c:if>
                    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                </div>
                <span style="font-size:30px;cursor:pointer" onclick="openNav()">
                    <div class='ballon'>
                        <div class='ballon-inner'>
                            <div class='ballon-ball'>
                                <div class='ball-inner'>
                                    <div class='ball-inner--0'></div>
                                    <div class='ball-inner--1'></div>
                                    <div class='ball-inner--2'></div>
                                    <div class='ball-inner--3'></div>
                                    <div class='ball-inner--4'></div>
                                </div>
                            </div>
                            <div class='ballon-rope'></div>
                            <div class='ballon-seat'></div>
                        </div>
                    </div>
                </span>
            </div>
        </div>

        <div class="col-xs-offset-3 col-xs-6">
            <div id="listId">

                <ul class="list">
                    <c:forEach var="currentBlog" items="${blogList}">
                        <span>
                            <div style="text-align: center" id="generic">
                                <a href="displayBlog?blogId=${currentBlog.blogId}">
                                    <h1><c:out value="${currentBlog.title}"/></h1>
                                    <br><br>
                                </a>

                                ${currentBlog.article}<br><br>

                                <i class="glyphicon glyphicon-user"></i><c:out value="${currentBlog.author.displayName}"/>
                                <i class="glyphicon glyphicon-folder-close"></i><c:out value="${currentBlog.category.category}"/>
                                <i class="glyphicon glyphicon-time"></i><c:out value="${currentBlog.startDate}" /><br>
                                <i class="glyphicon glyphicon-tags"></i>
                                <c:forEach var="currentTag" items="${currentBlog.tagList}" varStatus="loop">
                                    <c:out value="${currentTag.tag}"/>
                                    <c:if test="${!loop.last}">|</c:if>
                                </c:forEach> 
                                <br>
                                <c:if test="${currentBlog.commentList.size() > 0}">
                                    <i class="glyphicon glyphicon-comment"></i><c:out value="${currentBlog.commentList.size()}" />
                                </c:if>                                
                                <c:if test="${currentBlog.featured==true}">
                                    <i class="glyphicon glyphicon-star"></i>Featured Blog
                                </c:if>

                                
                                <br>
                                <br>
                                    
                                <sec:authorize access="hasRole('ROLE_ADMIN')">
                                    <a href="displayEditBlogForm?blogId=${currentBlog.blogId}">Edit</a> | 
                                    <a href="deleteBlog?blogId=${currentBlog.blogId}">Delete</a>
                                </sec:authorize>

                            </div>
                        </span>
                    </c:forEach> 
                </ul>
                <ul class="pagination"></ul>
            </div>
        </div>


        <div class="row">
            <div class="col-xs-4" id="back">
                <div class='sky'>
                    <div class='cloud-1 cloud'>
                        <div class='cloud--inner'></div>
                    </div>
                    <div class='cloud-2 cloud'>
                        <div class='cloud--inner'></div>
                    </div>
                    <div class='cloud-3 cloud'>
                        <div class='cloud--inner'></div>
                    </div>
                </div>
            </div>
            <div class="col-xs-4" style="text-align: center" >
                <div class='sky'>
                    <div class='cloud-1 cloud'>
                        <div class='cloud--inner'></div>
                    </div>
                    <div class='cloud-2 cloud'>
                        <div class='cloud--inner'></div>
                    </div>
                    <div class='cloud-3 cloud'>
                        <div class='cloud--inner'></div>
                    </div>
                </div>


            </div>
            <div class="col-xs-4" id="back">
                <div class='sky'>
                    <div class='cloud-1 cloud'>
                        <div class='cloud--inner'></div>
                    </div>
                    <div class='cloud-2 cloud'>
                        <div class='cloud--inner'></div>
                    </div>
                    <div class='cloud-3 cloud'>
                        <div class='cloud--inner'></div>
                    </div>
                </div>
            </div>
        </div>



        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/sidemenu.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/pagination.js"></script>



    </body>
</html>

