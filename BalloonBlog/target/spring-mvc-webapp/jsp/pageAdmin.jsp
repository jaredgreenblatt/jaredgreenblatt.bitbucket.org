<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Static Page Administration</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/HomeBalloons.css" rel="stylesheet"> 
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png"> 
    </head>
    <body style="background-color: #e6ffff">
        <div class="row" id="navbar">
            <div class="col-xs-11">        
                <img class="float" src="http://www.clker.com/cliparts/2/Y/t/a/C/o/hot-air-balloon-outline.svg" alt="" />
                <img class=float1 src="http://www.clker.com/cliparts/2/Y/t/a/C/o/hot-air-balloon-outline.svg" alt="" />
            </div>
            <div class="col-xs-1">  
                <%@include file="sideMenu.jsp" %>
            </div>
        </div>




        <div class="container">


            <h2 class="text-center">Static Page Administration</h2>
            <div class="col-xs-6" style="text-align: center"><h3>Approved Pages</h3>
                <table id="approvedPages" class="table table-bordered" style="background-color: white ;border:solid; border-color: green">
                    <tr>
                        <th width="40%">Page Category</th>
                        <th width="30%">Page Name</th>
                        <th width="10%">Edit</th>
                        <th width="10%">Delete</th>
                        <th width="10%">Disable</th>
                    </tr>
                    <c:forEach var="aPages" items="${pageListEnabled}">
                        <tr>
                            <td>
                                <c:out value="${aPages.category.category}"/>
                            </td>
                            <td>
                                <a href= "displayPage?pageId=${aPages.pageId}">

                                    <c:out value="${aPages.title}"/>
                                </a>
                            </td>
                            <td>
                                <a href="editPage?pageId=${aPages.pageId}">
                                    Edit
                                </a>
                            </td>
                            <td>
                                <a href="deletePage?pageId=${aPages.pageId}">
                                    Delete
                                </a>
                            </td>
                            <td>
                                <a href="disablePage?pageId=${aPages.pageId}">
                                    Disable
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
            <div class="col-xs-6" style="text-align: center"><h3>Pages to Approve</h3>
                <table id="pagesToApprove" class="table table-bordered" style="background-color: white ;border:solid; border-color: red">
                    <tr>
                        <th width="40%">Page Category</th>
                        <th width="30%">Page Name</th>
                        <th width="10%">Edit</th>
                        <th width="10%">Delete</th>
                        <th width="10%">Enable</th>
                    </tr>
                    <c:forEach var="dPages" items="${pageListDisabled}">
                        <tr>
                            <td>
                                <c:out value="${dPages.category.category}"/>
                            </td>
                            <td>
                                <a href= "displayPage?pageId=${dPages.pageId}">
                                    <c:out value="${dPages.title}"/>
                                </a>

                            </td>
                            <td>
                                <a href="editPage?pageId=${dPages.pageId}">
                                    Edit
                                </a>
                            </td>
                            <td>
                                <a href="deletePage?pageId=${dPages.pageId}">
                                    Delete
                                </a>
                            </td>
                            <td>
                                <a href="enablePage?pageId=${dPages.pageId}">
                                    Enable
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>

            </div>





        </div>



        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/sidemenu.js"></script>
        <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=jupqfvd6xbt805vuo0c45oi7px2yoj56wncg8r5m0oimlg6c"></script>
        <script>tinymce.init({selector: 'textarea'});</script>


    </body>
</html>

