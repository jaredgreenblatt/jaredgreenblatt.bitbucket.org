



<div class="container">
    <div class="col-xs-offset-2 col-xs-8" style="text-align: center" >
        <h3 class = "text-center">Add Tag</h3>
        <form class ="form-horizontal"
              role ="form"  role="form" id="add-tag-form">
            <div class="form-group">
                <label for="add-powerName" class="col-md-4 control-label">
                    Tag:
                </label>
                <div class ="col-md-8">
                    <input type="text" class ="form-control" id="tagNameSnip" placeholder="Enter Tag" required />
                    <br>
                    <input class="col-xs-6" type="button" class="btn btn-default" value="Add Tag" onclick="addTagSnip()"/>
                </div>
            </div>
            <br><br>
            <div class="form-group">
                <h3 class = "text-center">Remove Tag</h3>
                <label for="add-tag" class="col-xs-4 control-label">Select Tag:</label>
                <div class="col-md-8">
                    <select class="form-control"   placeholder="tag" id="tagSnip">
                    </select>
                    <br>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <input class="col-xs-6"  type="button" class="btn btn-default" value="Remove Tag" onclick="removeTagSnip()"/>
                    </sec:authorize>
                </div>
            </div>
        </form>

    </div>
</div>

