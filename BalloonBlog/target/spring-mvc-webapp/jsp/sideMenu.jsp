
<div id="mySidenav" class="sidenav">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <a href="${pageContext.request.contextPath}/">Home</a>
    <sec:authorize access="hasRole('ROLE_USER')">
        <a href="${pageContext.request.contextPath}/createBlog">Create Blog</a>
        <a href="${pageContext.request.contextPath}/createPage">Create Page</a>
    </sec:authorize>
    <sec:authorize access="hasRole('ROLE_ADMIN')">
        <a href="${pageContext.request.contextPath}/blogApproval">Approve Blog</a>
        <a href="${pageContext.request.contextPath}/staticPageAdmin">Static Page Administration</a>
        <a href="${pageContext.request.contextPath}/displayUserList">Display User List</a>
        <a href="${pageContext.request.contextPath}/displayUserForm">Display User Form</a>


        
    </sec:authorize>
    <a href="${pageContext.request.contextPath}/login">Login</a>
    <c:forEach var="currentCat" items="${catList}">
        <nav class="navbar" id="item">
            <button class="navbar-toggler" type="button" data-toggle="collapse" 
                    data-target="#${currentCat.categoryId}" aria-controls="navbarToggleExternalContent" 
                    aria-expanded="false" aria-label="Toggle navigation">
                <span>  <a>${currentCat.category}</a></span>
            </button>
        </nav>
        <div class="pos-f-t">
            <div class="collapse" id="${currentCat.categoryId}">
                <c:forEach var="currentPage" items="${pageListEnabled}">
                    <c:if test="${fn:contains(currentPage.category, currentCat.category)}">

                        <div>
                            <a href= "displayPage?pageId=${currentPage.pageId}">
                                ${currentPage.title}</a>
                        </div>

                    </c:if>
                </c:forEach>
            </div>
        </div>
    </c:forEach>
    <sec:authorize access="hasRole('ROLE_USER')">
        <a href="${pageContext.request.contextPath}/category">Add Category</a>
        <a href="${pageContext.request.contextPath}/tag">Add Tag</a>
        <a href="${pageContext.request.contextPath}/author">Add Author</a>
    </sec:authorize>>
    <c:if test="${pageContext.request.userPrincipal.name != null}">
        <p>
            <a href="<c:url value="/j_spring_security_logout"/>" > Logout</a>
        </p>
    </c:if>


    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
</div>
<span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span>


