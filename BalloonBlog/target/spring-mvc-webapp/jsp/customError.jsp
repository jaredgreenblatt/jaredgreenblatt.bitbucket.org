<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Error!</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/HomeBalloons.css" rel="stylesheet"> 
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png"> 
        <style>
            ul#menu li{
                display:inline;
                padding: 20px;
            }
            table{
                width: 100%;
            }
            td{
                text-align: center;
                width: 25%;
            }


            p{
                text-align: center;
            }

            #format{
                color: white;
                font-weight:bold;
                border: 6px white dashed;
                font-size: 150%;
                background-color:red ;
            }

        </style>
    </head>
    <body style="background-color: #e6ffff">
        <div class="row" id="navbar">
            <div class="col-xs-11">        
                <img class="float" src="http://www.clker.com/cliparts/2/Y/t/a/C/o/hot-air-balloon-outline.svg" alt="" />
                <img class=float1 src="http://www.clker.com/cliparts/2/Y/t/a/C/o/hot-air-balloon-outline.svg" alt="" />
            </div>
            <div class="col-xs-1">  
                <%@include file="sideMenu.jsp" %>
            </div>
        </div>




        <div class="container">
            <center>
                <div class="col-xs-12" style="text-align: center"><h3>Something unexpected happened!</h3>        
                    <Center><p>404 (Page Not Found), sorry!</p>
                        <img src="http://68.media.tumblr.com/9e0f635c677213bc95599367df5f4bec/tumblr_nb4z0npCp31qak30bo1_500.gif" alt="Reaper" style="width:500px;height:500px;">
                    </Center>
                    <br><br><br>
                    <div id="format">
                        <div class="row">
                            <div class="col-xs-12" style="text-align: center">
                                <h3>Guess you're already here might as well try your luck!</h3> 
                            </div>
                                <div class="col-sm-12">
                                    <span style="text-align:center">
                                        <h1>Lucky Sevens</h1>
                                    </span>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-sm-12">	
                                    <span style="text-align:center">


                                        <p><b>Starting Bet:  <input style="color:black;font-weight:bold"id="startingNumber"></b></p>
                                    </span>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <span style="text-align:center">
                                        <div  class="input">


                                            <input style="background-color:ghostwhite;color:black;width: 250px;border-radius: 12px;border: 6px black solid"onclick="startGame();" type="button" value="Play" id="PlayButton"></input>


                                        </div>
                                    </span>
                                </div>
                            </div>

                            <div class="row">
                                <span class="border">	
                                    <div class="col-sm-12">	
                                        <span style="text-align:center">
                                            <p style="font-size:150% ;text-decoration:underline;"id="result"></p>
                                        </span>
                                    </div>

                            </div>

                            <div class="row">
                                <span style="text-align:right">
                                    <div class="col-xs-6">

                                        <p id="iBetText"></p>
                                    </div>
                                </span>
                                <span style="text-align:left">
                                    <div class="col-xs-6">
                                        <p id="iBet"></p>
                                    </div>
                            </div>
                            </span>


                            <div class="row">
                                <span style="text-align:right">
                                    <div class="col-xs-6">

                                        <p id="totalRollsText"></p>
                                    </div>
                                </span>
                                <span style="text-align:left">
                                    <div class="col-xs-6">
                                        <p id="totalRolls"></p>
                                    </div>
                                </span>
                            </div>
                            <div class="row">
                                <span style="text-align:right">
                                    <div class="col-xs-6">
                                        <p id="bWinText"></p>
                                    </div>
                                </span>
                                <span style="text-align:left">
                                    <div class="col-xs-6">
                                        <p id="bWin"></p>
                                    </div>
                                </span>
                            </div>
                            <div class="row">
                                <span style="text-align:right">
                                    <div class="col-xs-6">
                                        <p id="bWinRollNumberText"></p>
                                    </div>
                                </span>
                                <span style="text-align:left">
                                    <div class="col-xs-6">
                                        <p id="bWinRollNumber"></p>
                                    </div>
                                </span>
                            </div>
                        </div>
                    </div>
            </center>
        </span>

        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/sidemenu.js"></script>
        <script src="${pageContext.request.contextPath}/js/luckysevens.js"></script>
        <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=jupqfvd6xbt805vuo0c45oi7px2yoj56wncg8r5m0oimlg6c"></script>
        <script>tinymce.init({selector: 'textarea'});</script>

    </div>
</body>
</html>

