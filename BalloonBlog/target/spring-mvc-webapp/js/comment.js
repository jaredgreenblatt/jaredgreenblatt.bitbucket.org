/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    
    loadComments();
    
});


function loadComments() {

    var contentRows = $('#comment');
    var blogId = $('#blogId').val();
    
    
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/BalloonBlog/restCommentByBlog/' + blogId,
        success: function (commentArray) {
            var numberOfComments = commentArray.length;
            var commentCountTitle = '<center><p><h3>' + numberOfComments + ' Comment(s)</p></h3></center>';
            contentRows.append(commentCountTitle);
            
            $.each(commentArray, function (index, comment) {
                var commentID = index+1;
                var displyName = comment.displyName;
                var month = comment.commentDate.monthValue;
                var day = comment.commentDate.dayOfMonth;
                var year = comment.commentDate.year;
                var content = comment.comment;
                var dateStr = month + '-' + day + '-' + year + ' ';
                
                var commentLine = '<br><br><br>' + commentID + '. ' + content + '<br><br><i>' +
                        '<p>Comment by ' + displyName + ' — ' + dateStr + '</p>';
                        
                
                contentRows.append(commentLine);
            });
        },
        error: function () {
            $('#errorMessages')
                    .append($('<h3>')

                            .text('Error Calling Web Service. Please try again later.'));
        }
    });
}
function addComment() {
    
    // check that the fields coming from display jsp are populated
    if ((document.getElementById('commentName').value === '') || 
        (document.getElementById('commentAuthor').value === '')) {
        alert("A required field is empty");
        return false;
    } else {
    
    $.ajax({
        type: 'POST',
        url: 'http://localhost:8080/BalloonBlog/restComment',
        data: JSON.stringify({
            comment: $('#commentName').val(),
            displyName: $('#commentAuthor').val(),
            blogId: $('#blogId').val()
        
        
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },

        success: function () {
            clearError();
            $('#commentName').val('');
            $('#commentAuthor').val('');
            clearComment();
            loadComments();
            
        },
        error: function () {
            clearError();
            $('#errorMessages')
                    .append($('<h3>')

                            .text('Error Calling Web Service. Please try again later.'));

        }
    });
    
    
    }
    
}

function deleteComment() {

    var commentId = $('#removeComment').val();

    $.ajax({
        type: 'DELETE',
        url: 'http://localhost:8080/BalloonBlog/restComment/' + commentId,
        success: function () {
            clearError();
            clearComment();
            loadComments();
        }

    });

}


function clearComment() {
    $('#removeComment').empty();
    $('#comment').empty();

}

function  clearError() {
    $('#errorMessages').empty();
}

