/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//tag.js
$(document).ready(function () {

    loadTags();
});
function loadTags() {

    var contentRows = $('#tag');
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/BalloonBlog/restTag',
        success: function (contactArray) {
            $.each(contactArray, function (index, tag) {
                var tagID = tag.tagId;
                var tag = tag.tag;
                var select = '<option value =' + tagID + ' id=' + tagID + '>';
                select += tag + '</option>';
                contentRows.append(select);
            });
        },
        error: function () {
            $('#errorMessages')
                    .append($('<h3>')

                            .text('Error Calling Web Service. Please try again later.'));
        }


    });
}
function addTag() {



    $.ajax({
        type: 'POST',
        url: 'http://localhost:8080/BalloonBlog/restTag',
        data: JSON.stringify({
            tag: $('#tagName').val()
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
//        'dataType': 'json',
        success: function () {
            clearError();
            $('#tagName').val('');
            clearTags();
            loadTags();

        },
        error: function () {
            clearError();
            $('#errorMessages')
                    .append($('<h3>')

                            .text('Error Calling Web Service. Please try again later.'));

        }
    });




}
function removeTag() {

    var tagId = $('#tag').val();

    $.ajax({
        type: 'DELETE',
        url: 'http://localhost:8080/BalloonBlog/restTag/' + tagId,
        success: function () {
            clearError();
            clearTags();
            loadTags();

        },
        error: function () {
            clearError();
            $('#errorMessages')
                    .append($('<h3>')
                            .text('Tag cannot be deleted as it is associated with live blog content.'));

        }


    });


}


function clearTags() {
    $('#tag').empty();
}

function  clearError() {
    $('#errorMessages').empty();
}





