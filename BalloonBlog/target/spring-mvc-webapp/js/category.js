

//category.js
$(document).ready(function () {
    $('#catSnipDiv').hide();
    clearCategory();
    loadCategory();




});
function loadCategory() {

    var contentRows = $('#category');
    var categorySnipList = $('#categorySnip');
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/BalloonBlog/restCategory',
        success: function (contactArray) {
            $.each(contactArray, function (index, category) {
                var categoryID = category.categoryId;
                var category = category.category;
                var select = '<option value =' + categoryID + ' id=' + categoryID + '>';
                select += category + '</option>';
                contentRows.append(select);
                categorySnipList.append(select);
            });
        },
        error: function () {
            $('#errorMessages')
                    .append($('<h3>')

                            .text('Error Calling Web Service. Please try again later.'));
        }


    });
}
function addCategorySnip() {



    $.ajax({
        type: 'POST',
        url: 'http://localhost:8080/BalloonBlog/restCategory',
        data: JSON.stringify({
            category: $('#categoryNameSnip').val()
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
//        'dataType': 'json',
        success: function () {
            $('#errorMessages').empty();
            $('#categoryNameSnip').val('');
            clearCategory();
            loadCategory();
            hideCatSnip();

        },
        error: function () {
            $('#errorMessages')
                    .append($('<h3>')

                            .text('Error Calling Web Service. Please try again later.'));

        }
    });




}
function removeCategorySnip() {

    clearErrorMessage();

    var categoryId = $('#categorySnip').val();

    $.ajax({
        type: 'DELETE',
        url: 'http://localhost:8080/BalloonBlog/restCategory/' + categoryId,
        success: function () {
            $('#errorMessages').empty();
            $('#categoryNameSnip').val('');
            loadCategory();
            clearCategory();
            hideCatSnip();
        },
        error: function () {
            $('#errorMessages')
                    .append($('<h3>')

                            .text('Category cannot be deleted as it is associated with live blog or page content.'));

        }
    });
}


function clearCategory() {
    $('#categorySnip').empty();
    $('#category').empty();


}

function clearErrorMessage() {
    $('#errorMessages').empty();
}

function showCatSnip() {
    $('#pageCreate').hide();
    $('#catSnipDiv').show();


}
function hideCatSnip() {
    $('#pageCreate').show();
    $('#catSnipDiv').hide();


}






