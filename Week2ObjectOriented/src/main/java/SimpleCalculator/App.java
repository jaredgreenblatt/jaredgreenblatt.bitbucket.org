/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SimpleCalculator;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class App {

    public static void main(String[] args) {
       String playAgain;
       Scanner sc = new Scanner(System.in);
       

        do {
            boolean anotherOperation = false;
            
            String operation;
            double operand1 = 0;
            double operand2 = 0;

            System.out.println("Please enter in your operation.");
            System.out.println("Addition enter A, Subtraction enter S, Multiplication enter M , Division enter D or Exit");
            operation = sc.next();

           switch (operation) {
               case "EXIT":
                   System.out.println("Thank you!");
                   anotherOperation = false;
                   break;
               case "A":
               case "S":
               case "M":
               case "D":
                   System.out.println("Please Enter Operand 1");
                   operand1 = sc.nextDouble();
                   System.out.println("Please Enter Operand 2");
                   operand2 = sc.nextDouble();
                   anotherOperation = true;
                   break;
               
           }

            if (operation.equals("A")) {
                System.out.println(SimpleCalculator.addition(operand1, operand2));

            } else if (operation.equals("S")) {
                System.out.println(SimpleCalculator.subtraction(operand1, operand2));

            } else if (operation.equals("M")) {
                System.out.println(SimpleCalculator.multiplication(operand1, operand2));

            } else if (operation.equals("D")) {
                System.out.println(SimpleCalculator.division(operand1, operand2));

            }
            System.out.println("Do want to do any more calculations?");
            playAgain =sc.next();
            playAgain = playAgain.toUpperCase();
            

        } while (playAgain.equals("Y")|| playAgain.equals("YES"));
        if (playAgain.equals("N")|| playAgain.equals("NO")) {
            System.out.println("Thanks for adding stuff.");
            
        }
         
        
        
        

    }

}
