/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RockPaperScissor;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class App {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean validInputs = false;
        String playersChoice = "";
        boolean gameInputCheck = false;
        int pcWins = 0;
        int ties = 0;
        int humanWins = 0;
        int rounds = 0;

        String playAgain;
        do {
            System.out.println("How Many Rounds Would you like to play?");
            if ((!(sc.hasNextInt()))) {
                return;
            }
            int gameRounds = sc.nextInt();

            if (gameRounds > 0 && gameRounds <= 10) {

                validInputs = true;
                while (gameRounds > 0) {
                    while (gameInputCheck == false) {
                        System.out.println("Rock Paper Scissor....");
                        System.out.println("Shoot");
                        playersChoice = sc.next();
                        playersChoice = playersChoice.toUpperCase();
                        gameInputCheck = RockPaper.validateInputs(playersChoice);

                    }

                    String result
                            = RockPaper.playGame(RockPaper.convertToInt(playersChoice));

                    if (result.equals("h")) {
                        System.out.println("----------------------------");
                        System.out.println("Congrats you won this Round!");
                        System.out.println("----------------------------");
                        humanWins++;

                    } else if (result.equals("p")) {
                        System.out.println("----------------------------");
                        System.out.println("Pc Won this round :-(");
                        System.out.println("----------------------------");
                        pcWins++;

                    } else if (result.equals("tie")) {
                        System.out.println("----------------------------");
                        System.out.println("we Tied This round");
                        System.out.println("----------------------------");
                        ties++;

                    }

                    gameInputCheck = false;
                    gameRounds--;
                    rounds++;

                }
                System.out.println("_____________________________________________");
                System.out.println("After Playing " + rounds + " Rounds.");
                System.out.println("The user won " + humanWins + " games.");
                System.out.println("The PC won " + pcWins + " games.");
                System.out.println("The PC and user tied " + ties + "games.");
                System.out.println("_____________________________________________");
                if (pcWins == humanWins) {
                    System.out.println("_____________________________________________");
                    System.out.println("TIE GAME");
                    System.out.println("_____________________________________________");

                } else if (pcWins > humanWins) {
                    System.out.println("_____________________________________________");
                    System.out.println("PC WON");
                    System.out.println("_____________________________________________");

                } else if (pcWins < humanWins) {
                    System.out.println("_____________________________________________");
                    System.out.println("User WON");
                    System.out.println("_____________________________________________");

                } else {
                    System.out.println("Bad Input");
                    return;
                }

            }
            System.out.println("Do want to play rock paper scissor again?");
            playAgain = sc.next();
            playAgain = playAgain.toUpperCase();

        } while (playAgain.equals("Y") || playAgain.equals("YES"));
        if (playAgain.equals("N") || playAgain.equals("NO")) {
            System.out.println("Thanks for playing.");

        }
    }
}

