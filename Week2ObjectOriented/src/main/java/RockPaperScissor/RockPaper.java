/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RockPaperScissor;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class RockPaper {

    public static String playGame(int userInput) {

        Random computerValue = new Random();
        int pcTurn;
        int ties = 0;
        int pcWins = 0;
        int humanWins = 0;
        String result = "";

        /*Math Random that will return 1 2 or 3. This lines up with  the 
            convereted values of the user inputs*/
        pcTurn = computerValue.nextInt(3) + 1;
        if (pcTurn == 1) {
            System.out.println("The computer chose Rock.");

        } else if (pcTurn == 2) {
            System.out.println("The computer chose Paper.");
        } else if (pcTurn == 3) {
            System.out.println("The Computer chose Scissors.");
        }

        /* Logic below handles the evaluation between the user input and
            the random selections made by the computer it will evaluate if the 
            human wins loses or ties based off the rules of the rock paper
            scissor game.*/
        if (userInput == pcTurn) {
            //Ties so we add 
            //t for tie
            result = "t";

        } else if (userInput == 1 && pcTurn == 2 || userInput == 2 && pcTurn == 3
                || userInput == 3 && pcTurn == 1) {
            // human loses pc+1
            //p for pc win
            result = "p";

        } else if (userInput == 1 && pcTurn == 3 || userInput == 2 && pcTurn == 1
                || userInput == 3 && pcTurn == 2) {
            //. human wins  human+1
            //h for human win
            result = "h";

        }
        return result;
    }

    public static int convertToInt(String input) {

        int x = 0;
        if (input.equals("ROCK")) {
            x = 1;

        } else if (input.equals("PAPER")) {
            x=2;
        }else if(input.equals("SCISSORS")){
            x=3;
        }
        
            return x;
        }
    

    public static boolean validateInputs(String input) {
        //Method used to check in the in game inputs are Rock Paper Scissor or Shoot!
        boolean x;
        
        if (input.equals("ROCK") || input.equals("PAPER") || input.equals("SCISSOR")) {
            x = true;

        } else {
            x = false;
        }

        return x;
    }

}
