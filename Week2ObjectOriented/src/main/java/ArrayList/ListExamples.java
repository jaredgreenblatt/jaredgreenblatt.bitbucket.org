/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ArrayList;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class ListExamples {

    public static void main(String[] args) {

        List<String> stringList = new ArrayList<>();

        stringList.add("String 1");
        stringList.add("String 2");
        stringList.add("string 3");
        stringList.add("string 4");
        System.out.println("list Size " + stringList.size());
        
        for(String currentString : stringList){
            System.out.println(currentString);
        }
        
        //iterators job in life is to visit all thing in  collection once. 
        // not all collections are in order so iterator more useful there.
        
        Iterator<String> iterator = stringList.iterator();
        
        while(iterator.hasNext()){
            String currentStrin = iterator.next();
            System.out.println(currentStrin);
        }
        

    }
}
