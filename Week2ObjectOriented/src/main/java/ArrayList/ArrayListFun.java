/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ArrayList;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author apprentice
 */
public class ArrayListFun {
    public static void main(String[] args) {
        // create an ArrayList of String objects
        ArrayList<String> stringList=new ArrayList<>();
        
        //ask how big it is.
        System.out.println("How Big is this with no stings "+ stringList.size());
        // add a String object to our list
    stringList.add("My First String");

    // ask the list how big it is
    System.out.println("List size after adding one String: " + 
            stringList.size());
    // add another String object to our list
    stringList.add("My Second String");

    // ask the list how big it is
    System.out.println("List size after adding two Strings: " + 
            stringList.size());
    
     // remove the second String object from our list - remember that
    // our indexes start counting at 0 instead of 1
    stringList.remove(1);

    // ask the list how big it is
    System.out.println("List size after removing one String: " + 
            stringList.size());
       
    // remove the remaining String object from our list - remember
    // that the list resizes automatically so if there is only one
    // element in a list it is alway at index 0
    stringList.remove(0);
       
    // ask the list how big it is
    System.out.println("List size after removing last String: " + 
            stringList.size());
    
     // add a String object to our list
    stringList.add("My First String");

    // add another String object to our list
    stringList.add("My Second String");

    // add another String object to our list
    stringList.add("My Third String");

    // add another String object to our list
    stringList.add("My Fourth String");

    // ask the list how big it is
    System.out.println("List size: " + stringList.size());
       
    // print every String in our list with an enhanced for loop
    for (String s : stringList) {
        System.out.println(s);
    }
     // add a String object to our list
    stringList.add("My First String");

    // add another String object to our list
    stringList.add("My Second String");

    // add another String object to our list
    stringList.add("My Third String");

    // add another String object to our list
    stringList.add("My Fourth String");

    // ask the list how big it is
    System.out.println("List size: " + stringList.size());
       
    // print every String in our list with an iterator

    // ask for the iterator - we must ask for an iterator of Strings
    // What happens if we don't?
    Iterator<String> iter = stringList.iterator();
       
    // get String objects from the list while there are still Strings
    // remaining
    while(iter.hasNext()) {
        String current = iter.next();
        System.out.println(current);
    }
    }
    
    
    
    //
    
    
}
