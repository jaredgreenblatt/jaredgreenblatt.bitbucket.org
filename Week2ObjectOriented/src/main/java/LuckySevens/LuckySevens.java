/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LuckySevens;

import java.util.Random;

/**
 *
 * @author apprentice
 */
public class LuckySevens {

    private int myrollCount;
    private int highestRoll;
    private int biggestWin;

    public LuckySevens(int betAmount) {
        int bigWin = betAmount;
        int originalBet = betAmount;
        int highRoll = 1;
        int dice1;
        int dice2;
        int diceSum;
        int rollCount = 0;

        Random dice = new Random();

        int currentBet = betAmount;
        while (currentBet >= 1) {
            dice1 = dice.nextInt(6) + 1;
            dice2 = dice.nextInt(6) + 1;
            diceSum = dice1 + dice2;

            if (diceSum == 7) {
                currentBet += 4;
                rollCount++;
                if (currentBet > bigWin) {
                    bigWin = currentBet;
                    highRoll = rollCount;

                }

            } else {
                currentBet -= 1;
                rollCount++;

            }

        }
        this.biggestWin=bigWin;
        this.myrollCount=rollCount;
        this.highestRoll=highRoll;
        

    }

    public int getRollCount() {
        return myrollCount;
    }

    public void setRollCount(int rollCount) {
        this.myrollCount = rollCount;
    }

    public int getHighestRoll() {
        return highestRoll;
    }

    public void setHighestRoll(int highestRoll) {
        this.highestRoll = highestRoll;
    }

    public int getBiggestWin() {
        return biggestWin;
    }

    public void setBiggestWin(int biggestWin) {
        this.biggestWin = biggestWin;
    }

}
