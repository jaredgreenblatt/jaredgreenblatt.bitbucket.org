/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ShapesAndPerimeters;

/**
 *
 * @author apprentice
 */
public class Triangle extends Shape {

    private double base;
    private double side1;
    private double side2;
    private double height;

    public Triangle(double base, double side1, double side2, double height) {
        this.base = base;
        this.side1 = side1;
        this.side2 = side2;
        this.height = height;
    }

    @Override
    public double area() {
        double area = (.5 * height) * (.5 * base);
        return area;
    }

    @Override
    public double perimeter() {
        double perimeter = side1 + side2 + base;
        return perimeter;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getSide1() {
        return side1;
    }

    public void setSide1(double side1) {
        this.side1 = side1;
    }

    public double getSide2() {
        return side2;
    }

    public void setSide2(double side2) {
        this.side2 = side2;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

}
