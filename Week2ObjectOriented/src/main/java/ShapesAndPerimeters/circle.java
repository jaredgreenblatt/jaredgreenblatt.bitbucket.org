/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ShapesAndPerimeters;

/**
 *
 * @author apprentice
 */
public class circle extends Shape{
    private double radius;

   
    
    
    public circle(double radius){
        this.radius=radius;
    }

    @Override
    public double area() {
        double area = (radius*radius)*Math.PI;
        return area;
    }

    @Override
    public double perimeter() {
        double perimeter = 2*Math.PI*radius;
        return perimeter;
    }
     public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
}
