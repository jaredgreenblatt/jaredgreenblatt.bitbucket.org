/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ShapesAndPerimeters;

/**
 *
 * @author apprentice
 */
public class Square extends Shape {
    private double side;

   
    
  public Square(double side){
      this.side=side;
      
  }

    @Override
    public double area() {
        
       double area =this.side*this.side;
       
       
       return area;
        
    }

    @Override
    public double perimeter() {
        double perimeter = this.side*4;
        return perimeter;
    }
    
     public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }
    
    
}
