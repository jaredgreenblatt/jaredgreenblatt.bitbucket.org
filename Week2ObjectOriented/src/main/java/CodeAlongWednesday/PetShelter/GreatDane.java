/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CodeAlongWednesday.PetShelter;

/**
 *
 * @author apprentice
 */
public class GreatDane extends Dog implements Rideable {

    // if you dont write a constuctor in a child class 
    // this what it looks like.
    public GreatDane(String size, int id) {
        super(id, size);// this is basicially a dog.
    }

    public GreatDane(String color, String size) {
        super();// if i don't write it is always there
        this.color = color;
        this.setSize(size);

    }

    /**
     * Danes don't bark they WOOOOF!
     */
    @Override
    public void bark() {
        System.out.println("WOOF!");
    }

    @Override
    public void ride() {
        this.bark();
        this.bark();
        System.out.println("I have been ridden please give me a biscuit.");

    }
}
