/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CodeAlongWednesday.PetShelter;

/**
 *
 * @author apprentice
 */
public class Dog {

    private String name;
    private double weight;
    private String size;
    protected String color;// subclasses can access this now without permission)
    private int id;

    public Dog(String name, double weight, String size, String color, int id) {
        this.name = name;
        this.weight = weight;
        this.size = size;
        this.color = color;
        this.id = id;

    }

    /// my dev said they don't like providing all the info.
    // sometimes they only have acces to size and id so we are allowing them to
    //overload the constructor.
    public Dog(int id, String size) {
        this.id = id;
        this.size = size;
        this.name = "UnAssigned";
        this.weight = -1;
        this.color = "UnKnown";

    }
//    
//    public Dog(int id, String name){
//        // cant do this method overloading is broken
    // the method sig conflicts is the same size as the id/size constructor
//    }

    // so by definition by providing  a constructor i have written the default  contrcuctor(or no args 
    //contrcutor or no args constructor) goes away. 
    //but, alot of times, people want those. so...
    /**
     * @param name - the name of the dog Hey this is a deafult but you better
     * provide a unique id asap because right now its missing.
     *
     *
     */
  public Dog() {
       // but remember, this comes with a risk.
        // of null pointer exceptions
    }
    // HERE ARE MY BEHAVIOR METHODS

    public void bark() {
        System.out.println("BARK!");
    }
    public boolean adobpt(String owner){
        return true;
    }

    //GETERS AND SETTERS HERE.
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double wieght) {
        this.weight = wieght;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
