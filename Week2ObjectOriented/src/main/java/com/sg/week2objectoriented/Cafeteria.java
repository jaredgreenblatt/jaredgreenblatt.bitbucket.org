/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.week2objectoriented;

/**
 *
 * @author apprentice
 */
class Cafeteria {
    private int numberOfStoves;
    private int seatingCapcity;
    private double squareFootage;
    private int numberOfServingLines;
    private int mealsPerHour;

    public int getNumberOfStoves() {
        return numberOfStoves;
    }

    public void setNumberOfStoves(int numberOfStoves) {
        this.numberOfStoves = numberOfStoves;
    }

    public int getSeatingCapcity() {
        return seatingCapcity;
    }

    public void setSeatingCapcity(int seatingCapcity) {
        this.seatingCapcity = seatingCapcity;
    }

    public double getSquareFootage() {
        return squareFootage;
    }

    public void setSquareFootage(double squareFootage) {
        this.squareFootage = squareFootage;
    }

    public int getNumberOfServingLines() {
        return numberOfServingLines;
    }

    public void setNumberOfServingLines(int numberOfServingLines) {
        this.numberOfServingLines = numberOfServingLines;
    }

    public int getMealsPerHour() {
        return mealsPerHour;
    }

    public void setMealsPerHour(int mealsPerHour) {
        this.mealsPerHour = mealsPerHour;
    }
    
    
}
