/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CodeAlongArrays;

/**
 *
 * @author apprentice
 */
public class ArrayCheckForUnderstanding {

    public static void main(String[] args) {

        String[] firstNames = {"Mary", "John", "Patricia",
            "Robert", "Linda", "Michael", "Barbara", "William",
            "Elizabeth", "David", "Jennifer", "Richard", "Maria",
            "Charles", "Susan", "Joeseph", "Dorothy", "Thomas",
            "Lisa", "Christopher", "Nancy", "Daniel", "Karen",
            "Paul", "Betty", "Mark", "Helen", "Donald",
            "Sandra", "George", "Donna", "Kenneth"};
        String[] lastNames = {"Smith", "Johnson", "Williams",
            "Jones", "Brown", "Davis", "Miller", "Wilson",
            "Moore", "Taylor", "Anderson", "Thomas",
            "Jackson", "White", "Harris", "Martin", "Thompson",
            "Garcia", "Martinez", "Robinson", "Clark", "Rodriguez",
            "Lewis", "Lee", "Walker", "Hall", "Allen", "Young",
            "Hernadez", "King", "Wright", "Lopez"};

        String[] petNames = {"Queenie", "Denver", "Fritz", "Blue", "Pegasus",
            "Perseus", "Jimmy TwoPipes", "FluffyPet", "FancyPants", "Athensa",
            "Knoppler", "Marie", "FrooFroo", "Magpie", "Bonnie", "Clyde",
            "Benji", "Mensa", "Pumpernickel", "Zazoo", "Duchesse", "Moof",
            "Prue", "Mitch", "Bagel", "Darkwing Duck", "Bobbafett", "Dr. What",
            "Mochi", "Floyd", "Prism", "Winnie the Poo", "Piglet", "Frankenfurter"};
        int[] randomNumbers = {2, 89, 60, 88, 11, 35,
            80, 10, 27, 98, 73, 49, 64, 32, 71,
            28, 66, 7, 85, 22, 31, 86, 100, 25,
            82, 99, 97, 92, 62, 5, 8, 16, 3, 78,
            65, 47, 38, 77, 39, 79, 94, 95, 29, 13,
            20, 34, 15, 9, 19, 42, 51, 58, 36, 37,
            84, 18, 40, 45, 59, 46, 21, 96, 12, 1, 57,
            55, 33, 87, 17, 24, 76, 68, 30, 81, 90, 26, 14,
            23, 56, 91, 61, 63, 43, 75, 53, 6, 67, 74, 44, 41,
            50, 4, 93, 52, 48, 72, 54, 83, 70, 69};
        
        int lastNameLength = lastNames.length;
        System.out.println("The length of the last name array is "+lastNameLength);
        int firstNameLength = firstNames.length;
        System.out.println("The length of the first name array is "+firstNameLength);
        int petNameLength =petNames.length;
        System.out.println("The length of the pet name array is "+petNameLength);
        int randomNumbersLength =  randomNumbers.length;
        System.out.println("The length of the random number array is "+randomNumbersLength);

        for (int i = 0; i < firstNames.length; i++) {

            System.out.println(firstNames[i] + " " + " " + petNames[i] + " " + lastNames[i]);

        }
        System.out.println("");
        System.out.println("_____________________________________________________");
        System.out.println("");
        for (int i = 0; i < randomNumbers.length; i++) {
            System.out.println(randomNumbers[i]);
            
        }
        for (int num: randomNumbers) {
            System.out.println("Element = "+ num);
       
        }

    }

}
