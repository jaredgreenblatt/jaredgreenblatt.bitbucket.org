package CodeAlongArrays;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class Arrays {

    public static void main(String[] args) {
        int[] teamScores;
        teamScores = new int[5];
        teamScores[0] = 2;
        teamScores[1] = 45;
        teamScores[2] = 4;
        teamScores[3] = 8;
        teamScores[4] = 99;
        
        
        
        
                
        
             

        int[] golfScores = {72, 74, 68, 71};

        int currentGolfScore = golfScores[0];

        golfScores[2] = 70;
        //traditional for loop
        
        for (int i = 0; i < golfScores.length; i++) {
            System.out.println(golfScores[i]);
        }
        
         System.out.println("");
        //enhanced for loop
        // For each int current scores for golf score. IT will pull it out one 
        // at a time and print the score.
        for(int currentScore : golfScores){
           
            System.out.println(currentScore);
            
        }
                

    }

}
