/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CodeAlongArrays;

/**
 *
 * @author apprentice
 */
public class ARainbow {

    public static void main(String[] args) {
        int[] someNums = new int[10];
        int[] moreNums = {2001, 2010, 2020};

        someNums[0] = 5;
        someNums[1] = 3;
        int x = someNums[0] + moreNums[1];
        System.out.println(x);

        String[] colors = {"Red", "Orange", "Yellow", "Green", "Blue", "Indigo",
            "Violet"};
        for (String st : colors) {
            System.out.println(st);

        }

    }

}
