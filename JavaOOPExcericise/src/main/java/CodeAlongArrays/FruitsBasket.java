/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CodeAlongArrays;

/**
 *
 * @author apprentice
 */
public class FruitsBasket {

    public static void main(String[] args) {
        String[] fruit = {"Orange", "Apple", "Orange", "Apple", "Orange",
            "Apple", "Orange", "Apple", "Orange", "Orange", "Orange", "Apple",
            "Orange", "Orange", "Apple", "Orange", "Orange", "Apple", "Apple",
            "Orange", "Apple", "Apple", "Orange", "Orange", "Apple", "Apple",
            "Apple", "Apple", "Orange", "Orange", "Apple", "Apple", "Orange",
            "Orange", "Orange", "Orange", "Apple", "Apple", "Apple", "Apple",
            "Orange", "Orange", "Apple", "Orange", "Orange", "Apple", "Orange",
            "Orange", "Apple", "Apple", "Orange", "Orange", "Apple", "Orange",
            "Apple", "Orange", "Apple", "Orange", "Apple", "Orange", "Orange"};

        // Fruit sorting code goes here!
        int appleCount = 0, orangeCount = 0;

        System.out.println("Total # of Fruits in the Basket:" + fruit.length);

        for (int i = 0; i < fruit.length; i++) {
            if (fruit[i].equals("Apple")) {
                appleCount++;

            } else if (fruit[i].equals("Orange")) {
                orangeCount++;

            }

        }
        String[] orange = new String[orangeCount];
        String[] apple = new String[appleCount];

        for (int i = 0; i < apple.length; i++) {

            apple[i] = "Apple";

        }
        for (int i = 0; i < orange.length; i++) {
            orange[i] = "Orange";

        }
        System.out.println("Number Of Apples: " + appleCount);

        System.out.println("Number of Oranges: " + orangeCount);

    }

}
