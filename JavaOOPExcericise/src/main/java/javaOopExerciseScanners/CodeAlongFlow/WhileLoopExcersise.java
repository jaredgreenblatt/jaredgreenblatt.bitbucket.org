/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaOopExerciseScanners.CodeAlongFlow;

import java.util.Random;

/**
 *
 * @author apprentice
 */
public class WhileLoopExcersise {

    public static void main(String[] args) {

        Random rGen = new Random();
        int randomNum = rGen.nextInt(10) + 1;
               
        while (randomNum < 8) {
            System.out.println(randomNum);
            randomNum = rGen.nextInt(10)+ 1;
        }

    }

}
