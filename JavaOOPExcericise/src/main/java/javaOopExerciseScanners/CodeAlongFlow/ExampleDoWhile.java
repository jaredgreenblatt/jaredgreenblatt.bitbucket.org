/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaOopExerciseScanners.CodeAlongFlow;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ExampleDoWhile {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int userNumber;
        String userNumberString;

        do {
            System.out.println("Please enter a "
                    + "number between 1 and 20");
            userNumberString = sc.nextLine();
            userNumber = Integer.parseInt(userNumberString);

        } while (userNumber < 1 || userNumber > 20);
        
          System.out.println("Thank You!1 You Number was "+userNumber);
    }

}
