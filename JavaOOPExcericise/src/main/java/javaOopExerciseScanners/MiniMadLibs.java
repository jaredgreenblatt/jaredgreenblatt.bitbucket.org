/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaOopExerciseScanners;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class MiniMadLibs {
    public static void main(String[] args) {
        String nounOne,adjectiveOne,nounTwo,number,adjective2,
        pluralNounOne,pluralNounTwo,pluralNounThree,verbPresentTense,sameVerbPastTense;
        Scanner inputReader = new Scanner(System.in);
        
        
        System.out.println("I need a noun:");
        nounOne= inputReader.nextLine();
        System.out.println("Now an adj:");
        adjectiveOne=inputReader.nextLine();
        System.out.println("Another noun:");
        nounTwo =inputReader.nextLine();
        System.out.println("And a Number");
        number =inputReader.nextLine();
        System.out.println("Another adj:");
        adjective2 =inputReader.nextLine();
        System.out.println("A plural noun::");
        pluralNounOne =inputReader.nextLine();
        System.out.println("Another one:");
        pluralNounTwo =inputReader.nextLine();
        System.out.println("one more:");
        pluralNounThree =inputReader.nextLine();
        System.out.println("A verb present tense:");
        verbPresentTense = inputReader.nextLine();
        System.out.println("A verb past tense:");
        sameVerbPastTense = inputReader.nextLine();
        
        System.out.println("*** NOW LETS GET MAD (libs) ***");
        System.out.println(nounOne+": the "+adjectiveOne+" frontier. These are"
                + " the voyages of the starship "+nounTwo);
        System.out.println("Its " +number+" -year mission: to "
                + "explore strange "+adjective2+" "+pluralNounOne+", to seek "
                + "out red" );
        System.out.println(pluralNounTwo+" and red "+pluralNounThree+", to boldly "
                +verbPresentTense+" where no one has "+sameVerbPastTense+" before.");
        
        
        
        
        
    }
    
}
