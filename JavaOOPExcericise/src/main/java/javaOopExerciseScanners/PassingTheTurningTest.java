/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaOopExerciseScanners;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class PassingTheTurningTest {
    public static void main(String[] args) {
        String color, name, food;
        int number;
        int compNumber =7;
        int multy;
        Scanner inputReader = new Scanner(System.in);
        
        System.out.println("Hello there!?");
        System.out.println("What's your Name? ");
        name = inputReader.nextLine();
        
        System.out.println("");
        System.out.println("Hi, "+name+" What's your favorite color?");
        color = inputReader.nextLine();
        System.out.println("Huh, "+color+ " mine is hot pink.");
        
        System.out.println("I really like limes. They're my favorite fruit, too.");
        System.out.println("What's YOUR favorite fruit, "+name);
        food=inputReader.nextLine();
        
        System.out.println("Really? "+food +"?" + " That's wild!");
        System.out.println("Speaking of favorites, what's your favorite number?");
        number =inputReader.nextInt();
        
        System.out.println(number+" is a cool number. Mine's is 7.");
        multy =number * compNumber;
        System.out.println("Did you know "+number+" * "+compNumber+" is "+multy+
                "? That's a cool number too!");
        
        System.out.println("Well, thanks for talking to me, "+name);
        
        
    }
    
}
