/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaOopExerciseScanners;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class BiggerBeterAdder {
    public static void main(String[] args) {
        //3 main variables and the sum
        int variable1, variable2, variable3,sumOfVariables;
        
        
        
        Scanner inputReader = new Scanner(System.in);
        System.out.println("Please give me a value for vairable one");
        variable1= inputReader.nextInt();
        System.out.println("Please give me a value for vairable two");
        variable2= inputReader.nextInt();
        System.out.println("Please give me a value for vairable three");
        variable3= inputReader.nextInt();
        
        sumOfVariables= variable1 +variable2;
        
        //Printing out the values of the 3 variables
        System.out.println("Variable1 is "+variable1);
        System.out.println("Variable2 is "+variable2);
        System.out.println("Variable3 is "+variable3);
        
        //Printing out the sum twice
        System.out.println("The Sum of Variable1 and 2 "+sumOfVariables);
        System.out.println("The Sum of Variable1 and 2 again is "+sumOfVariables);
    }
}
