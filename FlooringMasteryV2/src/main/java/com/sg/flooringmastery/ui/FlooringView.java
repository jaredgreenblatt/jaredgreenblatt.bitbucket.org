/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.ui;

import com.sg.flooringmastery.dto.Order;
import com.sg.flooringmastery.dto.ProductType;
import com.sg.flooringmastery.dto.TaxRate;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.List;

/**
 *
 * @author Jared
 */
public class FlooringView {

    private UserIO io;

    public FlooringView(UserIO io) {
        this.io = io;

    }

    /**
     * Method used to get the users selection for what action they would like to
     * perform. That int is returned to the controller so that the correct
     * workflow will be executed.
     *
     * @return
     */
    public int printMenuAndGetSelection() {
        io.print("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *");
        io.print("*  <<Flooring Program>>");
        io.print("* 1. Display Orders");
        io.print("* 2. Add an Order");
        io.print("* 3. Edit an Order");
        io.print("* 4. Remove an Order");
        io.print("* 5. Save Current Work");
        io.print("* 6. Quit");
        io.print("*");
        io.print("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *");

        return io.readInt("Please select from the above choices.", 1, 6);
    }

    /**
     *
     * @param productTypeList
     * @param taxRateList
     * @return Order Method is used to create a new vending order. It will
     * prompt the user for the required fields. Name, State,Product Type, Area
     * and order Date.
     */
    public Order getNewOrderInfo(List<ProductType> productTypeList, List<TaxRate> taxRateList) {
        //Setting up variables used for getting new orders.
        boolean validState = false;
        boolean validProductType = false;
        String state = null;
        String productType = null;
         BigDecimal costPerSquareFoot = null;
         BigDecimal laborCostPerSquareFoot = null;
          BigDecimal taxRate = null;
                 
        // Getting customer name. Passing false so that the userIo will validate that we are not getting null string.
        String customerName = io.readString("Please enter the customer Name", false);

        /*
        For State we need to run it in a while loop as we need to prompt the user
        until we get an acceptable value. Here we will use the taxRate list that
        was passed into the method. First we ask the the user for there state. We 
        then check that the customer state matches the content in the tax rate list.
        If it does the input will be accepted.if not it will display the acceptable inputs
        the user will then try to add the value again.
         */
        while (validState == false) {

            state = io.readString("Please enter the customers State. ");
            state = state.toUpperCase();
            int matchCount = 0;
            for (TaxRate stateCheck : taxRateList) {
                if (state.equals(stateCheck.getState())) {
                    taxRate = stateCheck.getTaxRate();
                    matchCount++;
                    
                }

            }
            if (matchCount == 1) {
                validState = true;

            } else {
                io.print("Error! You have entered and invalid State. " + state);
                for (TaxRate stateCheck : taxRateList) {
                    io.print(stateCheck.getState());
                }
                io.print("___________________________________________________");
                io.readString("Press enter to continue.");
                validState = false;
            }

        }

        /*
        For productType we need to run it in a while loop as we need to prompt the user
        until we get an acceptable value. Here we will use the productType list that
        was passed into the method. First we ask the the user for there productType. We 
        then check that the customer product type matches the content in the productTypelist.
        If it does the input will be accepted. if not it will display the acceptable inputs
        the user will then try to add the value again.
         */
        while (validProductType == false) {

            productType = io.readString("Please enter the product type");
            int matchCount = 0;
           
            for (ProductType productCheck : productTypeList) {
                if (productType.equals(productCheck.getProductType())) {
                    costPerSquareFoot=productCheck.getCostPerSquareFoot();
                    laborCostPerSquareFoot = productCheck.getLaborCostPerSquareFoot();
                    matchCount++;
                }

            }
            if (matchCount == 1) {
                validProductType = true;

            } else {
                io.print("Error! You have entered an invalid ProductType" + productType);
                io.print("The Valid Products are:");
                for (ProductType productCheck : productTypeList) {
                    io.print(productCheck.getProductType());
                }
                io.print("___________________________________________________");
                io.readString("Press enter to continue.");

                validProductType = false;
            }

        }

        BigDecimal area = io.readBigDecimalNoNull("Please enter your Area", false);
        LocalDate orderDate = io.readLocalDate("Please enter your LocalDate in format MMddyyyy");
        /*
         These values will then be set to 0. So that once the data above is 
        validated they can recieve there real values. 

         */
        int orderNumber = 0;
        // BigDecimal taxRate = new BigDecimal(BigInteger.ZERO);
       // BigDecimal taxRate = new BigDecimal(BigInteger.ZERO);

        BigDecimal materialCost= new BigDecimal(BigInteger.ZERO);
        BigDecimal laborCost = new BigDecimal(BigInteger.ZERO);
      //  BigDecimal materialCost = new BigDecimal(BigInteger.ZERO);

      //  BigDecimal laborCost = new BigDecimal(BigInteger.ZERO);

        BigDecimal tax = new BigDecimal(BigInteger.ZERO);

        BigDecimal total = new BigDecimal(BigInteger.ZERO);
        /*
        Here we set the current order to the values using the inputed information.
         */
        Order currentOrder = new Order(orderNumber);
        currentOrder.setCustomerName(customerName);
        currentOrder.setState(state);
        currentOrder.setProductType(productType);
        currentOrder.setArea(area);
        currentOrder.setOrderDate(orderDate);
        currentOrder.setTaxRate(taxRate);
        currentOrder.setCostPerSquareFoot(costPerSquareFoot);
        currentOrder.setLaborCostPerSquareFoot(laborCostPerSquareFoot);
        currentOrder.setMaterialCost(materialCost);
        currentOrder.setLaborCost(laborCost);
        currentOrder.setTax(tax);
        currentOrder.setTotal(total);
//Passing current Order Value to the controller. 
        return currentOrder;
    }

    /**
     *
     * @param order
     * @param productType
     * @param taxRate
     * @param uniqueOrderId
     * @return
     *
     * The Finish new order method is displayed after the state tax and product
     * type information is retrieved from the service layer. This is where the
     * calculations are performed to get the cost information for the orders.
     */
//    public Order finishNewOrder(Order order, ProductType productType,
//            TaxRate taxRate, int uniqueOrderId) {
         public Order finishNewOrder(Order order){
//        int newOrderId = uniqueOrderId + 1;
//        Order finalOrder;
//        finalOrder = new Order(newOrderId);
//        finalOrder.setCustomerName(order.getCustomerName());
//        finalOrder.setState(order.getState());
//        finalOrder.setOrderDate(order.getOrderDate());
//        finalOrder.setTaxRate(taxRate.getTaxRate());
//        finalOrder.setProductType(order.getProductType());
//        finalOrder.setArea(order.getArea());
//        finalOrder.setCostPerSquareFoot(productType.getCostPerSquareFoot());
//        finalOrder.setLaborCostPerSquareFoot(productType.getLaborCostPerSquareFoot());
//        //Calculate Material Cost
//        BigDecimal materialCost = (finalOrder.getArea().multiply(finalOrder.getCostPerSquareFoot()));
//        materialCost = materialCost.setScale(2, RoundingMode.HALF_DOWN);
//        finalOrder.setMaterialCost(materialCost);
//        //Calculate Labor Cost
//        BigDecimal laborCost = (finalOrder.getArea().multiply(finalOrder.getLaborCostPerSquareFoot()));
//        laborCost = laborCost.setScale(2, RoundingMode.HALF_DOWN);
//        finalOrder.setLaborCost(laborCost);
//        //calculate tax
//        BigDecimal tax = (finalOrder.getTaxRate().divide(new BigDecimal(100), 2, RoundingMode.HALF_DOWN));
//        tax = tax.multiply(finalOrder.getLaborCost().add(finalOrder.getMaterialCost()));
//        tax = tax.setScale(2, RoundingMode.HALF_DOWN);
//        finalOrder.setTax(tax);
//        //Calculate Total
//        BigDecimal total = finalOrder.getTax().add(finalOrder.getMaterialCost()).add(finalOrder.getLaborCost());
//        total = total.setScale(2, RoundingMode.HALF_DOWN);
//        finalOrder.setTotal(total);
//Order final valus are displayed for the user to review. It will then be passed to the controler
        io.print("=== Please review  your order! ===");
        io.print("Order Number " + order.getOrderNumber());
        io.print("Customer Name " + order.getCustomerName());
        io.print("Order Date (MMddYYYY) " + order.getOrderDate());
        io.print("State " + order.getState());
        io.print("Tax Rate " + order.getTaxRate());
        io.print("Product Type " + order.getProductType());
        io.print("Area " + order.getArea());
        io.print("Cost Per Square Foot " + order.getCostPerSquareFoot());
        io.print("Labor Cost Per Square Foot " + order.getLaborCostPerSquareFoot());
        io.print("Material Cost " + order.getMaterialCost());
        io.print("Labor Cost " + order.getLaborCost());
        io.print("Tax " + order.getTax());
        io.print("Total " + order.getTotal());

        return order;
    }

    /**
     * This method is used to save the new orders. The user will be asked if
     * they want to save. if Yes then true will be returned to the controller if
     * No then false will be.
     *
     * @return
     */
    public boolean saveThisNewOrder() {
        boolean saveOrder = false;
        while (saveOrder == false) {
            io.print("___________________________________________________________");
            String yesOrNo = io.readString("Would you like to save this new order? YES(Y) or NO(N)");
            yesOrNo = yesOrNo.toUpperCase();
            if (yesOrNo.equals("YES") || yesOrNo.equals("Y")) {
                return saveOrder = true;
            } else if (yesOrNo.equals("NO") || yesOrNo.equals("N")) {
                break;
            } else {
                saveOrder = false;
            }

        }
        return saveOrder;
    }

    /**
     * This method is used to save the new orders. The user will be asked if
     * they want to save. if Yes then true will be returned to the controller if
     * No then false will be.
     *
     * @return
     */
    public boolean editThisOrder() {
        boolean saveOrder = false;
        while (saveOrder == false) {
            io.print("___________________________________________________________");
            String yesOrNo = io.readString("Would you like to edit this new order? YES(Y) or NO(N)");
            yesOrNo = yesOrNo.toUpperCase();
            if (yesOrNo.equals("YES") || yesOrNo.equals("Y")) {
                return saveOrder = true;
            } else if (yesOrNo.equals("NO") || yesOrNo.equals("N")) {
                break;
            } else {
                saveOrder = false;
            }

        }
        return saveOrder;
    }

    /**
     * This method is used to save the new orders. The user will be asked if
     * they want to save. if Yes then true will be returned to the controller if
     * No then false will be.
     *
     * @return
     */
    public boolean saveEditedOrder() {
        boolean saveOrder = false;
        while (saveOrder == false) {
            io.print("___________________________________________________________");
            String yesOrNo = io.readString("Would you like to save this edited order?"
                    + " YES(Y) or NO(N)");
            yesOrNo = yesOrNo.toUpperCase();
            if (yesOrNo.equals("YES") || yesOrNo.equals("Y")) {
                return saveOrder = true;
            } else if (yesOrNo.equals("NO") || yesOrNo.equals("N")) {
                break;
            } else {
                saveOrder = false;
            }

        }
        return saveOrder;
    }

    /**
     * This method is used to save the new orders. The user will be asked if
     * they want to save. if Yes then true will be returned to the controller if
     * No then false will be.
     *
     * @return
     */
    public boolean deleteThisOrder() {
        boolean deleteOrder = false;
        while (deleteOrder == false) {
            io.print("___________________________________________________________");
            String yesOrNo = io.readString("Are you sure you want to delete this Order?"
                    + " YES(Y) or NO(N)");
            yesOrNo = yesOrNo.toUpperCase();
            if (yesOrNo.equals("YES") || yesOrNo.equals("Y")) {
                return deleteOrder = true;
            } else if (yesOrNo.equals("NO") || yesOrNo.equals("N")) {
                break;
            } else {
                deleteOrder = false;
            }

        }
        return deleteOrder;
    }

    public Order editOrder(Order order) {

        Order editedOrder = new Order(order.getOrderNumber());

        String customerName = io.readString("Enter customer name: (Current Value = " + order.getCustomerName() + ")");
        if (customerName.isEmpty()) {
            io.print("Customer name will remain :" + order.getCustomerName());
            io.print("_______________________________________________________");
            editedOrder.setCustomerName(order.getCustomerName());
        } else {
            editedOrder.setCustomerName(customerName);
        }
        String state = io.readString("Enter State : (Current Value = " + order.getState());
        if (state.isEmpty()) {
            io.print("State will remain: " + order.getState());
            io.print("_______________________________________________________");
            editedOrder.setState(order.getState());

        } else {
            editedOrder.setState(state);
        }
        BigDecimal taxRate = io.readBigDecimal("Enter Tax Rate : (Current Value = " + order.getTaxRate() + ")", false);
        if (taxRate == null) {
            io.print("Tax Rate will remain " + order.getTaxRate());
            io.print("_______________________________________________________");
            editedOrder.setTaxRate(order.getTaxRate());

        } else {
            editedOrder.setTaxRate(taxRate);
        }
        String productType = io.readString("Enter ProductType : (Current Value = " + order.getProductType() + ")");
        if (productType.isEmpty()) {
            io.print("Product Type will remain " + order.getProductType());
            io.print("_______________________________________________________");
            editedOrder.setProductType(order.getProductType());
        } else {
            editedOrder.setProductType(productType);
        }
        BigDecimal area = io.readBigDecimal("Enter area : (Current Value = " + order.getArea() + ")", false);
        if (area == null) {
            io.print("Area will remain " + order.getArea());
            io.print("_______________________________________________________");
            editedOrder.setArea(order.getArea());
        } else {
            editedOrder.setArea(area);
        }
        BigDecimal costPerSquareFoot = io.readBigDecimal("Enter Cost Per Square Foot: (Current Value = " + order.getCostPerSquareFoot() + ")", false);
        if (costPerSquareFoot == null) {
            io.print("Cost Per Square Foot will remain " + order.getCostPerSquareFoot());
            io.print("_______________________________________________________");
            editedOrder.setCostPerSquareFoot(order.getCostPerSquareFoot());
        } else {
            editedOrder.setCostPerSquareFoot(costPerSquareFoot);
        }
        BigDecimal laborPerSquareFoot = io.readBigDecimal("Enter Labor Per Square Foot : (Current Value = " + order.getLaborCostPerSquareFoot() + ")", false);
        if (laborPerSquareFoot == null) {
            io.print("Labor Cost Per Square Foot will remain " + order.getLaborCostPerSquareFoot());
            io.print("_______________________________________________________");
            editedOrder.setLaborCostPerSquareFoot(order.getLaborCostPerSquareFoot());
        } else {
            editedOrder.setLaborCostPerSquareFoot(laborPerSquareFoot);
        }

        BigDecimal materialCost = io.readBigDecimal("Enter Material Cost: (Current Value = " + order.getMaterialCost() + ")", false);
        if (materialCost == null) {
            io.print("Material Cost will remain " + order.getMaterialCost());
            io.print("_______________________________________________________");
            editedOrder.setMaterialCost(order.getMaterialCost());
        } else {
            editedOrder.setMaterialCost(materialCost);
        }
        BigDecimal laborCost = io.readBigDecimal("Enter Labor Cost: (Current Value = " + order.getLaborCost() + ")", false);
        if (laborCost == null) {
            io.print("Labor Cost Per Square Foot will remain " + order.getLaborCost());
            io.print("_______________________________________________________");
            editedOrder.setLaborCost(order.getLaborCost());
        } else {
            editedOrder.setLaborCost(laborCost);
        }

        BigDecimal tax = io.readBigDecimal("Enter Tax: (Current Value = " + order.getTax() + ")", false);
        if (tax == null) {
            io.print("Labor Cost Per Square Foot will remain " + order.getTax());
            io.print("_______________________________________________________");
            editedOrder.setTax(order.getTax());
        } else {
            editedOrder.setTax(tax);
        }
        BigDecimal total = io.readBigDecimal("Enter Total : (Current Value = " + order.getTotal() + ")", false);
        if (total == null) {
            io.print("Total will remain " + order.getTotal());
            io.print("_______________________________________________________");
            editedOrder.setTotal(order.getTotal());
        } else {
            editedOrder.setTotal(total);
        }
        LocalDate orderDate = io.readLocalDate("Enter Order Date Format (MMddYYYY): (Current Value = " + order.getOrderDate() + ")", false);
        if (orderDate == null) {
            io.print("Order Date will remain " + order.getOrderDate());
            io.print("_______________________________________________________");
            editedOrder.setOrderDate(order.getOrderDate());
        } else {
            editedOrder.setOrderDate(orderDate);
        }

        return editedOrder;
    }

    public void displayOrderList(List<Order> orderList) {
        for (Order currentOrder : orderList) {
            io.print("Order Number " + currentOrder.getOrderNumber() + ": "
                    + "Customer Name " + currentOrder.getCustomerName() + ": "
                    + "State " + currentOrder.getState() + ": "
                    + "Product Type " + currentOrder.getProductType() + ": "
                    + "Area " + currentOrder.getArea() + ": "
                    + "Order Date (MMddYYYY) " + currentOrder.getOrderDate() + ": "
                    + "Tax Rate " + currentOrder.getTaxRate() + ": "
                    + "Cost Per Square Foot " + currentOrder.getCostPerSquareFoot() + ": "
                    + "Labor Cost Per Square Foot " + currentOrder.getLaborCostPerSquareFoot() + ": "
                    + "Material Cost " + currentOrder.getMaterialCost() + ": "
                    + "Labor Cost " + currentOrder.getLaborCost() + ": "
                    + "Tax " + currentOrder.getTax() + ": "
                    + "Total " + currentOrder.getTotal());
        }
        io.readString("Please hit enter to continue.");
    }

    public void displayOrder(Order order) {
        if (order != null) {
            io.print("Order Number " + order.getOrderNumber());
            io.print("Customer Name " + order.getCustomerName());
            io.print("State " + order.getState());
            io.print("Product Type " + order.getProductType());
            io.print("Area " + order.getArea());
            io.print("Order Date (MMddYYYY) " + order.getOrderDate());
            io.print("Tax Rate " + order.getTaxRate());
            io.print("Cost Per Square Foot " + order.getCostPerSquareFoot());
            io.print("Labor Cost Per Square Foot " + order.getLaborCostPerSquareFoot());
            io.print("Material Cost " + order.getMaterialCost());
            io.print("Labor Cost " + order.getLaborCost());
            io.print("Tax " + order.getTax());
            io.print("Total " + order.getTotal());

        }
    }

    public LocalDate getOrderDate() {
        return io.readLocalDate("Please enter the Order Date.");
    }

    public int getOrderNumber() {
        return io.readInt("Please enter the Order Number.");
    }

    public void displayProdCreateOrderBanner() {
        io.print("=== Create New Order ====");
    }

    public void displayProdOrderCancelledBanner() {
        io.readString("New Order Cancelled press enter to continue.");
    }

    public void displayProdCreateSuccessBanner() {
        io.readString(
                "Order successfully created.  Please hit enter to continue");
    }

    public void displayOrdersByDateBanner() {
        io.print("=== Display Orders by Date ===");
    }

    public void displayOrdersByDateNoMatchFoundBanner(LocalDate ld) {
        io.print("___________________________________________________________");
        io.print("xxx No Match found for " + ld.toString() + " xxx");
        io.print(" ▄██████████████▄▐█▄▄▄▄█▌");
        io.print("██████▌▄▌▄▐▐▌███▌▀▀██▀▀");
        io.print("████▄█▌▄▌▄▐▐▌▀███▄▄█▌");
        io.print("▄▄▄▄▄██████████████▀");

        io.readString(
                "Press Enter to Continue.");
    }

    public void displayDisplayOrderBanner() {
        io.print("=== Display Orders ===");
    }

    public void displayRemoveOrderBanner() {
        io.print("=== Remove Order ===");
    }

    public void displayEditOrderBanner() {
        io.print("=== Edit Order ===");
    }

    public void displayItemNotEditedBanner() {
        io.print("___________________________________________________________");
        io.readString("The item has not been edited press enter to continue.");
    }

    public void displaySaveFilesBanner() {
        io.print("=== Saving Files ===");
    }

    public void displayExitBanner() {
        io.print("Good Bye!!!");
    }

    public void displayProductionModeBanner() {
        io.print("=== Production Mode ===");
    }

    public void displayTestModeBanner() {
        io.print("=== Test Mode ===");

    }

    public void displayUnknownCommandBanner() {
        io.print("Unknown Command!!!");
    }

    public void displayTrainingModeCantSave() {
        io.print("You are in training mode of the flooring application. Files Can not be saved.");
        io.print(" ▄██████████████▄▐█▄▄▄▄█▌");
        io.print("██████▌▄▌▄▐▐▌███▌▀▀██▀▀");
        io.print("████▄█▌▄▌▄▐▐▌▀███▄▄█▌");
        io.print("▄▄▄▄▄██████████████▀");

        io.readString("Press enter to continue.");
    }

    public void displayTrainingModeExit() {
        io.print("You are exiting the application in training moded. Changes will not be saved.");
        io.readString("Press enter to continue.");
    }

    public void displayRemoveSuccessBanner() {
        io.readString("Order successfully removed. Please hit enter to continue.");
    }

    public void displayOrderNotRemoved() {
        io.readString("The Order has not been deleted.. Please hit enter to continue.");
    }

    public void displayOrderNotFound() {
        io.readString("The Order you searched for could not be found. Please hit enter to continue.");

        io.print(" ▄██████████████▄▐█▄▄▄▄█▌");
        io.print("██████▌▄▌▄▐▐▌███▌▀▀██▀▀");
        io.print("████▄█▌▄▌▄▐▐▌▀███▄▄█▌");
        io.print("▄▄▄▄▄██████████████▀");
    }

    public void displayErrorMessage(String errorMsg) {
        io.print("=== ERROR ===");
        io.print(" ▄██████████████▄▐█▄▄▄▄█▌");
        io.print("██████▌▄▌▄▐▐▌███▌▀▀██▀▀");
        io.print("████▄█▌▄▌▄▐▐▌▀███▄▄█▌");
        io.print("▄▄▄▄▄██████████████▀");

        io.print(errorMsg);
    }

}
