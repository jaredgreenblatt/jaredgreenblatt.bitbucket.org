/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.ui;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 *
 * @author Jared
 */
public class UserIOConsoleImpl implements UserIO {

    Scanner sc = new Scanner(System.in);

    @Override
    public void print(String message) {
        System.out.println(message);
    }

    @Override
    public void printOneLine(String message) {
        System.out.print(message);
    }

    @Override
    public double readDouble(String prompt) {
        double x;
        try {
            System.out.println(prompt);
            x = Double.parseDouble(sc.nextLine());

        } catch (NumberFormatException e) {
            System.out.println("You have entered a non double number");
            x = -1;

        }
        return x;

    }

    @Override
    public double readDouble(String prompt, double min, double max
    ) {
        double x, y;
        try {
            while (true) {
                System.out.println(prompt);
                x = Double.parseDouble(sc.nextLine());
                if (x >= min && x <= max) {
                    y = x;

                    break;

                }
            }
        } catch (NumberFormatException e) {
            System.out.println("You have entered a non double number");
            y = -1;

        }
        return y;
    }

    @Override
    public float readFloat(String prompt
    ) {
        float x;
        try {
            System.out.println(prompt);
            x = Float.parseFloat(sc.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("You have entered a non float number");
            x = -1;

        }

        return x;

    }

    @Override
    public float readFloat(String prompt, float min, float max
    ) {
        float x, y;

//        
        try {
            while (true) {
                System.out.println(prompt);
                x = Float.parseFloat(sc.nextLine());
                if (x >= min && x <= max) {
                    y = x;
                    break;

                }

            }
        } catch (NumberFormatException e) {
            System.out.println("You have entered a non float number");
            y = -1;

        }

        return y;

    }

    @Override
    public int readInt(String prompt
    ) {
        int x;

        try {
            System.out.println(prompt);
            x = Integer.parseInt(sc.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("You have entered a non interger number");
            x = -1;
        }

        return x;

    }

    @Override
    public int readInt(String prompt, int min, int max
    ) {
        int x, y;
        try {
            while (true) {
                System.out.println(prompt);

                x = Integer.parseInt(sc.nextLine());
                if (x >= min && x <= max) {
                    y = x;
                    break;
                } else {
                    System.out.println("_______________________________________________");
                    System.out.println("Please Select a number from 1-6 to correspond ");
                    System.out.println("with the activity you would like to perform on");
                    System.out.println("on the Flooring Master.");
                    System.out.println("_______________________________________________");
                    y = -1;

                }

            }

        } catch (NumberFormatException e) {
            System.out.println("_______________________________________________");
            System.out.println("Please Select a number from 1-6 to correspond ");
            System.out.println("with the activity you would like to perform on");
            System.out.println("on the Flooring Master.");
            System.out.println("_______________________________________________");
            y = -1;

        }
        return y;

    }

    @Override
    public long readLong(String prompt) {
        long x;
        try {
            System.out.println(prompt);
            x = Long.parseLong(sc.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("You've entered non-Long number");
            x = -1;

        }

        return x;

    }

    @Override
    public long readLong(String prompt, long min, long max
    ) {
        long x, y;
        try {

            while (true) {
                System.out.println(prompt);
                x = Long.parseLong(sc.nextLine());
                if (x >= min && x <= max) {
                    y = x;
                    break;

                }

            }
        } catch (NumberFormatException e) {
            System.out.println("You've entered non-Long number");
            y = -1;
        }
        return y;

    }

    @Override
    public String readString(String prompt) {
        String x;
        System.out.println(prompt);
        x = sc.nextLine();
        return x;
    }

    @Override
    public String readString(String prompt, boolean isDate) {
        String x = null;
        while (isDate == false) {
            System.out.println(prompt);
            x = sc.nextLine();
            if (x.isEmpty() == false) {
                isDate = true;
            } else {
                x = "";
                System.out.println("You have entered invalid data.");
                isDate = false;

            }

        }

        return x;

    }

    @Override
    public BigDecimal readBigDecimal(String prompt) {
        BigDecimal x = new BigDecimal("0");
        try {
            System.out.println(prompt);
            x = new BigDecimal(sc.nextLine());
            x = x.setScale(2, RoundingMode.HALF_EVEN);
        } catch (NumberFormatException | InputMismatchException e) {
            System.out.println("You've entered non-BigDecimal Number");

        }

        return x;

    }

    @Override
    public BigDecimal readBigDecimal(String prompt, boolean isBigDecimal) {
        BigDecimal x = new BigDecimal("0");

        try {
            System.out.println(prompt);
            x = new BigDecimal(sc.nextLine());
            x = x.setScale(2, RoundingMode.HALF_EVEN);
            isBigDecimal = true;
        } catch (NumberFormatException | InputMismatchException e) {
            x = null;
            System.out.println("You've entered non-BigDecimal Number");
            isBigDecimal = false;

        }

        return x;

    }

    public BigDecimal readBigDecimal(String prompt, BigDecimal min, BigDecimal max) {

        BigDecimal x, w, v;
        double y;
        String z;
        try {
            while (true) {
                System.out.println(prompt);
                y = sc.nextDouble();
                z = Double.toString(y);
                x = new BigDecimal(z);
                if (x.compareTo(min) > -1 && x.compareTo(max) < 1) {
                    w = x;
                    break;
                }

            }
            return w;
        } catch (NumberFormatException | InputMismatchException e) {
            System.out.println("You've entered non-BigDecimal Number");
            v = min.subtract(BigDecimal.ONE);

        }

        return v;
    }

    @Override
    public LocalDate readLocalDate(String prompt) {
        LocalDate ld = null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMddyyyy");
        while (ld == null) {
            try {
                System.out.println(prompt);

                ld = LocalDate.parse(sc.nextLine(), formatter);

            } catch (DateTimeParseException e) {
                System.out.println("You've entered a non date");

            }

        }

        return ld;

    }

    @Override
    public LocalDate readLocalDate(String prompt, boolean isDate) {
        LocalDate ld = null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMddyyyy");

        try {
            System.out.println(prompt);

            ld = LocalDate.parse(sc.nextLine(), formatter);

            isDate = true;

        } catch (DateTimeParseException e) {
            System.out.println("You've entered a non date");
            ld = null;

            isDate = false;

        }

        return ld;

    }

    @Override
    public BigDecimal readBigDecimalNoNull(String prompt, boolean isBigDecimal) {
        BigDecimal x = new BigDecimal("0");
        while (isBigDecimal == false) {

            try {
                System.out.println(prompt);
                x = new BigDecimal(sc.nextLine());
                x = x.setScale(2, RoundingMode.HALF_EVEN);
                isBigDecimal = true;
            } catch (NumberFormatException | InputMismatchException e) {
                x = null;
                System.out.println("You've entered non-BigDecimal Number");
                isBigDecimal = false;

            }

        }

        return x;

    }

}
