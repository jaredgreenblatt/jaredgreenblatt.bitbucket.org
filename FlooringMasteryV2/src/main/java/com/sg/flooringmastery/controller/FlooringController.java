/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.controller;

import com.sg.flooringmastery.dao.LoadProgramPersistanceException;
import com.sg.flooringmastery.dao.OrderPersistanceException;
import com.sg.flooringmastery.dao.ProductTypePersistanceException;
import com.sg.flooringmastery.dao.StateTaxPersistanceException;
import com.sg.flooringmastery.dto.Order;
import com.sg.flooringmastery.dto.ProductType;
import com.sg.flooringmastery.dto.TaxRate;
import com.sg.flooringmastery.servicelayer.FlooringServiceLayer;
import com.sg.flooringmastery.servicelayer.OrderDataValidationException;
import com.sg.flooringmastery.servicelayer.OrderDuplicateIdException;
import com.sg.flooringmastery.servicelayer.ProductTypeDoesntExistException;
import com.sg.flooringmastery.servicelayer.StateTaxDoesntExistException;
import com.sg.flooringmastery.ui.FlooringView;
import java.time.LocalDate;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class FlooringController {

    FlooringView view;
    FlooringServiceLayer service;

    public FlooringController(FlooringServiceLayer service, FlooringView view) {
        this.service = service;
        this.view = view;
    }

    /**
     * Run Method in the controller is used to run the menu of the application.
     * It also opens the Order Files, Program Mode, Tax Rate and product type
     * files. The users inputs taken from the view are taken and then it directs
     * the user to the appropriate workflow.
     */
    public void run() {
        boolean keepGoing = true;
        int menuSelection = 0;

        try {
            service.openOrderFile();
            service.openProductTypeFile();
            service.openTaxRateFile();
            service.openFlooringProgramMode();
            //Checking the mode of the file. If the mode is true. The app will be
            // in production Mode. If false it will be in test mode. 
            boolean inProduction = service.productionMode();
            if (inProduction == true) {
                view.displayProductionModeBanner();
            } else if (inProduction == false) {
                view.displayTestModeBanner();
            }
            while (keepGoing) {
                menuSelection = getMenuSelection();
                switch (menuSelection) {
                    case 1:
                        displayOrders();
                        break;
                    case 2:
                        createOrder();
                        break;
                    case 3:
                        editOrder();
                        break;
                    case 4:
                        removeOrder();
                        break;
                    case 5:
                        saveAllOrderFiles();
                        break;
                    case 6:
                        keepGoing = false;
                        break;
                    default:
                        unknownCommand();
                }

            }
            saveOnExit();
            exitMessage();
        } catch (OrderPersistanceException | LoadProgramPersistanceException | ProductTypePersistanceException |
                StateTaxPersistanceException e) {
            view.displayErrorMessage(e.getMessage());
        }
    }
//Gets the user selection from the view. So the menu above can run the requested
//workflow.

    private int getMenuSelection() {
        return view.printMenuAndGetSelection();
    }

    /**
     * Controller method to create a new order. It will bring in a list of
     * product types and state tax information and pass those to the view for
     * the user to enter there initial order information. Once entered that
     * information will be used to get the specific product and tax information
     * so that the order calculations can be confirmed. These will then be
     * displayed to the user. The user can choose to save or discard the order.
     * If saved an ID will be assigned. If no is selected the order will be
     * canceled.
     */
    private void createOrder() {
        try {
            //Displays Create order menu
            view.displayProdCreateOrderBanner();
            //gets a list of product types
            List<ProductType> productTypeList = service.getAllProductTypes();
            //gets list of taxRates
            List<TaxRate> taxRateList = service.getAllStateTaxRates();
            // passes the list into the order information .
            Order newOrder = view.getNewOrderInfo(productTypeList, taxRateList);
            //gets the product type based on the user input in the view.
          //  ProductType productType = service.getProductType(newOrder.getProductType());
            //gets the taxRate based on the user input in the view.
           // TaxRate taxRate = service.getTaxRate(newOrder.getState());
            
            //gets unique id for the order. 
           // int uniqueId = service.uniqueOrderId();
            // Passes the order, product type, taxRate and Unique Id  back to the view.
            //So the user is able to view the completed order information.
           // Order completedOrder = view.finishNewOrder(newOrder, productType, taxRate, uniqueId);
           Order completedOrder = service.calculateOrder(newOrder);
           
           
            //They are then asked if they would like to save the order. 
            //There answer wil be returned as a boolean. yes true/no false.
            view.finishNewOrder(newOrder);
            Boolean createNewOrder = view.saveThisNewOrder();

            /*
            If true a new order will be created and a success message will be 
            diplayed.
             */
            if (createNewOrder == true) {

                service.createOrder(completedOrder.getOrderNumber(), completedOrder);
                view.displayProdCreateSuccessBanner();
            //  If the answer is no the order will not be saved.
            } else if (createNewOrder == false) {
                view.displayProdOrderCancelledBanner();

            }
        } catch (OrderDuplicateIdException | OrderDataValidationException | StateTaxDoesntExistException |
                OrderPersistanceException e) {
            view.displayErrorMessage(e.getMessage());

        }
    }

    /**
     * Display orders controller method. Is used to search orders by date. We
     * then return a list of orders by the inputed date. If the list size is
     * greater than 0 the orders from that date will be displayed. If the list
     * size is 0 the message no orders found will be displayed.
     */
    private void displayOrders() {
        view.displayOrdersByDateBanner();
        LocalDate searchDate = view.getOrderDate();
        List<Order> orderList = service.getOrdersByDate(searchDate);
        /*
        if the list size is greater than 0 it will be displayed else an error 
        message will be printed to inform the user that no matches were found 
        for there query.
        */
        if (orderList.size() > 0) {
            view.displayOrderList(orderList);
        } else {
            view.displayOrdersByDateNoMatchFoundBanner(searchDate);

        }

    }

    /**
     * Controller Method to remove orders. First it gets the order date and
     * order number from the user. It will then try to get an order that matches
     * those criteria. If there is a match it will display the results. It will
     * then ask the user if they want to delete the item. If they select yes the
     * order will be deleted.
     */
    private void removeOrder() {
        view.displayRemoveOrderBanner();
        // getting the local date and order number from the user.
        LocalDate orderDate = view.getOrderDate();
        int orderNumber = view.getOrderNumber();
        try {
            //get Order for the date and order number.
            Order getOrder = service.getOrder(orderNumber, orderDate);
            /*
            If order is not null. Display the order on the screen  and then ask 
            the user if they are sure they want to delete. Store there answer as a 
            boolean yes=true and no = false. If yes the order will be removed. If 
            no a message will be printed that the order is not deleted
            
            if no order is found an order no found message will be printed.
            */
            if (getOrder != null) {
                view.displayOrder(getOrder);
                Boolean DeleteOrder = view.deleteThisOrder();
                if (DeleteOrder == true) {
                    service.removeOrder(orderNumber);

                } else if (DeleteOrder == false) {
                    view.displayOrderNotRemoved();
                }

            } else {
                view.displayOrderNotFound();
            }

        } catch (OrderPersistanceException e) {
            view.displayErrorMessage(e.getMessage());

        }
    }

    /**
     * Controller Method for editing orders. First it gets the order date and
     * order number from the user. It will then try to get an order that matches
     * those criteria. If there is a match it will display the results. It will
     * then prompt the user if they want to edit. If they select no the item
     * will not be edited. If they select yes they will then be prompted to edit
     * the items fields.
     */
    private void editOrder() {
        //displays edited order banner.
        view.displayEditOrderBanner();
        // get orderDate and orderNumber.
        LocalDate orderDate = view.getOrderDate();
        int orderNumber = view.getOrderNumber();
        try {
            // Try to get an order based on the user inputs.
            Order getOrder = service.getOrder(orderNumber, orderDate);
            /*
            If its not null it will display the user and then ask the user if they
            would like to edit the order.
            */
            if (getOrder != null) {
                view.displayOrder(getOrder);
                Boolean editTheOrder = view.editThisOrder();
                /*
                If the user selects yes the user will then go through with the edit process.
                First the view will be called were the user will go through 
                prompts to update the order. Once finished they will be shown 
                this infomation back. They will then be asked to save the data.
                If yes the data will be saved if not the data will not be saved.
                */
                if (editTheOrder == true) {
                    Order editedOrder = view.editOrder(getOrder);
                    view.displayOrder(editedOrder);
                    Boolean saveThisEditedOrder = view.saveEditedOrder();
                    if (saveThisEditedOrder == true) {
                        service.removeOrder(getOrder.getOrderNumber());
                        service.createOrder(editedOrder.getOrderNumber(), editedOrder);

                    } else if (saveThisEditedOrder == false) {
                        view.displayItemNotEditedBanner();
                    }

                } else if (editTheOrder == false) {
                    view.displayItemNotEditedBanner();
                }

            } else {
                view.displayOrderNotFound();
            }

        } catch (OrderPersistanceException | OrderDuplicateIdException | OrderDataValidationException e) {
            view.displayErrorMessage(e.getMessage());

        }
    }

    /**
     * Controller method to save the files in the application. First it checks
     * what mode the application is running in. If its in the production it will
     * save the files. If the application is in test it will display a message
     * to the user that the files cannot be saved.
     */
    private void saveAllOrderFiles() {
        boolean inProduction = service.productionMode();
        if (inProduction == true) {
            view.displaySaveFilesBanner();

            try {
                service.closeOrderFile();
            } catch (OrderPersistanceException e) {
                view.displayErrorMessage(e.getMessage());
            }

        } else if (inProduction == false) {

            view.displayTrainingModeCantSave();
        }

    }

    /**
     * Controller method used to save on exit . It first checks the mode the
     * user is in if they are in prod the application will save the file and
     * then exit the app. If the user is in test the application will exit
     * without saving and will notify the user.
     *
     */
    private void saveOnExit() {
        boolean inProduction = service.productionMode();
        if (inProduction == true) {

            try {
                service.closeOrderFile();
            } catch (OrderPersistanceException e) {
                view.displayErrorMessage(e.getMessage());
            }

        } else if (inProduction == false) {

            view.displayTrainingModeExit();
        }

    }

    private void unknownCommand() {
        view.displayUnknownCommandBanner();
    }

    private void exitMessage() {
        view.displayExitBanner();
    }
}
