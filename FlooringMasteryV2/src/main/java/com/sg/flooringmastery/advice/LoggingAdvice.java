/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.advice;

import com.sg.flooringmastery.dao.FlooringAuditDao;
import com.sg.flooringmastery.dao.OrderPersistanceException;
import org.aspectj.lang.JoinPoint;

/**
 *
 * Logging Advice is used in conjunction with Spring AOP. This allows us to write 
 * audit entries while our program is run.
 * @author Jared
 */
public class LoggingAdvice {

    FlooringAuditDao auditDao;

    public LoggingAdvice(FlooringAuditDao auditDao) {
        this.auditDao = auditDao;
    }
/**
 * Create a join point entry for methods. It gets the signature name of the  method
 * then writes out the objects  to audit file.
 * @param jp 
 */
    public void createAuditEntry(JoinPoint jp) {
        Object[] args = jp.getArgs();
        String auditEntry = jp.getSignature().getName() + ": ";
        for (Object currentArg : args) {
            auditEntry += currentArg;
        }
        try {
            auditDao.writeAuditEntry(auditEntry);
        } catch (OrderPersistanceException e) {
            System.err.println(
                    "ERROR: Could not create audit entry in LoggingAdvice.");
        }
    }
/**
 * creates a join point to write in audit file any time an exception is thrown. 
 * The error message will printed with the method name and the word error
 * Also the exception message will be thrown.
 * @param jp
 * @param e 
 */
    public void exceptionLogger(JoinPoint jp, Exception e) {

        String methodName = jp.getSignature().getName();
        String exceptionMessage = e.getMessage();
        try {
            auditDao.writeAuditEntry(methodName + " ERROR!");
            auditDao.writeAuditEntry(exceptionMessage);
        } catch (OrderPersistanceException ex) {
            System.err.println(
                    "ERROR: Could not create audit entry in LoggingAdvice.");
        }

    }
}
