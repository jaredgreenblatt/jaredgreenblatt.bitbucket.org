/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.servicelayer;

/**
 *
 * @author Jared
 */
public class StateTaxDoesntExistException extends Exception {

    public StateTaxDoesntExistException(String message) {
        super(message);

    }

    public StateTaxDoesntExistException(String message, Throwable cause) {
        super(message, cause);

    }
}
