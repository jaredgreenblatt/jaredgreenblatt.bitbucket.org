/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.Order;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Jared
 */
public interface FlooringOrderDao {

    Order addOrder(int OrderNumber, Order order);

    List<Order> getAllOrders();

    Order getOrder(int OrderNumber);

    Order removeOrder(int OrderNumber);

    void editOrder(Order order);

    public void Open() throws OrderPersistanceException;

    public void OpenProgramModeFile() throws LoadProgramPersistanceException;

    public void Close() throws OrderPersistanceException;

    public String getProgramMode();

    public void setProgramMode(String programMode);

}
