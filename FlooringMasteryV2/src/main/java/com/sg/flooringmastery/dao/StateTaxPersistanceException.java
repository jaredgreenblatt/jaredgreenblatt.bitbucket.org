/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

/**
 *
 * @author Jared
 */
public class StateTaxPersistanceException extends Exception {

    public StateTaxPersistanceException(String message) {
        super(message);

    }

    public StateTaxPersistanceException(String message, Throwable cause) {
        super(message, cause);
    }

}
