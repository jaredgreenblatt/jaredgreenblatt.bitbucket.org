/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

/**
 *
 * @author Jared
 */
public class ProductTypePersistanceException extends Exception {

    public ProductTypePersistanceException(String message) {
        super(message);

    }

    public ProductTypePersistanceException(String message, Throwable cause) {
        super(message, cause);
    }

}
