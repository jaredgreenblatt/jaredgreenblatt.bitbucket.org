/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.Order;
import com.sg.flooringmastery.dto.ProductType;
import java.util.List;

/**
 *
 * @author Jared
 */
public interface ProductTypeDao {

    ProductType addProductType(String productType, ProductType product);

    List<ProductType> getProductType();

    ProductType getProductType(String productType);

    ProductType removeProductType(String productType);

    void editProductType(String productType);

    public void Open() throws ProductTypePersistanceException ;

    public void Close() throws ProductTypePersistanceException ;

}
