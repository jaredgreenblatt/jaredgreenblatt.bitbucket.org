/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.Order;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author Jared
 */
public class FlooringOrderDaoImpl implements FlooringOrderDao {

    private Map<Integer, Order> orders = new HashMap<>();
    public static final String ORDER_FILE = "orders/Orders_";
    public static final String FILE_TYPE = ".txt";
    public static final String DELIMITER = ", ";
    public static final String DELIMITER_WITH_SPACE = ", |\\,";

    public static final File DIRECTORY = new File("orders/");
    public static final String PROGRAM_MODE = "programMode.txt";
    public static final String PROGRAM_MODE_DELIMITER = "::";
    private String programMode;

    String fileDate = null;

    private void loadOrders() throws OrderPersistanceException {

        //creat an array of files.
        File[] fileList = DIRECTORY.listFiles();
        //loop through each file and get the name as a string 
        //take a substring of the name from (7,15) spots this will get the date 
        // in the format 01012000
        for (File file : fileList) {
            fileDate = file.getName();
            fileDate = fileDate.substring(7, 15);
            Scanner scanner;

            try {
                /*
                 Now we will read each file with the format order/Order_MMddyyy.txt
                into the orders map.

                 */
                // Create Scanner for reading the file
                scanner = new Scanner(
                        new BufferedReader(
                                new FileReader(ORDER_FILE + fileDate + FILE_TYPE)));
            } catch (FileNotFoundException e) {
                throw new OrderPersistanceException(
                        "-_- Could not load roster data into memory.", e);
            }
            // currentLine holds the most recent line read from the file
            String currentLine;
            // currentTokens holds each of the parts of the currentLine after it has
            // been split on our DELIMITER

            String[] currentTokens;
            // Go through files line by line
            while (scanner.hasNextLine()) {
                // get the next line in the file
                currentLine = scanner.nextLine();
                // break up the line into tokens
//                currentTokens = currentLine.split(DELIMITER);
                currentTokens = currentLine.split(DELIMITER_WITH_SPACE);

                // Create a new Order object and put it into the map of orders
                Order currentOrder = new Order(Integer.parseInt(currentTokens[0]));
                // Set the remaining vlaues on  orders manually

                currentOrder.setCustomerName(currentTokens[1].trim());
                currentOrder.setState(currentTokens[2].trim());
                currentOrder.setTaxRate(new BigDecimal(currentTokens[3].trim()));
                currentOrder.setProductType(currentTokens[4].trim());
                currentOrder.setArea(new BigDecimal(currentTokens[5].trim()));
                currentOrder.setCostPerSquareFoot(new BigDecimal(currentTokens[6].trim()));
                currentOrder.setLaborCostPerSquareFoot(new BigDecimal(currentTokens[7].trim()));
                currentOrder.setMaterialCost(new BigDecimal(currentTokens[8].trim()));
                currentOrder.setLaborCost(new BigDecimal(currentTokens[9].trim()));
                currentOrder.setTax(new BigDecimal(currentTokens[10].trim()));
                currentOrder.setTotal(new BigDecimal(currentTokens[11].trim()));
                LocalDate ld;
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMddyyyy");
                ld = LocalDate.parse(fileDate, formatter);
                currentOrder.setOrderDate(ld);

                // Put currentOrders into the map using orderNumber as the key
                orders.put(currentOrder.getOrderNumber(), currentOrder);
            }
            // close scanner
            scanner.close();
        }

    }

    /**
     * Writes all Orders in the orders map out to a order file. See loadOrders
     * for file format.
     *
     *
     */
    private void writeOrderFile() throws OrderPersistanceException {
        /*
        Created a file object named filelist. We then use the DIRECTORY String
        that we created to delete all files in the order directory.
        This is done by looping through the file Array. 
         */
        File[] fileList = DIRECTORY.listFiles();
        for (File file : fileList) {
            file.delete();
        }

        //created new printWriter named out.
        PrintWriter out;
        //get a list of all the orders.
        List<Order> orderList = this.getAllOrders();
        // create a date time formatter with the patern 01012017
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMddyyyy");
        //create a new local date
        LocalDate uniqueDates;
        // enhanced for loop to go through all the orders in the order list/
        for (Order orderDateList : orderList) {
            //grab the date and then format the date.
            uniqueDates = orderDateList.getOrderDate();
            fileDate = uniqueDates.format(formatter);
            try {
                /*
             try to print the order infromation into the file. Using the the file name
                order/Order_01012000.txt  it will then create the file if it does 
                not exist or will append to the file.

                 */
                out = new PrintWriter(new FileWriter(ORDER_FILE + fileDate + FILE_TYPE), true);
                for (Order writeOrder : orderList) {
                    if (uniqueDates.equals(writeOrder.getOrderDate())) {
                        out.println(writeOrder.getOrderNumber() + DELIMITER
                                + writeOrder.getCustomerName() + DELIMITER
                                + writeOrder.getState() + DELIMITER
                                + writeOrder.getTaxRate() + DELIMITER
                                + writeOrder.getProductType() + DELIMITER
                                + writeOrder.getArea() + DELIMITER
                                + writeOrder.getCostPerSquareFoot() + DELIMITER
                                + writeOrder.getLaborCostPerSquareFoot() + DELIMITER
                                + writeOrder.getMaterialCost() + DELIMITER
                                + writeOrder.getLaborCost() + DELIMITER
                                + writeOrder.getTax() + DELIMITER
                                + writeOrder.getTotal()
                        );
                        // force PrintWriter to write line to the file
                        out.flush();
                    }

                }
                // Clean up
                out.close();
                // write the order object to the file
            } catch (IOException e) {
                throw new OrderPersistanceException(
                        "Could not save student data.", e);
            }

        }
    }
//Adding an Order
    @Override
    public Order addOrder(int OrderNumber, Order order) {
        Order newOrder = orders.put(OrderNumber, order);
        return newOrder;
    }
//Listing orders
    @Override
    public List<Order> getAllOrders() {
        return new ArrayList<Order>(orders.values());
    }
//getting orders
    @Override
    public Order getOrder(int OrderNumber) {
        return orders.get(OrderNumber);
    }
//removing orders
    @Override
    public Order removeOrder(int OrderNumber) {
        Order removeOrder = orders.remove(OrderNumber);
        return removeOrder;
    }
//editing orders
    @Override
    public void editOrder(Order order) {
        removeOrder(order.getOrderNumber());
        orders.put(order.getOrderNumber(), order);

    }

    @Override
    public void Open() throws OrderPersistanceException {
        loadOrders();
    }

    @Override
    public void Close() throws OrderPersistanceException {
        writeOrderFile();
    }

    @Override
    public void OpenProgramModeFile() throws LoadProgramPersistanceException {
        loadProgramMode();
    }
//loading the program mode file.
    private void loadProgramMode() throws LoadProgramPersistanceException {

        Scanner scanner;
//reads the file
        try {
            // Create Scanner for reading the file
            scanner = new Scanner(
                    new BufferedReader(
                            new FileReader(PROGRAM_MODE)));
        } catch (FileNotFoundException e) {
            throw new LoadProgramPersistanceException(
                    "-_- Could not load roster data into memory.", e);
        }
        // currentLine holds the most recent line read from the file
        String currentLine;
        // currentTokens holds each of the parts of the currentLine after it has

        String[] currentTokens;
        // Go through PROGRAM_MODE line by line, decoding each line into a 

        while (scanner.hasNextLine()) {
            // get the next line in the file
            currentLine = scanner.nextLine();
            // break up the line into tokens
            currentTokens = currentLine.split(PROGRAM_MODE_DELIMITER);

        //set the string programModeInfo 
            String programModeInfo = currentTokens[0];
            // Set the program mode to token 1. This will either be test or prod.
            programMode = currentTokens[1];

        }
        // close scanner
        scanner.close();
    }

    public String getProgramMode() {
        return programMode;
    }

    public void setProgramMode(String programMode) {
        this.programMode = programMode;
    }

}
