/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.ProductType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jared
 */
public class ProductTypeDaoStubImpl implements ProductTypeDao {

    ProductType productTypeTest;
    List<ProductType> productTypeList = new ArrayList<>();

    public ProductTypeDaoStubImpl() {
        productTypeTest = new ProductType("Wood");
        productTypeTest.setCostPerSquareFoot(new BigDecimal(5.15));
        productTypeTest.setLaborCostPerSquareFoot(new BigDecimal(4.75));
        productTypeList.add(productTypeTest);
    }

    @Override
    public ProductType addProductType(String productType, ProductType product) {
        return this.productTypeTest;
    }

    @Override
    public List<ProductType> getProductType() {
        return productTypeList;
    }

    @Override
    public ProductType getProductType(String productType) {
        if (productType.equals(productTypeTest.getProductType())) {
            return productTypeTest;
        }
        else{
            return null;
        }
    }

    @Override
    public ProductType removeProductType(String productType) {
        if (productType.equals(productTypeTest.getProductType())) {
            return productTypeTest;
        }
        else{
            return null;
        }
    }

    @Override
    public void editProductType(String productType) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void Open() throws ProductTypePersistanceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void Close() throws ProductTypePersistanceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
