/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;

/**
 *
 * @author Jared
 */
public class FlooringAuditDaoStubImpl implements FlooringAuditDao {
public static final String AUDIT_FILE = "test_audit.txt";
  
    @Override
    public void writeAuditEntry(String entry) throws OrderPersistanceException {
        PrintWriter out;

        try {
            out = new PrintWriter(new FileWriter(AUDIT_FILE, true));
        } catch (IOException ex) {
            throw new OrderPersistanceException("Could not persist audit infomration", ex);
        }

        LocalDateTime timestamp = LocalDateTime.now();
        out.println(timestamp.toString() + " : " + entry);
        out.flush();
    }

}
