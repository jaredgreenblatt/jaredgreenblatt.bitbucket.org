/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.TaxRate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jared
 */
public class StateTaxDaoStubEmptyImpl implements StateTaxDao {

    @Override
    public TaxRate addStateTax(String State, TaxRate taxRate) {
        return taxRate;
    }

    @Override
    public List<TaxRate> getAllStateTax() {
        return new ArrayList<TaxRate>();
    }

    @Override
    public TaxRate getTaxRate(String State) {
        return null;
    }

    @Override
    public TaxRate removeTaxRate(String State) {
        return null;
    }

    @Override
    public void editTaxRate(String State) {
        return;
    }

    @Override
    public void Open() throws StateTaxPersistanceException {
        return;
    }

    @Override
    public void Close() throws StateTaxPersistanceException {
        return;
    }

}
