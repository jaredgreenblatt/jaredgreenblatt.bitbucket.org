/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.Order;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 *
 * @author Jared
 */
public class FlooringOrderDaoTest {

    private FlooringOrderDao dao = new FlooringOrderDaoImpl();

    public FlooringOrderDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        List<Order> orderList = dao.getAllOrders();
        for (Order order : orderList) {
            dao.removeOrder(order.getOrderNumber());
        }
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addOrder method, of class FlooringOrderDao.
     */
    @Test
    public void testGetOrderListgainstEmptyDAO() throws OrderPersistanceException {
        List<Order> orderItemList = dao.getAllOrders();

        // Check it's an empty List
        assertNotNull("Get all Orders, expected list to be not null", orderItemList);
        assertTrue("Was expecting empty list, with new dao", orderItemList.isEmpty());
        assertEquals("Was expecting empty list, with new dao", orderItemList.size(), 0);

    }

    @Test
    public void testGetOrderending() throws Exception {

        Order fromDao = dao.getOrder(001);

        assertEquals(fromDao, null);

    }

    public void testAddAndThenGet() throws Exception {
        Order orderTest = new Order(001);
        orderTest.setArea(BigDecimal.ONE);
        orderTest.setCostPerSquareFoot(BigDecimal.ZERO);
        orderTest.setCustomerName("SubZero");
        orderTest.setLaborCost(BigDecimal.ZERO);
        orderTest.setLaborCostPerSquareFoot(BigDecimal.ZERO);
        orderTest.setMaterialCost(BigDecimal.ZERO);
        orderTest.setOrderDate(LocalDate.MAX);
        orderTest.setProductType("Cup");
        orderTest.setState("MA");
        orderTest.setTax(BigDecimal.TEN);
        orderTest.setTaxRate(BigDecimal.ZERO);
        orderTest.setTotal(BigDecimal.ONE);

        dao.addOrder(orderTest.getOrderNumber(), orderTest);

        Order fromDao = dao.getOrder(orderTest.getOrderNumber());
        assertEquals(orderTest, fromDao);
    }

    @Test
    public void testGetAllOrdersWith2() throws Exception {
        Order Order1 = new Order(001);
        Order1.setArea(BigDecimal.ONE);
        Order1.setCostPerSquareFoot(BigDecimal.ZERO);
        Order1.setCustomerName("SubZero");
        Order1.setLaborCost(BigDecimal.ZERO);
        Order1.setLaborCostPerSquareFoot(BigDecimal.ZERO);
        Order1.setMaterialCost(BigDecimal.ZERO);
        Order1.setOrderDate(LocalDate.MAX);
        Order1.setProductType("Cup");
        Order1.setState("MA");
        Order1.setTax(BigDecimal.TEN);
        Order1.setTaxRate(BigDecimal.ZERO);
        Order1.setTotal(BigDecimal.ONE);

        dao.addOrder(Order1.getOrderNumber(), Order1);

        Order Order2 = new Order(002);
        Order2.setArea(BigDecimal.ONE);
        Order2.setCostPerSquareFoot(BigDecimal.ZERO);
        Order2.setCustomerName("Jeff");
        Order2.setLaborCost(BigDecimal.ZERO);
        Order2.setLaborCostPerSquareFoot(BigDecimal.ZERO);
        Order2.setMaterialCost(BigDecimal.ZERO);
        Order2.setOrderDate(LocalDate.MAX);
        Order2.setProductType("Tile");
        Order2.setState("NH");
        Order2.setTax(BigDecimal.TEN);
        Order2.setTaxRate(BigDecimal.ZERO);
        Order2.setTotal(BigDecimal.ONE);
        dao.addOrder(Order2.getOrderNumber(), Order2);

        assertEquals(2, dao.getAllOrders().size());

    }

    @Test
    public void testGetAllVendingItemsWith1Item() throws Exception {
        Order Order1 = new Order(001);
        Order1.setArea(BigDecimal.ONE);
        Order1.setCostPerSquareFoot(BigDecimal.ZERO);
        Order1.setCustomerName("SubZero");
        Order1.setLaborCost(BigDecimal.ZERO);
        Order1.setLaborCostPerSquareFoot(BigDecimal.ZERO);
        Order1.setMaterialCost(BigDecimal.ZERO);
        Order1.setOrderDate(LocalDate.MAX);
        Order1.setProductType("Cup");
        Order1.setState("MA");
        Order1.setTax(BigDecimal.TEN);
        Order1.setTaxRate(BigDecimal.ZERO);
        Order1.setTotal(BigDecimal.ONE);

        dao.addOrder(Order1.getOrderNumber(), Order1);

        assertEquals(1, dao.getAllOrders().size());

    }

    @Test
    public void RemoveOrder() throws Exception {
        Order Order1 = new Order(001);
        Order1.setArea(BigDecimal.ONE);
        Order1.setCostPerSquareFoot(BigDecimal.ZERO);
        Order1.setCustomerName("SubZero");
        Order1.setLaborCost(BigDecimal.ZERO);
        Order1.setLaborCostPerSquareFoot(BigDecimal.ZERO);
        Order1.setMaterialCost(BigDecimal.ZERO);
        Order1.setOrderDate(LocalDate.MAX);
        Order1.setProductType("Cup");
        Order1.setState("MA");
        Order1.setTax(BigDecimal.TEN);
        Order1.setTaxRate(BigDecimal.ZERO);
        Order1.setTotal(BigDecimal.ONE);

        dao.addOrder(Order1.getOrderNumber(), Order1);

        Order Order2 = new Order(002);
        Order2.setArea(BigDecimal.ONE);
        Order2.setCostPerSquareFoot(BigDecimal.ZERO);
        Order2.setCustomerName("Jeff");
        Order2.setLaborCost(BigDecimal.ZERO);
        Order2.setLaborCostPerSquareFoot(BigDecimal.ZERO);
        Order2.setMaterialCost(BigDecimal.ZERO);
        Order2.setOrderDate(LocalDate.MAX);
        Order2.setProductType("Tile");
        Order2.setState("NH");
        Order2.setTax(BigDecimal.TEN);
        Order2.setTaxRate(BigDecimal.ZERO);
        Order2.setTotal(BigDecimal.ONE);
        dao.addOrder(Order2.getOrderNumber(), Order2);

        assertEquals(2, dao.getAllOrders().size());
        dao.removeOrder(002);
        assertEquals(1, dao.getAllOrders().size());
        dao.removeOrder(001);
        assertEquals(0, dao.getAllOrders().size());

    }

    @Test
    public void testEdit() throws Exception {
        Order originalOrder = new Order(001);
        originalOrder.setArea(BigDecimal.ONE);
        originalOrder.setCostPerSquareFoot(BigDecimal.ZERO);
        originalOrder.setCustomerName("SubZero");
        originalOrder.setLaborCost(BigDecimal.ZERO);
        originalOrder.setLaborCostPerSquareFoot(BigDecimal.ZERO);
        originalOrder.setMaterialCost(BigDecimal.ZERO);
        originalOrder.setOrderDate(LocalDate.MAX);
        originalOrder.setProductType("Cup");
        originalOrder.setState("MA");
        originalOrder.setTax(BigDecimal.TEN);
        originalOrder.setTaxRate(BigDecimal.ZERO);
        originalOrder.setTotal(BigDecimal.ONE);

        dao.addOrder(originalOrder.getOrderNumber(), originalOrder);

        Order editedOrder = new Order(001);
        editedOrder.setArea(BigDecimal.ONE);
        editedOrder.setCostPerSquareFoot(BigDecimal.ZERO);
        editedOrder.setCustomerName("Taco");
        editedOrder.setLaborCost(BigDecimal.ZERO);
        editedOrder.setLaborCostPerSquareFoot(BigDecimal.ZERO);
        editedOrder.setMaterialCost(BigDecimal.ZERO);
        editedOrder.setOrderDate(LocalDate.MAX);
        editedOrder.setProductType("Ruby");
        editedOrder.setState("TX");
        editedOrder.setTax(BigDecimal.TEN);
        editedOrder.setTaxRate(BigDecimal.ZERO);
        editedOrder.setTotal(BigDecimal.ONE);

        dao.removeOrder(originalOrder.getOrderNumber());
        dao.addOrder(editedOrder.getOrderNumber(), editedOrder);

        assertEquals(originalOrder.getOrderNumber(),editedOrder.getOrderNumber());
        assertFalse(originalOrder.getProductType().equals(editedOrder.getProductType()));
    }

}
