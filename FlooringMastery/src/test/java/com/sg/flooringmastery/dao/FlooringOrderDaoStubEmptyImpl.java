/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.Order;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jared
 */
public class FlooringOrderDaoStubEmptyImpl implements FlooringOrderDao {

    @Override
    public Order addOrder(int OrderNumber, Order order) {
      return order;
    }

    @Override
    public List<Order> getAllOrders() {
      return new ArrayList<Order>();
    }

    @Override
    public Order getOrder(int OrderNumber) {
        return null;
    }

    @Override
    public Order removeOrder(int OrderNumber) {
        return null;
    }

    @Override
    public void editOrder(Order order) {
        return;
    }

    @Override
    public void Open() throws OrderPersistanceException {
        return;
    }

    @Override
    public void OpenProgramModeFile() throws LoadProgramPersistanceException {
        return;
    }

    @Override
    public void Close() throws OrderPersistanceException {
        return;
    }

    @Override
    public String getProgramMode() {
        return null;
    }

    @Override
    public void setProgramMode(String programMode) {
        return;
    }

}
