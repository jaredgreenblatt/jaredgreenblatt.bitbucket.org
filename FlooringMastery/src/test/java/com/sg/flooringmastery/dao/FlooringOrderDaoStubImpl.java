/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.Order;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jared
 */
public class FlooringOrderDaoStubImpl implements FlooringOrderDao {

    Order order;
    List<Order> orderList = new ArrayList<>();

    public FlooringOrderDaoStubImpl() {
        order = new Order(001);
        order.setArea(new BigDecimal(100));
        order.setCostPerSquareFoot(new BigDecimal(5.15));
        order.setCustomerName("Jared");
        order.setLaborCost(new BigDecimal(475.00));
        order.setLaborCostPerSquareFoot(new BigDecimal(4.75));
        order.setMaterialCost(new BigDecimal(515));
        order.setOrderDate(LocalDate.now());
        order.setProductType("Wood");
        order.setState("OH");
        order.setTax(new BigDecimal(59.40));
        order.setTaxRate(new BigDecimal(6.25));
        order.setTotal(new BigDecimal(1049.40));

        orderList.add(order);

    }

    @Override
    public Order addOrder(int OrderNumber, Order order) {
        if (OrderNumber == this.order.getOrderNumber()) {
            return this.order;
        } else {
            return null;
        }

    }

    @Override
    public List<Order> getAllOrders() {
        return orderList;
    }

    @Override
    public Order getOrder(int OrderNumber) {
        return order;
    }

    @Override
    public Order removeOrder(int OrderNumber) {
        return order;
    }

    public int editOrderCount = 0;
    public Order lastEditedOrder = null;
    public int lastEditedOrderNumber;

    @Override
    public void editOrder(Order order) {
        editOrderCount++;
        this.lastEditedOrderNumber = order.getOrderNumber();
        this.lastEditedOrder = order;
        return;

    }

    @Override
    public void Open() throws OrderPersistanceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void OpenProgramModeFile() throws LoadProgramPersistanceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void Close() throws OrderPersistanceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getProgramMode() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setProgramMode(String programMode) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
