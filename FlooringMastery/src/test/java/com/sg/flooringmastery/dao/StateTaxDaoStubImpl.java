/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.TaxRate;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jared
 */
public class StateTaxDaoStubImpl implements StateTaxDao {

    TaxRate taxRateTest;
    List<TaxRate> taxRateList = new ArrayList<>();

    public StateTaxDaoStubImpl() {
        taxRateTest = new TaxRate("OH");
        taxRateTest.setTaxRate(new BigDecimal(6.25));

        taxRateList.add(taxRateTest);
    }

    @Override
    public TaxRate addStateTax(String State, TaxRate taxRate) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<TaxRate> getAllStateTax() {
        return taxRateList;
    }

    @Override
    public TaxRate getTaxRate(String State) {
        if (State.equals(this.taxRateTest.getState())) {
            return this.taxRateTest;
        } else {
            return null;
        }
    }

    @Override
    public TaxRate removeTaxRate(String State) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void editTaxRate(String State) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void Open() throws StateTaxPersistanceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void Close() throws StateTaxPersistanceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
