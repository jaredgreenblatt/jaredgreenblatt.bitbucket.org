/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.ProductType;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jared
 */
public class ProductTypeDaoStubEmptyImpl implements ProductTypeDao {

    @Override
    public ProductType addProductType(String productType, ProductType product) {
        return product;
    }

    @Override
    public List<ProductType> getProductType() {
        return new ArrayList<ProductType>();
    }

    @Override
    public ProductType getProductType(String productType) {
        return null;
    }

    @Override
    public ProductType removeProductType(String productType) {
        return null;
    }

    @Override
    public void editProductType(String productType) {
        return;
    }

    @Override
    public void Open() throws ProductTypePersistanceException {
        return;
    }

    @Override
    public void Close() throws ProductTypePersistanceException {
        return;
    }

}
