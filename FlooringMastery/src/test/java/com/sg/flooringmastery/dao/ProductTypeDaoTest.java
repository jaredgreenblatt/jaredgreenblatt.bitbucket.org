/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.ProductType;
import java.math.BigDecimal;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Jared
 */
public class ProductTypeDaoTest {

    ProductTypeDao dao = new ProductTypeDaoImpl();

    public ProductTypeDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test

    public void testADDGetProductType() throws Exception {
        ProductType productTypeTest = new ProductType("Tile");
        productTypeTest.setCostPerSquareFoot(new BigDecimal(1.25));
        productTypeTest.setLaborCostPerSquareFoot(new BigDecimal(3.25));

        dao.addProductType(productTypeTest.getProductType(), productTypeTest);
        
        ProductType fromDao = dao.getProductType(productTypeTest.getProductType());
        
        
        assertEquals(fromDao, productTypeTest);
        assertEquals(fromDao.getProductType(), productTypeTest.getProductType());
        

    }

}
