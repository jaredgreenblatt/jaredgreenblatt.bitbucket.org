/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.TaxRate;
import java.math.BigDecimal;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Jared
 */
public class StateTaxDaoTest {

    private StateTaxDao dao = new StateTaxDaoImpl();

    public StateTaxDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        List<TaxRate> taxList = dao.getAllStateTax();
        for (TaxRate tax : taxList) {
            dao.removeTaxRate(tax.getState());

        }
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addStateTax method, of class StateTaxDao.
     */
    @Test
    public void testGetStateTax() throws Exception {
        TaxRate newTaxRate = new TaxRate("MA");
        newTaxRate.setTaxRate(new BigDecimal(6.25));
        dao.addStateTax(newTaxRate.getState(), newTaxRate);

        TaxRate fromDao = dao.getTaxRate(newTaxRate.getState());
        assertEquals(newTaxRate, fromDao);
        assertEquals(fromDao.getState(), newTaxRate.getState());
        assertEquals(fromDao.getTaxRate(), newTaxRate.getTaxRate());
    }

}
