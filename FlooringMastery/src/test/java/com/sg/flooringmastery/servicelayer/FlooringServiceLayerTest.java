/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.servicelayer;

import com.sg.flooringmastery.dto.Order;
import com.sg.flooringmastery.dto.ProductType;
import com.sg.flooringmastery.dto.TaxRate;
import java.time.LocalDate;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author Jared
 */
public class FlooringServiceLayerTest {

    FlooringServiceLayer serviceLayerWorkingDao;
    FlooringServiceLayer serviceLayerEmptyDao;

    public FlooringServiceLayerTest() {

        ApplicationContext ctx
                = new ClassPathXmlApplicationContext("testApplicationContext.xml");
        serviceLayerWorkingDao
                = ctx.getBean("serviceLayer", FlooringServiceLayer.class);
        ApplicationContext ctxEmptyDaoStub
                = new ClassPathXmlApplicationContext("testApplicationContext.xml");
        serviceLayerEmptyDao
                = ctxEmptyDaoStub.getBean("serviceLayerEmptyDao", FlooringServiceLayer.class);
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getTaxRate method, of class FlooringServiceLayer.
     */
    @Test
    public void testGetTaxRate() throws Exception {

        TaxRate testGetTR = serviceLayerWorkingDao.getTaxRate("OH");
        assertNotNull("Checking that the item is not null.", testGetTR);
    }

    @Test
    public void testGetTaxRateEmptyDao() throws Exception {
        TaxRate testGetTR = serviceLayerEmptyDao.getTaxRate("OH");

        assertEquals(testGetTR, null);

    }

    @Test
    public void testGetProductType() throws Exception {

        ProductType testGetProductType = serviceLayerWorkingDao.getProductType("Wood");
        assertNotNull("Checking that the item is not null.", testGetProductType);
    }

    @Test
    public void testGetProductTypeEmptyDao() throws Exception {
        ProductType testGetProductType = serviceLayerEmptyDao.getProductType("Wood");

        assertEquals(testGetProductType, null);

    }

    @Test
    public void testGetOrder() throws Exception {

        Order testGetOrder = serviceLayerWorkingDao.getOrder(001);
        assertNotNull("Checking that the item is not null.", testGetOrder);
    }

    @Test
    public void testGetOrderEmptyDao() throws Exception {
        Order testGetOrder = serviceLayerEmptyDao.getOrder(001);
        assertEquals(testGetOrder, null);

    }

    @Test
    public void testGetAllOrders() throws Exception {
        List<Order> orderList = serviceLayerWorkingDao.getAllOrders();
        // check it is empty list

        Assert.assertNotNull("Empty List Not Null", orderList);
        Assert.assertTrue("Checking If List is not Empty.", orderList.size() == 1);
        Assert.assertEquals(001, orderList.get(0).getOrderNumber());

    }

    @Test
    public void testGetAllOrdersEmptyDao() throws Exception {
        List<Order> orderList = serviceLayerEmptyDao.getAllOrders();
        // check it is empty list

        Assert.assertNotNull("Empty List Not Null", orderList);
        Assert.assertTrue("Checking If List is Empty.", orderList.isEmpty());

    }

    @Test
    public void testGetAllProductTypes() throws Exception {
        List<ProductType> productTypesList = serviceLayerWorkingDao.getAllProductTypes();
        // check it is empty list

        Assert.assertNotNull("Empty List Not Null", productTypesList);
        Assert.assertTrue("Checking If List is not Empty.", productTypesList.size() == 1);
        Assert.assertEquals("Wood", productTypesList.get(0).getProductType());

    }

    @Test
    public void testGetAllProductTypesEmptyDao() throws Exception {
        List<ProductType> productTypesList = serviceLayerEmptyDao.getAllProductTypes();
        // check it is empty list

        Assert.assertNotNull("Empty List Not Null", productTypesList);
        Assert.assertTrue("Checking If List is Empty.", productTypesList.isEmpty());

    }

    @Test
    public void testGetAllTaxRates() throws Exception {
        List<TaxRate> taxRateList = serviceLayerWorkingDao.getAllStateTaxRates();
        // check it is empty list

        Assert.assertNotNull("Empty List Not Null", taxRateList);
        Assert.assertTrue("Checking If List is not Empty.", taxRateList.size() == 1);
        Assert.assertEquals("OH", taxRateList.get(0).getState());

    }

    @Test
    public void testGetAllTaxRatesEmptyDao() throws Exception {
        List<TaxRate> taxRateList = serviceLayerEmptyDao.getAllStateTaxRates();
// check it is empty list

        Assert.assertNotNull("Empty List Not Null", taxRateList);
        Assert.assertTrue("Checking If List is Empty.", taxRateList.isEmpty());

    }

    @Test
    public void TestRemoveOrder() throws Exception {
        Order order = serviceLayerWorkingDao.getOrder(001);
        assertNotNull(order);
        assertEquals(001, order.getOrderNumber());

    }

    @Test
    public void TestRemoveOrderEmptyDao() throws Exception {
        Order order = serviceLayerEmptyDao.getOrder(001);

        assertTrue(order == null);

    }

    @Test
    public void testEditOrder() throws Exception {
        Order order = serviceLayerWorkingDao.getOrder(001);
        assertEquals(order.getCustomerName(), "Jared");
        order.setCustomerName("Jared Greenblatt");
        serviceLayerWorkingDao.editOrder(order.getOrderNumber(), order);
        assertEquals(order.getCustomerName(), "Jared Greenblatt");

    }

    @Test
    public void testIDUnique() throws Exception {
        int orderId = serviceLayerWorkingDao.uniqueOrderId();
        assertEquals(orderId, 1);
        int uniqueId =orderId+1;
        assertFalse(uniqueId==orderId);
        assertEquals("ORDER ID 2",uniqueId, 2);
                    

    }
      @Test
    public void testIDEmptyDaoUnique() throws Exception {
        int orderId = serviceLayerEmptyDao.uniqueOrderId();
        assertEquals(orderId, 0);
        int uniqueId =orderId+1;
        assertFalse(uniqueId==orderId);
        assertEquals("ORDER ID 1",uniqueId, 1);
                    

    }
       @Test
    public void getOrdersByDate() throws Exception {
      List<Order> orderList =  serviceLayerWorkingDao.getOrdersByDate(LocalDate.now());
           assertTrue(orderList.size()>0);

    }
        @Test
    public void getOrdersByDateEmptyDao() throws Exception {
      List<Order> orderList =  serviceLayerEmptyDao.getOrdersByDate(LocalDate.now());
           assertTrue(orderList.size()==0);

    }
    
    @Test
    public void testgetOrderByNumbeAndDate() throws Exception{
        
        Order testOrder=  serviceLayerWorkingDao.getOrder(001, LocalDate.now());
            assertNotNull(testOrder);
    }
    
      @Test
    public void testgetOrderByNumbeAndDateEmptyDao() throws Exception{
        
        Order testOrder=  serviceLayerEmptyDao.getOrder(001, LocalDate.now());
            assertTrue(testOrder==null);
    }
    
    
    

}
