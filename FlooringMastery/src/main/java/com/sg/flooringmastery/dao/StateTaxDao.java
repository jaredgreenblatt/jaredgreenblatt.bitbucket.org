/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.TaxRate;
import java.lang.Thread.State;
import java.util.List;

/**
 *
 * @author Jared
 */
public interface StateTaxDao {

    TaxRate addStateTax(String State, TaxRate taxRate);

    List<TaxRate> getAllStateTax();

    TaxRate getTaxRate(String State);

    TaxRate removeTaxRate(String State);

    void editTaxRate(String State);

    public void Open() throws StateTaxPersistanceException;

    public void Close() throws StateTaxPersistanceException;

}
