/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.ProductType;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author Jared
 */
public class ProductTypeDaoImpl implements ProductTypeDao {

    Map<String, ProductType> productTypes = new HashMap<>();
    public static final String PRODUCTS = "Products.txt";
    public static final String DELIMITER = ",";
    public static final String DELIMITER_WITH_SPACE = ", |\\,";

    private void loadProductType() throws ProductTypePersistanceException {
        Scanner scanner;

        try {
            // Create Scanner for reading the file
            scanner = new Scanner(
                    new BufferedReader(
                            new FileReader(PRODUCTS)));
        } catch (FileNotFoundException e) {
            throw new ProductTypePersistanceException(
                    "-_- Could not load roster data into memory.", e);
        }
        // currentLine holds the most recent line read from the file
        String currentLine;
        // currentTokens holds each of the parts of the currentLine after it has
        // been split on our DELIMITER
      
        String[] currentTokens;
        // Go through PRODUCT line by line, decoding each line into a 
        // product object.
        // Process while we have more lines in the file
        while (scanner.hasNextLine()) {
            // get the next line in the file
            currentLine = scanner.nextLine();
            // break up the line into tokens
            currentTokens = currentLine.split(DELIMITER_WITH_SPACE);
            
            // Create a new Product object and put it into the map of students

            ProductType currentProductType = new ProductType(currentTokens[0].trim());
            // Set the remaining vlaues on currentProductType manually
            currentProductType.setCostPerSquareFoot(new BigDecimal(currentTokens[1].trim()));
            currentProductType.setLaborCostPerSquareFoot(new BigDecimal(currentTokens[2].trim()));

            // Put currentProductType into the map using productType as the key
            productTypes.put(currentProductType.getProductType(), currentProductType);
        }
        // close scanner
        scanner.close();
    }

    @Override
    public ProductType addProductType(String productType, ProductType product) {
        ProductType newProductType = productTypes.put(productType, product);
        return newProductType;
    }

    @Override
    public List<ProductType> getProductType() {
        return new ArrayList<ProductType>(productTypes.values());
    }

    @Override
    public ProductType getProductType(String productType) {
        return productTypes.get(productType);
    }

    @Override
    public ProductType removeProductType(String productType) {
        ProductType removedProductType = productTypes.remove(productType);
        return removedProductType;
    }

    @Override
    public void editProductType(String productType) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void Open() throws ProductTypePersistanceException {
        loadProductType();

    }

    @Override
    public void Close() throws ProductTypePersistanceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
