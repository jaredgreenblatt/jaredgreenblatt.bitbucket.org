/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.TaxRate;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author Jared
 */
public class StateTaxDaoImpl implements StateTaxDao {

    private Map<String, TaxRate> taxRates = new HashMap<>();
    public static final String TAXES = "Taxes.txt";
    public static final String DELIMITER = ", ";
    public static final String DELIMITER_WITH_SPACE = ", |\\,";

    private void loadTaxFile() throws StateTaxPersistanceException {
        Scanner scanner;

        try {
            // Create Scanner for reading the file
            scanner = new Scanner(
                    new BufferedReader(
                            new FileReader(TAXES)));
        } catch (FileNotFoundException e) {
            throw new StateTaxPersistanceException(
                    "-_- Could not load roster data into memory.", e);
        }
        // currentLine holds the most recent line read from the file
        String currentLine;
        // currentTokens holds each of the parts of the currentLine after it has
        // been split on our DELIMITER
     
        String[] currentTokens;
        // Go through TAXES line by line, decoding each line into a 
        
        while (scanner.hasNextLine()) {
            // get the next line in the file
            currentLine = scanner.nextLine();
            // break up the line into tokens
            currentTokens = currentLine.split(DELIMITER_WITH_SPACE);
            // Create a new TAXES object and put it into the map of TAXES
           
            TaxRate currentTaxRate = new TaxRate(currentTokens[0].trim());
            // Set the remaining vlaues on curentTaxRate manually
            currentTaxRate.setTaxRate(new BigDecimal(currentTokens[1].trim()));

            // Put currentTaxRate into the map using state as the key
            taxRates.put(currentTaxRate.getState(), currentTaxRate);
        }
        // close scanner
        scanner.close();
    }

    @Override
    public TaxRate addStateTax(String state, TaxRate taxRate) {
        TaxRate newTaxRate = taxRates.put(state, taxRate);
        return newTaxRate;
    }

    @Override
    public List<TaxRate> getAllStateTax() {
        return new ArrayList<TaxRate>(taxRates.values());
    }

    @Override
    public TaxRate getTaxRate(String state) {
        return taxRates.get(state);
    }

    @Override
    public TaxRate removeTaxRate(String state) {
        TaxRate removeTaxRate = taxRates.remove(state);
        return removeTaxRate;

    }

    @Override
    public void editTaxRate(String state) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void Open() throws StateTaxPersistanceException {
        loadTaxFile();
    }

    @Override
    public void Close() throws StateTaxPersistanceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
