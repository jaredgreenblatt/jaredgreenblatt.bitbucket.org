/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;

/**
 *
 * @author Jared
 */
public class FlooringAuditDaoImpl implements FlooringAuditDao {
// Creates a text file name for the audit file.
    public static final String AUDIT_FILE = "audit.txt";

    @Override
    public void writeAuditEntry(String entry) throws OrderPersistanceException {
        PrintWriter out;

        try {
            //creates a new file writter that will append to the audit file.
            out = new PrintWriter(new FileWriter(AUDIT_FILE, true));
        } catch (IOException ex) {
            throw new OrderPersistanceException("Could not persist audit infomration", ex);
        }
//Will then enter in the localdate and the string entry into the next line of text file
        LocalDateTime timestamp = LocalDateTime.now();
        out.println(timestamp.toString() + " : " + entry);
        out.flush();
    }
}


