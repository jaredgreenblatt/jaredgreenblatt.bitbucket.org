/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.servicelayer;

import com.sg.flooringmastery.dao.FlooringAuditDao;
import com.sg.flooringmastery.dao.FlooringOrderDao;
import com.sg.flooringmastery.dao.LoadProgramPersistanceException;
import com.sg.flooringmastery.dao.OrderPersistanceException;
import com.sg.flooringmastery.dao.ProductTypeDao;
import com.sg.flooringmastery.dao.ProductTypePersistanceException;
import com.sg.flooringmastery.dao.StateTaxDao;
import com.sg.flooringmastery.dao.StateTaxPersistanceException;
import com.sg.flooringmastery.dto.Order;
import com.sg.flooringmastery.dto.ProductType;
import com.sg.flooringmastery.dto.TaxRate;
import com.sg.flooringmastery.ui.FlooringView;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jared
 */
public class FlooringServiceLayerImpl implements FlooringServiceLayer {

    FlooringView view;
    FlooringOrderDao flooringOrderDao;
    ProductTypeDao productTypeDao;
    StateTaxDao stateTaxDao;
    FlooringAuditDao auditDao;

    /**
     *
     * Constructor for the service layer.
     *
     * @param flooringOrderDao
     * @param productTypeDao
     * @param stateTaxDao
     * @param auditDao
     */
    public FlooringServiceLayerImpl(FlooringOrderDao flooringOrderDao, ProductTypeDao productTypeDao,
            StateTaxDao stateTaxDao, FlooringAuditDao auditDao) {
        this.flooringOrderDao = flooringOrderDao;
        this.productTypeDao = productTypeDao;
        this.stateTaxDao = stateTaxDao;
        this.auditDao = auditDao;

    }

    //Validating the order data.
    private void validateOrderData(Order order) throws
            OrderDataValidationException {
        if (order.getCustomerName() == null
                || order.getCustomerName().trim().length() == 0
                || order.getState() == null
                || order.getState().trim().length() == 0
                || order.getTaxRate() == null
                || order.getTaxRate().toString().trim().length() == 0
                || order.getTaxRate() == null
                || order.getTaxRate().toString().trim().length() == 0
                || order.getProductType() == null
                || order.getProductType().trim().length() == 0
                || order.getArea() == null
                || order.getArea().toString().trim().length() == 0
                || order.getCostPerSquareFoot() == null
                || order.getCostPerSquareFoot().toString().trim().length() == 0
                || order.getLaborCostPerSquareFoot() == null
                || order.getLaborCostPerSquareFoot().toString().trim().length() == 0
                || order.getMaterialCost() == null
                || order.getMaterialCost().toString().trim().length() == 0
                || order.getLaborCost() == null
                || order.getLaborCost().toString().trim().length() == 0
                || order.getTax() == null
                || order.getTax().toString().trim().length() == 0
                || order.getTotal() == null
                || order.getTotal().toString().trim().length() == 0) {

            throw new OrderDataValidationException(
                    "Error: All fields are required"
            );

        }
    }

    /*
    Creating a new order. This checks to see if that order number has already been used.
    if it has it will throw an exception.

     */
    @Override
    public void createOrder(int OrderNumber, Order order) throws OrderDuplicateIdException, OrderDataValidationException, OrderPersistanceException {
        if (flooringOrderDao.getOrder(order.getOrderNumber()) != null) {
            throw new OrderDuplicateIdException(
                    "ERROR: Could not create order. OrderNumber "
                    + order.getOrderNumber()
                    + " already exists."
            );

        }
        validateOrderData(order);

        flooringOrderDao.addOrder(order.getOrderNumber(), order);

    }
//validates the order info and then edits the data.

    @Override
    public void editOrder(int OrderNumber, Order order) throws OrderDataValidationException, OrderPersistanceException {
        validateOrderData(order);
        flooringOrderDao.editOrder(order);
    }
//gets all the order data.

    @Override
    public List<Order> getAllOrders() throws OrderPersistanceException {
        return flooringOrderDao.getAllOrders();
    }
//gets an order

    @Override
    public Order getOrder(int OrderNumber) throws OrderPersistanceException {
        return flooringOrderDao.getOrder(OrderNumber);
    }
//removes an order

    @Override
    public Order removeOrder(int OrderNumber) throws OrderPersistanceException {
        return flooringOrderDao.removeOrder(OrderNumber);
    }

    /**
     * get order by date. This will get the orders by date. First it will get
     * all of the orders in the application. Then we create a new
     * orderListByDate list. Then we go through each order in the orderList if
     * the passed in date equals the order date then we add the order to the
     * orderListByDate list.
     *
     * @param orderDate
     * @return
     */
    @Override
    public List<Order> getOrdersByDate(LocalDate orderDate) {
        List<Order> orderList = flooringOrderDao.getAllOrders();
        List<Order> orderListByDate = new ArrayList<>();
        for (Order order : orderList) {
            if (orderDate.equals(order.getOrderDate())) {
                orderListByDate.add(order);
            }
        }
        return orderListByDate;
    }

    /**
     * uniqueOrderId() creating a unique id number first it gets a list of all
     * the order numbers. this is in an integer list. Then we loop for the
     * integer list and compare the uniqueOrderNumber to the integer from the
     * list. if the number from the list is bigger then unique order number we
     * will save that number as the uniqueOrder number. This compare will be
     * done for every number in the list.
     *
     * @return uniqueOrderNumber uniqueOrderNumber will be returned if there are
     * no items in the list it will return 0. If there are many numbers it will
     * return the highest number.
     *
     */
    @Override
    public int uniqueOrderId() {
        int uniqueOrderNumber = 0;
        List<Order> orderIdList = flooringOrderDao.getAllOrders();
        List<Integer> uniqueIdList = new ArrayList<>();

        for (Order order : orderIdList) {
            uniqueIdList.add(order.getOrderNumber());
        }
        if (uniqueIdList.size() > 0) {
            for (int orderNumber : uniqueIdList) {
                if (orderNumber > uniqueOrderNumber) {
                    uniqueOrderNumber = orderNumber;
                }

            }

            return uniqueOrderNumber + 1;

        } else {
            return uniqueOrderNumber;
        }
    }

//get product type
    @Override
    public ProductType getProductType(String ProductType) throws ProductTypePersistanceException, ProductTypeDoesntExistException {
        
        return productTypeDao.getProductType(ProductType);

    }

//get taxrate 
    @Override
    public TaxRate getTaxRate(String State) throws StateTaxPersistanceException, StateTaxDoesntExistException {
       
        return stateTaxDao.getTaxRate(State);
    }

    @Override
    public void OpenOrderFile() throws OrderPersistanceException {
        flooringOrderDao.Open();
    }

    @Override
    public void CloseOrderFile() throws OrderPersistanceException {
        flooringOrderDao.Close();
    }

    @Override
    public void OpenTaxRateFile() throws StateTaxPersistanceException {
        stateTaxDao.Open();
    }

    @Override
    public void OpenProductTypeFile() throws ProductTypePersistanceException {
        productTypeDao.Open();

    }

    /**
     * getOrder is used to get the order information based on orderNumber and
     * order date. We create an list of orders and pull in all orders. Second we
     * create a new array list called orderListByDate. We will put the orders
     * that match the date search there. Next we loop through orderList and add
     * any orders that match the date param to the new orders by date list. Then
     * we check if the list has more than 0 entries if it does. We will search
     * the list for the orderNumber parameter. if there is a match found the
     * order will return the searched for order. if no match found for date or
     * number the order that will be returned is a null.
     *
     * @param orderNumber
     * @param orderDate
     * @return
     * @throws OrderPersistanceException
     */
    @Override
    public Order getOrder(int orderNumber, LocalDate orderDate) throws OrderPersistanceException {
        Order getOrder = null;
        List<Order> orderList = flooringOrderDao.getAllOrders();
        List<Order> orderListByDate = new ArrayList<>();
        for (Order order : orderList) {
            if (orderDate.equals(order.getOrderDate())) {
                orderListByDate.add(order);
            }

        }

        if (orderListByDate.size() > 0) {
            for (Order order : orderList) {
                if (order.getOrderNumber() == orderNumber) {
                    getOrder = order;

                }
            }

        }

        return getOrder;

    }

    @Override
    public Set<String> getSetOfOrders() {
        List<Order> orderList = flooringOrderDao.getAllOrders();
        Set<String> setOfDates = new HashSet<>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMddyyyy");
        LocalDate uniqueDates;
        String formattedUniqueDates;

        if (orderList.size() > 0) {
            for (Order order : orderList) {
                uniqueDates = order.getOrderDate();
                formattedUniqueDates = uniqueDates.format(formatter);
                setOfDates.add(formattedUniqueDates);

            }
        }
        return setOfDates;
    }

    /**
     * service layer logic to handle the productionMode() it gets the
     * programMode from the dao as a string. It then converts it to uppercase.
     * If the string is equal to prod it will return the boolean true. If it is
     * no TEST or anything else it will return the boolean false.
     *
     * @return
     */
    @Override
    public Boolean productionMode() {
        boolean inProd = false;
        String programMode = flooringOrderDao.getProgramMode();
        programMode = programMode.toUpperCase();
        if (programMode.equals("PROD")) {
            inProd = true;
        } else if (programMode.equals("TEST")) {
            inProd = false;
        } else {
            inProd = false;
        }
        return inProd;
    }

    @Override
    public void OpenFlooringProgramMode() throws LoadProgramPersistanceException {
        flooringOrderDao.OpenProgramModeFile();
    }

    @Override
    public List<ProductType> getAllProductTypes() throws StateTaxDoesntExistException {
        return productTypeDao.getProductType();
    }

    @Override
    public List<TaxRate> getAllStateTaxRates() throws StateTaxDoesntExistException {
        return stateTaxDao.getAllStateTax();
    }

    @Override
    public Order calculateOrder(Order order, ProductType productType, TaxRate taxRate) throws ProductTypePersistanceException, ProductTypeDoesntExistException, StateTaxPersistanceException, StateTaxDoesntExistException {
        
            //Calclate Material Cost
            Order calculatedOrder = null;
            BigDecimal materialCost = (order.getArea().multiply(productType.getCostPerSquareFoot()));
            materialCost = materialCost.setScale(2, RoundingMode.HALF_DOWN);
            calculatedOrder.setMaterialCost(materialCost);
            //Caluclate LaborCost
            // BigDecimal
            BigDecimal laborCost = (order.getArea().multiply(productType.getLaborCostPerSquareFoot()));
            laborCost = laborCost.setScale(2, RoundingMode.HALF_DOWN);
            calculatedOrder.setLaborCost(laborCost);
            
            //calculate tax
            //        BigDecimal tax = (finalOrder.getTaxRate().divide(new BigDecimal(100), 2, RoundingMode.HALF_DOWN));

            BigDecimal tax = (taxRate.getTaxRate().divide(new BigDecimal(100), 2, RoundingMode.HALF_DOWN));

            tax = tax.divide(new BigDecimal(100), 2, RoundingMode.HALF_DOWN);
           
            tax = tax.multiply(calculatedOrder.getLaborCost().add(calculatedOrder.getMaterialCost()));
            tax = tax.setScale(2, RoundingMode.HALF_DOWN);
            calculatedOrder.setTax(tax);
            //Calculate Total
            BigDecimal total = calculatedOrder.getTax().add(calculatedOrder.getMaterialCost()).add(calculatedOrder.getLaborCost());
            total = total.setScale(2, RoundingMode.HALF_DOWN);
            calculatedOrder.setTotal(total);
            
            
            calculatedOrder = new Order(uniqueOrderId());
            calculatedOrder.setCustomerName(order.getCustomerName());
            calculatedOrder.setState(order.getState());
            calculatedOrder.setOrderDate(order.getOrderDate());
            calculatedOrder.setTaxRate(getTaxRate(order.getState()).getTaxRate());
            calculatedOrder.setProductType(order.getProductType());
            calculatedOrder.setArea(order.getArea());
            calculatedOrder.setCostPerSquareFoot(getProductType(order.getProductType()).getCostPerSquareFoot());
            calculatedOrder.setLaborCostPerSquareFoot(getProductType(order.getProductType()).getLaborCostPerSquareFoot());
            
            return calculatedOrder;
            
//           //Calculate Material Cost
//        BigDecimal materialCost = (finalOrder.getArea().multiply(finalOrder.getCostPerSquareFoot()));
//        materialCost = materialCost.setScale(2, RoundingMode.HALF_DOWN);
//        finalOrder.setMaterialCost(materialCost);
//        //Calculate Labor Cost
//        BigDecimal laborCost = (finalOrder.getArea().multiply(finalOrder.getLaborCostPerSquareFoot()));
//        laborCost = laborCost.setScale(2, RoundingMode.HALF_DOWN);
//        finalOrder.setLaborCost(laborCost);
//        //calculate tax
//        BigDecimal tax = (finalOrder.getTaxRate().divide(new BigDecimal(100), 2, RoundingMode.HALF_DOWN));
//        tax = tax.multiply(finalOrder.getLaborCost().add(finalOrder.getMaterialCost()));
//        tax = tax.setScale(2, RoundingMode.HALF_DOWN);
//        finalOrder.setTax(tax);
//        //Calculate Total
//        BigDecimal total = finalOrder.getTax().add(finalOrder.getMaterialCost()).add(finalOrder.getLaborCost());
//        total = total.setScale(2, RoundingMode.HALF_DOWN);
//        finalOrder.setTotal(total);
////Order final valus are displayed for the user to review. It will then be passed to the controler
//       
    
    }

}
