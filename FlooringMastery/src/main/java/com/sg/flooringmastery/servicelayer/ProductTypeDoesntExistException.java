/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.servicelayer;

/**
 *
 * @author Jared
 */
public class ProductTypeDoesntExistException extends Exception {

    public ProductTypeDoesntExistException(String message) {
        super(message);

    }

    public ProductTypeDoesntExistException(String message, Throwable cause) {
        super(message, cause);

    }
}
