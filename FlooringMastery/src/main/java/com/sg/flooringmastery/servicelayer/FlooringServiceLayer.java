/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.servicelayer;

import com.sg.flooringmastery.dao.LoadProgramPersistanceException;
import com.sg.flooringmastery.dao.OrderPersistanceException;
import com.sg.flooringmastery.dao.ProductTypePersistanceException;
import com.sg.flooringmastery.dao.StateTaxPersistanceException;
import com.sg.flooringmastery.dto.Order;
import com.sg.flooringmastery.dto.ProductType;
import com.sg.flooringmastery.dto.TaxRate;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Jared
 */
public interface FlooringServiceLayer {

    void createOrder(int OrderNumber, Order order) throws
            OrderDuplicateIdException,
            OrderDataValidationException,
            OrderPersistanceException;

    void editOrder(int OrderNumber, Order order) throws
            OrderDataValidationException,
            OrderPersistanceException;

    List<Order> getAllOrders() throws
            OrderPersistanceException;

    List<ProductType> getAllProductTypes() throws StateTaxDoesntExistException;

    List<TaxRate> getAllStateTaxRates() throws StateTaxDoesntExistException;

    Order getOrder(int orderNumber, LocalDate ld) throws
            OrderPersistanceException;

    Order getOrder(int orderNumber) throws
            OrderPersistanceException;

    ProductType getProductType(String ProductType) throws
            ProductTypePersistanceException,
            ProductTypeDoesntExistException;

    TaxRate getTaxRate(String State) throws
            StateTaxPersistanceException,
            StateTaxDoesntExistException;

    Order removeOrder(int OrderNumber) throws
            OrderPersistanceException;
    
    Order calculateOrder(Order order, ProductType productType, TaxRate taxRate) throws
            ProductTypePersistanceException,
            ProductTypeDoesntExistException,
            StateTaxPersistanceException,
            StateTaxDoesntExistException;
            
    
      

    List<Order> getOrdersByDate(LocalDate ld);

    Set<String> getSetOfOrders();

    int uniqueOrderId();

    Boolean productionMode();

    void OpenFlooringProgramMode() throws LoadProgramPersistanceException;

    void OpenOrderFile() throws OrderPersistanceException;

    void CloseOrderFile() throws OrderPersistanceException;

    void OpenTaxRateFile() throws StateTaxPersistanceException;

    void OpenProductTypeFile() throws ProductTypePersistanceException;

}
