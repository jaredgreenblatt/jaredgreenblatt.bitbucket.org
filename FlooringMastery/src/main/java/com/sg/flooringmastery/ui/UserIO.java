/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.ui;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 *
 *
 * @author Jared
 */
public interface UserIO {

    void print(String message);
    void printOneLine(String message);

    double readDouble(String prompt);

    double readDouble(String prompt, double min, double max);

    float readFloat(String prompt);

    float readFloat(String prompt, float min, float max);

    int readInt(String prompt);

    int readInt(String prompt, int min, int max);

    long readLong(String prompt);

    long readLong(String prompt, long min, long max);

    LocalDate readLocalDate(String prompt);

    BigDecimal readBigDecimal(String prompt);

    BigDecimal readBigDecimal(String prompt, boolean isBigDecimal);

    BigDecimal readBigDecimal(String prompt, BigDecimal min, BigDecimal max);
     BigDecimal readBigDecimalNoNull(String prompt, boolean isBigDecimal);

    LocalDate readLocalDate(String prompt, boolean isDate);

    String readString(String prompt);

    String readString(String prompt ,boolean notNull);

}
