/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package addressbookdbimpl.dao;

import addressbookdbimpl.model.Address;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class AddressBookDaoDbImpl implements AddressBookDao {

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String SQL_INSERT_ADDRESS
            = "insert into address (full_name, street_address, city, state, zipcode )"
            + "values (?, ?, ?, ?, ?)";

    private static final String SQL_DELETE_ADDRESS
            = "delete from address where address_id = ?";

    private static final String SQL_UPDATE_ADDRESS
            = "update address set full_name = ?, street_address = ?, city = ?, "
            + "state = ?, zipcode = ? where address_id =  ?";

    private static final String SQL_SELECT_ADDRESS
            = "select * from address where address_id = ?";

    private static final String SQL_SELECT_ALL_ADDRESS
            = "select * from address";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)

    public void addAddress(Address address) {
        jdbcTemplate.update(SQL_INSERT_ADDRESS,
                address.getFullName(),
                address.getStreetAddress(),
                address.getCity(),
                address.getState(),
                address.getZipCode());

        int addressId
                = jdbcTemplate.queryForObject("select LAST_INSERT_ID()",
                        Integer.class);

        address.setAddressId(addressId);
    }

    @Override
    public void deleteAddress(int addressId) {
        jdbcTemplate.update(SQL_DELETE_ADDRESS, addressId);
    }

    @Override
    public void updateAddress(Address address) {
        jdbcTemplate.update(SQL_UPDATE_ADDRESS,
                address.getFullName(),
                address.getStreetAddress(),
                address.getCity(),
                address.getState(),
                address.getZipCode(),
                address.getAddressId());
    }

    @Override
    public Address getAddressById(int addressId) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_ADDRESS,
                    new AddressMapper(),
                    addressId);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public List<Address> getAllAddresses() {
        return jdbcTemplate.query(SQL_SELECT_ALL_ADDRESS,
                new AddressMapper());
    }

    private static final class AddressMapper implements RowMapper<Address> {

        @Override
        public Address mapRow(ResultSet rs, int i) throws SQLException {
            Address ad = new Address();
            ad.setFullName(rs.getString("full_name"));
            ad.setStreetAddress(rs.getString("street_address"));
            ad.setCity(rs.getString("city"));
            ad.setState(rs.getString("state"));
            ad.setZipCode(rs.getString("zipcode"));
            ad.setAddressId(rs.getInt("address_id"));
            return ad;
        }

    }

}
