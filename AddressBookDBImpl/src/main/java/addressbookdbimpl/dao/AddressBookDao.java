/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package addressbookdbimpl.dao;

import addressbookdbimpl.model.Address;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface AddressBookDao {

    public void addAddress(Address address);

    public void deleteAddress(int addressId);

    public void updateAddress(Address address);

    public Address getAddressById(int addressId);

    public List<Address> getAllAddresses();

}
