/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package addressbookdbimpl.dao;

import addressbookdbimpl.model.Address;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class AddressBookDaoTest {

    AddressBookDao dao;
    
    public AddressBookDaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        ApplicationContext ctx
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        
        dao = ctx.getBean("addressBookDao", AddressBookDao.class);

        // delete all addresses
        List<Address> addresses = dao.getAllAddresses();
        for (Address currentAddress : addresses) {
            dao.deleteAddress(currentAddress.getAddressId());
        }
        
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void addGetPublisher() {
        Address ad = new Address();
        ad.setFullName("Joe User");
        ad.setStreetAddress("123 Main Street");
        ad.setCity("Publisher City");
        ad.setState("OH");
        ad.setZipCode("44123");
        
        dao.addAddress(ad);
        
        Address fromDao = dao.getAddressById(ad.getAddressId());
        assertEquals(fromDao, ad);
    }
    
}
