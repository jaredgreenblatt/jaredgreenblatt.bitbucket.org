Create database SGRoster;

USE SGRoster;
Create Table Cohort(
CohortID Int not null auto_increment,
StartDate Date Not Null,
Subject varchar(30) Not Null,
Location varchar(30) Not Null,
Primary Key (CohortID)


);

CREATE TABLE Apprentice(
ApprenticeID INT NOT NULL auto_increment,
FirstName varchar(30) not null,
LastName varchar(30) not null,
CohortID INT not null,
primary key(ApprenticeId),
foreign key(cohortid) references Cohort(cohortID)
);

ALTER TABLE Apprentice DROP FOREIGN KEY apprentice_ibfk_1;
 
ALTER TABLE Apprentice DROP COLUMN CohortID;

DROP TABLE Apprentice;

CREATE TABLE Apprentice(
ApprenticeID INT NOT NULL auto_increment,
FirstName varchar(30) not null,
LastName varchar(30) not null,
primary key(ApprenticeId)

);

Create Table ApprenticeCohort(
ApprenticeID int not null,
CohortID int not null,
primary key(ApprenticeID,CohortID)
);

Alter Table ApprenticeCohort
add constraint fk_ApprenticeCohort_Apprentice
foreign key (ApprenticeID)
references Apprentice(ApprenticeID);


Alter Table ApprenticeCohort
add constraint fk_ApprenticeCohort_Cohort
foreign key (CohortID)
references Cohort(CohortID);

ALTER TABLE Apprentice ADD DateOfBirth DATE not null;
ALTER TABLE Apprentice MODIFY COLUMN 
	DateOfBirth DATETIME NULL;
    
--     DROP DATABASE SGRoster;