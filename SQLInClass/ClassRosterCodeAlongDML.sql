USE SGRoster;

INSERT INTO Apprentice (ApprenticeID, FirstName, LastName)
VALUES (1, 'Bill', 'Smith');

INSERT INTO Apprentice (FirstName, LastName)
VALUES ('Bill', 'Smith');

insert into 
Apprentice(FirstName, LastName)
values
('Bob','Jones'),
('Brenda', 'Walters'),
('Shauna', 'Mullins');

INSERT INTO Apprentice(ApprenticeID, FirstName, LastName)
VALUES (50, 'Bartholomew', 'Simpson');




select * from Apprentice;

INSERT INTO Cohort (StartDate, Subject, Location)
VALUES ('2017/1/9', 'C#', 'Akron');

INSERT INTO ApprenticeCohort(ApprenticeID, CohortID)
VALUES(1,2); -- bill smith, cohort 2

INSERT INTO Cohort (StartDate, Subject, Location)
VALUES ('2017/1/9', 'Java', 'Akron');


INSERT INTO ApprenticeCohort(ApprenticeID, CohortID)
VALUES (2,1), -- Bob, C# Akron
(3,2), -- Brenda, Java Akron
(4,2), -- Shauna, Java Akron
(50,1); -- Bartholomew, C# 


UPDATE ApprenticeCohort
SET CohortID = 2
WHERE ApprenticeID = 2;

UPDATE ApprenticeCohort
SET CohortID = 1
WHERE ApprenticeID IN (3,4);


DELETE FROM Cohort
WHERE CohortID = 2;


-- move all cohort 2 to cohort 1
UPDATE ApprenticeCohort
SET CohortID = 1
WHERE CohortID = 2;
 
-- now delete cohort 2
DELETE FROM Cohort
WHERE CohortID = 2;