drop  database MovieCatalogue;
Create database if not exists MovieCatalogue;

use MovieCatalogue;

create table if not exists Movie(
MovieID int not null auto_increment,
GenreId int not null,
DirectorId int,
RatingID int,
Title varchar(128),
ReleaseDate date,
primary key (movieId)


);



create table if not exists Genre(
GenreId int not null auto_increment,
GenreName varchar(30) not null,
primary key (GenreId)

);

create table if not exists Director(
DirectorID int not null auto_increment,
FirstName varchar(30) not null,
LastName varchar(30) not null,
BirthDate date,
primary key(DirectorId)
);

create table if not exists Rating(
RatingID int not null auto_increment,
RatingName char(5) not null,
primary key(RatingID)
);

create table if not exists Actor(
ActorID int not null auto_increment,
FirstName varchar(30) not null,
LastName varchar(30) not null,
BirthDate date,
primary key(ActorID)

);


create table if not exists CastMember(
CastMemberID int not null auto_increment,
ActorID int ,
MovieID int,
Role VarChar(50) not null,
primary key(CastMemberID)
);

Alter Table Movie
add constraint fk_Movie_Genre
foreign key (GenreID)
references Genre(GenreID);

Alter Table Movie
add constraint fk_Movie_Director
foreign key (DirectorID)
references Director(DirectorID);

Alter Table Movie
add constraint fk_Movie_Rating
foreign key (RatingID)
references Rating(RatingID);

Alter Table CastMember
add constraint fk_CastMember_Actor
foreign key (ActorID)
references Actor(ActorID);

Alter Table CastMember
add constraint fk_CastMember_Movie
foreign key (MovieID)
references Movie(MovieID);
