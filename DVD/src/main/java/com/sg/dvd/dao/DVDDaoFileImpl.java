/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvd.dao;

import com.sg.DVD.dto.DVD;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class DVDDaoFileImpl implements DVDDao {

    public static final String DVD_FILE = "collection.txt";
    public static final String DELIMETER = "::";
    private Map<String, DVD> dvds = new HashMap<>();

    /*
    LoadCollection() is used to read the Object Information to the flat text 
    file.
     */
    private void loadCollection() throws DVDDaoException {
        Scanner scanner;
        try {
            //create a scanner for reading file
            scanner = new Scanner(
                    new BufferedReader(
                            new FileReader(DVD_FILE)));
        } catch (FileNotFoundException e) {
            throw new DVDDaoException(
                    "-__ Could not load collection data into memory.", e);

        }
        // currentLine holds the most recent line read from the file
        String currentLine;
        // currentTokens holds each of the parts of the currentLine after it has
        // been split on our DELIMITER
        String[] currentTokens;
        //[] so we go through the elements after there split by delimeter.
        // Go through ROSTER_FILE line by line, decoding each line into a 
        // Student object.
        // Process while we have more lines in the file

        while (scanner.hasNextLine()) {
            // get the next line in the file
            currentLine = scanner.nextLine();
            // break up the line into tokens
            currentTokens = currentLine.split(DELIMETER);
            // Create a new DVD object and put it into the map of dvds
            //We are going to use the DVD id
            // which is currentTokens[0] as the map key for our DVD object.
            // We also have to pass the student id into the DVD constructor
            DVD currentDvd = new DVD(currentTokens[0]);
            //SETTING the remaining values manually.
            currentDvd.setDvdTitle(currentTokens[1]);
            currentDvd.setDvdReleaseDate(currentTokens[2]);
            currentDvd.setDvdMppaRating(currentTokens[3]);
            currentDvd.setDvdDirectorsName(currentTokens[4]);
            currentDvd.setDvdStudio(currentTokens[5]);
            currentDvd.setDvdUserRating(currentTokens[6]);
            // Put currentDVD into the map using dvdID as the key
            dvds.put(currentDvd.getDvdId(), currentDvd);
        }
        //closes Scanner
        scanner.close();
    }

    /*
    writeCollection() is used to write the Object Information to the flat text 
    file.
     */
    private void writeCollection() throws DVDDaoException {
        PrintWriter out;
        try {
            out = new PrintWriter(new FileWriter(DVD_FILE));
        } catch (IOException e) {
            throw new DVDDaoException("Could not save DVD Data.", e);
        }
        //Write out the DVD objects to the collection file.
        //Reusing the method to get the list of DVD's.
        List<DVD> dvdList = this.getAllDvds();
        for (DVD currentDvd : dvdList) {
            // write the DVD object to the file
            out.println(currentDvd.getDvdId() + DELIMETER
                    + currentDvd.getDvdTitle() + DELIMETER
                    + currentDvd.getDvdReleaseDate() + DELIMETER
                    + currentDvd.getDvdMppaRating() + DELIMETER
                    + currentDvd.getDvdDirectorsName() + DELIMETER
                    + currentDvd.getDvdStudio() + DELIMETER
                    + currentDvd.getDvdUserRating());
            // force PrintWriter to write line to the file
            out.flush();

        }
        // Clean up

        out.close();
    }

    @Override
    public DVD addDvd(String dvdId, DVD dvd) throws DVDDaoException {
        DVD newDvd = dvds.put(dvdId, dvd);
        writeCollection();

        return newDvd;
    } 

    @Override
    public List<DVD> getAllDvds() throws DVDDaoException {
        loadCollection();
        return new ArrayList<DVD>(dvds.values());
    }

    @Override
    public DVD getDvd(String dvdId) throws DVDDaoException {
        loadCollection();
        return dvds.get(dvdId);
    }

    @Override
    public DVD removeDvd(String dvdId) throws DVDDaoException {
        DVD removedDvd = dvds.remove(dvdId);
        writeCollection();
        return removedDvd;
    }

    @Override
    public List<String> SearchDvdTitle(String titleQuery) throws DVDDaoException {
        loadCollection();
        List<DVD> dvdList = this.getAllDvds();
        List<String> titleList = new ArrayList();
        
            for (DVD dvd: dvdList) {
                if (dvd.getDvdTitle().toLowerCase().contains(titleQuery.toLowerCase())) {
                    titleList.add("Dvd ID: "+dvd.getDvdId()+
                            " DVD TITLE: "+dvd.getDvdTitle());
                    
                }
            
        }

        
        return titleList;

    }

}
