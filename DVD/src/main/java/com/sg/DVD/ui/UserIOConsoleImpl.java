/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.DVD.ui;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class UserIOConsoleImpl implements UserIO {

    Scanner sc = new Scanner(System.in);

    public void print(String message) {
        System.out.println(message);
    }

    @Override
    public double readDouble(String prompt) {
        double x;
        try {
            System.out.println(prompt);
            x = Double.parseDouble(sc.nextLine());

        } catch (NumberFormatException e) {
            System.out.println("You have entered a non double number");
            x = -1;

        }
        return x;

    }

    @Override
    public double readDouble(String prompt, double min, double max
    ) {
        double x, y;
        try {
            while (true) {
                System.out.println(prompt);
                x = Double.parseDouble(sc.nextLine());
                if (x >= min && x <= max) {
                    y = x;

                    break;

                }
            }
        } catch (NumberFormatException e) {
            System.out.println("You have entered a non double number");
            y = -1;

        }
        return y;
    }

    @Override
    public float readFloat(String prompt
    ) {
        float x;
        try {
            System.out.println(prompt);
            x = Float.parseFloat(sc.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("You have entered a non float number");
            x = -1;

        }

        return x;

    }

    @Override
    public float readFloat(String prompt, float min, float max
    ) {
        float x, y;

//        
        try {
            while (true) {
                System.out.println(prompt);
                x = Float.parseFloat(sc.nextLine());
                if (x >= min && x <= max) {
                    y = x;
                    break;

                }

            }
        } catch (NumberFormatException e) {
            System.out.println("You have entered a non float number");
            y = -1;

        }

        return y;

    }

    @Override
    public int readInt(String prompt
    ) {
        int x;

        try {
            System.out.println(prompt);
            x = Integer.parseInt(sc.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("You have entered a non interger number");
            x = -1;
        }

        return x;

    }

    @Override
    public int readInt(String prompt, int min, int max
    ) {
        int x, y;
        try {
            while (true) {
                System.out.println(prompt);

                x = Integer.parseInt(sc.nextLine());
                if (x >= min && x <= max) {
                    y = x;
                    break;
                } else {
                    System.out.println("_______________________________________________");
                    System.out.println("Please Select a number from 1-7 to correspond ");
                    System.out.println("with the activity you would like to perform on");
                    System.out.println("on the DVD collection");
                    System.out.println("_______________________________________________");
                    y = -1;

                }

            }

        } catch (NumberFormatException e) {
            System.out.println("_______________________________________________");
            System.out.println("Please Select a number from 1-7 to correspond ");
            System.out.println("with the activity you would like to perform on");
            System.out.println("on the DVD collection");
            System.out.println("_______________________________________________");
            y = -1;

        }
        return y;

    }

    @Override
    public long readLong(String prompt) {
        long x;
        try {
            System.out.println(prompt);
            x = Long.parseLong(sc.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("You've entered non-Long number");
            x = -1;

        }

        return x;

    }

    @Override
    public long readLong(String prompt, long min, long max
    ) {
        long x, y;
        try {

            while (true) {
                System.out.println(prompt);
                x = Long.parseLong(sc.nextLine());
                if (x >= min && x <= max) {
                    y = x;
                    break;

                }

            }
        } catch (NumberFormatException e) {
            System.out.println("You've entered non-Long number");
            y = -1;
        }
        return y;

    }

    @Override
    public String readString(String prompt
    ) {
        String x;
        System.out.println(prompt);
        x = sc.nextLine();
        return x;

    }

    @Override

    public String dvdInput(String prompt) {
        String x, y;
        while (true) {
            System.out.println(prompt);
            x = sc.nextLine();

            if (x.isEmpty()) {
                System.out.println("_________________________________________");
                System.out.println("Please Enter Text String with the DVD info.");
                System.out.println("_________________________________________");

            } else {
                y = x;

                if (x.equals("Groundhog Day")) {
                    for (int i = 0; i < 10; i++) {
                        System.out.println("Groundhog Day");

                    }
                    System.out.println("That Was Wierd!");
                } else if (x.contains("Star Wars")) {
                    System.out.println("LUKE I AM YOUR FATHER");

                } else if (x.contains("Good Will Hunting")) {
                    System.out.println("Do You Like Apples?");
                    String apples = sc.nextLine();
                    apples = apples.toUpperCase();
                    if (apples.equals("Y") || apples.equals("YES")) {
                        System.out.println("How do you like them Apples?");
                    } else {
                        System.out.println("You probably should watch Good Will"
                                + "Hunting again!");
                    }
                }

                break;

            }

        }
        return y;
    }

    @Override
    public String dvdSearch(String prompt) {

        String x, y;
        while (true) {
            System.out.println(prompt);
            x = sc.nextLine();

            if (x.isEmpty()) {
                System.out.println("_________________________________________");
                System.out.println("Please Enter a DVD TITLE TO SEARCH.");
                System.out.println("_________________________________________");

            } else {
                y = x;
                break;

            }

        }
        return y;
    }

}
