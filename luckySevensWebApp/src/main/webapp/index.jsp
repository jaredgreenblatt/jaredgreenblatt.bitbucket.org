<%-- 
    Document   : index
    Created on : Mar 20, 2017, 10:30:46 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lucky Sevens</title>
    </head>
    <body>
        <h1>Lucky Sevens!!</h1>
        <p>
            Each Round the program rolls two dice if the pair =7 you win $ 4. If anything else you lose $1.
        </p>
        <p>
            Enter your Bet!
        </p>
         <form method="post" action="luckySevenServlet">
            <input type="text" name ="initialBetAmount"/>
            <br><br>
            <input type ="submit" value ="Lets Play"/>
        </form> 
    </body>
</html>
