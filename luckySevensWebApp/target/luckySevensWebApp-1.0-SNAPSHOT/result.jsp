<%-- 
    Document   : result
    Created on : Mar 20, 2017, 10:30:56 PM
    Author     : apprentice
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Luck Sevens Result</title>
    </head>
    <body>
        <h1>Lucky Sevens Results</h1>
        <p>
            You bet $
            <c:out value="${originalBet}" />

        </p>
        <p>
            You are broke after 
            <c:out value="${rollCount} rolls." />

        </p>
        
         <p>
            You should have quit after
            <c:out value="${highRoll} rolls when you had $" />
            <c:out value=" ${bigWin}." />
            

        </p>
        
       
        
        <p>
        <a href="index.jsp">Play Again?.</a>
    </p>
    </body>
</html>
