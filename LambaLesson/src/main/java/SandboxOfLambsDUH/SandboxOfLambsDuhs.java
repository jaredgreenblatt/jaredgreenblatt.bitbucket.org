/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SandboxOfLambsDUH;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author apprentice
 */
public class SandboxOfLambsDuhs {

    public static void main(String[] args) {
        ArrayList<String> llamaHerd = new ArrayList<>();

        llamaHerd.add("Felicity");
        llamaHerd.add("BeeBop");
        llamaHerd.add("NoWhite");
        llamaHerd.add("Prism");
        llamaHerd.add("Kalidescope");
        llamaHerd.add("FLoyd");
        llamaHerd.add("Dixie");
        llamaHerd.add("Oreo");
        llamaHerd.add("Samurai");

        System.out.println("I have this many Llams in my herd " + llamaHerd.size());

        for (String llamaname : llamaHerd) {

            System.out.println(llamaname);
        }
        Stream<String> llamaStream = llamaHerd
                .stream();
        /// filter takes in a predicate so it has one parameter of the same type
        // your straming an it has to return boolean.
        llamaHerd.stream().filter(llamaName -> {
            return true;
        });
// public boolean test(String llamaName) 
//This format will allow for lines of code.
        llamaHerd.stream().filter((String llamaName) -> {
            return true;
        });

        llamaHerd.stream().filter(llamaName
                -> {
            return llamaName.equals("Oreo");
        });
        //one line of code.
        llamaHerd.stream().filter(llamaName
                -> llamaName.equals("Oreo"));
        llamaHerd
                .stream()
                .filter(llamaName -> llamaName.equals("Oreo"))
                // public void accept(String llamaName)
                .forEach(shouldBeOreo -> System.out.println(shouldBeOreo));

        int result = sumStuff((Integer bob) -> {
            return true;
        });
        System.out.println("The result of summing of all of the stuff is:" + result);

        result = sumStuff((Integer bob) -> {
            return bob % 2 == 0;
        });
        System.out.println("The result of summing of all of the even stuff is:" + result);

        result = sumStuff((Integer bob) -> {
            return bob > 5;
        });
        System.out.println("The result of summing of all of the stuff greater than 5 is:" + result);

        result = sumStuff(num -> num % 2 == 0);
        System.out.println("");
        System.out.println("The result of summing of all of the evens is:" + result);

    }

    public static int sumStuff(Predicate<Integer> whatToSum) {
        List<Integer> someIntsIHave = new ArrayList<>();
        someIntsIHave.add(1);
        someIntsIHave.add(2);
        someIntsIHave.add(3);
        someIntsIHave.add(4);
        someIntsIHave.add(5);
        someIntsIHave.add(6);
        someIntsIHave.add(7);
        someIntsIHave.add(8);
        someIntsIHave.add(9);
        someIntsIHave.add(10);

        List<Integer> filteredInts
                = someIntsIHave.stream()
                .filter(whatToSum)
                .collect(Collectors.toList());

        int sum = 0;
        for (int x : filteredInts) {
            sum += x;

        }

        return sum;
    }

}
