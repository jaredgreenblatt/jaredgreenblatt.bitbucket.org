/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hashmaps.exercises;

import java.util.HashMap;
import java.util.List;
import java.util.Set;
import misc.Dog;
import misc.MiscObjectFactory;
import misc.Person;

/**
 *
 * @author ahill
 */
public class Indexing {

    public static void main(String[] args) {

        // Exercise 1:
        // Index this list of people by full name into a hashmap
        // Then print out all the fullNames and ages.
        List<Person> peopleToIndexByName = MiscObjectFactory.makePeople(20);
        Person test = peopleToIndexByName.get(0);
        System.out.println(test.getFullName());

        for (Person person : peopleToIndexByName) {
            System.out.println(person.getFullName());

        }
        HashMap<String, Person> sortedPeopleMap = new HashMap<>();
        for (Person sally : peopleToIndexByName) {
            String nameKey = sally.getFullName();

            sortedPeopleMap.put(nameKey, sally);

        }
        for (String name : sortedPeopleMap.keySet()) {
            System.out.println(name);
            Person yum = sortedPeopleMap.get(name);
            System.out.println(yum.getBirthYear());

        }
        System.out.println("--------------------------------");

        Set<String> allDaNames = sortedPeopleMap.keySet();
        for (String name : allDaNames) {
            System.out.print(name);
            Person fromDaBox = sortedPeopleMap.get(name);
            System.out.print(" ");
            System.out.print(fromDaBox.getBirthYear());
            System.out.println("");

        }

        // Exercise 2:
        // Index this next list first by microchip ID, and then in another hashmap by name.
        // Print ou the size of both, what's the difference?
        List<Dog> dogsToIndex = MiscObjectFactory.makeDogs(500);
        for (Dog dog : dogsToIndex) {
            System.out.println(dog.getName());
        }
        HashMap<String, Dog> sortedDogMap = new HashMap<>();

        for (Dog pup : dogsToIndex) {
            String dogName = pup.getName();
            sortedDogMap.put(dogName, pup);

        }
        Set<String> allDogName = sortedDogMap.keySet();
//        Prints out the dog names with Name as alpha
//        for (String dogs : allDogName) {
//            System.out.println(dogs);
//            Dog dogMap = sortedDogMap.get(dogs);
//            System.out.println(dogMap.getMicroChip());
//        }
        int count = 0;
        for (String dogs : allDogName) {
            
            count++;
            
        }
          System.out.println("You have this many items when dog name is the key "+sortedDogMap.size());

          HashMap<String, Dog> sortedDogMapByChip = new HashMap<>();
           for(Dog chip: dogsToIndex){
               String dogChip = chip.getMicroChip();
               sortedDogMapByChip.put(dogChip, chip);
               
               
           }
            Set<String> allDogChip = sortedDogMapByChip.keySet();
            ///Prints out the dog names and Chips
//            for(String ch: allDogChip){
//                System.out.println(ch);
//                Dog dogChip = sortedDogMapByChip.get(ch);
//                System.out.println(dogChip.getName());
//                
//            }
            int chipCount=0;
            for(String ch: allDogChip){
                chipCount++;
            }
            System.out.println("There are "+sortedDogMapByChip.size()+" when the chip is the alpha");

          
    }
}
