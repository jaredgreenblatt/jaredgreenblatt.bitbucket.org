/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hashmaps.exercises;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import misc.*;

/**
 *
 * @author ahill
 */
public class Collecting {
    public static void main(String[] args) {
        
        Random r = new Random();
        
        /* 
        ** Exercise 1:
        ** Randomize a two 6 sided dice roll 50 times
        ** and collect the dice roll iteration against the sum of the dice
        ** Print out all the rolls on which it totalled a 6...
        ** Uncomment the following comments and fill in the ????
        */
        
       HashMap<Integer, List<Integer>> diceRolls = new HashMap<>();
      
     for (int i = 0; i < 50; i++) {
        int diceRollSum = r.nextInt(12) + 1;           
           if(diceRolls.containsKey(i)){
            
       } else {
                List<Integer> rollList = new ArrayList<Integer>();
                rollList.add(i);
                 diceRolls.put( i , rollList); 
            }
        }
        
        System.out.println("Rolls where you totalled a 6:");
        for(Integer roll : diceRolls.get(????){
            System.out.print(roll + ",");
        }
        
        // Exercise 2:
        // Randomize a dice roll 50 times
        
        
        // Exercise 3:
        // Sort this list of people by last name into a hashmap
        // Print out the first names of everyone named Smith, Johnson, Williams, Jones or Brown
        List<Person> peopleToSortByLastName = MiscObjectFactory.makePeople(50);
        
        
        // Exercise 4:
        // Sort this next list into a new hashmap, but by age. 
        // Print out the full names of everyone under 50.
        List<Person> peopleToSortByAge = MiscObjectFactory.makePeople(100);
        
        
        // Exercise 5:
        // Sort these dogs by breed
        // Print out the names of all Chihuahuas under 8 lbs,
        // and make all the brown Great Danes bark.
        List<Dog> dogsToSortByBreed = MiscObjectFactory.makeDogs(500);
        
    }
}
