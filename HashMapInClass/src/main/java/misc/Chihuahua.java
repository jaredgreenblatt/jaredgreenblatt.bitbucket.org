/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misc;

/**
 *
 * @author ahill
 */
public class Chihuahua extends Dog{
    
    public Chihuahua(String name, String color, double weight){
        super(name, color, weight);
    }
    
    @Override
    public void bark(){
        System.out.println("Yip!");
    }
    
    @Override
    public String getName(){
        return "World Ruler "+ this.name;
    }
}
