/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misc;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author ahill
 */
public class MiscObjectFactory {

    private static Random r = new Random();
    
    private static String[] firstNames = {"Mary", "John", "Patricia", "Robert", "Linda", "Michael", "Barbara", "William", "Elizabeth", "David", "Jennifer",
        "Richard", "Maria", "Charles", "Susan", "Joeseph", "Dorothy", "Thomas", "Lisa", "Christopher", "Nancy",
        "Daniel", "Karen", "Paul", "Betty", "Mark", "Helen", "Donald", "Sandra", "George", "Donna", "Kenneth"};
    
    private static String[] lastNames = {"Smith", "Johnson", "Williams", "Jones", "Brown", "Davis", "Miller", "Wilson", "Moore", "Taylor", "Anderson",
        "Thomas", "Jackson", "White", "Harris", "Martin", "Thompson", "Garcia", "Martinez", "Robinson", "Clark",
        "Rodriguez", "Lewis", "Lee", "Walker", "Hall", "Allen", "Young", "Hernadez", "King", "Wright", "Lopez"};

    private static String[] petColors = {"Brown", "White Spotted", "Black & Tan", "Blue", "Black & White", "Blue Merle", "White", "Tan", "Golden"};
    
    private static String[] petNames = {"Queenie", "Denver", "Fritz", "Blue", "Pegasus", "Perseus", "Jimmy TwoPipes", "FluffyPet", "FancyPants", "Athensa",
        "Knoppler", "Marie", "FrooFroo", "Magpie", "Bonnie", "Clyde", "Benji", "Mensa", "Pumpernickel", "Zazoo", "Duchesse", "Moof", "Prue", "Mitch",
        "Bagel", "Darkwing Duck", "Bobbafett", "Dr. What", "Mochi", "Floyd", "Prism", "Winnie the Poo", "Piglet", "Frankenfurter"};
    
    private static int[] randomNumbers = { 2, 89, 60, 88, 11, 35, 80, 10, 27, 98, 73, 49, 64, 32, 71, 28, 66,  7, 85, 22, 31, 86, 100, 25, 82, 99, 97, 92, 62,  
        5,  8, 16,  3, 78, 65, 47, 38, 77, 39, 79, 94, 95, 29, 13, 20, 34, 15,  9, 19, 42, 51, 58, 36, 37, 84, 18, 40, 45, 59, 46, 21, 96, 12,  1, 57, 55, 33, 
        87, 17, 24, 76, 68, 30, 81, 90, 26, 14, 23, 56, 91, 61, 63, 43, 75, 53,  6, 67, 74, 44, 41, 50,  4, 93, 52, 48, 72, 54, 83, 70, 69 };

    public static ArrayList<Dog> makeDogs(int numOfDogs) {

        ArrayList<Dog> dogs = new ArrayList<>();
        for (int i = 0; i < numOfDogs; i++) {
            String randomName = petNames[r.nextInt(petNames.length)];
            String randomColor = petColors[r.nextInt(petColors.length)];

            switch (r.nextInt(3)) {
                case 0:
                    dogs.add(new Husky(randomName, randomColor, r.nextInt(50) + 40));
                    break;
                case 1:
                    dogs.add(new GreatDane(randomName, randomColor, r.nextInt(50) + 70));
                    break;
                case 2:
                    dogs.add(new Chihuahua(randomName, randomColor, r.nextInt(10) + 5));
                    break;
            }

        }

        return dogs;
    }

    public static ArrayList<Person> makePeople(int numOfPeople) {

        ArrayList<Person> people = new ArrayList<>();

        for (int i = 0; i < numOfPeople; i++) {
            people.add(new Person(firstNames[r.nextInt(firstNames.length)], lastNames[r.nextInt(lastNames.length)], r.nextInt(50) + 15));
        }

        return people;
    }

    public static ArrayList<Fruit> makeFruit(int numOfFruit) {

        ArrayList<Fruit> fruit = new ArrayList<>();

        for (int i = 0; i < numOfFruit; i++) {
            switch (r.nextInt(19) + 1) {
                case 1:
                    fruit.add(new Fruit("Red", "Apple"));
                    break;
                case 2:
                    fruit.add(new Fruit("Green", "Apple"));
                    break;
                case 3:
                    fruit.add(new Fruit("Red", "Strawberry"));
                    break;
                case 4:
                    fruit.add(new Fruit("White", "Strawberry"));
                    break;
                case 5:
                    fruit.add(new Fruit("Yellow", "Banana"));
                    break;
                case 6:
                    fruit.add(new Fruit("Green", "Banana"));
                    break;
                case 7:
                    fruit.add(new Fruit("Red", "Cranberry"));
                    break;
                case 8:
                    fruit.add(new Fruit("Blue", "Blueberry"));
                    break;
                case 9:
                    fruit.add(new Fruit("Orange", "Orange"));
                    break;
                case 10:
                    fruit.add(new Fruit("Green", "Orange"));
                    break;
                case 11:
                    fruit.add(new Fruit("Yellow", "Lemon"));
                    break;
                case 12:
                    fruit.add(new Fruit("Purple", "Plum"));
                    break;
                case 13:
                    fruit.add(new Fruit("Purple", "Grapes"));
                    break;
                case 14:
                    fruit.add(new Fruit("Green", "Grapes"));
                    break;
                case 15:
                    fruit.add(new Fruit("Red", "Cherries"));
                    break;
                case 16:
                    fruit.add(new Fruit("Red", "Grapes"));
                    break;
                case 17:
                    fruit.add(new Fruit("Peachy", "Apricot"));
                    break;
                case 18:
                    fruit.add(new Fruit("Peachy", "Peach"));
                    break;
                case 19:
                    fruit.add(new Fruit("Purple", "Fig"));
                    break;

            }
        }

        return fruit;
    }
}
