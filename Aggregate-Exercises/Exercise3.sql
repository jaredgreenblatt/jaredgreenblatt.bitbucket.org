/*
	Find the gross total of all orders (sum of quantity * unit price) 
	for each customer, order it in descending order by the total.
*/

USE Northwind;


Select c.CompanyName, SUM(UnitPrice * Quantity) AS GrossTotal
from Order_Details od
inner join `Orders` o
on od.OrderId = o.OrderId
inner join Customers c 
on o.CustomerID = c.CustomerId
group by c.CompanyName
order by GrossTotal Desc;



