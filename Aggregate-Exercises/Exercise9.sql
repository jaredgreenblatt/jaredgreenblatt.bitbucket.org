/*
	Challenge: 
	Show the total amount of orders by
	year and country.  Data should be ordered 
	by year ascending and total descending.
	
	TotalSales    Year     Country
	41907.80      1996     USA
	37804.60      1996     Germany
	etc...
	
	Hint: Research the DatePart() function
*/

USE Northwind;
Select * from Orders;

Select Sum(od.Quantity * od.UnitPrice) as TotalSales, o.ShipCountry as Country, Year(o.OrderDate) as `Year`
From Orders o
inner join Order_Details od 
on o.orderID  = od.orderID
group by Country, Year(o.OrderDate)
order by `Year` , TotalSales Desc;


