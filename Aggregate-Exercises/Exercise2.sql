/*
	Find the gross total (sum of quantity * unit price) for 
	all orders placed by B's Beverages and Chop-suey Chinese.
*/

-- SUM(UnitPrice * Quantity) AS TotalSales,



USE Northwind;


Select c.CompanyName , SUM(UnitPrice * Quantity) AS 'Gross Total'
from Order_Details od
inner join `Orders` o
on od.OrderId = o.OrderId
inner join Customers c 
on o.CustomerID = c.CustomerId
where c.CompanyName in("B's Beverages",'Chop-suey Chinese')
group by c.CompanyName;

USE Northwind;


Select SUM(UnitPrice * Quantity) AS 'Gross Total'
from Order_Details od
inner join `Orders` o
on od.OrderId = o.OrderId
inner join Customers c 
on o.CustomerID = c.CustomerId
where c.CompanyName in("B's Beverages",'Chop-suey Chinese');




