/*
	Show the number of orders placed by customers 
	from fewest to most provided the customer has 
	a minimum of 4 orders.
*/

USE Northwind;

Select c.CompanyName as Customer, count(OrderId) as OrderCount
From Customers c
inner join Orders o
on c.CustomerID = o.CustomerId
group by Customer
having count(OrderId)>=4
order by orderCount ;

