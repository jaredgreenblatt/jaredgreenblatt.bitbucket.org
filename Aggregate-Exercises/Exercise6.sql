/*
	Get the count of how many unique countries
	are represented by our suppliers.
*/

USE Northwind;
SELECT DISTINCT count(Distinct(s.Country)) as UniqueCountry
from Suppliers as s;

