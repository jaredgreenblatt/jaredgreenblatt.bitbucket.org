/*
	Get the count of how many employees work for the 
	company
*/

USE Northwind;

Select count(*) as 'Total Employees' from Employees