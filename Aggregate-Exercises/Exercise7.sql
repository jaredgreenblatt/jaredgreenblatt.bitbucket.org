/*
	Find the total sales by supplier 
	ordered from most to least.
*/

USE Northwind;
Select sum(od.UnitPrice * od.Quantity) as TotalSales,  s.CompanyName
From Suppliers s
inner join Products p 
on s.SupplierId = p.SupplierId
inner join Order_Details od
on p.ProductId = od.ProductId
group by s.CompanyName 
order by TotalSales Desc;





