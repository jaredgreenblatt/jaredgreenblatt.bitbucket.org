/*
	Get the count of how many employees 
	report to someone else in the company 
	without using a WHERE clause.
*/

USE Northwind;
Select count(*) as 'Managed Employees'
From Employees as emp
inner join Employees as mgr
on mgr.ReportsTo = emp.EmployeeID;



