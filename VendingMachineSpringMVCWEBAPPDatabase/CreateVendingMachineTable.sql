drop  database  if exists  VendingMachine;
--  Creates Database if HollywoodTowerHotel exists.
Create database if not exists  VendingMachine;

use VendingMachine;
CREATE TABLE IF NOT EXISTS `vendingItems` (
 `itemID` int(11) NOT NULL AUTO_INCREMENT,
 `itemName` varchar(50) NOT NULL,
 `itemCost` decimal(10,2) NOT NULL,
 `itemsInInventory`int(11) NOT NULL,
 PRIMARY KEY (`itemID`)
) ;

insert into vendingItems(itemName, itemCost, itemsInInventory)
values
('Snicker', '1.25', 4),
('KitKat', '1.25', 4),
('Sour Patch Kids', '1.25', 4),
('Sweedish Fish', '1.25', 4),
('Gummy Bears', '1.25', 4),
('Take 5', '1.25', 4),
('MilkyWay', '1.25', 4),
("M&M's", '1.25', 4),
('Skittles', '1.25', 4),
('StarBust', '1.25', 4),
('Reeces', '1.25', 4),
('Crunch', '1.25', 5);
select * from vendingItems;

drop  database  if exists  VendingMachineTest;
--  Creates Database if HollywoodTowerHotel exists.
Create database if not exists  VendingMachineTest;

use VendingMachineTest;

CREATE TABLE IF NOT EXISTS `vendingItems` (
 `itemID` int(11) NOT NULL AUTO_INCREMENT,
 `itemName` varchar(50) NOT NULL,
 `itemCost` decimal(10,2) NOT NULL,
 `itemsInInventory`int(11) NOT NULL,
 PRIMARY KEY (`itemID`)
) ;

insert into vendingItems(itemName, itemCost, itemsInInventory)
values
('Snicker', '1.25', 4),
('KitKat', '1.25', 4),
('Sour Patch Kids', '1.25', 4),
('Sweedish Fish', '1.25', 4),
('Gummy Bears', '1.25', 4),
('Take 5', '1.25', 4),
('MilkyWay', '1.25', 4),
("M&M's", '1.25', 4),
('Skittles', '1.25', 4),
('StarBust', '1.25', 4),
('Reeces', '1.25', 4),
('Crunch', '1.25', 5);
select * from vendingItems;



