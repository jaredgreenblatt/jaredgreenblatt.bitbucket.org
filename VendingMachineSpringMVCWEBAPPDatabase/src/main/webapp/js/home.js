/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//contactList.js
$(document).ready(function () {
//    $('#runningTotal').val(0);


});
/*
 selectVendingItem is used to get the button click from the user and populate the
 item choice field. The id is from the button  onclick function. These ID's are
 assigned when the button is built.
 */
function selectVendingItem(id) {
    clearErrorMessages();
    var itemNumber = id;
    var itemSelection = $('#itemChoice');
    itemSelection.val(itemNumber);
}
/* This will clear the vending  Item selection and money after an item is purchased.*/
function clearVendingMachine() {
    // $('#VendingItems').empty();
    $('#itemChoice').val('');
    $('#moneyIn').val('');
    $('#runningTotal').val(parseFloat(0.00));
    //$('#messages').val('');

}

/* clearErrorMessages removes the error message in the messages box.*/
function clearErrorMessages() {
    $('#messages').empty();


}

/* Make change is used print change whhen the make change button is cliked. It will go
 through for each coin  amount  and see if the user has more or equal to that. if they do
 the counter will be added to. We then check the amount of the coins to append to the change
 return string.
 */
function makeChange() {
    //    alert('SUCCESS');
    var moneyInVendingMachine = parseFloat($('#runningTotal').val());
    var quarterCount = 0;
    var dimeCount = 0;
    var nickleCount = 0;
    var pennyCount = 0;
    var printChange = $('#change')

    while (moneyInVendingMachine >= 0.25) {
        quarterCount++;
        moneyInVendingMachine = moneyInVendingMachine - .25;
        moneyInVendingMachine = moneyInVendingMachine.toFixed(2);
    }
    while (moneyInVendingMachine >= 0.10) {
        dimeCount++;
        moneyInVendingMachine = moneyInVendingMachine - .10;
        moneyInVendingMachine = moneyInVendingMachine.toFixed(2);
    }
    while (moneyInVendingMachine >= 0.05) {
        nickleCount++;
        moneyInVendingMachine = moneyInVendingMachine - .05;
        moneyInVendingMachine = moneyInVendingMachine.toFixed(2);
    }
    while (moneyInVendingMachine >= 0.01) {
        pennyCount++;
        moneyInVendingMachine = moneyInVendingMachine - .01;
        moneyInVendingMachine = moneyInVendingMachine.toFixed(2);
    }

    var changeString = '';

    if (quarterCount === 1) {
        changeString += quarterCount + ' Quarter. ';

    } else if (quarterCount > 1) {
        changeString += quarterCount + ' Quarters. ';

    }
    if (dimeCount === 1) {
        changeString += dimeCount + ' Dime. ';

    } else if (dimeCount > 1) {
        changeString += dimeCount + ' Dimes. ';

    }
    if (nickleCount === 1) {
        changeString += nickleCount + ' Nickle. ';

    } else if (nickleCount > 1) {
        changeString += nickleCount + ' Nickles. ';

    }
    if (pennyCount === 1) {
        changeString += pennyCount + ' Penny. ';

    } else if (pennyCount > 1) {
        changeString += pennyCount + ' Pennies. ';

    }
    printChange.val(changeString);
    clearVendingMachine();



}



/* add money is the method that is called when you click any of the coin buttons.
 They all have onlicks with the addMoney Method with the parameter to identify the
 demoniation. The method will then go through a series of if statements so that
 it will add the appropriate moeney amount to the machine. In those functons the hidden
 running total value will be grabed and then parsed. Then the money will be added after that the running total will be
 appended. Finally that runningTotal value will be sent the moneyValue HTML so that
 the user can see it. */

function addMoney(inputMoneyType) {
    clearErrorMessages();

    $('#change').val('');
    var moneyAmount = 0.00;
    var dollarAmount = $('#moneyIn');

    if (inputMoneyType === 'dollar') {
        moneyAmount = parseFloat($('#runningTotal').val());
        moneyAmount += 1.00;
        moneyAmount = moneyAmount.toFixed(2);
        $('#runningTotal').val(moneyAmount);

    } else if (inputMoneyType === 'quarter') {
        moneyAmount = parseFloat($('#runningTotal').val());
        moneyAmount += 0.25;
        moneyAmount = moneyAmount.toFixed(2);
        $('#runningTotal').val(moneyAmount);
    } else if (inputMoneyType === 'dime') {
        moneyAmount = parseFloat($('#runningTotal').val());
        moneyAmount += 0.10;
        moneyAmount = moneyAmount.toFixed(2);
        $('#runningTotal').val(moneyAmount);
    } else if (inputMoneyType === 'nickle') {
        moneyAmount = parseFloat($('#runningTotal').val());
        moneyAmount += 0.05;
        moneyAmount = moneyAmount.toFixed(2);
        $('#runningTotal').val(moneyAmount);
    }

    dollarAmount.val(moneyAmount);



}




