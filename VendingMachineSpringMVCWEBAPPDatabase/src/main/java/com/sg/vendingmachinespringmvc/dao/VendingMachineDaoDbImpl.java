/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachinespringmvc.dao;

import com.sg.vendingmachinespringmvc.model.VendingItem;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class VendingMachineDaoDbImpl implements VendingMachineDao {

    private static final String SQL_INSERT_VENDING_ITEM
            = "insert into vendingItems "
            + "(itemName, itemCost, itemsInInventory) "
            + "values (?, ?, ?)";
    private static final String SQL_DELETE_VENDING_ITEM
            = "delete from vendingItems where itemID = ?";
    private static final String SQL_SELECT_VENDING_ITEM
            = "select * from vendingItems where itemID = ?";
    private static final String SQL_UPDATE_VENDING_ITEM
            = "update vendingItems set "
            + "itemName = ?, itemCost = ?, itemsInInventory = ? "
            + "where itemID = ? ";

    private static final String SQL_SELECT_ALL_VENDING_ITEMS
            = "select * from vendingItems";

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public VendingItem addVendingItem(VendingItem vendingItem) {
        jdbcTemplate.update(SQL_INSERT_VENDING_ITEM,
                vendingItem.getItemName(),
                vendingItem.getItemCost(),
                vendingItem.getNumberOfItemsInInventory());
        long newId = jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class);

        vendingItem.setItemId(newId);
        return vendingItem;
    }

    @Override
    public List<VendingItem> getAllVendingItems() {

        return jdbcTemplate.query(SQL_SELECT_ALL_VENDING_ITEMS, new VendingItemMapper());
    }

    @Override
    public VendingItem getVendingItem(long itemId) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_VENDING_ITEM,
                    new VendingItemMapper(), itemId);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public void removeItem(long itemId) {
        jdbcTemplate.update(SQL_DELETE_VENDING_ITEM, itemId);
      
    }

    @Override
    public void editVendingItem(VendingItem vendingItem) {
        jdbcTemplate.update(SQL_UPDATE_VENDING_ITEM,
                vendingItem.getItemName(),
                vendingItem.getItemCost(),
                vendingItem.getNumberOfItemsInInventory(),
                vendingItem.getItemId());
    }

    private static final class VendingItemMapper implements RowMapper<VendingItem> {

        @Override
        public VendingItem mapRow(ResultSet rs, int i) throws SQLException {
            VendingItem vendingItem = new VendingItem();
            vendingItem.setItemId(rs.getLong("itemID"));
            vendingItem.setItemName(rs.getString("itemName"));
            vendingItem.setItemCost(rs.getBigDecimal("itemCost").setScale(2, RoundingMode.HALF_DOWN));
            vendingItem.setNumberOfItemsInInventory(rs.getInt("itemsInInventory"));
            return vendingItem;
        }

    }

}
