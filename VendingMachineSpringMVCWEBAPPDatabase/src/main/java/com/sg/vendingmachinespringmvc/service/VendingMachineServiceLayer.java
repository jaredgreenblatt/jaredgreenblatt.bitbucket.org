/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachinespringmvc.service;

import com.sg.vendingmachinespringmvc.model.Change;
import com.sg.vendingmachinespringmvc.model.VendingItem;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author Jared Greenblatt
 */

public interface VendingMachineServiceLayer {

   

    void createVendingItem(VendingItem vendingItem) throws
            VendingMachineDataValidationException;

    void editVendingItem(VendingItem vendingItem) throws
            VendingMachineDataValidationException;
            

    List<VendingItem> getAllVendingItem() ;

    VendingItem getVendingItem(long itemId) ;

    void removeVendingItem(long itemId) ;

    VendingItem purchaseItem(VendingItem vendingItem) throws
            NoItemInventoryException,
            InsufficientFundsException;

    Change makeChange();

    public BigDecimal getrunningTotal();

    public void setrunningTotal(BigDecimal runningTotal);

}
