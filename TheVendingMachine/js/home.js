//contactList.js
$(document).ready(function() {
    $('#runningTotal').val(0);
    loadMachine();
});
/*
selectVendingItem is used to get the button click from the user and populate the
item choice field. The id is from the button  onclick function. These ID's are
assigned when the button is built.
*/
function selectVendingItem(id) {
    clearErrorMessages();
    var itemNumber = id;
    var itemSelection = $('#itemChoice');
    itemSelection.val(itemNumber);
}
/* This will clear the vending  Item selection and money after an item is purchased.*/
function clearVendingMachine() {
    $('#VendingItems').empty();
    $('#itemChoice').val('');
    $('#moneyIn').val('');
    $('#runningTotal').val(parseFloat(0.00));
    //$('#messages').val('');

}

/* clearErrorMessages removes the error message in the messages box.*/
function clearErrorMessages() {
    $('#messages').empty();


}

/* Make change is used print change whhen the make change button is cliked. It will go
through for each coin  amount  and see if the user has more or equal to that. if they do
the counter will be added to. We then check the amount of the coins to append to the change
return string.
*/
function makeChange() {
    //    alert('SUCCESS');
    var moneyInVendingMachine = parseFloat($('#runningTotal').val());
    var quarterCount = 0;
    var dimeCount = 0;
    var nickleCount = 0;
    var pennyCount = 0;
    var printChange = $('#change')

    while (moneyInVendingMachine >= 0.25) {
        quarterCount++;
        moneyInVendingMachine = moneyInVendingMachine - .25;
        moneyInVendingMachine = moneyInVendingMachine.toFixed(2);
    }
    while (moneyInVendingMachine >= 0.10) {
        dimeCount++;
        moneyInVendingMachine = moneyInVendingMachine - .1;
        moneyInVendingMachine = moneyInVendingMachine.toFixed(2);
    }
    while (moneyInVendingMachine >= 0.05) {
        nickleCount++;
        moneyInVendingMachine = moneyInVendingMachine - .05;
        moneyInVendingMachine = moneyInVendingMachine.toFixed(2);
    }
    while (moneyInVendingMachine >= 0.01) {
        pennyCount++;
        moneyInVendingMachine = moneyInVendingMachine - .01;
        moneyInVendingMachine = moneyInVendingMachine.toFixed(2);
    }

    var changeString = '';

    if (quarterCount == 1) {
        changeString += quarterCount + ' Quarter. '

    } else if (quarterCount > 1) {
        changeString += quarterCount + ' Quarters. '

    }
    if (dimeCount == 1) {
        changeString += dimeCount + ' Dime. '

    } else if (dimeCount > 1) {
        changeString += pennyCount + ' Dimes. '

    }
    if (nickleCount == 1) {
        changeString += nickleCount + ' Nickle. '

    } else if (nickleCount > 1) {
        changeString += nickleCount + ' Nickles. '

    }
    if (pennyCount == 1) {
        changeString += pennyCount + ' Penny. '

    } else if (pennyCount > 1) {
        changeString += pennyCount + ' Pennies. '

    }
    printChange.val(changeString);
    clearVendingMachine();
    loadMachine();



}

/* makePurchase is the function that is run when the make purchase button is clicked.
First it a needs the amount of money in the machine and the id of the selected item.
These are used to connect to the api to make the purchase.  The url is used to
connect to the api.  The api then will send back the change if the purchase was
succesful or an error if it if was not.
*/

function makePurchase() {
    clearErrorMessages();
    //clearErrorMessages();
    var printChange = $('#change')
    var amount = parseFloat($('#runningTotal').val());
    var id = $('#itemChoice').val();
    var concatUrl = 'http://localhost:8080/money/';
    concatUrl += +amount + '/item/' + id;


    $.ajax({
        type: 'GET',
        url: concatUrl,
        success: function(purchaseItem) {
            /* if succesful we will need to get the amount of each coin so that
            it can be printed for the user. IF statements are used to determine if
            that amount of change has more than one coin returned. If it does the
            change stirng will be appended if not that coin will not be included
            in the output.*/
            var quarters = purchaseItem.quarters;
            var dimes = purchaseItem.dimes;
            var nickels = purchaseItem.nickels;
            var pennies = purchaseItem.pennies;

            var changeString = '';

            if (quarters == 1) {
                changeString += quarters + ' Quarter. '

            } else if (quarters > 1) {
                changeString += quarters + ' Quarters. '

            }
            if (dimes == 1) {
                changeString += dimes + ' Dime. '

            } else if (dimes > 1) {
                changeString += dimes + ' Dimes. '

            }
            if (nickels == 1) {
                changeString += nickels + ' Nickle. '

            } else if (nickels > 1) {
                changeString += nickels + ' Nickles. '

            }
            if (pennies == 1) {
                changeString += pennies + ' Penny. '

            } else if (pennies > 1) {
                changeString += pennies + ' Pennies. '

            }
            printChange.val(changeString);
            clearVendingMachine();
            loadMachine();
            var thankYou = 'Thank You!';
            $('#messages').append(thankYou);

            //  });
        },
        //  alert("Success");


        error: function(data) {
          /* If an error is returned the JSON response will include an error message.
          We must grab that message and display it for the  user. This way they can
          see if they need to add more funds or selected an item that is out
          of stock.*/
            var errorMessage = data.responseJSON.message;

            $('#errorMessages')
                .append($('#messages')

                    .text(errorMessage));

        }


    });





}

/* add money is the method that is called when you click any of the coin buttons.
They all have onlicks with the addMoney Method with the parameter to identify the
demoniation. The method will then go through a series of if statements so that
it will add the appropriate moeney amount to the machine. In those functons the hidden
running total value will be grabed and then parsed. Then the money will be added after that the running total will be
appended. Finally that runningTotal value will be sent the moneyValue HTML so that
the user can see it. */

function addMoney(inputMoneyType) {
    clearErrorMessages();

    $('#change').val('');
    var moneyAmount = 0.00;
    var dollarAmount = $('#moneyIn');

    if (inputMoneyType == 'dollar') {
        moneyAmount = parseFloat($('#runningTotal').val());
        moneyAmount += 1.00;
        moneyAmount = moneyAmount.toFixed(2);
        $('#runningTotal').val(moneyAmount);

    } else if (inputMoneyType == 'quarter') {
        moneyAmount = parseFloat($('#runningTotal').val());
        moneyAmount += 0.25;
        moneyAmount = moneyAmount.toFixed(2);
        $('#runningTotal').val(moneyAmount);
    } else if (inputMoneyType == 'dime') {
        moneyAmount = parseFloat($('#runningTotal').val());
        moneyAmount += 0.10;
        moneyAmount = moneyAmount.toFixed(2);
        $('#runningTotal').val(moneyAmount);
    } else if (inputMoneyType == 'nickle') {
        moneyAmount = parseFloat($('#runningTotal').val());
        moneyAmount += 0.05;
        moneyAmount = moneyAmount.toFixed(2);
        $('#runningTotal').val(moneyAmount);
    }

    dollarAmount.val(moneyAmount);



}


/*
loadMachine is the method used to load the vending items into the machine. This
method is called in the on ready so that the items are in the machine. It connects up
to the api and then the response is turned into variables so that they can be used
to display in the HTML.  In the HTML buttons an onClick is added to each with the
id of the item so that it can be used by other function. Also an id is added to
each button this will also be used by other function.*/


function loadMachine() {


    var countItem = 0;
    var rowCount = 1;
    var innerRowCount = 0;

    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/items',
        success: function(vendingItemArray) {
            $.each(vendingItemArray, function(index, vendingItem) {
                var id = vendingItem.id;
                var name = vendingItem.name;
                var price = vendingItem.price;
                price = price.toFixed(2);
                var quantity = vendingItem.quantity;


                var rowItem1 = '<div class="col-sm-4">';
                rowItem1 += '<button type="button" id="button' + id + '" class="btn btn-secondary btn-lg" onclick="selectVendingItem(' + id + ') " style=" width:160px; height:160px; margin-bottom:4px; word-wrap:break-word;">';
                rowItem1 += '<p class="text-left">' + id + '</p>';
                rowItem1 += '<p class="text-center">' + name + '</p>';
                rowItem1 += '<p class="text-center">$ ' + price + '</p>';
                rowItem1 += '<p class="text-center">Quantity Left: ' + quantity + '</p>';
                rowItem1 += '</button>'
                rowItem1 +='<br />'
                rowItem1 +='<br />'
                rowItem1 +='<br />'


                var row = $('#VendingItems');





                row.append(rowItem1);






            });







        },

        error: function(data) {
            $('#errorMessages')
                .append($('#messages')

                    .text('Error Calling Web Service. Please try again later.'));

        }


    });



}
