drop  database  if exists  address_book;
--  Creates Database if HollywoodTowerHotel exists.
Create database if not exists  address_book;

use address_book;
CREATE TABLE IF NOT EXISTS `address` (
 `address_id` int(11) NOT NULL AUTO_INCREMENT,
 `full_name` varchar(100) NOT NULL,
 `street_address` varchar(100) NOT NULL,
 `city` varchar(50) NOT NULL,
 `state` varchar(10) NOT NULL,
 `zipcode` varchar(10) NOT NULL,
 PRIMARY KEY (`address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



drop  database  if exists  address_book_test;
--  Creates Database if HollywoodTowerHotel exists.
Create database if not exists  address_book_test;

use address_book_test;

CREATE TABLE IF NOT EXISTS `address` (
 `address_id` int(11) NOT NULL AUTO_INCREMENT,
 `full_name` varchar(100) NOT NULL,
 `street_address` varchar(100) NOT NULL,
 `city` varchar(50) NOT NULL,
 `state` varchar(10) NOT NULL,
 `zipcode` varchar(10) NOT NULL,
 PRIMARY KEY (`address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


