<%-- 
    Document   : index
    Created on : Mar 21, 2017, 1:32:27 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tip Calculator</title>
    </head>
    
    <h1>Tip Calculator!</h1>
        
      <form method="post" action="tipCalculatorServlet">
            <p> The Amount Of the Bill</p>
            <input type="text" name ="amountOfBill"/><br/>
             <p>The Percent of Tip to Leave(e.g., 15%)</p>
            <input type="text" name ="tipAmount"/><br/>
            
            <input type ="submit" value ="Calculate Tip"/>
        </form> 
    
</html>
