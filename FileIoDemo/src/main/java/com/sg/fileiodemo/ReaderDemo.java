/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.fileiodemo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ReaderDemo  {
    public static void main(String[] args)  throws FileNotFoundException {
        FileReader bookFinder = new FileReader("test.txt");
        BufferedReader translator = new BufferedReader(bookFinder);
        Scanner prompter = new Scanner(translator);
        
        // another way to do above on one line.
        //Scanner  sc = new Scanner(new BufferedReader(new FileReader("other.txt")));
        
        while(prompter.hasNextLine()){
            System.out.println(prompter.nextLine());
            
        }
        prompter.close();
        
        
    }
    
}
