/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.fileiodemo;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author apprentice
 */
public class WriterDemo {

    public static void main(String[] args) {
        try {
            FileWriter bookfinder = new FileWriter("test.txt");
            PrintWriter scribeAuthor = new PrintWriter(bookfinder);
           // BufferedWriter author = new BufferedWriter(scribe);
            scribeAuthor.println("Hey There.");
            scribeAuthor.print("What's Up!");
            scribeAuthor.print("Netbeans is a bully");
            
            
            // but the printwriter is lazy and doesnt clean up.
            // So when you done you have to make sure they do write the words 
            // to the file.
            
            // dont ever not have flush
            
            scribeAuthor.flush();
            scribeAuthor.close();
            
        } catch (IOException ex) {
            System.out.println("DUDE your car blew up");
        }

    }

}


