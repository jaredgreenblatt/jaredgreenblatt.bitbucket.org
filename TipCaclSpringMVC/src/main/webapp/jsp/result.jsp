<%-- 
    Document   : result
    Created on : Mar 21, 2017, 1:32:36 PM
    Author     : apprentice
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>TiP Amount Page</title>
    </head>
    <body>
        <h1>Tip Amount Results!</h1>

        <p>
            Amount: $
    <c:out value="${billAmount}" />
</p>
<p>
    Tip %
<c:out value="${tipPercent}" />

</p>
<p>
    Tip Amount: $
<c:out value="${tipAmount}" />

</p>
<p>
    Total Bill: $
<c:out value="${totalAmount}" />

</p>

<p>
    <a href="index.jsp">Calculate Another Tip?.</a>
</p>
</body>
</html>
