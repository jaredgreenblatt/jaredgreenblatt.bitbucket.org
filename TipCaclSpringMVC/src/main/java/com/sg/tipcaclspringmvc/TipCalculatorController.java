/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.tipcaclspringmvc;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class TipCalculatorController {

    @RequestMapping(value = "calulateTips", method = RequestMethod.POST)
    public String calulateTips(HttpServletRequest request, Map<String, Object> model) {

        String inputTipPercentage = request.getParameter("tipAmount");
        String inputBillAmount = request.getParameter("amountOfBill");
        BigDecimal tipPercent = new BigDecimal(inputTipPercentage);
        BigDecimal billAmount = new BigDecimal(inputBillAmount);
        BigDecimal tipAmount, totalAmount;

        tipAmount = tipPercent.divide(new BigDecimal(100), 2, RoundingMode.HALF_DOWN);
        tipAmount = tipAmount.multiply(billAmount);
        tipAmount = tipAmount.setScale(2, RoundingMode.HALF_DOWN);
        totalAmount = tipAmount.add(billAmount);
        totalAmount = totalAmount.setScale(2, RoundingMode.HALF_DOWN);

        model.put("billAmount", billAmount);
        model.put("tipPercent", tipPercent);
        model.put("tipAmount", tipAmount);
        model.put("totalAmount", totalAmount);

        return "result";

    }

}
