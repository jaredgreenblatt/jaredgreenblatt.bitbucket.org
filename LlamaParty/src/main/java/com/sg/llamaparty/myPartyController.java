package com.sg.llamaparty;

import com.sg.llamaparty.dto.PartyInfo;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class myPartyController {

    int llamaCounter = 0;
    int peopleCounter =0;
    List<PartyInfo> nameList = new ArrayList();

    @Inject
    public myPartyController() {

    }

    //addmoney and redsplay the screen
    @RequestMapping(value = "/RSVP", method = RequestMethod.GET)
    public String displayRSVP(HttpServletRequest request, Model model) {
        //clear out the error message

        // Return the logical name of our View component
        return "RSVP";
    }

    @RequestMapping(value = "/theList", method = RequestMethod.GET)
    public String displayTheList(HttpServletRequest request, Model model) {
        //clear out the error message
        model.addAttribute("attendingList", nameList);
        model.addAttribute("llamaNumber", "22");

        // Return the logical name of our View component
        return "theList";
    }

    @RequestMapping(value = "/RSVP", method = RequestMethod.POST)
    public String RSVP(HttpServletRequest request, Model model) {
        model.addAttribute("errorMessage", "");
        model.addAttribute("theMessage", "");
        model.addAttribute("theChangeMsg", "");

        //clear out the error message strings
        String theMsg = "";
        String theChangeMsg = "";
        String rsvpReturn = "";

        //get the input parameters
        String attendingParty = request.getParameter("attendingParty");
        String name = request.getParameter("attendeeName");
        String llamaString = request.getParameter("llamaNumber");

        String valMsg = validateInputParameters(attendingParty, name, llamaString);
        if (valMsg.equals("OK")) {

            int llamaNumber = Integer.parseInt(llamaString);

            //check if they have already attend
            //if duplicate update
            //if new add
            if (attendingParty.equals("yes")) {

                llamaCounter = llamaCounter + llamaNumber;
                peopleCounter++;
                PartyInfo partyInfo = new PartyInfo(peopleCounter);
                partyInfo.setName(name);
                nameList.add(partyInfo);
                
                int numberOfPeople = nameList.size();
                llamaNumber = llamaCounter / numberOfPeople;
                model.addAttribute("attendingList", nameList);
                model.addAttribute("llamaNumber", llamaNumber);
                rsvpReturn = "Yes";
            } else {
                rsvpReturn = "No";
            }

        } else {
            model.addAttribute("errMsg", "Please make a selection for your purchase");
            rsvpReturn = "Bad";
        }

        //return to the page dending on what they entered
        return rsvpReturn;
    }

    private String validateInputParameters(
            String attendingParty, String name, String llamaString) {

        String valMsg = "";

        //check to see if the parameters were numeric
        if (attendingParty == null || attendingParty.isEmpty()) {
            valMsg = "Please let us know if you're attending the party.\n";
        } else if (!(attendingParty.equals("yes") || attendingParty.equals("no"))) {
            valMsg = "Please let us know if you're attending the party.\n";
        }
        if (name == null || name.isEmpty()) {
            valMsg = "Please enter your name.\n";
        }
        if (llamaString == null || llamaString.isEmpty()) {
            valMsg = "Please enter the number of llamas you are bringing.\n";
        } else if (!isInt(llamaString)) {
            valMsg = "Please enter the number of llamas you are bringing.\n";
        }
        if (valMsg.isEmpty()) {
            valMsg = "OK";
        }
        return valMsg;
    }

    public boolean isInt(String str) {
        try {
            int i = Integer.parseInt(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

}
