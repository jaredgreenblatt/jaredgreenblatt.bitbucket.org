<%-- 
    Document   : RSVP
    Created on : Mar 29, 2017, 1:42:25 PM
    Author     : apprentice
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <title>The party List</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">   
        <link href="${pageContext.request.contextPath}/css/llamaParty.css" rel="stylesheet">   
        <link href="https://fonts.googleapis.com/css?family=Bungee+Shade" rel="stylesheet">


    </head>
    <body>
        <div class="container">
            <br/>
            <br/>
            <br/>


            <div class="col-xs-offset-4 col-xs-4 text-center">
                <form class ="form-horizontal"
                      role ="form" method ="POST"
                      action="RSVP">
                    <h1>These are the <c:out value="${peopleNumber}"/>   of cool kids coming to my party:</h1>


                    <li>
                        <c:forEach var="partyList" items="${attendingList}">
                            <ul><c:out value="${partylist}"/></ul>
                        </c:forEach>
                    </li>
                    <hr/>
                    <br/>

                    <p> And they are bringing <c:out value="${llamaNumber}"/> of llamas in total!!!! </p>
                    <br/>
                    <br/>
                    <p> 
                        <a href="${pageContext.request.contextPath}/RSVP">>> HERE <<</a> 
                        to RSVP someone else</p>




                </form>
            </div>

        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

