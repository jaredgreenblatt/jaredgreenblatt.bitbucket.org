<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>YAY!!!!!</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">   
        <link href="${pageContext.request.contextPath}/css/llamaParty.css" rel="stylesheet">   
        <link href="https://fonts.googleapis.com/css?family=Bungee+Shade" rel="stylesheet">

    </head>
    <body>
        <div class="container">
            <br/>
            <br/>
            <br/>


            <div class="col-xs-offset-4 col-xs-4 text-center">
                <h1>HURRAH!</h1>
                <p> So excited you are coming! Don't forget to bring the booze along with
                    those llamas!</p>
                <hr/>
                
                <p>
                    Did you know there will be <c:out value="${llamaNumber}"/> of llamas per person at my party!?!?!?
                    
                </p>
                <br/>
                <form class ="form-horizontal"
                      role ="form" method ="POST"
                      action="RSVP">
                <p><a href="${pageContext.request.contextPath}/theList">>> CLICK HERE <<</a></p>
                <p> To See other people coming!</p>
                <br/>
                <p> or
                 <a href="${pageContext.request.contextPath}/RSVP">>> HERE <<</a> 
                 to RSVP someone else</p>
              
            </div>

        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

