<%-- 
    Document   : RSVP
    Created on : Mar 29, 2017, 1:42:25 PM
    Author     : apprentice
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <title>Party Invite</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">   
        <link href="${pageContext.request.contextPath}/css/llamaParty.css" rel="stylesheet">   
        <link href="https://fonts.googleapis.com/css?family=Bungee+Shade" rel="stylesheet">

        <title>RSVP to My Party!</title>
    </head>
    <body>
        <div class="container">
            <br/>
            <br/>
            <br/>


            <div class="col-xs-offset-4 col-xs-4 text-center">
                <form class ="form-horizontal"
                      role ="form" method ="POST"
                      action="RSVP">
                    <h1>Coming To MY Party?</h1>
                    <label class="radio-inline"><input type="radio" name="attendingParty" value ='yes'>Yes</label>
                    <label class="radio-inline"><input type="radio" name="attendingParty" value ='no'>No</label>
                    <br>
                    Name:
                    <input type="text" class="form-control" id="attendeeName" name="attendeeName"required placeholder='Enter yourName'>
                    # of Llamas you are bringing:
                    <input type="text" class="form-control" id="llamaNumber" name="llamaNumber" required placeholder='Enter Number Of Llamas'>
                   
                     <button type="submit" class="btn btn-secondary" >RSVP </button>
                </form>
            </div>

        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

