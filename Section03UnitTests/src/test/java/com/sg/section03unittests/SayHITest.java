/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.section03unittests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class SayHITest {
    private SayHI sayHi = new SayHI();

    public SayHITest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of sayHi method, of class SayHI.
     */
    @Test
    public void testBob() {
        String expecctedResult = "Hello Bob!";
        assertEquals(expecctedResult, sayHi.sayHi("Bob"));
        

    }
       @Test
    public void testAlice() {
        String expecctedResult = "Hello Alice!";
        assertEquals(expecctedResult, sayHi.sayHi("Alice"));
        

    }
         @Test
    public void testX() {
        String expecctedResult = "Hello X!";
        assertEquals(expecctedResult, sayHi.sayHi("X"));
        

    }

}
