/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.section03unittests;


public class SayHI {
    /**
 *
 * @author apprentice
 */

/// Given a string name eg Bob rerturn the greeting of the 
//form "Hello Bob!"

//sayHi("Bob") -> "Hello Bob!"
//sayHi("Alice") -> "Hello Alice!"
////sayHi("x") -> "Hello x!"
    
    public String sayHi(String name){
        
        return "Hello "+name+"!";
    }
}
